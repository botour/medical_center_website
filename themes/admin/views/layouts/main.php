<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="<?php echo Yii::app()->language==" en"?"en":"ar";?>"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="<?php echo Yii::app()->language==" en"?"en":"ar";?>"><![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo Yii::app()->language == "en" ? "en" : "ar"; ?>" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $this->pageTitle; ?></title>
    <!-- start: META -->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1"/><![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet"
          href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/fonts/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/main.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/main-responsive.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
    <link rel="stylesheet"
          href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/theme_light.css" type="text/css"
          id="skin_color">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/print.css" type="text/css"
          media="print"/>
    <link
        href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap-modal/css/bootstrap-modal.css"
          rel="stylesheet" type="text/css"/>
    <!--[if IE 7]>
    <link rel="stylesheet"
          href="<?php echo Yii::app()->theme->baseUrl?>/assets/plugins/font-awesome/css/font-awesome-ie7.min.css" />
    <![endif]-->
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <?php foreach($this->cssAssets as $cssFileName):?>
        <link rel="stylesheet"
              href="<?php echo Yii::app()->baseUrl?>/css/assets/<?php echo $cssFileName ?>" />
    <?php endforeach;?>
    <?php foreach($this->cssCustomFiles as $cssFileName):?>
        <link rel="stylesheet"
              href="<?php echo Yii::app()->baseUrl?>/css/<?php echo $cssFileName ?>" />
    <?php endforeach;?>
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
<!-- start: HEADER -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->

        </div>
        <div class="navbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">
                <!-- start: USER DROPDOWN -->
                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true"
                       href="#">
                        <img src="assets/images/avatar-1-small.jpg" class="circle-img" alt="">
                        <span
                            class="username"><?php echo ucfirst(Yii::app()->user->first_name) . " " . ucfirst(Yii::app()->user->last_name) ?></span>
                        <i class="clip-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <?php echo CHtml::link('<i class="clip-exit"></i>&nbsp;' . Yii::t('template', 'main.function.logout'), Yii::app()->createUrl('home/logout')); ?>
                        </li>
                    </ul>
                </li>
                <!-- end: USER DROPDOWN -->
            </ul>
            <!-- end: TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- end: TOP NAVIGATION CONTAINER -->
</div>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <?php
        $this->widget('application.components.SidebarNavigation');
    ?>
    <!-- start: PAGE -->
    <div class="main-content">
        <!-- start: PANEL CONFIGURATION MODAL FORM -->
        <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Panel Configuration</h4>
                    </div>
                    <div class="modal-body">
                        Here will be a configuration form
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- end: SPANEL CONFIGURATION MODAL FORM -->
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->
                    <div id="style_selector" class="hidden-xs">
                        <div id="style_selector_container">
                            <div class="style-main-title">
                                Style Selector
                            </div>
                            <div class="box-title">
                                Choose Your Layout Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="layout">
                                        <option value="default">Wide</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                Choose Your Header Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="header">
                                        <option value="fixed">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                Choose Your Footer Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="footer">
                                        <option value="default">Default</option>
                                        <option value="fixed">Fixed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                Backgrounds for Boxed Version
                            </div>
                            <div class="images boxed-patterns">
                                <a id="bg_style_1" href="#"><img alt=""
                                                                 src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/bg.png"></a>
                                <a id="bg_style_2" href="#"><img alt=""
                                                                 src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/bg_2.png"></a>
                                <a id="bg_style_3" href="#"><img alt=""
                                                                 src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/bg_3.png"></a>
                                <a id="bg_style_4" href="#"><img alt=""
                                                                 src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/bg_4.png"></a>
                                <a id="bg_style_5" href="#"><img alt=""
                                                                 src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/bg_5.png"></a>
                            </div>
                            <div class="box-title">
                                5 Predefined Color Schemes
                            </div>
                            <div class="images icons-color">
                                <a id="light" href="#"><img class="active" alt=""
                                                            src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/lightgrey.png"></a>
                                <a id="dark" href="#"><img alt=""
                                                           src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/darkgrey.png"></a>
                                <a id="black_and_white" href="#"><img alt=""
                                                                      src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/blackandwhite.png"></a>
                                <a id="navy" href="#"><img alt=""
                                                           src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/navy.png"></a>
                                <a id="green" href="#"><img alt=""
                                                            src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/green.png"></a>
                            </div>
                            <div class="box-title">
                                Style it with LESS
                            </div>
                            <div class="images">
                                <div class="form-group">
                                    <label>
                                        Basic
                                    </label>
                                    <input type="text" value="#ffffff" class="color-base">

                                    <div class="dropdown">
                                        <a class="add-on dropdown-toggle" data-toggle="dropdown"><i
                                                style="background-color: #ffffff"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <div class="colorpalette"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Text
                                    </label>
                                    <input type="text" value="#555555" class="color-text">

                                    <div class="dropdown">
                                        <a class="add-on dropdown-toggle" data-toggle="dropdown"><i
                                                style="background-color: #555555"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <div class="colorpalette"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Elements
                                    </label>
                                    <input type="text" value="#007AFF" class="color-badge">

                                    <div class="dropdown">
                                        <a class="add-on dropdown-toggle" data-toggle="dropdown"><i
                                                style="background-color: #007AFF"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <div class="colorpalette"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div style="height:25px;line-height:25px; text-align: center">
                                <a class="clear_style" href="#">
                                    Clear Styles
                                </a>
                                <a class="save_style" href="#">
                                    Save Styles
                                </a>
                            </div>
                        </div>
                        <div class="style-toggle close"></div>
                    </div>
                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <?php $this->widget('application.components.Breadcrumbs', array(
                            'breadcrumbs' => $this->breadcrumbs,
                        )); ?>
                        <li class="search-box">
                            <form class="sidebar-search">
                                <div class="form-group">
                                    <input type="text"
                                           placeholder="<?php echo Yii::t('template', 'main.function.start.searching') ?>">
                                    <button class="submit">
                                        <i class="clip-search-3"></i>
                                    </button>
                                </div>
                            </form>
                        </li>
                    </ol>
                    <?php if ($this->contentTitleAvailable): ?>
                        <div class="page-header">
                            <h1><?php echo $this->contentTitle; ?>
                                <small><?php echo $this->contentSubTitle; ?></small>
                            </h1>
                        </div>
                    <?php endif; ?>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <?php echo $content; ?>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        <?php echo date('Y');?> &copy; Al-Hayat Medical Center - Dashboard.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="<?php echo Yii::app()->theme->baseUrl?>/assets/plugins/respond.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl?>/assets/plugins/excanvas.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--<![endif]-->
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script
    src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script
    src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script
    src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/less/less-1.5.0.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<script
    src="<?php echo Yii::app()->theme->baseUrl ?>/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/main.js"></script>

<?php Yii::app()->clientScript->registerScript("main-script", '
$(function(){
    var sideBarMainItemId = '. CJavaScript::encode($this->menuItemId) .';
    var sideBarSubItemId = '. CJavaScript::encode($this->subMenuItemId) .';
    $(".main-navigation li.open").each(function(){
        $(this).removeClass("open");
    });
    $(".main-navigation li.active").each(function(){
        $(this).removeClass("active");
    });
    $("#"+sideBarMainItemId).addClass("open");
    $("#"+sideBarMainItemId).addClass("active");
    $("#"+sideBarSubItemId).addClass("open");
    $("#"+sideBarSubItemId).addClass("active");
});
', CClientScript::POS_END); ?>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<?php foreach($this->jsAssets as $jsFileName):?>
    <script type="text/javascript"
        src="<?php echo Yii::app()->baseUrl ?>/js/assets/<?php echo $jsFileName;?>"></script>
<?php endforeach;?>
<?php foreach($this->jsCustomFiles as $jsFileName):?>
    <script type="text/javascript"
            src="<?php echo Yii::app()->baseUrl ?>/js/<?php echo $jsFileName;?>"></script>
<?php endforeach;?>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        Main.init();
    });
</script>
</body>
<!-- end: BODY -->
</html>