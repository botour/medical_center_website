<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>
    <div class="sectionWrapper">
					<div class="container">
						<div class="row">
				    		<aside class="cell-3 right-sidebar">

							</aside>
				    		<div class="cell-9">
                                <?php echo $content;?>
					    	</div>
			    		</div>
					</div>
    </div>

<?php echo $content;?>

<?php $this->endContent(); ?>