<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $this->pageTitle; ?></title>
    <meta name="description" content="EXCEPTION – Responsive Business HTML Template">
    <meta name="author" content="EXCEPTION">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Put favicon.ico and apple-touch-icon(s).png in the images folder -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/favicon.ico">

    <!-- CSS StyleSheets -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/news.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/ie.css">
    <![endif]-->
    <?php if ($this->loadBootstrap): ?>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/bootstrap-theme.min.css">
    <?php endif; ?>
    <?php if ($this->loadGalleryPluginLibrary): ?>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/gallery.css">
    <?php endif; ?>
    <?php if (!empty($this->cssCustomFiles)): ?>
        <?php foreach ($this->cssCustomFiles as $cssFile): ?>
            <!-- CSS Custom for particular page -->
            <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/<?php echo $cssFile ?>">
            <!-- -->
        <?php endforeach; ?>
    <?php endif; ?>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/html5.js"></script>


    <!-- Skin style (** you can change the link below with the one you need from skins folder in the css folder **) -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/skins/default.css">

    <?php if (Yii::app()->getLanguage() == "ar"): ?>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/rtl.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/rtl_custom.css">
    <?php endif; ?>

</head>
<body class="bg11">

<!-- site preloader start -->
<div class="page-loader">
    <div class="loader-in"></div>
</div>
<!-- site preloader end -->

<div class="pageWrapper fixedPage">

    <!-- login box start -->
    <div class="login-box">
        <a class="close-login" href="#"><i class="fa fa-times"></i></a>

        <form>
            <div class="container">
                <p>Hello our valued visitor, We present you the best web solutions and high quality graphic designs with
                    a lot of features. just login to your account and enjoy ...</p>

                <div class="login-controls">
                    <div class="skew-25 input-box left">
                        <input type="text" class="txt-box skew25" placeholder="User name Or Email"/>
                    </div>
                    <div class="skew-25 input-box left">
                        <input type="password" class="txt-box skew25" placeholder="Password"/>
                    </div>
                    <div class="left skew-25 main-bg">
                        <input type="submit" class="btn skew25" value="Login"/>
                    </div>
                    <div class="check-box-box">
                        <input type="checkbox" class="check-box"/><label>Remember me !</label>
                        <a href="#">Forgot your password ?</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- login box End -->
    <?php Yii::app()->clientScript->registerScript('content-create-script', '
$(function(){
	$("#lang-link").on("click",function(){
		$("#lang_form").submit();
	});
});
', CClientScript::POS_END); ?>
    <!-- top bar start -->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="cell-5">
                    <ul>
                        <li><a class="top-bar-links" href="#"><i class="fa fa-phone"></i>+972-42-234513</a></li>
                        <li>
                            <?php $langText = Yii::app()->getLanguage() == 'ar' ? 'EN' : 'العربية'; ?>
                            <?php echo CHtml::form(Yii::app()->request->requestUri, 'POST', array('id' => 'lang_form')); ?>
                            <?php echo CHtml::hiddenField('lang', Yii::app()->getLanguage() == 'ar' ? 'en' : 'ar'); ?>

                            <?php echo CHtml::link('<span><i
                                        class="fa fa-comments"> ' . $langText . '</i></span>', '#', array('class'=>'top-bar-links','id' => 'lang-link')) ?>
                            <?php echo CHtml::endForm(); ?>
                        </li>
                        <li><a class="top-bar-links" href="<?php echo Yii::app()->createUrl('home/login')?>"><i class="fa fa-key"></i>login</a></li>
                    </ul>
                </div>
                <div class="cell-7" style="padding: 10px;">

                </div>
            </div>
        </div>
    </div>
    <!-- top bar end -->
    <!-- Header Start -->
    <div id="headWrapper" class="clearfix ">


        <!-- Logo, global navigation menu and search start -->
        <header class="top-head nav-3" data-sticky="true">
            <div class="container">
                <div class="row">
                    <div class="logo cell-3">

                        <a href="<?php echo Yii::app()->createUrl('home/home')?>"></a>
                    </div>
                    <div class="cell-9 top-menu">
                        <!-- top navigation menu start -->
                        <nav class="top-nav mega-menu sub-menu-white">
                            <ul>
                                <li class="<?php echo $this->action->id == 'home' ? "current" : "" ?>"
                                "><a href="<?php echo $this->createUrl('home/home'); ?>"><i
                                        class="fa fa-home"></i><span><?php echo Yii::t('website', 'homepage.nav.item.home'); ?></span></a>

                                </li>
                                <li><a href="#"><i
                                            class="fa fa-key"></i><span><?php echo Yii::t('website', 'homepage.nav.item.clinics'); ?></span></a>
                                    <?php if (!empty($this->services)): ?>
                                        <ul>
                                            <?php foreach ($this->services as $service): ?>

                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('home/service', array('id' => $service->id)); ?>"><?php echo $service->getName(); ?></a>
                                                </li>


                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                                <li><a href="<?php echo Yii::app()->createUrl('home/staff'); ?>"><i
                                            class="fa fa-gift"></i><span><?php echo Yii::t('website', 'homepage.nav.item.doctors'); ?></span></a>

                                </li>
                                <li><a href="<?php echo Yii::app()->createUrl('home/galleries'); ?>"><i
                                            class="fa fa-gift"></i><span><?php echo Yii::t('website', 'homepage.nav.item.gallery'); ?></span></a>

                                </li>
                                <li class="<?php echo $this->action->id == 'article' ? "selected" : "" ?>"><a
                                        href="<?php echo Yii::app()->createUrl('home/articles'); ?>"><i
                                            class="fa fa-copy"></i><span><?php echo Yii::t('website', 'homepage.nav.item.articles'); ?></span></a>

                                </li>

                                <li><a href="<?php echo $this->createUrl('home/vacancies'); ?>"><i
                                            class="fa fa-book"></i><span><?php echo Yii::t('website', 'homepage.nav.item.vacancies'); ?></span></a>

                                </li>
                                <li><a href="#"><i
                                            class="fa fa-leaf"></i><span><?php echo Yii::t('website', 'homepage.nav.item.contact'); ?></span></a>
                                    <ul>
                                        <li>
                                            <a href="<?php echo Yii::app()->createUrl('home/contact'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.contact.address'); ?></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- top navigation menu end -->

                        <!-- top search start -->
                        <div class="top-search">
                            <a href="#"><span class="fa fa-search"></span></a>
                            <?php echo CHtml::beginForm(Yii::app()->createUrl('home/search'),'get');?>
                            <div class="search-box">
                                <div class="input-box left">
                                    <input type="text" name="q" id="t-search" class="txt-box"
                                           placeHolder="<?php echo Yii::t('website', 'enter.search.keyword.here'); ?>"/>
                                </div>
                                <div class="left">
                                    <input type="submit" id="b-search" class="btn main-bg" value="GO"/>
                                </div>
                            </div>
                            <?php echo CHtml::endForm();?>
                        </div>
                        <!-- top search end -->

                    </div>
                </div>
            </div>
        </header>
        <!-- Logo, Global navigation menu and search end -->

    </div>
    <!-- Header End -->

    <!-- Content Start -->
    <div id="contentWrapper" class="news-sitess" style="background-color: #FFF !important;">
        <?php if (Yii::app()->controller->action->id != "home"): ?>
            <div class="page-title title-1">
                <div class="container">
                    <div class="row">
                        <div class="cell-12">
                            <h1 class="fx" data-animate="fadeInLeft"><?php echo $this->pageTitle; ?></h1>

                            <div class="breadcrumbs main-bg fx" data-animate="fadeInUp">
                                <?php $this->widget('application.components.FrontEndBreadcrumbs', array(
                                    'breadcrumbs' => $this->breadcrumbs,
                                )); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php echo $content; ?>
    </div>


    <!-- Content End -->
    <!-- Footer start -->
    <footer id="footWrapper">
        <!-- footer bottom bar start -->
        <div class="footer-bottom minimal-foot">
            <div class="container">
                <div class="row">
                    <!-- footer copyrights left cell -->
                    <div class="copyrights cell-5">
                        <div class="clearfix"></div>
                        <ul class="social-list padd-top-15 hover_links_effect">
                            <li><a href="#" data-title="dribbble" data-tooltip="true" data-position="top"><span
                                        class="fa fa-dribbble"></span></a></li>
                            <li><a href="#" data-title="facebook" data-tooltip="true" data-position="top"><span
                                        class="fa fa-facebook"></span></a></li>
                            <li><a href="#" data-title="linkedin" data-tooltip="true" data-position="top"><span
                                        class="fa fa-linkedin"></span></a></li>
                            <li><a href="#" data-title="skype" data-tooltip="true" data-position="top"><span
                                        class="fa fa-skype"></span></a></li>
                            <li><a href="#" data-title="tumbler" data-tooltip="true" data-position="top"><span
                                        class="fa fa-tumblr"></span></a></li>
                            <li><a href="#" data-title="twitter" data-tooltip="true" data-position="top"><span
                                        class="fa fa-twitter"></span></a></li>
                        </ul>
                    </div>

                    <!-- footer social links right cell start -->
                    <div class="cell-7">
                        <ul class="footer-menu-inline">
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('home/home'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.home'); ?></a>
                            </li>
                            <li> /</li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('home/staff'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.doctors'); ?></a>
                            </li>
                            <li> /</li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('home/vacancies'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.vacancies'); ?></a>
                            </li>
                            <li> /</li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('home/galleries'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.gallery'); ?></a>
                            </li>
                            <li> /</li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('home/articles'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.articles'); ?></a>
                            </li>
                            <li> /</li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('home/contact'); ?>"><?php echo Yii::t('website', 'homepage.nav.item.contact'); ?></a>
                            </li>
                        </ul>
                    </div>
                    <!-- footer social links right cell end -->

                </div>
            </div>
        </div>
        <!-- footer bottom bar end -->

    </footer>
    <!-- Footer end -->

    <!-- Back to top Link -->
    <div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>


</div>


<!-- Load JS siles -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.min.js"></script>

<!-- Waypoints script -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/waypoints.min.js"></script>

<!-- Flex Slider Plugin  -->
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.flexslider-min.js"></script>


<!-- Animate numbers increment -->
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.animateNumber.min.js"></script>

<!-- slick slider carousel -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/slick.min.js"></script>

<!-- Animate numbers increment -->
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.easypiechart.min.js"></script>

<!-- PrettyPhoto script -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.prettyPhoto.js"></script>

<!-- Share post plugin script -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.sharrre.min.js"></script>

<!-- Product images zoom plugin -->
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.elevateZoom-3.0.8.min.js"></script>

<!-- Input placeholder plugin -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.placeholder.js"></script>

<!-- Tweeter API plugin -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/twitterfeed.js"></script>

<!-- Flickr API plugin -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jflickrfeed.min.js"></script>

<!-- MailChimp plugin -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/mailChimp.js"></script>

<!-- NiceScroll plugin -->
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.nicescroll.min.js"></script>

<?php if ($this->loadBootstrap): ?>
    <script type="text/javascript"
            src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/bootstrap.min.js"></script>

<?php endif; ?>

<?php if ($this->loadGalleryPluginLibrary): ?>
    <script type="text/javascript"
            src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.galleriffic.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/jquery.opacityrollover.js"></script>

<?php endif; ?>

<!-- general script file -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/script.js"></script>

<?php if (!empty($this->jsCustomFiles)): ?>
    <?php foreach ($this->jsCustomFiles as $jsFile): ?>
        <!-- CSS Custom for particular page -->
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/<?php echo $jsFile ?>">
        <!-- -->
    <?php endforeach; ?>
<?php endif; ?>
</body>
</html>