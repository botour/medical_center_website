<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>
    <div class="sectionWrapper">
        <div class="container">
            <div class="row">
                <div class="cell-9">
                    <?php echo $content;?>
                </div>
                <aside class="cell-3 right-sidebar">
                    <ul class="sidebar_widgets">


                        <li class="widget blog-cat-w fx" data-animate="fadeInRight">
                            <h3 class="widget-head"><?php echo Yii::t('website', 'our.services'); ?></h3>

                            <div class="widget-content">
                                <?php if (!empty($this->services)): ?>
                                    <ul class="list  alt">
                                        <?php foreach ($this->services as $service): ?>

                                            <li>
                                                <a href="<?php echo Yii::app()->createUrl('home/service',array('id'=>$service->id));?>"><?php echo $service->getName(); ?></a>
                                            </li>

                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>

                            </div>
                        </li>
                        <li class="widget blog-cat-w fx" data-animate="fadeInRight">
                            <h3 class="widget-head"><?php echo Yii::t('website', 'our.doctors'); ?></h3>

                            <div class="widget-content">
                                <?php if (!empty($this->doctors)): ?>
                                    <ul class="list  alt">
                                        <?php foreach ($this->doctors as $doctor): ?>
                                            <li>
                                                <a href="<?php echo Yii::app()->createUrl('home/staffm', array('sId' => $doctor->id)); ?>"><?php echo $doctor->getTitle() . " " . $doctor->getFullName(); ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </li>

                    </ul>
                </aside>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>