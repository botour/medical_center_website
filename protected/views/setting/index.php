<?php
/* @var $this SettingController */

?>

<h4>Data Management Settings:</h4>
<hr>
<p>
    <?php echo CHtml::link('Staff Jobs Management',array('staff/jobIndex'));?>

</p>
<p>
    <?php echo CHtml::link('Branches Data Management',array('setting/branchIndex'));?>
</p>

<p>
    <?php echo CHtml::link('Contact Type Data Management',array('setting/contactTypeIndex'));?>
</p>