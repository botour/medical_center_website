<?php

/* @var $this SettingController */
/* @var $contactSearchModel Branch */

$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    'Contact Types',
);

?>

<?php echo CHtml::link(Yii::t('setting','action.create.contact.type.type'),Yii::app()->createUrl('setting/contactTypeCreate'),array(
    'class'=>'btn btn-bricky',
    'id'=>'cr-branch-btn',
))?>

<hr/>

<?php
Yii::app()->clientScript->registerScript('job-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'branch-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty'=>true,
    'summaryText' => 'Content Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $contactSearchModel->search(),
    'showTableOnEmpty'=>false,
    'emptyText'=>'No Contact Types Data to manage',
    'pager'=>array(
        'class'=>'CLinkPager',
        'header'=>'',
        'htmlOptions'=>array(
            'class'=>'pagination pagination-sm',
        ),
    ),
    //'filter' => $userModel,
    'columns' => array(
        'name',
        'name_ar',
        array(
            'header'=>'',
            'value'=>array($this,'renderContactTypeOptions'),
        ),
    ),
));
?>


