<?php
/* @var $this SettingController */
/* @var $contactTypeCreateModel ContactType */

$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    array(
        'name' => 'Contact Types',
        'url' => Yii::app()->createUrl('setting/contactTypeIndex'),
    ),
    'Add Contact Type',
);
?>

<?php
$this->renderPartial(
    'contact-type/_contact-form',array(
        'contactTypeModel'=>$contactTypeCreateModel,
    )
);
?>
