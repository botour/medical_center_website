<?php
/* @var $this StaffController */
/* @var $contactTypeUpdateModel ContactType */
$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    array(
        'name' => 'Branches',
        'url' => Yii::app()->createUrl('setting/contactTypeIndex'),
    ),
    'Edit Contact Type Info: '.$contactTypeUpdateModel->name,
);

?>



<?php
$this->renderPartial(
    'contact-type/_contact-form',array(
        'contactTypeModel'=>$contactTypeUpdateModel,
    )
);
?>
