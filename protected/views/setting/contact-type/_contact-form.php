<?php
/* @var $this SettingController */
/* @var $contactTypeModel ContactType */
?>


<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-type-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($contactTypeModel, 'name'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if($contactTypeModel->hasErrors()):?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($contactTypeModel);?>

            </div>
        <?php endif;?>



        <div class="form-group<?php echo $contactTypeModel->hasErrors('name')?' has-error':'';echo !$contactTypeModel->hasErrors('name')&&isset($_POST['Job'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $contactTypeModel->getAttributeLabel('name'); ?>
                <?php if ($contactTypeModel->isAttributeRequired('name')): ?>
                    <?php if(!$contactTypeModel->hasErrors('name')&&isset($_POST['Job'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($contactTypeModel->hasErrors('name') || !isset($_POST['Job'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($contactTypeModel,'name',array(
                    'class'=>'form-control',
                ))?>
                <?php if($contactTypeModel->hasErrors('name')):?>
                    <span class="help-block"><?php echo $contactTypeModel->getError('name');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $contactTypeModel->hasErrors('name_ar')?' has-error':'';echo !$contactTypeModel->hasErrors('name_ar')&&isset($_POST['Job'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $contactTypeModel->getAttributeLabel('name_ar'); ?>
                <?php if ($contactTypeModel->isAttributeRequired('name_ar')): ?>
                    <?php if(!$contactTypeModel->hasErrors('name_ar')&&isset($_POST['Job'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($contactTypeModel->hasErrors('name_ar') || !isset($_POST['Job'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($contactTypeModel,'name_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($contactTypeModel->hasErrors('name_ar')):?>
                    <span class="help-block"><?php echo $contactTypeModel->getError('name_ar');?></span>
                <?php endif;?>
            </div>
        </div>



        <div id="add-job-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $contactTypeModel->isNewRecord?Yii::t('setting','controller.contact.type.action.add.contact.type'):Yii::t('setting','controller.contact.type.action.update.contact.type')?> <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->