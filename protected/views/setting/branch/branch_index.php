<?php

/* @var $this SettingController */
/* @var $branchSearchModel Branch */

$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    'Branch Types',
);

?>

<?php echo CHtml::link(Yii::t('setting','action.create.branch.type'),Yii::app()->createUrl('setting/branchCreate'),array(
    'class'=>'btn btn-bricky',
    'id'=>'cr-branch-btn',
))?>

<hr/>

<?php
Yii::app()->clientScript->registerScript('job-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'branch-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty'=>true,
    'summaryText' => 'Content Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $branchSearchModel->search(),
    'showTableOnEmpty'=>false,
    'emptyText'=>'No Branches Data to manage',
    'pager'=>array(
        'class'=>'CLinkPager',
        'header'=>'',
        'htmlOptions'=>array(
            'class'=>'pagination pagination-sm',
        ),
    ),
    //'filter' => $userModel,
    'columns' => array(
        'name',
        'name_ar',
        array(
            'header'=>'',
            'value'=>array($this,'renderBranchOptions'),
        ),
    ),
));
?>


