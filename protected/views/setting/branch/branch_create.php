<?php
/* @var $this SettingController */
/* @var $branchCreateModel Branch */

$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    array(
        'name' => 'Branches',
        'url' => Yii::app()->createUrl('setting/branchIndex'),
    ),
    'Add Branch',
);
?>

<?php
$this->renderPartial(
    'branch/_branch-form',array(
        'branchModel'=>$branchCreateModel,
    )
);
?>
