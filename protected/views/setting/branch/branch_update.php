<?php
/* @var $this StaffController */
/* @var $branchUpdateModel Branch */
$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    array(
        'name' => 'Branches',
        'url' => Yii::app()->createUrl('setting/branchIndex'),
    ),
    'Edit Branch Info: '.$branchUpdateModel->name,
);

?>



<?php
$this->renderPartial(
    'branch/_branch-form',array(
        'branchModel'=>$branchUpdateModel,
    )
);
?>
