<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 8/6/15
 * Time: 1:00 PM
 */

?>



<div id="services-list">
    <?php $this->renderPartial('_services_list', array(
        'services'=>$services,
    )); ?>
</div>
<hr/>



<?php echo Chtml::ajaxLink(Yii::t('department', 'action.create.service.item'), Yii::app()->createUrl('department/getCreateServiceItemForm'), array(
    'type' => 'GET',
    'data' => array(
        'department_id' => 'js:$("#department_id").val()',
    ),
    'success' => 'js:function(data){
        $("#createServiceItemModal .modal-body").html(data);
        $(".ok-sign").hide();
        $(".error-sign").hide();
    }',
    'beforeSend' => 'js:function(){
        $("#createServiceItemModal .modal-body").html("Please Wait...");
        $("#createServiceItemModal").modal("show");
    }',
    'error' => 'js:function(){
        $("#createServiceItemModal").modal("hide");
    }'
), array(
    'id' => 'link-' . uniqid(),
)); ?>
