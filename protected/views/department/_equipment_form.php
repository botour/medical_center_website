<?php

/* @var $this DepartmentController */
/* @var $equipmentCreateModel Equipment */

?>


<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($equipmentCreateModel, 'title'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if($equipmentCreateModel->hasErrors()):?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($equipmentCreateModel);?>
               
            </div>
        <?php endif;?>


    <?php echo $form->hiddenField($equipmentCreateModel,'department_id');?>

        <div class="form-group<?php echo $equipmentCreateModel->hasErrors('name')?' has-error':'';echo !$equipmentCreateModel->hasErrors('name')&&isset($_POST['Equipment'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $equipmentCreateModel->getAttributeLabel('name'); ?>
                <?php if ($equipmentCreateModel->isAttributeRequired('name')): ?>
                    <?php if(!$equipmentCreateModel->hasErrors('name')&&isset($_POST['Equipment'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($equipmentCreateModel->hasErrors('name') || !isset($_POST['Equipment'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($equipmentCreateModel,'name',array(
                    'class'=>'form-control',
                ))?>
                <?php if($equipmentCreateModel->hasErrors('name')):?>
                    <span class="help-block"><?php echo $equipmentCreateModel->getError('name');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $equipmentCreateModel->hasErrors('name_ar')?' has-error':'';echo !$equipmentCreateModel->hasErrors('name_ar')&&isset($_POST['Equipment'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $equipmentCreateModel->getAttributeLabel('name_ar'); ?>
                <?php if ($equipmentCreateModel->isAttributeRequired('name_ar')): ?>
                    <?php if(!$equipmentCreateModel->hasErrors('name_ar')&&isset($_POST['Equipment'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($equipmentCreateModel->hasErrors('name_ar') || !isset($_POST['Equipment'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($equipmentCreateModel,'name_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($equipmentCreateModel->hasErrors('name_ar')):?>
                    <span class="help-block"><?php echo $equipmentCreateModel->getError('name_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $equipmentCreateModel->hasErrors('order')?' has-error':'';echo !$equipmentCreateModel->hasErrors('order')&&isset($_POST['Equipment'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $equipmentCreateModel->getAttributeLabel('order'); ?>
                <?php if ($equipmentCreateModel->isAttributeRequired('order')): ?>
                    <?php if(!$equipmentCreateModel->hasErrors('order')&&isset($_POST['Equipment'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($equipmentCreateModel->hasErrors('order') || !isset($_POST['Equipment'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($equipmentCreateModel,'order',array(
                    'class'=>'form-control',
                ))?>
                <?php if($equipmentCreateModel->hasErrors('order')):?>
                    <span class="help-block"><?php echo $equipmentCreateModel->getError('order');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $equipmentCreateModel->hasErrors('body')?' has-error':'';echo !$equipmentCreateModel->hasErrors('body')&&isset($_POST['Equipment'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $equipmentCreateModel->getAttributeLabel('body'); ?>
                <?php if ($equipmentCreateModel->isAttributeRequired('body')): ?>
                    <?php if(!$equipmentCreateModel->hasErrors('body')&&isset($_POST['Equipment'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($equipmentCreateModel->hasErrors('body') || !isset($_POST['Equipment'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($equipmentCreateModel,'body',array(
                    'class'=>'form-control',
                    'id'=>'body_field_area',
                ))?>
                <?php if($equipmentCreateModel->hasErrors('body')):?>
                    <span class="help-block"><?php echo $equipmentCreateModel->getError('body');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $equipmentCreateModel->hasErrors('body_ar')?' has-error':'';echo !$equipmentCreateModel->hasErrors('body')&&isset($_POST['Equipment'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $equipmentCreateModel->getAttributeLabel('body_ar'); ?>
                <?php if ($equipmentCreateModel->isAttributeRequired('body_ar')): ?>
                    <?php if(!$equipmentCreateModel->hasErrors('body_ar')&&isset($_POST['Equipment'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($equipmentCreateModel->hasErrors('body_ar') || !isset($_POST['Equipment'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($equipmentCreateModel,'body_ar',array(
                    'class'=>'form-control',
                    'id'=>'body_ar_field_area',
                ))?>
                <?php if($equipmentCreateModel->hasErrors('body_ar')):?>
                    <span class="help-block"><?php echo $equipmentCreateModel->getError('body_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <div id="add-equipment-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $equipmentCreateModel->isNewRecord?Yii::t('department','controller.equipment.action.add.equipment'):Yii::t('department','controller.equipment.action.update.equipment')?> <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->