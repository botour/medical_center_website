<?php

/* @var $this DepartmentController */
/* @var $equipmentUpdateModel Equipment */



$this->breadcrumbs = array(
    array(
        'name' => 'Services',
        'url' => Yii::app()->createUrl('department/index'),
    ),
    array(
        'name' => $equipmentUpdateModel->department->getName(),
        'url' => Yii::app()->createUrl('department/view',array('id'=>$equipmentUpdateModel->department->id,'ref'=>'equip_update')),
    ),
    'Update Equipment'." : ".$equipmentUpdateModel->getName(),
);

Yii::app()->clientScript->registerScript('equipment-update-script','
$(function(){
	CKEDITOR.replace( "body_field_area");
    CKEDITOR.replace( "body_ar_field_area");

});
',CClientScript::POS_END);
$this->jsAssets[] = 'ckeditor/ckeditor.js';

?>

<?php $this->renderPartial('_equipment_form',array(
    'equipmentCreateModel'=>$equipmentUpdateModel,
));?>