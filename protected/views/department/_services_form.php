<?php
/* @var $this DepartmentController */
/* @var $serviceItemModel ServiceItem */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript("service-item-form-script", '
var serviceItemFormId = '.CJavaScript::encode($serviceItemModel->isNewRecord?"create-service-item-form":"update-service-item-form").';
var serviceItemFormSubmitUrl = '.CJavaScript::encode($serviceItemModel->isNewRecord?Yii::app()->createUrl('department/createServiceItem'):Yii::app()->createUrl('department/updateServiceItem')).';
var serviceItemModalId = '.CJavaScript::encode($serviceItemModel->isNewRecord?"createServiceItemModal":"updateServiceItemModal").';
updateServiceItemFormSubmit();
',CClientScript::POS_END);
CommonFunctions::fixAjax();
?>
<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => $serviceItemModel->isNewRecord?"create-service-item-form":"update-service-item-form",
            'action' => $serviceItemModel->isNewRecord?Yii::app()->createUrl('department/CreateServiceItem'):Yii::app()->createUrl('department/updateServiceItem'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($serviceItemModel, 'item'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>
        <?php echo $form->hiddenField($serviceItemModel,'department_id');?>
        <?php if(!$serviceItemModel->isNewRecord):?>
            <?php echo $form->hiddenField($serviceItemModel,'id');?>
        <?php endif;?>
        <div class="form-group<?php echo $serviceItemModel->hasErrors('item')?' has-error':'';echo !$serviceItemModel->hasErrors('item')&&isset($_POST['Department'])?' has-success':''?>">
            <label class="col-sm-4 control-label" for="form-field-2">
                <?php echo $serviceItemModel->getAttributeLabel('item'); ?>
                <?php if ($serviceItemModel->isAttributeRequired('item')): ?>
                    <?php if(!$serviceItemModel->hasErrors('item')&&isset($_POST['Department'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($serviceItemModel->hasErrors('item') || !isset($_POST['Department'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-8">
                <?php echo $form->textField($serviceItemModel,'item',array(
                    'class'=>'form-control',
                ))?>
                <?php if($serviceItemModel->hasErrors('item')):?>
                    <span class="help-block"><?php echo $serviceItemModel->getError('item');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group<?php echo $serviceItemModel->hasErrors('item_ar')?' has-error':'';echo !$serviceItemModel->hasErrors('item_ar')&&isset($_POST['Department'])?' has-success':''?>">
            <label class="col-sm-4 control-label" for="form-field-2">
                <?php echo $serviceItemModel->getAttributeLabel('item_ar'); ?>
                <?php if ($serviceItemModel->isAttributeRequired('item_ar')): ?>
                    <?php if(!$serviceItemModel->hasErrors('item_ar')&&isset($_POST['Department'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($serviceItemModel->hasErrors('item_ar') || !isset($_POST['Department'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-8">
                <?php echo $form->textField($serviceItemModel,'item_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($serviceItemModel->hasErrors('item_ar')):?>
                    <span class="help-block"><?php echo $serviceItemModel->getError('item_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <hr/>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-8">
                <button class="btn btn-yellow btn-block" type="submit">
                    <?php echo $serviceItemModel->isNewRecord?Yii::t('department','controller.content.action.add.service.item'):Yii::t('department','controller.content.action.update.service.item')?> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->