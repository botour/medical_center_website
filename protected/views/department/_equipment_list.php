<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 8/19/15
 * Time: 9:45 AM
 */



Yii::app()->clientScript->registerScript('content-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'equipment-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty'=>true,
    'summaryText' => 'Content Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $equipmentModel->search(),
    'showTableOnEmpty'=>false,
    'emptyText'=>'No Equipments Data to manage',
    'pager'=>array(
        'class'=>'CLinkPager',
        'header'=>'',
        'htmlOptions'=>array(
            'class'=>'pagination pagination-sm',

        ),
    ),
    //'filter' => $userModel,
    'columns' => array(
        'name',
        'name_ar',
        'order',
        array(
            'name'=>'active',
            'type'=>'raw',
            'value'=>'CommonFunctions::getLabel($data->active,CommonFunctions::CONTENT_ACTIVE)',
        ),
        array(
            'header'=>'',
            'value'=>array($this,'renderEquipmentOptions'),
        ),
    ),
));
?>


