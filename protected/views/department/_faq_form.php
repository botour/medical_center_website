<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/18/15
 * Time: 6:14 PM
 */


Yii::app()->clientScript->registerScript("faq-form-script", '
var faqFormId = '.CJavaScript::encode($faqModel->isNewRecord?"create-faq-form":"update-faq-form").';
var faqFormSubmitUrl = '.CJavaScript::encode($faqModel->isNewRecord?Yii::app()->createUrl('department/CreateFAQItem'):Yii::app()->createUrl('department/updateFAQ')).';
var faqModalId = '.CJavaScript::encode($faqModel->isNewRecord?"createFAQModal":"updateFAQModal").';
updateFAQFormSubmit();
',CClientScript::POS_END);
CommonFunctions::fixAjax();
?>

<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */
?>
<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => $faqModel->isNewRecord?"create-faq-form":"update-faq-form",
            'action' => $faqModel->isNewRecord?Yii::app()->createUrl('department/CreateFAQItem'):Yii::app()->createUrl('department/CreateFAQItem'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($faqModel, 'question'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>
        <?php if(!$faqModel->isNewRecord):?>
            <?php echo $form->hiddenField($faqModel,'order');?>
            <?php echo $form->hiddenField($faqModel,'id');?>
        <?php endif;?>
        <?php echo $form->hiddenField($faqModel,'department_id');?>


        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#english-info">
                        <?php echo Yii::t('template', 'content.language.option.english'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#arabic-info">
                        <?php echo Yii::t('template', 'content.language.option.arabic'); ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="english-info" class="tab-pane in active">
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textField($faqModel, 'question', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder'=>'')); ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textArea($faqModel, 'answer', array('size' => 60, 'maxlength' => 255,'rows'=>6, 'class' => 'form-control','id'=>'summary-field', 'placeholder'=>'')); ?>

                        </div>
                    </div>
                </div>
                <div id="arabic-info" class="tab-pane in">
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textField($faqModel, 'question_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder'=>'')); ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textArea($faqModel, 'answer_ar', array('size' => 60, 'maxlength' => 255, 'rows'=>6, 'class' => 'form-control','id'=>'summary-field', 'placeholder'=>'')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-8 col-md-4">
                <button class="btn btn-green btn-block" type="submit" id="<?php echo $faqModel->isNewRecord?"create-faq-btn":"update-faq-btn"?>">
                    <span class="ladda-label"> <?php echo $faqModel->isNewRecord ? Yii::t('department', 'action.create.faq')  : Yii::t('department', 'action.update.faq') ?></span>
                    <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->