<?php

/* @var $this DepartmentController */
/* @var $imageModel Image */
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'notifyOnFormSubmitModal',
    'title' => 'Note',
    'width' => '40',
));


Yii::app()->clientScript->registerScript("equip-image-form", "
var _URL = window.URL || window.webkitURL;
$(function(){
    $(\"#notifyOnFormSubmitModal\").on(\"hidden.bs.modal\",function(){
        $(\".fileupload\").fileupload(\"reset\");
    });
    $(\"#equipment-image-controll\").change(function (e) {
        var image, file;
        if ((file = this.files[0])) {
            image = new Image();
            image.onload = function () {
                if(this.width!=640 || this.height!=426){
                    $(\"#notifyOnFormSubmitModal .modal-body\").html(\"<p>The image width or height is not appropriate</p><p>Select an image in the size of: 640X426 for maximum fit</p>\");
                    $(\"#notifyOnFormSubmitModal\").modal(\"show\");
                }
            };
            image.src = _URL.createObjectURL(file);
        }
    });
});
", CClientScript::POS_END);
?>


<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($imageModel, 'name'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if($imageModel->hasErrors()):?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($imageModel);?>

            </div>
        <?php endif;?>



        <div class="form-group<?php echo $imageModel->hasErrors('name')?' has-error':'';echo !$imageModel->hasErrors('name')&&isset($_POST['Image'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $imageModel->getAttributeLabel('name'); ?>
                <?php if ($imageModel->isAttributeRequired('name')): ?>
                    <?php if(!$imageModel->hasErrors('name')&&isset($_POST['Image'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($imageModel->hasErrors('name') || !isset($_POST['Image'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($imageModel,'name',array(
                    'class'=>'form-control',
                ))?>
                <?php if($imageModel->hasErrors('name')):?>
                    <span class="help-block"><?php echo $imageModel->getError('name');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $imageModel->hasErrors('name_ar')?' has-error':'';echo !$imageModel->hasErrors('name_ar')&&isset($_POST['Image'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $imageModel->getAttributeLabel('name_ar'); ?>
                <?php if ($imageModel->isAttributeRequired('name_ar')): ?>
                    <?php if(!$imageModel->hasErrors('name_ar')&&isset($_POST['Image'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($imageModel->hasErrors('name_ar') || !isset($_POST['Image'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($imageModel,'name_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($imageModel->hasErrors('name_ar')):?>
                    <span class="help-block"><?php echo $imageModel->getError('name_ar');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                                    src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                            <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($imageModel, 'image', array(
                                                                'id'=>'equipment-image-controll',
                                                            )); ?>
														</span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <div class="form-group<?php echo $imageModel->hasErrors('description')?' has-error':'';echo !$imageModel->hasErrors('description')&&isset($_POST['Image'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $imageModel->getAttributeLabel('description'); ?>
                <?php if ($imageModel->isAttributeRequired('description')): ?>
                    <?php if(!$imageModel->hasErrors('description')&&isset($_POST['Image'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($imageModel->hasErrors('description') || !isset($_POST['Image'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($imageModel,'description',array(
                    'class'=>'form-control',
                    'id'=>'body_field_area',
                ))?>
                <?php if($imageModel->hasErrors('description')):?>
                    <span class="help-block"><?php echo $imageModel->getError('description');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $imageModel->hasErrors('description_ar')?' has-error':'';echo !$imageModel->hasErrors('description_ar')&&isset($_POST['Image'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $imageModel->getAttributeLabel('description_ar'); ?>
                <?php if ($imageModel->isAttributeRequired('description_ar')): ?>
                    <?php if(!$imageModel->hasErrors('description_ar')&&isset($_POST['Image'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($imageModel->hasErrors('description_ar') || !isset($_POST['Image'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($imageModel,'description_ar',array(
                    'class'=>'form-control',
                    'id'=>'body_ar_field_area',
                ))?>
                <?php if($imageModel->hasErrors('description_ar')):?>
                    <span class="help-block"><?php echo $imageModel->getError('description_ar');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group<?php echo $imageModel->hasErrors('alternative_text')?' has-error':'';echo !$imageModel->hasErrors('alternative_text')&&isset($_POST['Image'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $imageModel->getAttributeLabel('alternative_text'); ?>
                <?php if ($imageModel->isAttributeRequired('alternative_text')): ?>
                    <?php if(!$imageModel->hasErrors('alternative_text')&&isset($_POST['Image'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($imageModel->hasErrors('alternative_text') || !isset($_POST['Image'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($imageModel,'alternative_text',array(
                    'class'=>'form-control',
                    'id'=>'alternative_field_area',
                ))?>
                <?php if($imageModel->hasErrors('alternative_text')):?>
                    <span class="help-block"><?php echo $imageModel->getError('alternative_text');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group<?php echo $imageModel->hasErrors('alternative_text_ar')?' has-error':'';echo !$imageModel->hasErrors('alternative_text_ar')&&isset($_POST['Image'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $imageModel->getAttributeLabel('alternative_text_ar'); ?>
                <?php if ($imageModel->isAttributeRequired('alternative_text_ar')): ?>
                    <?php if(!$imageModel->hasErrors('alternative_text_ar')&&isset($_POST['Image'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($imageModel->hasErrors('alternative_text_ar') || !isset($_POST['Image'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($imageModel,'alternative_text_ar',array(
                    'class'=>'form-control',
                    'id'=>'alternative_ar_field_area',
                ))?>
                <?php if($imageModel->hasErrors('alternative_text_ar')):?>
                    <span class="help-block"><?php echo $imageModel->getError('alternative_text_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <div id="add-Image-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $imageModel->isNewRecord?Yii::t('department','controller.equipment.Image.action.add.Image'):Yii::t('department','controller.equipment.Image.action.update.Image')?> <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->