<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/18/15
 * Time: 1:19 PM
 */
/* @var $model Department */
?>


<table class="table table-condensed table-hover">
    <thead>
    <tr>
        <th colspan="3">Service Information</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Service Name</td>
        <td>
            <a href="#">
                <?php echo $model->name;?>
            </a></td>

    </tr>
    <tr>
        <td>Service Name (Arabic)</td>
        <td>
            <a href="#">
                <?php echo $model->name_ar;?>
            </a></td>

    </tr>
    <tr>
        <td>Service Order</td>
        <td>
            <a href="#">
                <?php echo $model->order;?>
            </a></td>

    </tr>
    <tr>
        <td>Edit Basic Info</td>

        <td><a href="<?php echo Yii::app()->createUrl('department/update',array('id'=>$model->id));?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
    </tr>
    </tbody>
</table>