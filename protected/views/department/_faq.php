<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/18/15
 * Time: 5:44 PM
 */
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl."/css/assets/nestable.css");

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/assets/jquery.nestable.js",CClientScript::POS_END);


?>

<div id="faq-list">
<?php $this->renderPartial('_faq_list',array(
    'faqs'=>$faqs,
));?>
</div>
<hr/>



<?php echo Chtml::ajaxLink(Yii::t('department','action.create.faq'),Yii::app()->createUrl('department/getCreateFAQForm'),array(
    'type'=>'GET',
    'data'=>array(
        'department_id'=>'js:$("#department_id").val()',
    ),
    'success'=>'js:function(data){
        $("#createFAQModal .modal-body").html(data);
        $(".ok-sign").hide();
        $(".error-sign").hide();
    }',
    'beforeSend'=>'js:function(){
        $("#createFAQModal .modal-body").html("Please Wait...");
        $("#createFAQModal").modal("show");
    }',
    'error'=>'js:function(){
        $("#createFAQModal").modal("hide");
    }'
),array(
    'id'=>'link-'.uniqid(),
));?>
