<?php
/* @var $this DepartmentController */
/* @var $model Department */
$this->breadcrumbs=array(
	array(
		'name'=>'Services',
		'url'=>Yii::app()->createUrl('department/index'),
	),
	'Create',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>