<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/11/15
 * Time: 11:37 AM
 */
?>
<?php if(empty($images)):?>
    <?php echo "No Images Available";?>
<?php endif;?>
<?php if(!empty($images)):?>
    <div class="row">
        <?php foreach($images as $image):?>
            <div class="col-md-3 col-sm-4 gallery-img">
                <div class="wrap-image">
                    <a class="gallery" href="<?php echo $image->getPath();?>" title="Clip-One Business Card">
                        <img src="<?php echo $image->getPath();?>" alt="" class="img-responsive">
                    </a>
                    <div class="chkbox" data-id="<?php echo $image->id;?>"></div>
                    <div class="tools tools-bottom">
                        <a href="#" data-id="<?php echo $image->id;?>" class="move-image">
                            <i class="fa fa-mail-reply"></i>
                        </a>
                        <a href="#" data-id="<?php echo $image->id;?>" class="edit-image">
                            <i class="clip-pencil-3 "></i>
                        </a>
                        <a href="<?php echo Yii::app()->createUrl("gallery/deleteImage");?>" data-id="<?php echo $image->id;?>" class="delete-image">
                            <i class="clip-close-2"></i>
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>

<?php endif;?>
