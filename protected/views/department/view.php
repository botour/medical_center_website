<?php
/* @var $this DepartmentController */
/* @var $model Department */

$this->breadcrumbs = array(
    array(
        'name' => 'Services',
        'url' => Yii::app()->createUrl('department/index'),
    ),
    'Service' . " " . $model->name,
);


Yii::app()->clientScript->registerScript("department-script",'
var CUSTOM_SECTION_CREATE = 1;
var CUSTOM_SECTION_UPDATE = 2;
var updateFQAListUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/updateFQAList')).';
var updateServiceItemListUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/updateServiceItemList')).';
var getUpdateFAQFormUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/getUpdateFAQForm')).';
var deleteFAQUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/deleteFAQ')).';
var getUpdateServiceItemFormUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/getUpdateServiceItemForm')).';
var deleteServiceItemUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/deleteServiceItem')).';
var actionGetCustomSectionTabContentUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/getCustomSectionTabContent')).';
var deleteCustomSectionUrl = '.CJavaScript::encode(Yii::app()->createUrl('department/deleteCustomSection')).';

',CClientScript::POS_END);

$this->cssAssets[] = "bootstrap-editable.css";
$this->cssAssets[] = "bootstrap-fileupload.min.css";
$this->jsAssets[] = "bootstrap-editable.min.js";
$this->jsAssets[] = "bootstrap-fileupload.min.js";
$this->jsCustomFiles[] = "department-supplement.js";

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'createFAQModal',
    'title' => 'Create FAQ',
    'width' => '40',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'updateFAQModal',
    'title' => 'Update FAQ',
    'width' => '40',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'createCustomSectionModal',
    'title' => 'Create Custom Section',
    'width' => '40',
));


$this->widget('application.components.Ajaxmodal', array(
    'name' => 'createServiceItemModal',
    'title' => 'Add Service Item',
    'width' => '40',
));


$this->widget('application.components.Ajaxmodal', array(
    'name' => 'updateServiceItemModal',
    'title' => 'Update Service Item',
    'width' => '40',
));


?>
<?php echo CHtml::hiddenField("department_id",$model->id,array(
    'id'=>'department_id',
));?>



<div class="row">
    <div class="col-md-6">
        <?php echo $this->renderPartial('_info', array(
            'model' => $model,
        )); ?>
    </div>
</div>
<div class="tabbable">
    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
        <li class="<?php echo isset($_GET['ref'])&&$_GET['ref']=="equip_update"?"":" active";?>">
            <a data-toggle="tab" href="#service-items">
                <?php echo Yii::t('department', 'controller.view.panel.tab.service-item'); ?>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#service-staff">
                <?php echo Yii::t('department', 'controller.view.panel.tab.service-staff'); ?>
            </a>
        </li>
        <li class="<?php echo isset($_GET['ref'])&&$_GET['ref']=="equip_update"?" active":"";?>">
            <a data-toggle="tab" href="#service-equipment">
                <?php echo Yii::t('department', 'controller.view.panel.tab.service-equipment'); ?>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#service-faq">
                <?php echo Yii::t('department', 'controller.view.panel.tab.service-faq'); ?>
            </a>
        </li>
        <?php foreach($customSections as $customSection):?>
            <li>
                <a data-toggle="tab" href="#content-<?php echo $customSection->id?>" id="tab-<?php echo $customSection->id?>">
                    <?php echo $customSection->getTitle(); ?>
                </a>
            </li>
        <?php endforeach;?>
        <!-- <li>
            <a data-toggle="tab" href="#" class="custom-tab" id="custom-section-create-tab" url="<?php // echo $this->createUrl("department/getCustomSectionCreateForm")?>" data-id="<?php // echo $model->id;?>" style="color: #FFF;opacity: 0.5;">
                <span class="ladda-label"> <?php //echo Yii::t('department', 'controller.view.panel.tab.add-new-section') ?></span>
                <i class="clip-plus-circle-2"></i>
            </a>
        </li>-->
    </ul>
    <div class="tab-content">

        <div id="service-items" class="tab-pane in<?php echo isset($_GET['ref'])&&$_GET['ref']=="equip_update"?"":" active";?>">
            <?php echo $this->renderPartial('_services',array(
                'services'=>$services,
            )); ?>
        </div>
        <div id="service-staff" class="tab-pane in">

        </div>
        <div id="service-equipment" class="tab-pane in<?php echo isset($_GET['ref'])&&$_GET['ref']=="equip_update"?" active":"";?>">
            <?php echo $this->renderPartial('_equipment',array(
                'equipmentModel'=>$equipmentModel,
                'department_id'=>$model->id,
            )); ?>
        </div>
        <div id="service-faq" class="tab-pane in">
            <?php echo $this->renderPartial('_faq',array(
                'faqs'=>$faqs,
            )); ?>
        </div>
        <?php foreach($customSections as $customSection):?>
            <div id="content-<?php echo $customSection->id;?>" class="tab-pane in">
                <?php echo $this->renderPartial('partial/_custom_section_tab',array(
                    'customSection'=>$customSection,
                ));?>
            </div>
        <?php endforeach;?>

    </div>
</div>