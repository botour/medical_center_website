<?php


/* @var $this DepartmentController */
/* @var $equipmentModel Equipment */
/* @var $imageModel Image */


Yii::app()->clientScript->registerScript('equipment-image-create-script','
$(function(){
	CKEDITOR.replace( "body_field_area");
    CKEDITOR.replace( "body_ar_field_area");


});
',CClientScript::POS_END);


$this->cssAssets[] = 'bootstrap-fileupload.min.css';

$this->jsAssets[] = 'bootstrap-fileupload.min.js';
$this->jsAssets[] = 'ckeditor/ckeditor.js';


$this->breadcrumbs = array(
    array(
        'name' => 'Services',
        'url' => Yii::app()->createUrl('department/index'),
    ),
    array(
        'name' => $equipmentModel->department->getName(),
        'url' => Yii::app()->createUrl('department/view',array('id'=>$equipmentModel->department->id,'ref'=>'equip_update')),
    ),
    array(
        'name' => $equipmentModel->getName(),
        'url' => Yii::app()->createUrl('department/equipmentView',array('id'=>$equipmentModel->id)),
    ),
    'Add Image for'." : ".$equipmentModel->getName(),
);
?>

<?php
$this->renderPartial('_equipment_image_form',array(
    'imageModel'=>$imageModel,
));


