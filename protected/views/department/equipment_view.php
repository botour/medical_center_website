<?php
/* @var $this DepartmentController */
/* @var $equipmentModel Equipment */


$this->breadcrumbs = array(
    array(
        'name' => 'Services',
        'url' => Yii::app()->createUrl('department/index'),
    ),
    array(
        'name' => 'Clinic: '.$equipmentModel->department->getName(),
        'url' => Yii::app()->createUrl('department/view',array('id'=>$equipmentModel->department->id,'ref'=>'equip_update')),
    ),
    'View Equipment: '.$equipmentModel->getName(),
);


$this->jsAssets[] = "jquery.colorbox-min.js";
$this->cssCustomFiles[] = "colorbox.css";
?>

<?php echo CHtml::link(Yii::t('department','action.create.equipment.image'),Yii::app()->createUrl('department/equipmentImageCreate',array(
    'eID'=>$equipmentModel->id,
)),array(
    'class'=>'btn btn-bricky',
    'id'=>'cr-equipment-btn',
))?>
<hr/>
<table class="table table-striped table-bordered table-hover table-full-width">
    <tbody>
    <tr>
        <td><?php echo $equipmentModel->getAttributeLabel('order');?></td>
        <td><?php echo $equipmentModel->order;?></td>
    </tr>
    <tr>
        <td><?php echo $equipmentModel->getAttributeLabel('name');?></td>
        <td><?php echo $equipmentModel->name;?></td>
    </tr>
    <tr>
        <td><?php echo $equipmentModel->getAttributeLabel('name_ar');?></td>
        <td><?php echo $equipmentModel->name_ar;?></td>
    </tr>
    <tr>
        <td><?php echo $equipmentModel->getAttributeLabel('body');?></td>
        <td><?php echo $equipmentModel->body;?></td>
    </tr>
    <tr>
        <td><?php echo $equipmentModel->getAttributeLabel('body_ar');?></td>
        <td><?php echo $equipmentModel->body_ar;?></td>
    </tr>
    </tbody>
</table>
<hr/>
<?php
$this->renderPartial('_equipment_images',array(
    'images'=>$equipmentModel->images,
));
?>
