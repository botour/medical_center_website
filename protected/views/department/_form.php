<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript("department-form-script",'
$(function(){
    updateDepartmentFormControls();
});
');
?>
<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($model, 'name'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>

        <div class="form-group<?php echo $model->hasErrors('name')?' has-error':'';echo !$model->hasErrors('name')&&isset($_POST['Department'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('name'); ?>
                <?php if ($model->isAttributeRequired('name')): ?>
                    <?php if(!$model->hasErrors('name')&&isset($_POST['Department'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($model->hasErrors('name') || !isset($_POST['Department'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-6">
                <?php echo $form->textField($model,'name',array(
                    'class'=>'form-control',
                ))?>
                <?php if($model->hasErrors('name')):?>
                    <span class="help-block"><?php echo $model->getError('name');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group<?php echo $model->hasErrors('name_ar')?' has-error':'';echo !$model->hasErrors('name_ar')&&isset($_POST['Department'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('name_ar'); ?>
                <?php if ($model->isAttributeRequired('name_ar')): ?>
                    <?php if(!$model->hasErrors('name_ar')&&isset($_POST['Department'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($model->hasErrors('name_ar') || !isset($_POST['Department'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-6">
                <?php echo $form->textField($model,'name_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($model->hasErrors('name_ar')):?>
                    <span class="help-block"><?php echo $model->getError('name_ar');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group<?php echo $model->hasErrors('type')?' has-error':'';echo !$model->hasErrors('type')&&isset($_POST['Department'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('type'); ?>
                <?php if ($model->isAttributeRequired('type')): ?>
                    <?php if(!$model->hasErrors('type')&&isset($_POST['Department'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($model->hasErrors('type') || !isset($_POST['Department'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-6">
                <?php echo $form->dropDownList($model,'type',Department::getDepartmentTypeArray(),array(
                    'class'=>'form-control',
                ))?>
                <?php if($model->hasErrors('type')):?>
                    <span class="help-block"><?php echo $model->getError('type');?></span>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group<?php echo $model->hasErrors('order')?' has-error':'';echo !$model->hasErrors('order')&&isset($_POST['Department'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('order'); ?>
                <?php if ($model->isAttributeRequired('order')): ?>
                    <?php if(!$model->hasErrors('order')&&isset($_POST['Department'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($model->hasErrors('order') || !isset($_POST['Department'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-6">
                <?php echo $form->textField($model,'order',array(
                    'class'=>'form-control',
                ))?>
                <?php if($model->hasErrors('order')):?>
                    <span class="help-block"><?php echo $model->getError('order');?></span>
                <?php endif;?>
            </div>
        </div>



        <hr/>
        <div class="row">
            <div class="col-md-offset-8 col-md-4">
                <button class="btn btn-yellow btn-block" type="submit">
                    <?php echo $model->isNewRecord?Yii::t('department','controller.content.action.add.service'):Yii::t('department','controller.content.action.update.service')?> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->