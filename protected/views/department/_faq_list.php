<?php if(empty($faqs)):?>
    <span style="color: #BBB;font-size: 13px;"><?php echo "NO FAQs Available"?></span>
<?php endif;?>
<?php if(!empty($faqs)):?>
    <ul>
        <?php foreach($faqs as $faq):?>
            <li><?php echo CHtml::link(Yii::t('department','action.view.faq.update'),'#',array(
                    'class'=>'faq-update-link',
                    'data-id'=>$faq->id,
                ))?> <?php echo CHtml::link(Yii::t('department','action.view.faq.delete'),'#',array(
                    'class'=>'faq-delete-link',
                    'data-id'=>$faq->id,
                ))?><?php echo $faq->getQuestion()?></li>
        <?php endforeach;?>
    </ul>
<?php endif;?>