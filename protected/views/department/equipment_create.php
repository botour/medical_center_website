<?php

/* @var $this DepartmentController */
/* @var $equipmentCreateModel Equipment */
/* @var $departmentModel Department */


$this->breadcrumbs = array(
    array(
        'name' => 'Services',
        'url' => Yii::app()->createUrl('department/index'),
    ),
    array(
        'name' => $departmentModel->getName(),
        'url' => Yii::app()->createUrl('department/view',array('id'=>$departmentModel->id,'ref'=>'equip_update')),
    ),
    'Create Equipment',
);


Yii::app()->clientScript->registerScript('equipment-create-script','
$(function(){
	CKEDITOR.replace( "body_field_area");
    CKEDITOR.replace( "body_ar_field_area");

});
',CClientScript::POS_END);
$this->jsAssets[] = 'ckeditor/ckeditor.js';


?>
<div class="row">
    <div class="col-md-6">
        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th colspan="3">Service Name</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $departmentModel->getName();?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<?php
$this->renderPartial('_equipment_form',array(
    'equipmentCreateModel'=>$equipmentCreateModel,
));

?>