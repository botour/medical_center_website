<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/25/15
 * Time: 6:24 PM
 */
Yii::app()->clientScript->registerScript("section-script-".$customSection->id,'
$(function(){
    $.fn.editable.defaults.mode = "inline";
    $("#title-'.$customSection->id.'").editable();
});
',CClientScript::POS_END);

?>
<?php echo CHtml::link(Yii::t('department', 'controller.action.delete.custom.section'), Yii::app()->createUrl('department/deleteCustomSection'), array(
    'class' => 'custom-section-delete-link btn btn-danger btn-sm',
    'data-id' => $customSection->id,
)); ?>

<hr>


<table id="user" class="table table-bordered table-striped" style="clear: both">
    <tbody>
    <tr>
        <td class="column-left"><?php echo $customSection->getAttributeLabel("title"); ?></td>
        <td class="column-right">
            <a href="#" id="title-<?php echo $customSection->id; ?>" data-name="title" data-original-title="Enter username"="<?php echo Yii::t('department','model.custom.section.title.enter.request');?>" data-type="textarea"
               data-pk="<?php echo $customSection->id; ?>" data-url="<?php echo Yii::app()->createUrl('department/editCustomSection')?>"
               data-title="Enter <?php echo $customSection->getAttributeLabel("title"); ?>">
                <?php echo $customSection->title; ?>
            </a>
        </td>
    </tr>
    <tr>
        <td><?php echo $customSection->getAttributeLabel("title_ar"); ?></td>
        <td><a href="#" id="firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required"
               data-original-title="Enter your firstname">
            </a></td>
    </tr>
    <tr>
        <td><?php echo $customSection->getAttributeLabel("summary"); ?></td>
        <td><a href="#" id="sex" data-type="select" data-pk="1" data-value="" data-original-title="Select sex">
            </a></td>
    </tr>
    <tr>
        <td><?php echo $customSection->getAttributeLabel("summary_ar"); ?></td>
        <td>
            <a href="#" id="group" data-type="select" data-pk="1" data-value="5" data-source="/groups"
               data-original-title="Select group">
                Admin
            </a></td>
    </tr>
    <tr>
        <td><?php echo $customSection->getAttributeLabel("body"); ?></td>
        <td>
            <a href="#" id="group" data-type="select" data-pk="1" data-value="5" data-source="/groups"
               data-original-title="Select group">
                Admin
            </a></td>
    </tr>
    <tr>
        <td><?php echo $customSection->getAttributeLabel("body_ar"); ?></td>
        <td>
            <a href="#" id="group" data-type="select" data-pk="1" data-value="5" data-source="/groups"
               data-original-title="Select group">
                Admin
            </a></td>
    </tr>
    </tbody>
</table>
