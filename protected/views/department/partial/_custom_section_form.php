<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/18/15
 * Time: 6:14 PM
 */


Yii::app()->clientScript->registerScript("faq-form-script", '
var customSectionFormId = '.CJavaScript::encode($model->isNewRecord?"create-custom-section-form":"update-custom-section-form").';
var customSectionFormSubmitUrl = '.CJavaScript::encode($model->isNewRecord?Yii::app()->createUrl('department/CreateCustomSection'):Yii::app()->createUrl('department/EditCustomSection')).';
var customSectionModalId = '.CJavaScript::encode($model->isNewRecord?"createCustomSectionModal":"updateCustomSectionModal").';
updateCustomSectionFormSubmit();
',CClientScript::POS_END);
CommonFunctions::fixAjax();
?>

<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */
?>
<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => $model->isNewRecord?"create-custom-section-form":"update-custom-section-form",
            'action' => $model->isNewRecord?Yii::app()->createUrl('department/CreateCustomSection'):Yii::app()->createUrl('department/EditCustomSection'),
            'enableAjaxValidation' => false,
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>
        <?php if(!$model->isNewRecord):?>
            <?php echo $form->hiddenField($model,'order');?>
            <?php echo $form->hiddenField($model,'id');?>
        <?php endif;?>
        <?php echo $form->hiddenField($model,'department_id');?>


        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#english-info">
                        <?php echo Yii::t('template', 'content.language.option.english'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#arabic-info">
                        <?php echo Yii::t('template', 'content.language.option.arabic'); ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="english-info" class="tab-pane in active">
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder'=>'')); ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textArea($model, 'summary', array('size' => 60, 'maxlength' => 255,'rows'=>6, 'class' => 'form-control','id'=>'summary-field', 'placeholder'=>'')); ?>

                        </div>
                    </div>
                </div>
                <div id="arabic-info" class="tab-pane in">
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textField($model, 'title_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder'=>'')); ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-md-12">
                            <?php echo $form->textArea($model, 'summary_ar', array('size' => 60, 'maxlength' => 255, 'rows'=>6, 'class' => 'form-control','id'=>'summary-field', 'placeholder'=>'')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-8 col-md-4">
                <button class="btn btn-green btn-block" type="submit" id="<?php echo $model->isNewRecord?"create-custom-section-btn":"update-custom-section-btn"?>">
                    <span class="ladda-label"> <?php echo $model->isNewRecord ? Yii::t('department', 'action.create.custom.section')  : Yii::t('department', 'action.update.custom.section') ?></span>
                    <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->