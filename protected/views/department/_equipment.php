


<?php echo Chtml::link(Yii::t('department','action.create.equipment'),Yii::app()->createUrl('department/equipmentCreate',array(
    'dID'=>$department_id,
)),array(
    'class'=>'btn btn-bricky',
    'id'=>'cr-equipment-btn',
))?>

<hr/>


<div id="equipment-list">
    <?php $this->renderPartial('_equipment_list',array(
        'equipmentModel'=>$equipmentModel,
    ));?>
</div>