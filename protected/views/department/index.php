<?php
/* @var $this DepartmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Services',
);


Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/department-logic.js", CClientScript::POS_END);
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'department-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"grid-row",
            )',
                'summaryText' => 'Services Count: {count}',
                'summaryCssClass' => 'complete-summery',
                'beforeAjaxUpdate' => 'js:function(){
                    $("#loading-indicator").show();
                    }',
                'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $departmentModel->search(),
    //'filter' => $userModel,
    'columns' => array(
        'name',
        'name_ar',
        array(
            'name'=>'type',
            'type'=>'raw',
            'value'=>'CommonFunctions::getLabel($data->type,CommonFunctions::DEPARTMENT_TYPE)',
        ),
        array(
            'name'=>'active',
            'type'=>'raw',
            'value'=>'CommonFunctions::getLabel($data->active,CommonFunctions::DEPARTMENT_ACTIVE)',
        ),
        array(
            'class'=>'CButtonColumn'
        )
    ),
));
?>