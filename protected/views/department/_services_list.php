<?php if(empty($services)):?>
    <span style="color: #BBB;font-size: 13px;"><?php echo "NO Services Available"?></span>
<?php endif;?>
<?php if(!empty($services)):?>
    <ul>
        <?php foreach($services as $service):?>
            <li><?php echo CHtml::link(Yii::t('department','action.view.service.item.update'),'#',array(
                    'class'=>'service-item-update-link',
                    'data-id'=>$service->id,
                ))?> <?php echo CHtml::link(Yii::t('department','action.view.faq.delete'),'#',array(
                    'class'=>'service-item-delete-link',
                    'data-id'=>$service->id,
                ))?><?php echo $service->getItem()?></li>
        <?php endforeach;?>
    </ul>
<?php endif;?>