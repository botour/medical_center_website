<?php
/* @var $this DepartmentController */
/* @var $model Department */

$this->breadcrumbs=array(
	array(
		'name'=>'Services',
		'url'=>Yii::app()->createUrl('department/index'),
	),
	array(
		'name'=>'Service'." ".$model->name,
		'url'=>Yii::app()->createUrl('department/view',array('id'=>$model->id)),
	),
	'Update Service'." ".$model->name,
);

Yii::app()->clientScript->registerScript("department-script",'
var imagesListJSONFile = '.CJavaScript::encode(Yii::app()->baseUrl."/json/images_list.json").';
',CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/assets/ckeditor/ckeditor.js");
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/department-logic.js",CClientScript::POS_END);

?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>