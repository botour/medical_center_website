<?php

/* @var $this StaffController */
/* @var $jobModel Job */

?>


<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($jobModel, 'title'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if($jobModel->hasErrors()):?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($jobModel);?>

            </div>
        <?php endif;?>


        
        <div class="form-group<?php echo $jobModel->hasErrors('title')?' has-error':'';echo !$jobModel->hasErrors('title')&&isset($_POST['Job'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $jobModel->getAttributeLabel('title'); ?>
                <?php if ($jobModel->isAttributeRequired('title')): ?>
                    <?php if(!$jobModel->hasErrors('title')&&isset($_POST['Job'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($jobModel->hasErrors('title') || !isset($_POST['Job'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($jobModel,'title',array(
                    'class'=>'form-control',
                ))?>
                <?php if($jobModel->hasErrors('title')):?>
                    <span class="help-block"><?php echo $jobModel->getError('title');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $jobModel->hasErrors('title_ar')?' has-error':'';echo !$jobModel->hasErrors('title_ar')&&isset($_POST['Job'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $jobModel->getAttributeLabel('title_ar'); ?>
                <?php if ($jobModel->isAttributeRequired('title_ar')): ?>
                    <?php if(!$jobModel->hasErrors('title_ar')&&isset($_POST['Job'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($jobModel->hasErrors('title_ar') || !isset($_POST['Job'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($jobModel,'title_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($jobModel->hasErrors('title_ar')):?>
                    <span class="help-block"><?php echo $jobModel->getError('title_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        

        <div id="add-job-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $jobModel->isNewRecord?Yii::t('setting','controller.job.action.add.Job'):Yii::t('setting','controller.job.action.update.Job')?> <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->