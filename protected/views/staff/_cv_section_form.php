<?php

/* @var $this DepartmentController */
/* @var $cvSectionModel CvSection */

?>


<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($cvSectionModel, 'title'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>



        <?php echo $form->hiddenField($cvSectionModel,'staff_id');?>

        <div class="form-group<?php echo $cvSectionModel->hasErrors('title')?' has-error':'';echo !$cvSectionModel->hasErrors('title')&&isset($_POST['CvSection'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $cvSectionModel->getAttributeLabel('title'); ?>
                <?php if ($cvSectionModel->isAttributeRequired('title')): ?>
                    <?php if(!$cvSectionModel->hasErrors('title')&&isset($_POST['CvSection'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($cvSectionModel->hasErrors('title') || !isset($_POST['CvSection'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($cvSectionModel,'title',array(
                    'class'=>'form-control',
                ))?>
                <?php if($cvSectionModel->hasErrors('title')):?>
                    <span class="help-block"><?php echo $cvSectionModel->getError('title');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $cvSectionModel->hasErrors('title_ar')?' has-error':'';echo !$cvSectionModel->hasErrors('title_ar')&&isset($_POST['CvSection'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $cvSectionModel->getAttributeLabel('title_ar'); ?>
                <?php if ($cvSectionModel->isAttributeRequired('title_ar')): ?>
                    <?php if(!$cvSectionModel->hasErrors('title_ar')&&isset($_POST['CvSection'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($cvSectionModel->hasErrors('title_ar') || !isset($_POST['CvSection'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($cvSectionModel,'title_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($cvSectionModel->hasErrors('title_ar')):?>
                    <span class="help-block"><?php echo $cvSectionModel->getError('title_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $cvSectionModel->hasErrors('order')?' has-error':'';echo !$cvSectionModel->hasErrors('order')&&isset($_POST['CvSection'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $cvSectionModel->getAttributeLabel('order'); ?>
                <?php if ($cvSectionModel->isAttributeRequired('order')): ?>
                    <?php if(!$cvSectionModel->hasErrors('order')&&isset($_POST['CvSection'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($cvSectionModel->hasErrors('order') || !isset($_POST['CvSection'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($cvSectionModel,'order',array(
                    'class'=>'form-control',
                ))?>
                <?php if($cvSectionModel->hasErrors('order')):?>
                    <span class="help-block"><?php echo $cvSectionModel->getError('order');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $cvSectionModel->hasErrors('body')?' has-error':'';echo !$cvSectionModel->hasErrors('body')&&isset($_POST['CvSection'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $cvSectionModel->getAttributeLabel('body'); ?>
                <?php if ($cvSectionModel->isAttributeRequired('body')): ?>
                    <?php if(!$cvSectionModel->hasErrors('body')&&isset($_POST['CvSection'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($cvSectionModel->hasErrors('body') || !isset($_POST['CvSection'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($cvSectionModel,'body',array(
                    'class'=>'form-control',
                    'id'=>'body_field_area',
                ))?>
                <?php if($cvSectionModel->hasErrors('body')):?>
                    <span class="help-block"><?php echo $cvSectionModel->getError('body');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $cvSectionModel->hasErrors('body_ar')?' has-error':'';echo !$cvSectionModel->hasErrors('body')&&isset($_POST['CvSection'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $cvSectionModel->getAttributeLabel('body_ar'); ?>
                <?php if ($cvSectionModel->isAttributeRequired('body_ar')): ?>
                    <?php if(!$cvSectionModel->hasErrors('body_ar')&&isset($_POST['CvSection'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($cvSectionModel->hasErrors('body_ar') || !isset($_POST['CvSection'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextArea($cvSectionModel,'body_ar',array(
                    'class'=>'form-control',
                    'id'=>'body_ar_field_area',
                ))?>
                <?php if($cvSectionModel->hasErrors('body_ar')):?>
                    <span class="help-block"><?php echo $cvSectionModel->getError('body_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <div id="add-CvSection-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $cvSectionModel->isNewRecord?Yii::t('department','Add CV Section'):Yii::t('department','Edit CV Section')?> <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->