<?php
/* @var $staffModel Staff */
?>

<div class="row">
    <div class="col-sm-5 col-md-4">
        <div class="user-left">
            <div class="center">
                <h4><?php echo $staffModel->full_name." (".$staffModel->full_name_ar.")"; ?></h4>

                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="user-image">
                        <div class="fileupload-new thumbnail"><img
                                src="<?php echo $staffModel->profileImg->getPath(); ?>"
                                alt="<?php $staffModel->profileImg->getPath(); ?>">
                        </div>


                    </div>
                </div>

                <hr>
            </div>
            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th colspan="3">Info English: </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('full_name')?>:</td>
                    <td><?php echo $staffModel->full_name;?></td>
                </tr>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('dob')?>:</td>
                    <td><?php echo $staffModel->dob;?></td>
                </tr>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('m_status')?>:</td>
                    <td><?php echo $staffModel->m_status;?></td>
                </tr>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('contact')?>:</td>
                    <td><?php echo $staffModel->contact;?></td>
                </tr>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('nationality')?>:</td>
                    <td><?php echo $staffModel->nationality;?></td>
                </tr>


                </tbody>
            </table>
            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th colspan="3">Info Arabic: </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('full_name_ar')?>:</td>
                    <td><?php echo $staffModel->full_name_ar;?></td>
                </tr>

                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('m_status_ar')?>:</td>
                    <td><?php echo $staffModel->m_status_ar;?></td>
                </tr>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('contact_ar')?>:</td>
                    <td><?php echo $staffModel->contact_ar;?></td>
                </tr>
                <tr>
                    <td><?php echo $staffModel->getAttributeLabel('nationality_ar')?>:</td>
                    <td><?php echo $staffModel->nationality_ar;?></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-7 col-sm-8">
        <?php echo CHtml::link(Yii::t('department','Add CV Section'),Yii::app()->createUrl('staff/cvSectionCreate',array(
            'staff_id'=>$staffModel->id,
        )),array(
            'class'=>'btn btn-bricky',
            'id'=>'cr-equipment-btn',
        ))?>
        <hr/>
        <?php
        Yii::app()->clientScript->registerScript('cv-section-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);


        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'content-grid',
            'rowHtmlOptionsExpression' => 'array("class"=>"center")',
            'showTableOnEmpty'=>true,
            'summaryText' => 'CV Sections\' Count: {count}',
            'summaryCssClass' => 'complete-summery',

            'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
            'htmlOptions' => array(
                'class' => 'Tabled',
            ),
            'cssFile' => Yii::app()->baseUrl . '/css/main.css',
            'dataProvider' => $cvSectionSearchModel->search(),
            'showTableOnEmpty'=>false,
            'emptyText'=>'No CV Sections available to manage',
            'pager'=>array(
                'class'=>'CLinkPager',
                'header'=>'',
                'htmlOptions'=>array(
                    'class'=>'pagination pagination-sm',
                ),
            ),
            //'filter' => $userModel,
            'columns' => array(
                'title',
                'title_ar',
                'order',
                array(
                    'header'=>'',
                    'value'=>array($this,'renderCVSectionOptions'),
                ),
            ),
        ));
        ?>
    </div>
</div>
