<?php

/* @var $this StaffController */
/* @var $jobSearchModel Job */

$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
   'Job Types',
);

?>

<?php echo CHtml::link(Yii::t('setting','action.create.job.type'),Yii::app()->createUrl('staff/jobCreate'),array(
    'class'=>'btn btn-bricky',
    'id'=>'cr-equipment-btn',
))?>

<hr/>

<?php
Yii::app()->clientScript->registerScript('job-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'content-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty'=>true,
    'summaryText' => 'Content Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $jobSearchModel->search(),
    'showTableOnEmpty'=>false,
    'emptyText'=>'No Contents to manage',
    'pager'=>array(
        'class'=>'CLinkPager',
        'header'=>'',
        'htmlOptions'=>array(
            'class'=>'pagination pagination-sm',
        ),
    ),
    //'filter' => $userModel,
    'columns' => array(
        'title',
        'title_ar',

        array(
            'header'=>'',
            'value'=>array($this,'renderJobOptions'),
        ),
    ),
));
?>


