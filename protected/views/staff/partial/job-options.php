<div class="visible-md visible-lg hidden-sm hidden-xs">
    <a href="<?php echo $this->createUrl('staff/jobUpdate',array('job_id'=>$data->id));?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit Job Info"><i class="fa fa-edit"></i></a>


    <form id="deleteForm_<?php echo $data->id;?>" style="display: inline;" action="<?php echo $this->createUrl('staff/jobDelete');?>" method="post">
        <input type="hidden" name="j_id" value="<?php echo $data->id;?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" onclick="if(confirm('Are you Sure you want to delete the job?')){$('#deleteForm_<?php echo $data->id;?>').submit();}" data-original-title="Remove Job Type Data"><i class="fa fa-times fa fa-white"></i></a>
    </form>

</div>