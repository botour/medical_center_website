<div class="visible-md visible-lg hidden-sm hidden-xs">
    <a href="<?php echo $this->createUrl('staff/update',array('id'=>$data->id));?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit Staff Member Info"><i class="fa fa-edit"></i></a>
    <a href="<?php echo $this->createUrl('staff/view',array('id'=>$data->id,'ref'=>'view'));?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Staff Info"><i class="fa fa-share"></i></a>
    <form id="deleteForm_<?php echo $data->id;?>" style="display: inline;" action="<?php echo $this->createUrl('staff/delete');?>" method="post">
        <input type="hidden" name="s_id" value="<?php echo $data->id;?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" onclick="if(confirm('Are you Sure you want to delete the selected staff member?')){$('#deleteForm_<?php echo $data->id;?>').submit();}" data-original-title="Remove Staff Member Data"><i class="fa fa-times fa fa-white"></i></a>
    </form>
</div>