<?php
/* @var $this StaffController */
/* @var $staffModel Staff */

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'notifyOnFormSubmitModal',
    'title' => 'Note',
    'width' => '40',
));


?>



<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($staffModel, 'title'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if ($staffModel->hasErrors()): ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($staffModel); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group<?php echo $staffModel->hasErrors('title') ? ' has-error' : '';
                echo !$staffModel->hasErrors('title') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('title'); ?>
                        <?php if ($staffModel->isAttributeRequired('title')): ?>
                            <?php if (!$staffModel->hasErrors('title') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('title') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->dropDownList($staffModel, 'title', Staff::getStaffTitleArray(), array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('title')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('title'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group<?php echo $staffModel->hasErrors('branch_id') ? ' has-error' : '';
                echo !$staffModel->hasErrors('branch_id') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('branch_id'); ?>
                        <?php if ($staffModel->isAttributeRequired('branch_id')): ?>
                            <?php if (!$staffModel->hasErrors('branch_id') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('branch_id') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->dropDownList($staffModel, 'branch_id', CHtml::listData(Branch::model()->findAll(), 'id', 'name'), array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('branch_id')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('branch_id'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group<?php echo $staffModel->hasErrors('job_id') ? ' has-error' : '';
                echo !$staffModel->hasErrors('job_id') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('job_id'); ?>
                        <?php if ($staffModel->isAttributeRequired('job_id')): ?>
                            <?php if (!$staffModel->hasErrors('job_id') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('job_id') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->dropDownList($staffModel, 'job_id', CHtml::listData(Job::model()->findAll(), 'id', 'title'), array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('job_id')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('job_id'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group<?php echo $staffModel->hasErrors('section_id') ? ' has-error' : '';
                echo !$staffModel->hasErrors('section_id') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('section_id'); ?>
                        <?php if ($staffModel->isAttributeRequired('section_id')): ?>
                            <?php if (!$staffModel->hasErrors('section_id') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('section_id') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->dropDownList($staffModel, 'section_id', CHtml::listData(Department::model()->findAll(), 'id', 'name'), array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('section_id')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('section_id'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if ($staffModel->isNewRecord): ?>
                <div class="col-md-6">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 170px; height: 200px;"><img
                                src="http://www.placehold.it/170x200/EFEFEF/AAAAAA?text=no+image" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 170px; max-height: 200px; line-height: 20px;"></div>
                        <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select Profile Pic</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($staffModel, 'profileImage', array(
                                                                'id'=>'staff-image-controll',
                                                            )); ?>
														</span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!$staffModel->isNewRecord): ?>
                <div class="col-md-6">
                    <?php echo CHtml::image($staffModel->profileImg->getPath(), $staffModel->profileImg->alternative_text); ?>
                </div>
            <?php endif; ?>
        </div>


        <hr/>
        <div class="row">
            <div class="col-md-6">
                <h4>English Info: </h4>

                <div class="form-group<?php echo $staffModel->hasErrors('full_name') ? ' has-error' : '';
                echo !$staffModel->hasErrors('full_name') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('full_name'); ?>
                        <?php if ($staffModel->isAttributeRequired('full_name')): ?>
                            <?php if (!$staffModel->hasErrors('full_name') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('full_name') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'full_name', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('full_name')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('full_name'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('dob') ? ' has-error' : '';
                echo !$staffModel->hasErrors('dob') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('dob'); ?>
                        <?php if ($staffModel->isAttributeRequired('dob')): ?>
                            <?php if (!$staffModel->hasErrors('dob') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('dob') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'dob', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('dob')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('dob'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('address') ? ' has-error' : '';
                echo !$staffModel->hasErrors('address') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('address'); ?>
                        <?php if ($staffModel->isAttributeRequired('address')): ?>
                            <?php if (!$staffModel->hasErrors('address') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('address') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textArea($staffModel, 'address', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('address')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('address'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('contact') ? ' has-error' : '';
                echo !$staffModel->hasErrors('contact') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('contact'); ?>
                        <?php if ($staffModel->isAttributeRequired('contact')): ?>
                            <?php if (!$staffModel->hasErrors('contact') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('contact') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textArea($staffModel, 'contact', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('contact')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('contact'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('m_status') ? ' has-error' : '';
                echo !$staffModel->hasErrors('m_status') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('m_status'); ?>
                        <?php if ($staffModel->isAttributeRequired('m_status')): ?>
                            <?php if (!$staffModel->hasErrors('m_status') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('m_status') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'm_status', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('m_status')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('m_status'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('nationality') ? ' has-error' : '';
                echo !$staffModel->hasErrors('nationality') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('nationality'); ?>
                        <?php if ($staffModel->isAttributeRequired('nationality')): ?>
                            <?php if (!$staffModel->hasErrors('nationality') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('nationality') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'nationality', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('nationality')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('nationality'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Arabic Info: </h4>

                <div class="form-group<?php echo $staffModel->hasErrors('full_name_ar') ? ' has-error' : '';
                echo !$staffModel->hasErrors('full_name_ar') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('full_name_ar'); ?>
                        <?php if ($staffModel->isAttributeRequired('full_name_ar')): ?>
                            <?php if (!$staffModel->hasErrors('full_name_ar') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('full_name_ar') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'full_name_ar', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('full_name_ar')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('full_name_ar'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('dob_ar') ? ' has-error' : '';
                echo !$staffModel->hasErrors('dob_ar') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('dob_ar'); ?>
                        <?php if ($staffModel->isAttributeRequired('dob_ar')): ?>
                            <?php if (!$staffModel->hasErrors('dob_ar') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('dob_ar') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'dob_ar', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('dob_ar')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('dob_ar'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('address_ar') ? ' has-error' : '';
                echo !$staffModel->hasErrors('address_ar') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('address_ar'); ?>
                        <?php if ($staffModel->isAttributeRequired('address_ar')): ?>
                            <?php if (!$staffModel->hasErrors('address_ar') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('address_ar') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textArea($staffModel, 'address_ar', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('address_ar')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('address_ar'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('contact_ar') ? ' has-error' : '';
                echo !$staffModel->hasErrors('contact') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('contact_ar'); ?>
                        <?php if ($staffModel->isAttributeRequired('contact_ar')): ?>
                            <?php if (!$staffModel->hasErrors('contact_ar') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('contact_ar') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textArea($staffModel, 'contact_ar', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('contact_ar')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('contact_ar'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('m_status_ar') ? ' has-error' : '';
                echo !$staffModel->hasErrors('m_status_ar') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('m_status_ar'); ?>
                        <?php if ($staffModel->isAttributeRequired('m_status_ar')): ?>
                            <?php if (!$staffModel->hasErrors('m_status_ar') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('m_status_ar') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'm_status_ar', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('m_status_ar')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('m_status_ar'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo $staffModel->hasErrors('nationality_ar') ? ' has-error' : '';
                echo !$staffModel->hasErrors('nationality_ar') && isset($_POST['Staff']) ? ' has-success' : '' ?>">
                    <label class="col-sm-4 control-label" for="form-field-2">
                        <?php echo $staffModel->getAttributeLabel('nationality_ar'); ?>
                        <?php if ($staffModel->isAttributeRequired('nationality_ar')): ?>
                            <?php if (!$staffModel->hasErrors('nationality_ar') && isset($_POST['Staff'])): ?>
                                <span class="symbol ok"></span>
                            <?php endif; ?>
                            <?php if ($staffModel->hasErrors('nationality_ar') || !isset($_POST['Staff'])): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </label>

                    <div class="col-sm-8">
                        <?php echo $form->textField($staffModel, 'nationality_ar', array(
                            'class' => 'form-control',
                        )) ?>
                        <?php if ($staffModel->hasErrors('nationality_ar')): ?>
                            <span class="help-block"><?php echo $staffModel->getError('nationality_ar'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


        <div id="add-Staff-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $staffModel->isNewRecord ? Yii::t('staff', 'controller.Staff.action.add.Staff') : Yii::t('staff', 'controller.Staff.action.update.Staff') ?>
                        <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->