<?php
/* @var $this StaffController */
/* @var $jobUpdateModel Job */
$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    array(
        'name' => 'Job Types',
        'url' => Yii::app()->createUrl('staff/jobIndex'),
    ),
    'Update Job Type: '.$jobUpdateModel->title,
);

?>



<?php
$this->renderPartial(
    '_job-form',array(
        'jobModel'=>$jobUpdateModel,
    )
);
?>
