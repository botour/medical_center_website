<?php
/* @var $this StaffController */
/* @var $staffUpdateModel Staff */

$staffName = $staffUpdateModel->title==Staff::TITLE_DR?"Dr. ":"";
$staffName .= $staffUpdateModel->full_name;
$this->breadcrumbs = array(
	array(
		'name' => 'Staff',
		'url' => Yii::app()->createUrl('staff/index'),
	),
	array(
		'name' => $staffName,
		'url' => Yii::app()->createUrl('staff/view',array('id'=>$staffUpdateModel->id)),
	),

	'Update Staff Member: '.$staffName,
);

?>

<?
$this->renderPartial('_form',array(
		'staffModel'=>$staffUpdateModel,
	)
);?>