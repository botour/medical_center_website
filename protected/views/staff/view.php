<?php
/* @var $this StaffController */
/* @var $staffModel Staff */
/* @var $viewModel Staff */


$this->cssAssets[] = 'bootstrap-fileupload.min.css';

$this->jsAssets[] = 'bootstrap-fileupload.min.js';

$staffName = $staffModel->title==Staff::TITLE_DR?"Dr. ":"";
$staffName .= $staffModel->full_name;
$this->breadcrumbs = array(
    array(
        'name' => 'Staff',
        'url' => Yii::app()->createUrl('staff/index'),
    ),
    $staffName,
);

?>


<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="<?php echo $ref=='submit_edit'?"":" active";?>">
                    <a data-toggle="tab" href="#panel_overview">
                        Info
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" class="<?php echo $ref=='submit_edit'?" active":"";?>" href="#panel_edit_account">
                        Edit Profile Data
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div id="panel_overview" class="tab-pane in <?php echo $ref=='submit_edit'?"":" active";?>">
                    <?php $this->renderPartial('_info',array(
                        'staffModel'=>$viewModel,
                        'cvSectionSearchModel'=>$cvSectionSearchModel,
                    ));?>
                </div>
                <div id="panel_edit_account" class="tab-pane in <?php echo $ref=='submit_edit'?" active":"";?>">
                    <?php $this->renderPartial('_form',array(
                        'staffModel'=>$staffModel,
                    ));?>
                </div>

        </div>
    </div>
</div>