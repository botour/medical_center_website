<?php
/* @var $this StaffController */
/* @var $staffSearchModel Staff */


?>
<?php
Yii::app()->clientScript->registerScript('job-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'content-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty'=>true,
    'summaryText' => 'Staff Member Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $staffSearchModel->search(),
    'showTableOnEmpty'=>false,
    'emptyText'=>'No Staff to manage',
    'pager'=>array(
        'class'=>'CLinkPager',
        'header'=>'',
        'htmlOptions'=>array(
            'class'=>'pagination pagination-sm',
        ),
    ),
    //'filter' => $userModel,
    'columns' => array(
        array(
            'name'=>'profile_image_id',
            'value'=>'CHtml::image($data->profileImg->getPath(),$data->profileImg->alternative_text);',
            'type'=>'raw',
        ),
        array(
            'name'=>'title',
            'value'=>'$data->title==Staff::TITLE_DR?"Dr":"-"',
        ),
        'full_name',
        'full_name_ar',

        array(
            'name'=>'job_id',
            'value'=>'$data->job->title',
        ),
        array(
            'name'=>'section_id',
            'value'=>'$data->section->name'
        ),
        array(
            'header'=>'',
            'value'=>array($this,'renderStaffOptions'),
        ),
    ),
));
?>


