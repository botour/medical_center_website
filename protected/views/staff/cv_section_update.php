<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/5/15
 * Time: 9:46 PM
 */


$this->breadcrumbs = array(
    array(
        'name' => 'Staff',
        'url' => Yii::app()->createUrl('staff/index'),
    ),
    array(
        'name' => $staffModel->full_name,
        'url' => Yii::app()->createUrl('staff/view',array('ref'=>'view','id'=>$staffModel->id)),
    ),
    'Update CV Section',
);


Yii::app()->clientScript->registerScript('cv-section-update-script','
$(function(){
	CKEDITOR.replace( "body_field_area");
    CKEDITOR.replace( "body_ar_field_area");

});
',CClientScript::POS_END);

$this->jsAssets[] = 'ckeditor/ckeditor.js';

?>


<?php
$this->renderPartial('_cv_section_form',array(
    'cvSectionModel'=>$cvSectionUpdateModel,
));

?>