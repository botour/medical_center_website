<?php
/* @var $this StaffController */
/* @var $jobCreateModel Job */

$this->breadcrumbs = array(
    array(
        'name' => 'Settings',
        'url' => Yii::app()->createUrl('setting/index'),
    ),
    array(
        'name' => 'Job Types',
        'url' => Yii::app()->createUrl('staff/jobIndex'),
    ),
    'Create Job Type',
);


?>


<?php
    $this->renderPartial(
        '_job-form',array(
            'jobModel'=>$jobCreateModel,
        )
    );
?>
