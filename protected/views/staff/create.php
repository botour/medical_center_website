<?php
/* @var $this StaffController */
/* @var $cvSectionCreateModel CvSection */
/* @var $staffModel StaffModel */

$this->breadcrumbs = array(
    array(
        'name' => 'Staff',
        'url' => Yii::app()->createUrl('staff/index'),
    ),

    'Add Staff',
);


$this->cssAssets[] = 'bootstrap-fileupload.min.css';

$this->jsAssets[] = 'bootstrap-fileupload.min.js';

$this->renderPartial('_form',array(
    'staffModel'=>$staffCreateModel,
));