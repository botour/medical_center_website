<?php
/* @var $this VacancyController */
/* @var $vacancyUpdateModel Vacancy */

$this->breadcrumbs = array(
    array(
        'name' => 'Vacancies',
        'url' => Yii::app()->createUrl('vacancy/index'),
    ),

    'Update Job Vacancy',
);


Yii::app()->clientScript->registerScript('vacancy-create-script', '
$(function(){
	CKEDITOR.replace( "description_field_area");
    CKEDITOR.replace( "description_ar_field_area");

});
', CClientScript::POS_END);

$this->jsAssets[] = 'ckeditor/ckeditor.js';
?>

<?php
$this->renderPartial(
    '_form', array(
        'vacancyModel' => $vacancyUpdateModel,
    )
);
?>
