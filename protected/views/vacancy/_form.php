<?php
/* @var $this VacancyController */
/* @var $vacancyModel Vacancy */
?>


<div class="form row">
	<div class="col-md-12">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'contact-type-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'focus' => array($vacancyModel, 'title'),
			'clientOptions' => array(
				'successCssClass' => 'has-success',
				'errorCssClass' => 'has-error',
				'validatingErrorMessage' => '',
				'inputContainer' => '.form-group',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
			),
			'htmlOptions' => array(
				'role' => 'form',
				'class' => 'form-horizontal',
				'enctype' => 'multipart/form-data',
			),
		)); ?>



		<?php echo $form->hiddenField($vacancyModel,'active');?>

		<div class="form-group<?php echo $vacancyModel->hasErrors('title')?' has-error':'';echo !$vacancyModel->hasErrors('title')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('title'); ?>
				<?php if ($vacancyModel->isAttributeRequired('title')): ?>
					<?php if(!$vacancyModel->hasErrors('title')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('title') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'title',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('title')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('title');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('title_ar')?' has-error':'';echo !$vacancyModel->hasErrors('title_ar')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('title_ar'); ?>
				<?php if ($vacancyModel->isAttributeRequired('title_ar')): ?>
					<?php if(!$vacancyModel->hasErrors('title_ar')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('title_ar') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'title_ar',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('title_ar')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('title_ar');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('position')?' has-error':'';echo !$vacancyModel->hasErrors('position')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('position'); ?>
				<?php if ($vacancyModel->isAttributeRequired('position')): ?>
					<?php if(!$vacancyModel->hasErrors('position')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('position') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'position',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('position')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('position');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('position_ar')?' has-error':'';echo !$vacancyModel->hasErrors('position_ar')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('position_ar'); ?>
				<?php if ($vacancyModel->isAttributeRequired('position_ar')): ?>
					<?php if(!$vacancyModel->hasErrors('position_ar')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('position_ar') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'position_ar',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('position_ar')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('position_ar');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('salary')?' has-error':'';echo !$vacancyModel->hasErrors('salary')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('salary'); ?>
				<?php if ($vacancyModel->isAttributeRequired('salary')): ?>
					<?php if(!$vacancyModel->hasErrors('salary')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('salary') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'salary',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('salary')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('salary');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('salary_ar')?' has-error':'';echo !$vacancyModel->hasErrors('salary_ar')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('salary_ar'); ?>
				<?php if ($vacancyModel->isAttributeRequired('salary_ar')): ?>
					<?php if(!$vacancyModel->hasErrors('salary_ar')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('salary_ar') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'salary_ar',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('salary_ar')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('title_ar');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('description')?' has-error':'';echo !$vacancyModel->hasErrors('description')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('description'); ?>
				<?php if ($vacancyModel->isAttributeRequired('description')): ?>
					<?php if(!$vacancyModel->hasErrors('description')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('description') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textArea($vacancyModel,'description',array(
					'class'=>'form-control',
					'id'=>'description_field_area',
				))?>
				<?php if($vacancyModel->hasErrors('description')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('description');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $vacancyModel->hasErrors('description_ar')?' has-error':'';echo !$vacancyModel->hasErrors('description_ar')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('description_ar'); ?>
				<?php if ($vacancyModel->isAttributeRequired('description_ar')): ?>
					<?php if(!$vacancyModel->hasErrors('description_ar')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('description_ar') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textArea($vacancyModel,'description_ar',array(
					'class'=>'form-control',
					'id'=>'description_ar_field_area',
				))?>
				<?php if($vacancyModel->hasErrors('description_ar')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('description_ar');?></span>
				<?php endif;?>
			</div>
		</div>
		<div class="form-group<?php echo $vacancyModel->hasErrors('contact_email')?' has-error':'';echo !$vacancyModel->hasErrors('contact_email')&&isset($_POST['Vacancy'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $vacancyModel->getAttributeLabel('contact_email'); ?>
				<?php if ($vacancyModel->isAttributeRequired('contact_email')): ?>
					<?php if(!$vacancyModel->hasErrors('contact_email')&&isset($_POST['Vacancy'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($vacancyModel->hasErrors('contact_email') || !isset($_POST['Vacancy'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-9">
				<?php echo $form->textField($vacancyModel,'contact_email',array(
					'class'=>'form-control',
				))?>
				<?php if($vacancyModel->hasErrors('contact_email')):?>
					<span class="help-block"><?php echo $vacancyModel->getError('contact_email');?></span>
				<?php endif;?>
			</div>
		</div>




		<div id="add-Vacancy-submit-button-container">
			<hr/>
			<div class="row">
				<div class="col-md-offset-8 col-md-4">
					<button class="btn btn-yellow btn-block" type="submit">
						<?php echo $vacancyModel->isNewRecord?Yii::t('vacancy','controller.vacancy.action.add.vacancy'):Yii::t('vacancy','controller.vacancy.action.update.vacancy')?> <i class="fa fa-arrow-circle-right"></i>
					</button>
				</div>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div><!-- End Form Container-->