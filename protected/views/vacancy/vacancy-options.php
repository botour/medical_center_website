<div class="visible-md visible-lg hidden-sm hidden-xs">
    <a href="<?php echo $this->createUrl('vacancy/update',array('id'=>$data->id));?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit Vacancy Info"><i class="fa fa-edit"></i></a>
    <form id="deleteForm_<?php echo $data->id;?>" style="display: inline;" action="<?php echo $this->createUrl('vacancy/delete');?>" method="post">
        <input type="hidden" name="v_id" value="<?php echo $data->id;?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" onclick="if(confirm('Are you Sure you want to delete the selected Vacancy?')){$('#deleteForm_<?php echo $data->id;?>').submit();}" data-original-title="Remove vacancy Data"><i class="fa fa-times fa fa-white"></i></a>
    </form>

    <form id="activateForm_<?php echo $data->id;?>" style="display: inline;" action="<?php echo $this->createUrl('vacancy/active');?>" method="post">
        <input type="hidden" name="vacancy_id" value="<?php echo $data->id;?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" onclick="if(confirm('Are You Sure?')){$('#activateForm_<?php echo $data->id;?>').submit();}" data-original-title="<?php echo $data->active==Vacancy::STATUS_ACTIVE?"deActivate":"Activate"?>"><i class="fa fa-star fa fa-white"></i></a>
    </form>
</div>