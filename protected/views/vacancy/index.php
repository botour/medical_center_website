<?php
/* @var $this VacancyController */
/* @var $searchModel Vacancy */

$this->breadcrumbs = array(
	'Vacancies',
);

?>

<?php echo CHtml::link(Yii::t('vacancy','action.create.vacancy'),Yii::app()->createUrl('vacancy/create'),array(
	'class'=>'btn btn-bricky',
	'id'=>'cr-vacancy-btn',
))?>

<hr/>


<?php
Yii::app()->clientScript->registerScript('vacancy-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);


$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'vacancy-grid',
	'rowHtmlOptionsExpression' => 'array("class"=>"center")',
	'showTableOnEmpty'=>true,
	'summaryText' => 'Vacancy Count: {count}',
	'summaryCssClass' => 'complete-summery',

	'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
	'htmlOptions' => array(
		'class' => 'Tabled',
	),
	'cssFile' => Yii::app()->baseUrl . '/css/main.css',
	'dataProvider' => $searchModel->search(),
	'showTableOnEmpty'=>false,
	'emptyText'=>'No Vacancies Data to manage',
	'pager'=>array(
		'class'=>'CLinkPager',
		'header'=>'',
		'htmlOptions'=>array(
			'class'=>'pagination pagination-sm',
		),
	),
	//'filter' => $userModel,
	'columns' => array(
		'title',
		'title_ar',
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>'CommonFunctions::getLabel($data->active,CommonFunctions::VACANCY_ACTIVE)'
		),
		array(
			'header'=>'Options',
			'value'=>array($this,'renderVacancyOptions'),
		),
	),
));
?>


