<?php
$this->breadcrumbs = array(
    array(
        'name' => Yii::t('website', 'breadcrumb.home'),
        'url' => Yii::app()->createUrl('home/home'),
    ),
    '/',
    $serviceModel->getName(),
);

?>
<div class="cell-12">
    <div class="row">
        <div id="tabs" class="tabs">
            <ul>
                <li class=" active"><a href="#" class=""><?php echo Yii::t('website', 'service.tab.description'); ?></a>
                </li>
                <li class=""><a href="#" class=""><?php echo Yii::t('website', 'service.tab.staff'); ?></a></li>
                <li class=""><a href="#" class=""><?php echo Yii::t('website', 'service.tab.equipment'); ?></a></li>
                <li class=""><a href="#" class=""><?php echo Yii::t('website', 'service.tab.faq'); ?></a></li>
            </ul>
            <div class="tabs-pane">
                <div class="tab-panel active">
                    <?php $services = ServiceItem::model()->findAll('department_id=:department_id', array(':department_id' => $serviceModel->id)); ?>
                    <?php if (!empty($services)): ?>
                        <ul class="list alt list-ok">
                            <?php foreach ($services as $service): ?>
                                <li><?php echo $service->getItem(); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php if (empty($services)): ?>
                        <div class="box warning-box">
                            <h3><?php echo Yii::t('website', 'no.contents.available'); ?></h3>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="tab-panel">
                    <?php if (empty($serviceModel->staff)): ?>
                        <div class="box warning-box">
                            <h3><?php echo Yii::t('website', 'no.contents.available'); ?></h3>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($serviceModel->staff)): ?>
                        <?php $counter = 0; ?>
                        <div class="grid-list list">
                            <div class="row">
                                <?php foreach ($serviceModel->staff as $staff): ?>
                                    <div class="cell-4 fx shop-item" data-animate="fadeInUp">
                                        <div class="item-box">
                                            <?php if ($counter == 0): ?>
                                                <h3 class="item-title"><a
                                                        href="product.html"><?php echo Yii::t('website', 'service.tab.content.staff.header'); ?></a>
                                                </h3>
                                            <?php endif; ?>
                                            <?php $counter += 1; ?>

                                            <div class="item-img">
                                                <a href="<?php echo Yii::app()->createUrl('home/staffm', array('id' => $staff->id)); ?>"><img
                                                        alt="<?php echo $staff->getTitle() . " " . $staff->getFullName(); ?>"
                                                        src="<?php echo $staff->profileImg->getPath(); ?>"></a>
                                            </div>
                                            <div class="item-details">
                                                <p>Phasellus blandit elementum tellus, nec adipiscing dui elementum non
                                                    Phasellus blandit elementum tellus, nec adipiscing dui elementum non
                                                    Phasellus blandit elementum tellus, nec adipiscing dui elementum
                                                    non</p>

                                                <div class="right">

                                                </div>
                                                <div class="left">
                                                    <div class="item-cart">

                                                    </div>
                                                    <div class="item-tools">
                                                        <a href="<?php echo Yii::app()->createUrl('home/staffm', array('sId' => $staff->id)); ?>"><?php echo Yii::t('website', 'service.tab.content.staff.read.more'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="tab-panel">
                    <?php if (!empty($serviceModel->equipments)): ?>
                        <?php foreach ($serviceModel->equipments as $equipment): ?>
                            <div class="row">
                                <div class="cell-8">
                                    <div class="block" data-animate="fadeInLeft">
                                        <h4 class="block-head  margin-bottom-40 style3" data-animate="fadeInUp"
                                            data-animation-delay="600">
                                            <span> <?php echo $equipment->getName(); ?> </span>
                                        </h4>
                                        <?php echo $equipment->getBody(); ?>
                                    </div>
                                </div>
                                <div class="cell-4">
                                    <?php if (!empty($equipment->images)): ?>
                                        <div class="portfolio-item">
                                            <div class="img-holder">
                                                <div class="gallery portfolio-img-slick">
                                                    <?php foreach ($equipment->images as $image): ?>
                                                        <div>
                                                            <a href="<?php echo $image->getPath() ?>"
                                                               class="zoom" data-gal="prettyPhoto[pp_gal]"
                                                               title="<?php $image->getName(); ?>">
                                                                <img
                                                                    src="<?php echo $image->getThumbPath(); ?>"
                                                                    alt="<?php $image->getAlternativeText(); ?>"></a>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="tab-panel">
                    <?php if (!empty($serviceModel->faqs)): ?>
                        <ul id="accordion" class="accordion">
                            <?php foreach ($serviceModel->faqs as $faq): ?>
                                <li>
                                    <h3 class=""><a href="#"><span
                                                class=""><?php echo $faq->getQuestion(); ?></span></a>
                                    </h3>

                                    <div class="accordion-panel">
                                        <?php echo $faq->getAnswer(); ?>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
