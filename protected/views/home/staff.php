<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',

    Yii::t('website','breadcrumb.staff'),
);

?>

<div class="portfolio-filterable">

    <div class="toolsBar">
        <div class="cell-10 left products-filter-top">
            <?php echo CHtml::beginForm(array('home/staff'), 'get'); ?>
            <div class="left">
                <span><?php echo Yii::t('website', 'staff.filter.label.section'); ?></span>
                <?php echo CHtml::activeDropDownList($staffModel, 'section_id', Yii::app()->getLanguage() == "ar" ? CHtml::listData(Department::model()->findAll(), 'id', 'name_ar') : CHtml::listData(Department::model()->findAll(), 'id', 'name'), array(
                    'prompt' => Yii::t('website', 'please.select.a.section'),
                )); ?>
            </div>
            <div class="left">
                <span><?php echo Yii::t('website', 'staff.filter.label.branch'); ?></span>
                <?php echo CHtml::activeDropDownList($staffModel, 'branch_id', Yii::app()->getLanguage() == 'ar' ? CHtml::listData(Branch::model()->findAll(), 'id', 'name_ar') : CHtml::listData(Branch::model()->findAll(), 'id', 'name'), array(
                    'prompt' => Yii::t('website', 'please.select.a.branch'),
                )); ?>
            </div>
            <div class="left">
                <span><?php echo Yii::t('website', 'staff.filter.label.job'); ?></span>
                <?php echo CHtml::activeDropDownList($staffModel, 'job_id', Yii::app()->getLanguage() == "ar" ? CHtml::listData(Job::model()->findAll(), 'id', 'title_ar') : CHtml::listData(Job::model()->findAll(), 'id', 'title'), array(
                    'prompt' => Yii::t('website', 'please.select.a.job'),
                )); ?>
            </div>

        </div>
        <div class="right cell-2 list-grid">
            <input type="submit" value="<?php echo Yii::t('website', 'filter.staff'); ?>">
        </div>

        </form>


    </div>

    <?php if (!empty($staffArray)): ?>
        <?php foreach ($staffArray as $staff): ?>
            <div class="cell-3 design" data-category="design">
                <div class="portfolio-item">
                    <div class="img-holder">
                        <div class="img-over">
                            <a href="portfolio-single.html" class="fx link"><b class="fa fa-link"></b></a>
                            <a href="<?php echo $staff->profileImg->getPath() ?>"
                               class="fx zoom" data-gal="prettyPhoto[pp_gal]"
                               title="<?php echo $staff->getFullName(); ?>"><b
                                    class="fa fa-search-plus"></b></a>
                        </div>
                        <img alt="<?php echo $staff->profileImg->getAlternativeText() ?>"
                             src="<?php echo $staff->profileImg->getThumbPath() ?>">
                    </div>
                    <div class="name-holder">
                        <a href="<?php echo Yii::app()->createUrl('home/staffm', array('sId' => $staff->id)); ?>"
                           class="project-name"><?php echo $staff->getFullName(); ?></a>
                        <span class="project-options"><?php echo $staff->job->getTitle(); ?></span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>


</div>
</div>
