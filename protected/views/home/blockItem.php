<?php
/* @var $this HomeController */
/* @var $block Block */


?>
<?php if($block->type == Block::TYPE_TEXT_ONLY):?>
    <p>
        <?php echo $block->getText();?>
    </p>
<?php endif;?>

<?php if($block->type == Block::TYPE_IMAGE_ONLY):?>
    <p>
        <?php $images =  $block->images;?>
        <img src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[0]->url?>">
    </p>
<?php endif;?>

<?php if($block->type == Block::TYPE_TEXT_PLUS_ONE_IMAGE):?>
    <p>
        <?php $images =  $block->images;?>
        <?php $float = Yii::app()->language=='ar'?'right':'left';?>
        <img style="float: <?php echo $float;?>;width: 90px;height: 90px;margin: 5px;" src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[0]->url?>">
        <?php echo $block->getText();?>
    </p>

<?php endif;?>

<?php if($block->type == Block::TYPE_TEXT_PLUS_TWO_IMAGES):?>
    <p>
        <?php echo $block->getText();?>
    </p>
    <div class="row">
        <br/>
        <table style="width: 100%;" class="content-table">
            <?php $images =  $block->images;?>
            <tr>
                <td align="center"><img style="width: 90px;height: 90px;" src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[0]->url?>"></td>
                <td align="center"><img style="width: 90px;height: 90px;" src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[1]->url?>"></td>
            </tr>
        </table>
        <br/>
    </div>
<?php endif;?>

<?php if($block->type == Block::TYPE_TEXT_PLUS_THREE_IMAGES):?>
    <p>
        <?php echo $block->getText();?>
    </p>
    <div class="row">
        <br/>
        <table style="width: 100%;" class="content-table">
            <?php $images =  $block->images;?>
            <tr>
                <td align="center"><img style="width: 90px;height: 90px;" src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[0]->url?>"></td>
                <td align="center"><img style="width: 90px;height: 90px;" src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[1]->url?>"></td>
                <td align="center"><img style="width: 90px;height: 90px;" src="<?php echo Yii::app()->baseUrl.'/'.CommonFunctions::IMAGES_PATH.CommonFunctions::NEWS_IMAGES_PATH.$images[2]->url?>"></td>
            </tr>
        </table>
        <br/>
    </div>
<?php endif;?>
<div class="clearfix"></div>
<br/>