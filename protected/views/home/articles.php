<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',
    Yii::t('website','breadcrumb.articles'),
);

?>

<div class="sectionWrapper">
    <div class="container">
        <div class="blog-thumbs no-bar">
            <?php if(!empty($articles)):?>
            <div class="blog-posts">
                <?php foreach($articles as $article):?>
                <div class="post-item fx" data-animate="fadeInLeft">
                    <article class="post-content">
                        <div class="post-info-container">
                            <div class="post-info">
                                <h2><a class="main-color" href="<?php echo Yii::app()->createUrl('home/article',array('id'=>$article->id));?>"><?php echo $article->getTitle();?></a></h2>
                                <ul class="post-meta">
                                    <li class="meta-user"><i class="fa fa-user"></i><?php echo Yii::t('website','By');?> <a href="<?php echo Yii::app()->createUrl('home/staffm',array('sId'=>$article->doctor->id))?>"><?php echo $article->doctor->getFullName();?></a></li>

                                </ul>
                            </div>
                        </div>
                        <p><?php echo $article->getSummary();?> <a class="read-more" href="<?php echo Yii::app()->createUrl('home/article',array('id'=>$article->id));?>">
                                Read more</a> </p>
                    </article>
                </div>

                <?php endforeach;?>


            </div>
            <?php endif;?>
            <?php if(empty($articles)):?>
                <div class="cell-12">
                    <div
                        class="box warning-box fx animated fadeInRight"><?php echo Yii::t('website', 'no.articles.available.for.now'); ?></div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>