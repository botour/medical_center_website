<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',
    array(
        'name'=>Yii::t('website','breadcrumb.articles'),
        'url'=>Yii::app()->createUrl('home/articles'),
    ),
    '/',
    $article->getTitle(),
);

?>