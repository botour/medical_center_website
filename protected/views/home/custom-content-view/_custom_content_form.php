<?php

/*
 * @var $this HomeController
 * @var $model CustomCustomContent
 *
 */
?>

<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($model, 'value'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if ($model->hasErrors()): ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($model); ?>

            </div>
        <?php endif; ?>


        <div class="form-group<?php echo $model->hasErrors('value') ? ' has-error' : '';
        echo !$model->hasErrors('value') && isset($_POST['CustomContent']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('value'); ?>
                <?php if ($model->isAttributeRequired('value')): ?>
                    <?php if (!$model->hasErrors('value') && isset($_POST['CustomContent'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('value') || !isset($_POST['CustomContent'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-9">
                <?php echo $form->textArea($model, 'value', array(
                    'class' => 'form-control',
                    'id' => 'value_field',
                )) ?>
                <?php if ($model->hasErrors('value')): ?>
                    <span class="help-block"><?php echo $model->getError('value'); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group<?php echo $model->hasErrors('value_ar') ? ' has-error' : '';
        echo !$model->hasErrors('value_ar') && isset($_POST['CustomContent']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('value_ar'); ?>
                <?php if ($model->isAttributeRequired('value_ar')): ?>
                    <?php if (!$model->hasErrors('value_ar') && isset($_POST['CustomContent'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('value_ar') || !isset($_POST['CustomContent'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-9">
                <?php echo $form->textArea($model, 'value_ar', array(
                    'class' => 'form-control',
                    'id' => 'value_ar_field',
                )) ?>
                <?php if ($model->hasErrors('value_ar')): ?>
                    <span class="help-block"><?php echo $model->getError('value_ar'); ?></span>
                <?php endif; ?>
            </div>
        </div>


        <hr/>
        <div class="row">
            <div class="col-md-offset-8 col-md-4">
                <button class="btn btn-yellow btn-block" type="submit">
                    <?php echo Yii::t('CustomContent', 'Save') ?> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
        <br/>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->

