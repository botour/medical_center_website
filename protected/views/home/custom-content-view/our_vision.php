<?php

/*
 * @var $this HomeController
 * @var $model CustomContent
 *
 */

Yii::app()->clientScript->registerScript("custom-content-create-script",'
CKEDITOR.replace( "value_field");
    CKEDITOR.replace( "value_ar_field");
',CClientScript::POS_END);


$this->jsAssets[] = 'ckeditor/ckeditor.js';
$this->renderPartial('custom-content-view/_custom_content_form',array(
    'model'=>$model,
));
?>
