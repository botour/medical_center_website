<!-- This is the gallery container -->
<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',
    array(
        'name'=>Yii::t('website','breadcrumb.gallery'),
        'url'=>Yii::app()->createUrl('home/galleries'),
    ),
    '/',
    $galleryModel->getTitle(),
);

?>
<div class="our_gallery">
    <div id="gallery" class="content cell-9">
        <div id="controls" class="controls"></div>
        <div class="slideshow-container">
            <div id="loading" class="loader"></div>
            <div id="slideshow" class="slideshow"></div>
        </div>
        <div id="caption" class="caption-container"></div>
    </div>
    <div id="thumbs" class="navigation cell-3">
        <?php if (!empty($galleryModel->images)): ?>
            <ul class="thumbs noscript">
                <?php foreach ($galleryModel->images as $image): ?>
                    <li>
                        <a class="thumb" name="leaf" href="<?php echo $image->getPath(); ?>"
                           title="<?php echo $image->getName();?>">
                            <img src="<?php echo $image->getThumbPath(); ?>" alt="<?php echo $image->getAlternativeText();?>"/>
                        </a>
                        <div class="caption">
                            <div class="download">
                                <a class="zoom" href="<?php echo $image->getPath();?>">Download
                                    Original</a>
                            </div>
                            <div class="image-title"><?php echo $image->getName()?></div>
                            <div class="image-desc"><?php echo $image->getDescription();?></div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
    <div style="clear: both;"></div>
</div>


<!-- End of gallery container -->