<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',
    array(
        'name'=>Yii::t('website','breadcrumb.staff'),
        'url'=>Yii::app()->createUrl('home/staff'),
    ),
    '/',
    $staffModel->getFullName(),
);
?>

<div class="my-img">
    <div class="my-details">
        <img class="fx" data-animate="fadeInLeft" alt="" src="<?php echo $staffModel->profileImg->getPath() ?>"
             alt="<?php echo $staffModel->getFullName(); ?>">
        <h4 class="bold main-color my-name fx"
            data-animate="slideInDown"><?php echo $staffModel->getTitle() . " " . $staffModel->getFullName(); ?></h4>
        <ul class="list alt list-bookmark cell-4">
            <li class="fx" data-animate="slideInDown"><strong><?php echo Yii::t('website', 'staff.dob'); ?>
                    : </strong><?php echo $staffModel->getDob(); ?></li>
            <li class="fx" data-animate="slideInDown" data-animation-delay="100">
                <strong><?php echo Yii::t('website', 'staff.address'); ?>
                    : </strong><?php echo $staffModel->getAddress(); ?></li>
            <li class="fx" data-animate="slideInDown" data-animation-delay="100">
                <strong><?php echo Yii::t('website', 'staff.job'); ?>: </strong><?php echo $staffModel->getAddress(); ?>
            </li>

        </ul>
        <ul class="list alt list-bookmark cell-4">
            <li class="fx" data-animate="slideInDown" data-animation-delay="300">
                <strong><?php echo Yii::t('website', 'staff.contact'); ?>
                    : </strong><?php echo $staffModel->getContact(); ?></li>
            <li class="fx" data-animate="slideInDown" data-animation-delay="400">
                <strong><?php echo Yii::t('website', 'staff.m.status'); ?>
                    : </strong><?php echo $staffModel->getMStatus(); ?></li>
            <li class="fx" data-animate="slideInDown" data-animation-delay="500">
                <strong><?php echo Yii::t('website', 'staff.nationality'); ?>
                    : </strong><?php echo $staffModel->getNationality(); ?></li>
        </ul>
    </div>
</div>

<hr class="hr-style4">
<div class="row padd-top-20">
    <aside class="cell-3 right-sidebar">
        <?php if (!empty($staffModel->cvSections)): ?>

            <li class="widget blog-cat-w fx" data-animate="fadeInLeft">
                <h3 class="widget-head"><?php echo Yii::t('website', 'staff.cv.sections'); ?></h3>
                <div class="widget-content">
                    <ul class="list list-ok alt">
                        <?php foreach ($staffModel->cvSections as $cvSection): ?>
                            <li><a href="blog-single.html"><?php echo $cvSection->getTitle();?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </li>

        <?php endif; ?>
    </aside>
    <div class="cell-9 fx" data-animate="fadeInLeft">
        <?php if (!empty($staffModel->cvSections)): ?>
            <?php foreach ($staffModel->cvSections as $cvSection): ?>
                <div class="row" style="overflow:auto;">
                    <div class="cell-12">
                        <h3 class="block-head"><?php echo $cvSection->getTitle(); ?></h3>
                        <?php echo $cvSection->getBody(); ?>
                    </div>
                </div>
            <?php endforeach ?>
        <?php endif; ?>
    </div>
</div>
<div class="clearfix"></div>