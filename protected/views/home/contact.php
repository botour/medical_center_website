<?php
/* @var $this HomeController */
/* @var $contactModel Contact */
/* @var $form CActiveForm */
/* @var $contactDetails ContactDetailsItem[] */
?>

<?php
$this->breadcrumbs = array(
    array(
        'name' => Yii::t('website', 'breadcrumb.home'),
        'url' => Yii::app()->createUrl('home/home'),
    ),
    '/',
    Yii::t('website', 'breadcrumb.contact'),
);
?>

<div class="row">

    <?php if ($saved): ?>
        <div class="cell-12">
            <div
                class="box warning-box fx animated fadeInRight"><?php echo Yii::t('website', 'your.contact.has.been.successfully.sent'); ?></div>
        </div>
    <?php endif; ?>
    <div class="cell-7 contact-form fx" data-animate="fadeInLeft" id="contact">
        <h3 class="block-head"><?php echo Yii::t('website', 'get.in.touch'); ?></h3>
        <mark id="message"></mark>

        <?php echo CHtml::beginForm(array('home/contact', 'POST', array(
            'id' => 'cform',
            'autocomplete' => 'on',
            'class' => 'form-signin cform',
        ))) ?>
        <div class="form-input">
            <label><?php echo $contactModel->getAttributeLabel('full_name'); ?><span class="red">*</span></label>
            <input type="text" required="required" name="Contact[full_name]" id="c1">
        </div>
        <div class="form-input">
            <label><?php echo $contactModel->getAttributeLabel('full_name_ar'); ?><span class="red">*</span></label>
            <input type="text" required="required" name="Contact[full_name_ar]" id="c2">
        </div>
        <div class="form-input">
            <label><?php echo $contactModel->getAttributeLabel('email'); ?><span class="red">*</span></label>
            <input name="Contact[email]" type="email" id="email" required="required">
        </div>
        <div class="form-input">
            <label><?php echo $contactModel->getAttributeLabel('contact_type'); ?><span class="red">*</span></label>
            <?php
            echo CHtml::activeDropDownList($contactModel, 'contact_type_id', Yii::app()->getLanguage() == 'ar' ? CHtml::listData(ContactType::model()->findAll(), 'id', 'name_ar') : CHtml::listData(ContactType::model()->findAll(), 'id', 'name'), array('required' => 'required', 'prompt' => Yii::t('website', 'please.select.type')));
            ?>
        </div>
        <div class="form-input">
            <label><?php echo $contactModel->getAttributeLabel('body'); ?><span class="red">*</span></label>
            <textarea required="required" name="Contact[body]" cols="40" rows="7" id="messageTxt"
                      spellcheck="true"></textarea>
        </div>
        <div class="form-input">
            <input type="submit" class="btn btn-large main-bg"
                   value="<?php echo Yii::t('website', 'contact.submit'); ?>">&nbsp;&nbsp;<input type="reset"
                                                                                                 class="btn btn-large"
                                                                                                 value="Reset"
                                                                                                 id="reset">
        </div>
        </form>
    </div>
    <div class="cell-5 contact-detalis">
        <h3 class="block-head"><?php echo Yii::t('website', 'contact.details'); ?></h3>

        <p class="fx"
           data-animate="fadeInRight"><?php echo $this->customContents[CustomContent::CONTACT_INFO_KEY]; ?></p>
        <hr class="hr-style4">
        <div class="clearfix"></div>
        <?php if (!empty($contactDetails)): ?>
            <div class="padding-vertical">
                <?php $counter = 0; ?>
                <?php foreach ($contactDetails as $contactDetail): ?>
                    <?php $counter += 1; ?>
                    <div class="cell-5 fx" data-animate="fadeInRight">
                        <h4 class="main-color bold"><?php echo $contactDetail->getBranch(); ?></h4>
                        <h5><?php echo $contactDetail->getAttributeLabel('address');?>:</h5>

                        <p><?php echo $contactDetail->getAddress();?></p>
                        <?php if(strlen($contactDetail->tel_1)>0):?>
                            <h5><?php echo $contactDetail->getAttributeLabel('tel_1');?>:</h5>
                            <p><?php echo $contactDetail->tel_1;?></p>
                        <?php endif;?>
                        <?php if(strlen($contactDetail->tel_2)>0):?>
                            <h5><?php echo $contactDetail->getAttributeLabel('tel_2');?>:</h5>
                            <p><?php echo $contactDetail->tel_2;?></p>
                        <?php endif;?>
                        <?php if(strlen($contactDetail->mobile)>0):?>
                            <h5><?php echo $contactDetail->getAttributeLabel('mobile');?>:</h5>
                            <p><?php echo $contactDetail->mobile;?></p>
                        <?php endif;?>
                        <?php if(strlen($contactDetail->mobile_2)>0):?>
                            <h5><?php echo $contactDetail->getAttributeLabel('mobile_2');?>:</h5>
                            <p><?php echo $contactDetail->mobile_2;?></p>
                        <?php endif;?>
                        <?php if(strlen($contactDetail->email)>0):?>
                            <h5><?php echo $contactDetail->getAttributeLabel('email');?>:</h5>
                            <p><?php echo $contactDetail->email;?></p>
                        <?php endif;?>
                        <?php if(strlen($contactDetail->fax)>0):?>
                            <h5><?php echo $contactDetail->getAttributeLabel('fax');?>:</h5>
                            <p><?php echo $contactDetail->fax;?></p>
                        <?php endif;?>

                    </div>
                    <div class="cell-2"><br></div>
                    <?php if ($counter % 2 == 0): ?>
                    <div class="clearfix"></div>
                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        <?php endif; ?>
    </div>
</div>

