<?php
$this->breadcrumbs = array(
    array(
        'name' => Yii::t('website', 'breadcrumb.home'),
        'url' => Yii::app()->createUrl('home/home'),
    ),
    '/',
    Yii::t('website','search.result'),
);
?>