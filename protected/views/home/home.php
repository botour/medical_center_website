<?php
/* @var $this HomeController */
?>
<div class="container">
    <!-- Flex slider start -->
    <!-- Flex slider start -->
    <?php if (!empty($imageSlides)): ?>
        <div class="flexslider">

            <ul class="slides">
                <?php foreach ($imageSlides as $slide): ?>
                    <?php $image = Image::model()->findByPk($slide->image_id); ?>
                    <li>
                        <div class="absolute-slide">
                            <h3><?php echo $slide->getTitle(); ?></h3>

                            <p><?php echo $slide->getDescription(); ?></p>
                            <?php if (strlen($slide->link)): ?>
                                <a style="color: #FFF;"
                                   href="<?php echo $slide->link ?>"><?php echo Yii::t('website', 'read.more'); ?></a>
                            <?php endif; ?>
                        </div>
                        <img src="<?php echo $image->getPath(); ?>"/>

                    </li>
                <?php endforeach; ?>
            </ul>

        </div>
    <?php endif; ?>
    <!-- Flex slider end -->
</div>
<!-- END: Show the Slider -->
<!-- Insurence Cards -->

<!-- Insurence Cards End-->
<!-- About us and Features container start -->
<div class="sectionWrapper">
    <div class="container">
        <div class="row">
            <div class="cell-3">
                <ul class="sidebar_widgets">


                    <li class="widget blog-cat-w fx" data-animate="fadeInRight">
                        <h3 class="widget-head"><?php echo Yii::t('website', 'our.services'); ?></h3>

                        <div class="widget-content">
                            <?php if (!empty($this->services)): ?>
                                <ul class="list  alt">
                                    <?php foreach ($this->services as $service): ?>

                                        <li>
                                            <a href="<?php echo Yii::app()->createUrl('home/service', array('id' => $service->id)); ?>"><?php echo $service->getName(); ?></a>
                                        </li>

                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>

                        </div>
                    </li>
                    <li class="widget blog-cat-w fx" data-animate="fadeInRight">
                        <h3 class="widget-head"><?php echo Yii::t('website', 'our.doctors'); ?></h3>

                        <div class="widget-content">
                            <?php if (!empty($this->doctors)): ?>
                                <ul class="list  alt">
                                    <?php foreach ($this->doctors as $doctor): ?>
                                        <li>
                                            <a href="<?php echo Yii::app()->createUrl('home/staffm', array('sId' => $doctor->id)); ?>"><?php echo $doctor->getTitle() . " " . $doctor->getFullName(); ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </li>

                </ul>
            </div>


            <div class="cell-6">
                <!-- testimonials start -->
                <div class="fx" data-animate="fadeInLeft">
                    <h3 class="block-head"><?php echo Yii::t('website', 'welcome.title'); ?></h3>

                    <div class="testimonials-4">

                        <!-- testimonials item start -->
                        <div>
                            <div class="testimonials-bg">
                                <img style="width: 200px;height: 100px;" alt=""
                                     src="<?php echo Yii::app()->baseUrl; ?>/images/fawaz.jpg" class="testimonials-img">
                                <span><?php echo $this->customContents[CustomContent::WELCOME_MESSAGE_KEY]; ?></span>

                            </div>
                            <div class="testimonials-name">
                                <strong><?php echo Yii::t('website', 'dr.fawaz'); ?></strong><?php echo Yii::t('website', 'dr.fawaz.title'); ?>
                            </div>
                        </div>
                        <!-- testimonials item end -->

                    </div>
                    <h4 class="main-color"><?php echo Yii::t('website', 'our.message'); ?></h4>

                    <p><?php echo $this->customContents[CustomContent::OUR_VISION_KEY]; ?></p>

                </div>

                <!-- testimonials end -->

                <!-- News Section Start -->
                <div class="fx" data-animate="fadeInLeft">
                    <h3 class="block-head"><?php echo Yii::t('website', 'welcome.news'); ?></h3>


                    <?php if (empty($news)): ?>
                        <p>No News Available</p>
                    <?php endif; ?>
                    <?php if (!empty($news)): ?>
                        <div class="blog-posts">
                            <div class="post-item fx" data-animate="fadeInLeft">
                                <article class="post-content">
                                    <?php foreach ($news as $newsContent): ?>
                                        <div class="post-info-container">
                                            <h4 class="main-color"><?php echo $newsContent->getTitle(); ?></h4>
                                        </div>
                                        <?php foreach ($newsContent->blocks as $block): ?>
                                            <?php $this->renderPartial('blockItem', array(
                                                'block' => $block,
                                            )); ?>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                            </div>
                            </article>

                        </div><!-- .post-item end -->

                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- about us left block -->
            <div class="cell-3">
                <div class="box warning-box fx" data-animate="fadeInRight">

                    <h3><?php echo Yii::t('website', 'adds.goes.here'); ?></h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet purus quis
                        metus
                        imperdiet fermentum. Suspendisse hendrerit id lacus id lobortis. Vestibulum quam elit,
                        dapibus
                        ac augue ut, porttitor viverra dui.</p>
                </div>

            </div>
            <!-- News Section End -->


        </div>


    </div>


</div>
<!-- About us and Features container end -->
<!-- our clients block start -->
<div class="sectionWrapper">
    <div class="container">
        <h3 class="block-head"><?php echo Yii::t('website', 'welcome.insurance'); ?></h3>

        <div class="clients">
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_1.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_2.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_3.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_4.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_5.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_6.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_7.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_8.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_9.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_10.jpg"></a>
            </div>
            <div>
                <a class="white-bg" href="#"><img alt="" src="<?php echo Yii::app()->baseUrl; ?>/img/i_11.jpg"></a>
            </div>


        </div>
    </div>
</div>
<!-- our clients block end -->