<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-reorder"></i>
    </div>
    <div class="panel-body">
        <div class="tabbable panel-tabs">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#slider_management">
                        <?php echo Yii::t('home', 'home.index.tab.sliders') ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#custom_content_management">
                        <?php echo Yii::t('home', 'home.index.tab.custom.content') ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="slider_management" class="tab-pane active">
                    <?php echo CHtml::link('Add Slider Image', array('home/addSliderImage'), array(
                        'class' => 'btn btn-bricky',
                    )); ?>
                    <hr/>
                    <?php $this->renderPartial('partial/_slides_list', array(
                        'slidesSearchModel' => $slidesSearchModel,
                    )); ?>
                </div>
                <div id="custom_content_management" class="tab-pane">
                    <table class="table-striped table-bordered table">
                        <tr>
                            <td><?php echo Chtml::link('Welcome Message Text Manage', array('home/mWelcomeMessage'), array(
                                    'class' => 'btn btn-success',
                                )) ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo Chtml::link('Our Vision Text Manage', array('home/mOurVision'), array(
                                    'class' => 'btn btn-success',
                                )) ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo Chtml::link('Who We are Text Manage', array('home/mWhoWeAre'), array(
                                    'class' => 'btn btn-success',
                                )) ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo Chtml::link('Contact Details Text Manage', array('home/mContactDetails'), array(
                                    'class' => 'btn btn-success',
                                )) ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>