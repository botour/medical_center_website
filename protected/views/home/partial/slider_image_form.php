<?php
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'notifyOnFormSubmitModal',
    'title' => 'Note',
    'width' => '40',
));
if($model->isNewRecord){
Yii::app()->clientScript->registerScript("slider-form", "
var _URL = window.URL || window.webkitURL;
$(function(){
    $(\"#notifyOnFormSubmitModal\").on(\"hidden.bs.modal\",function(){
        $(\".fileupload\").fileupload(\"reset\");
    });
    $(\"#slider-image-controll\").change(function (e) {
        var image, file;
        if ((file = this.files[0])) {
            image = new Image();
            image.onload = function () {
                if(this.width!=1138 || this.height!=520){
                    $(\"#notifyOnFormSubmitModal .modal-body\").html(\"<p>The image width or height is not appropriate</p><p>Select an image in the size of: 1138X520 for maximum fit</p>\");
                    $(\"#notifyOnFormSubmitModal\").modal(\"show\");
                }
            };
            image.src = _URL.createObjectURL(file);
        }
    });
});
", CClientScript::POS_END);

}
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'slider_image_form',
        'focus' => array($model, 'type'),
        'htmlOptions' => array(
            'role' => 'form',
            'enctype' => 'multipart/form-data',
            'id' => 'slider-image-form',
            'class' => 'form-horizontal',
        ),
    )); ?>
    <div class="row">
        <?php if($model->isNewRecord):?>
        <div class="col-md-6">
            <h4>Image Info</h4>
            <hr/>
            <div class="form-group<?php echo $imageModel->hasErrors('name') ? ' has-error' : '';
            echo !$imageModel->hasErrors('name') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $imageModel->getAttributeLabel('name'); ?>
                    <?php if ($imageModel->isAttributeRequired('name')): ?>
                        <?php if (!$imageModel->hasErrors('name') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($imageModel->hasErrors('name') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($imageModel, 'name', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($imageModel->hasErrors('name')): ?>
                        <span class="help-block"><?php echo $imageModel->getError('name'); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group<?php echo $imageModel->hasErrors('name_ar') ? ' has-error' : '';
            echo !$imageModel->hasErrors('name_ar') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $imageModel->getAttributeLabel('name_ar'); ?>
                    <?php if ($imageModel->isAttributeRequired('name_ar')): ?>
                        <?php if (!$imageModel->hasErrors('name_ar') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($imageModel->hasErrors('name_ar') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($imageModel, 'name_ar', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($imageModel->hasErrors('name_ar')): ?>
                        <span class="help-block"><?php echo $imageModel->getError('name_ar'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group<?php echo $imageModel->hasErrors('image') ? ' has-error' : ''; ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $imageModel->getAttributeLabel('image'); ?>
                    <?php if ($imageModel->isAttributeRequired('image')): ?>
                        <?php if (!$imageModel->hasErrors('image') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($imageModel->hasErrors('image') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                                src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                        <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo $form->fileField($imageModel, 'image', array(
                                                                'id' => 'slider-image-controll',
                                                            )); ?>
														</span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                        <?php if ($imageModel->hasErrors('image')): ?>
                            <span class="help-block"><?php echo $imageModel->getError('image'); ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="form-group<?php echo $imageModel->hasErrors('alternative_text') ? ' has-error' : '';
            echo !$imageModel->hasErrors('alternative_text') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $imageModel->getAttributeLabel('alternative_text'); ?>
                    <?php if ($imageModel->isAttributeRequired('alternative_text')): ?>
                        <?php if (!$imageModel->hasErrors('alternative_text') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($imageModel->hasErrors('alternative_text') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($imageModel, 'alternative_text', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($imageModel->hasErrors('alternative_text')): ?>
                        <span class="help-block"><?php echo $imageModel->getError('alternative_text'); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group<?php echo $imageModel->hasErrors('alternative_text_ar') ? ' has-error' : '';
            echo !$imageModel->hasErrors('alternative_text_ar') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $imageModel->getAttributeLabel('alternative_text_ar'); ?>
                    <?php if ($imageModel->isAttributeRequired('alternative_text_ar')): ?>
                        <?php if (!$imageModel->hasErrors('alternative_text_ar') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($imageModel->hasErrors('alternative_text_ar') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($imageModel, 'alternative_text_ar', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($imageModel->hasErrors('alternative_text_ar')): ?>
                        <span class="help-block"><?php echo $imageModel->getError('alternative_text_ar'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif;?>

        <!-- Right Hand Side -->

        <div class="col-md-6">
            <h4>Slider Info</h4>
            <hr/>
            <div class="form-group<?php echo $model->hasErrors('title') ? ' has-error' : '';
            echo !$model->hasErrors('title') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('title'); ?>
                    <?php if ($model->isAttributeRequired('title')): ?>
                        <?php if (!$model->hasErrors('title') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('title') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($model, 'title', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($model->hasErrors('title')): ?>
                        <span class="help-block"><?php echo $model->getError('title'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group<?php echo $model->hasErrors('title_ar') ? ' has-error' : '';
            echo !$model->hasErrors('title_ar') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('title_ar'); ?>
                    <?php if ($model->isAttributeRequired('title_ar')): ?>
                        <?php if (!$model->hasErrors('title_ar') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('title_ar') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($model, 'title_ar', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($model->hasErrors('title_ar')): ?>
                        <span class="help-block"><?php echo $model->getError('title_ar'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group<?php echo $model->hasErrors('description') ? ' has-error' : '';
            echo !$model->hasErrors('description') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('description'); ?>
                    <?php if ($model->isAttributeRequired('description')): ?>
                        <?php if (!$model->hasErrors('description') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('description') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textArea($model, 'description', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($model->hasErrors('description')): ?>
                        <span class="help-block"><?php echo $model->getError('description'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group<?php echo $model->hasErrors('description_ar') ? ' has-error' : '';
            echo !$model->hasErrors('description_ar') && isset($_POST['SliderImage']) ? ' has-success' : '' ?>">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('description_ar'); ?>
                    <?php if ($model->isAttributeRequired('description_ar')): ?>
                        <?php if (!$model->hasErrors('description_ar') && isset($_POST['SliderImage'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('description_ar') || !isset($_POST['SliderImage'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-9">
                    <?php echo $form->textArea($model, 'description_ar', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($model->hasErrors('description_ar')): ?>
                        <span class="help-block"><?php echo $model->getError('description_ar'); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('link'); ?>

                </label>

                <div class="col-sm-9">
                    <?php echo $form->textField($model, 'link', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($model->hasErrors('link')): ?>
                        <span class="help-block"><?php echo $model->getError('name'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php if(!$model->isNewRecord):?>
        <div class="col-md-6">

        </div>
    <?php endif;?>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div>
                <span class="symbol required"></span>Required Fields
                <hr>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <p style="font-size: 15px;">
                <strong style="color: #FE0000;">Note: </strong>Select an image in the size of (1920X520) for maximum fit
            </p>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?php echo CHtml::submitButton(Yii::t('home', 'controller.add.slider.action'), array(
                    'class' => 'btn btn-yellow btn-block',
                )); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>