<?php
Yii::app()->clientScript->registerScript('content-script', '
$(function(){
$(".Tabled table").addClass("table");
$(".Tabled table").addClass("table-striped");
$(".Tabled table").addClass("table-bordered");
$(".pagination").parent("div").removeClass("pager");
$(".tooltip-link").tooltip();
});
', CClientScript::POS_END);
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'images-slides-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty' => true,
    'summaryText' => 'Content Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
        $(".Tabled table").addClass("table");
        $(".Tabled table").addClass("table-striped");
        $(".Tabled table").addClass("table-bordered");
        $(".pagination").parent("div").removeClass("pager");
        $(".tooltip-link").tooltip();
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $slidesSearchModel->search(),
    'showTableOnEmpty' => false,
    'emptyText' => 'No Slides to manage',
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'htmlOptions' => array(
            'class' => 'pagination pagination-sm',
        ),
    ),
//'filter' => $userModel,
    'columns' => array(
        'title',
        'title_ar',

        array(
            'type'=>'raw',
            'name'=>'image_id',
            'value'=>'CHtml::image(Yii::app()->baseUrl."/".CommonFunctions::IMAGES_PATH . CommonFunctions::SLIDER_IMAGES_PATH . CommonFunctions::SLIDER_IMAGES_THUMBS_PATH .$data->getImage()->url)'
        ),
        array(
            'name'=>'active',
            'type'=>'raw',
            'value'=>'CommonFunctions::getLabel($data->active,CommonFunctions::SLIDER_STATUS)',
        ),
        array(
            'header' => '',
            'value' => array($this, 'renderSliderImagesOptions'),
        ),
    ),
));
?>