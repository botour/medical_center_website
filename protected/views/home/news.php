<?php if(empty($news)):?>
    <p>No News Available</p>
<?php endif;?>
<?php if(!empty($news)):?>
<div class="blog-posts">
    <div class="post-item fx" data-animate="fadeInLeft">
        <article class="post-content">
    <?php foreach($news as $newsContent):?>
        <div class="post-info-container">
            <h1 class="main-color block-head"><?php echo $newsContent->title;?></h1>
        </div>
        <?php foreach($newsContent->blocks as $block):?>
            <?php $this->renderPartial('blockItem',array(
                'block'=>$block,
            ));?>
        <?php endforeach;?>

    <?php endforeach;?>
</div>

    </article>

</div><!-- .post-item end -->

<?php endif;?>