<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',
    Yii::t('website','breadcrumb.gallery'),
);

?>

<div class="sectionWrapper">
    <div class="container">
        <div class="portfolio-filterable">
            <div class="row">
                <?php if (empty($galleries)): ?>
                    <div class="cell-12">
                        <div
                            class="box warning-box fx animated fadeInRight"><?php echo Yii::t('website', 'no.galleries.available.for.now'); ?></div>
                    </div>
                <?php endif; ?>
                <?php if (!empty($galleries)): ?>
                    <?php foreach ($galleries as $gallery): ?>
                        <?php $galleryImage = $gallery->images[0]; ?>
                        <div class="cell-3 design" data-category="design">
                            <div class="portfolio-item">
                                <div class="img-holder">
                                    <div class="img-over">
                                        <a href="<?php echo Yii::app()->createUrl('home/gallery',array('id'=>$gallery->id))?>" class="fx link"><b class="fa fa-link"></b></a>
                                        <a href="<?php echo $galleryImage->getPath()?>" class="fx zoom" data-gal="prettyPhoto[pp_gal]"
                                           title="<?php echo $gallery->getTitle();?>"><b class="fa fa-search-plus"></b></a>
                                    </div>
                                    <img alt="" src="<?php echo $galleryImage->getPath()?>">
                                </div>
                                <div class="name-holder">
                                    <a href="<?php echo Yii::app()->createUrl('home/gallery',array('id'=>$gallery->id))?>" class="project-name"><?php echo $gallery->getTitle();?></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>


            </div>
        </div>
    </div>
</div>


