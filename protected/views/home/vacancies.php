<?php
$this->breadcrumbs = array(
    array(
        'name'=>Yii::t('website','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/home'),
    ),
    '/',
    Yii::t('website','breadcrumb.vacancies'),
);
?>
<div class="sectionWrapper img-pattern">
    <div class="container">
        <div class="row">
            <div class="cell-12 fx" data-animate="fadeInLeft">
                <h3 class="block-head"><?php echo Yii::t('website', 'job.opportunity'); ?></h3>

                <?php if (empty($vacancies)): ?>
                    <div
                        class="box warning-box fx animated fadeInRight"><?php echo Yii::t('website', 'no.job.opportunities.available.for.now'); ?></div>
                <?php endif; ?>
                <?php if (!empty($vacancies)): ?>
                    <ul id="accordion" class="accordion">
                        <?php foreach ($vacancies as $vacancy): ?>
                            <li>
                                <h3><a href="#"><span><?php echo $vacancy->getTitle(); ?></span></a></h3>

                                <div class="accordion-panel">
                                    <table>
                                        <tr>
                                            <td class="width150"><?php echo Yii::t('website', 'vacancy.label.position'); ?></td>
                                            <td><?php echo $vacancy->getPosition(); ?></td>
                                        </tr>
                                        <tr>
                                            <td class="width150"><?php echo Yii::t('website', 'vacancy.label.description'); ?></td>
                                            <td><?php echo $vacancy->getDescription(); ?></td>
                                        </tr>
                                        <?php if (strlen($vacancy->salary) > 0): ?>
                                            <tr>
                                                <td class="width150"><?php echo Yii::t('website', 'vacancy.label.salary'); ?></td>
                                                <td><a href="#"><?php echo $vacancy->getSalary(); ?></a></td>
                                            </tr>
                                        <?php endif; ?>

                                        <tr>
                                            <td class="width150"><?php echo Yii::t('website', 'vacancy.label.contact'); ?></td>
                                            <td><a href="#"><?php echo $vacancy->contact_email; ?></a></td>
                                        </tr>

                                    </table>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </div>

        </div>
    </div>
</div>