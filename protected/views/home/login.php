<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
?>

<div class="main-login col-sm-4 col-sm-offset-4">
	<div class="logo">Al-Hayat Medical Center<br/><small>Admin Dashboard</small>
	</div>
	<!-- start: LOGIN BOX -->
	<div class="box-login">
		<h3><?php echo Yii::t('home','home.login.header');?></h3>
		<p>
			<?php echo Yii::t('home','home.login.sub.header');?>
		</p>
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'project-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'focus' => array($model, 'email'),
			'clientOptions' => array(
				'successCssClass' => 'has-success',
				'errorCssClass' => 'has-error',
				'validatingErrorMessage' => '',
				'inputContainer' => '.form-group',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
			),
			'htmlOptions' => array(
				'role' => 'form',
				'class' => 'form-login',
			),
		)); ?>
			<div class="errorHandler alert alert-danger no-display">
				<i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
			</div>
			<fieldset>
				<div class="form-group has-feedback">
							<span class="input-icon">
								<?php echo $form->textField($model,'login_name',array(
									'class'=>'form-control',
									'placeholder'=>Yii::t('home','home.login.label.login.name'),
								))?>

								<i class="fa fa-user"></i> </span>

				</div>
				<div class="form-group form-actions has-feedback">
							<span class="input-icon">
								<?php echo $form->passwordField($model,'password',array(
									'class'=>'form-control password',
									'placeholder'=>Yii::t('home','home.login.label.password'),
								))?>
								<i class="fa fa-lock"></i>

				</div>
				<div class="form-actions">
					<label for="remember" class="checkbox-inline">

						<?php echo $form->checkBox($model,'rememberMe',array(
							'class'=>'grey remember',
							'id'=>'remember',
						))?>
						<?php echo Yii::t('home','home.login.label.remember.me');?>
					</label>
					<button type="submit" class="btn btn-bricky pull-right">
						<?php echo Yii::t('home','home.login.action.login');?> <i class="fa fa-arrow-circle-right"></i>
					</button>

				</div>

			</fieldset>
		<?php $this->endWidget();?>
	</div>
	<!-- end: LOGIN BOX -->

	<!-- start: COPYRIGHT -->
	<div class="copyright">
		2014 &copy; All rights reserved.
	</div>
	<!-- end: COPYRIGHT -->
</div>

