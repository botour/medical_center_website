<?php
$this->breadcrumbs=array(
    array(
        'name'=>Yii::t('home','breadcrumb.home'),
        'url'=>Yii::app()->createUrl('home/index'),
    ),
    Yii::t('home','breadcrumb.add.slider.image'),
);




//$this->jsCustomFiles[] = 'home-logic.js';

$this->cssAssets[] = 'bootstrap-fileupload.min.css';
$this->cssAssets[] = 'DT_bootstrap.css';

$this->jsAssets[] = 'bootstrap-fileupload.min.js';
$this->jsAssets[] = 'jquery.validate.min.js';

$this->jsAssets[] = 'jquery.datatables.min.js';
$this->jsAssets[] = 'DT_bootstrap.js';

$this->renderPartial('partial/slider_image_form',array(
    'model'=>$model,
    'imageModel'=>$imageModel,
));

