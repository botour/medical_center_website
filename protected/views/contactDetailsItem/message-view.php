<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/27/15
 * Time: 10:06 PM
 */
$this->breadcrumbs = array(
    array(
        'name' => 'Messages',
        'url' => Yii::app()->createUrl('contactDetailsItem/messages'),
    ),
    'Message Preview',
);
?>

<div class="row">
    <div class="col-md-9">
        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th colspan="3">Message Details</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $model->getAttributeLabel('full_name');?></td>
                <td><?php echo $model->full_name;?></td>
            </tr>
            <tr>
                <td><?php echo $model->getAttributeLabel('full_name_ar');?></td>
                <td><?php echo $model->full_name_ar;?></td>

            </tr>
            <tr>
                <td><?php echo $model->getAttributeLabel('email');?></td>
                <td><?php echo $model->email;?></td>

            </tr>
            <tr>
                <td><?php echo $model->getAttributeLabel('contact_type_id');?></td>
                <td><?php echo $model->contactType->name;?></td>

            </tr>
            <tr>
                <td><?php echo $model->getAttributeLabel('body');?></td>
                <td><?php echo $model->body;?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
