<?php

/* @var $this ContactDetailsItemController */
/* @var $messageModel Contact */

$this->breadcrumbs = array(
    'Contact Details Items',
);
Yii::app()->clientScript->registerScript('contact-script', '
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
', CClientScript::POS_END);


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'messages-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>"center")',
    'showTableOnEmpty' => true,
    'summaryText' => 'Content Count: {count}',
    'summaryCssClass' => 'complete-summery',

    'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $messageModel->search(),
    'showTableOnEmpty' => false,
    'emptyText' => 'No Messages to manage',
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'htmlOptions' => array(
            'class' => 'pagination pagination-sm',
        ),
    ),
    //'filter' => $userModel,
    'columns' => array(
        'full_name',
        'full_name_ar',
        array(
            'name'=>'contact_type_id',
            'value'=>'$data->contactType->getName()',
        ),
        array(
            'header' => '',
            'value' => array($this, 'renderMessageOptions'),
        ),
    ),
));
?>


