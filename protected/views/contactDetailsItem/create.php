<?php
/* @var $this ContactDetailsItemController */
/* @var $model ContactDetailsItem */

$this->breadcrumbs = array(
	array(
		'name' => 'Contact Details Management',
		'url' => Yii::app()->createUrl('contactDetailsItem/index'),
	),
	'Add Contact Details',
);
?>

<?php
$this->renderPartial(
	'_form',array(
		'model'=>$model,
	)
);
?>
