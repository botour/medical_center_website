<?php
/* @var $this ContactDetailsItemController */
/* @var $model ContactDetailsItem */

$this->breadcrumbs=array(
	'Contact Details Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ContactDetailsItem', 'url'=>array('index')),
	array('label'=>'Create ContactDetailsItem', 'url'=>array('create')),
	array('label'=>'Update ContactDetailsItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ContactDetailsItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ContactDetailsItem', 'url'=>array('admin')),
);
?>

<h1>View ContactDetailsItem #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'branch',
		'branch_ar',
		'email',
		'tel_1',
		'tel_2',
		'address',
		'address_ar',
		'fax',
		'mobile',
		'mobile_2',
	),
)); ?>
