<?php
/* @var $this ContactDetailsItemController */
/* @var $data ContactDetailsItem */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch')); ?>:</b>
	<?php echo CHtml::encode($data->branch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_ar')); ?>:</b>
	<?php echo CHtml::encode($data->branch_ar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_1')); ?>:</b>
	<?php echo CHtml::encode($data->tel_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_2')); ?>:</b>
	<?php echo CHtml::encode($data->tel_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('address_ar')); ?>:</b>
	<?php echo CHtml::encode($data->address_ar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fax')); ?>:</b>
	<?php echo CHtml::encode($data->fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile')); ?>:</b>
	<?php echo CHtml::encode($data->mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_2')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_2); ?>
	<br />

	*/ ?>

</div>