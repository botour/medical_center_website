<?php
/* @var $this ContactDetailsItemController */
/* @var $model ContactDetailsItem */
/* @var $form CActiveForm */
?>


<div class="form row">
	<div class="col-md-12">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'department-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'focus' => array($model, 'branch'),
			'clientOptions' => array(
				'successCssClass' => 'has-success',
				'errorCssClass' => 'has-error',
				'validatingErrorMessage' => '',
				'inputContainer' => '.form-group',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
			),
			'htmlOptions' => array(
				'role' => 'form',
				'class' => 'form-horizontal',
				'enctype' => 'multipart/form-data',
			),
		)); ?>
		<?php if($model->hasErrors()):?>
			<div class="errorHandler alert alert-danger no-display" style="display: block;">
				<?php echo $form->errorSummary($model);?>

			</div>
		<?php endif;?>



		<div class="form-group<?php echo $model->hasErrors('branch')?' has-error':'';echo !$model->hasErrors('branch')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('branch'); ?>
				<?php if ($model->isAttributeRequired('branch')): ?>
					<?php if(!$model->hasErrors('branch')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('branch') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'branch',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('branch')):?>
					<span class="help-block"><?php echo $model->getError('branch');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('branch_ar')?' has-error':'';echo !$model->hasErrors('branch_ar')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('branch_ar'); ?>
				<?php if ($model->isAttributeRequired('branch_ar')): ?>
					<?php if(!$model->hasErrors('branch_ar')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('branch_ar') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'branch_ar',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('branch_ar')):?>
					<span class="help-block"><?php echo $model->getError('branch_ar');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('address')?' has-error':'';echo !$model->hasErrors('address')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('address'); ?>
				<?php if ($model->isAttributeRequired('address')): ?>
					<?php if(!$model->hasErrors('branch')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('address') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'address',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('address')):?>
					<span class="help-block"><?php echo $model->getError('address');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('address_ar')?' has-error':'';echo !$model->hasErrors('address_ar')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('address_ar'); ?>
				<?php if ($model->isAttributeRequired('address_ar')): ?>
					<?php if(!$model->hasErrors('address_ar')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('address_ar') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'address_ar',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('address_ar')):?>
					<span class="help-block"><?php echo $model->getError('address_ar');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('tel_1')?' has-error':'';echo !$model->hasErrors('tel_1')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('tel_1'); ?>
				<?php if ($model->isAttributeRequired('tel_1')): ?>
					<?php if(!$model->hasErrors('tel_1')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('tel_1') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'tel_1',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('tel_1')):?>
					<span class="help-block"><?php echo $model->getError('tel_1');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('tel_2')?' has-error':'';echo !$model->hasErrors('tel_2')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('tel_2'); ?>
				<?php if ($model->isAttributeRequired('tel_2')): ?>
					<?php if(!$model->hasErrors('tel_2')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('tel_2') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'tel_2',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('tel_2')):?>
					<span class="help-block"><?php echo $model->getError('tel_2');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('mobile')?' has-error':'';echo !$model->hasErrors('mobile')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('mobile'); ?>
				<?php if ($model->isAttributeRequired('mobile')): ?>
					<?php if(!$model->hasErrors('mobile')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('mobile') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'mobile',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('mobile')):?>
					<span class="help-block"><?php echo $model->getError('mobile');?></span>
				<?php endif;?>
			</div>
		</div>
		<div class="form-group<?php echo $model->hasErrors('mobile_2')?' has-error':'';echo !$model->hasErrors('mobile_2')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('mobile_2'); ?>
				<?php if ($model->isAttributeRequired('mobile_2')): ?>
					<?php if(!$model->hasErrors('mobile_2')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('mobile_2') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'mobile_2',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('mobile_2')):?>
					<span class="help-block"><?php echo $model->getError('mobile_2');?></span>
				<?php endif;?>
			</div>
		</div>

		<div class="form-group<?php echo $model->hasErrors('fax')?' has-error':'';echo !$model->hasErrors('fax')&&isset($_POST['ContactDetailsItem'])?' has-success':''?>">
			<label class="col-sm-2 control-label" for="form-field-2">
				<?php echo $model->getAttributeLabel('fax'); ?>
				<?php if ($model->isAttributeRequired('fax')): ?>
					<?php if(!$model->hasErrors('fax')&&isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol ok"></span>
					<?php endif;?>
					<?php if($model->hasErrors('fax') || !isset($_POST['ContactDetailsItem'])):?>
						<span class="symbol required"></span>
					<?php endif;?>
				<?php endif; ?>
			</label>
			<div class="col-sm-6">
				<?php echo $form->textField($model,'fax',array(
					'class'=>'form-control',
				))?>
				<?php if($model->hasErrors('fax')):?>
					<span class="help-block"><?php echo $model->getError('fax');?></span>
				<?php endif;?>
			</div>
		</div>



		<div id="add-ContactDetailsItem-submit-button-container">
			<hr/>
			<div class="row">
				<div class="col-md-offset-8 col-md-4">
					<button class="btn btn-yellow btn-block" type="submit">
						<?php echo $model->isNewRecord?Yii::t('setting','Add Contact Details'):Yii::t('setting','Update Contact Details')?> <i class="fa fa-arrow-circle-right"></i>
					</button>
				</div>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div><!-- End Form Container-->