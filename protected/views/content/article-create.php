<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/24/15
 * Time: 3:17 PM
 */


$this->breadcrumbs=array(
    array(
        'name'=>'Content',
        'url'=>Yii::app()->createUrl('content/index'),
    ),
    array(
        'name'=>'Create',
        'url'=>Yii::app()->createUrl('content/create'),
    ),
    'Create Article',
);
