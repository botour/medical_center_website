<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/24/15
 * Time: 4:01 PM
 */

Yii::app()->clientScript->registerScript('creat-news-script','
$("#block-type-dropdown").on("change",function(){
    if($(this).val() == ""){
        $("#add-news-submit-button-container").hide();
        $("#block-form-container").hide();
    }else{
        $("#add-news-submit-button-container").show();
        $("#block-form-container").show();
    }
})
',CClientScript::POS_END);

?>


<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($newsContentModel, 'title'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if($newsContentModel->hasErrors() || $blockModel->hasErrors()):?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($newsContentModel);?>
                <?php echo $form->errorSummary($blockModel);?>
            </div>
        <?php endif;?>


        <div class="form-group<?php echo $newsContentModel->hasErrors('title')?' has-error':'';echo !$newsContentModel->hasErrors('title')&&isset($_POST['Content'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $newsContentModel->getAttributeLabel('title'); ?>
                <?php if ($newsContentModel->isAttributeRequired('title')): ?>
                    <?php if(!$newsContentModel->hasErrors('title')&&isset($_POST['Content'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($newsContentModel->hasErrors('title') || !isset($_POST['Content'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($newsContentModel,'title',array(
                    'class'=>'form-control',
                ))?>
                <?php if($newsContentModel->hasErrors('title')):?>
                    <span class="help-block"><?php echo $newsContentModel->getError('title');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $newsContentModel->hasErrors('title_ar')?' has-error':'';echo !$newsContentModel->hasErrors('title_ar')&&isset($_POST['Content'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $newsContentModel->getAttributeLabel('title_ar'); ?>
                <?php if ($newsContentModel->isAttributeRequired('title_ar')): ?>
                    <?php if(!$newsContentModel->hasErrors('title_ar')&&isset($_POST['Content'])):?>
                        <span class="symbol ok"></span>
                    <?php endif;?>
                    <?php if($newsContentModel->hasErrors('title_ar') || !isset($_POST['Content'])):?>
                        <span class="symbol required"></span>
                    <?php endif;?>
                <?php endif; ?>
            </label>
            <div class="col-sm-9">
                <?php echo $form->textField($newsContentModel,'title_ar',array(
                    'class'=>'form-control',
                ))?>
                <?php if($newsContentModel->hasErrors('title_ar')):?>
                    <span class="help-block"><?php echo $newsContentModel->getError('title_ar');?></span>
                <?php endif;?>
            </div>
        </div>

        <div class="form-group<?php echo $newsContentModel->hasErrors('primaryImage')?' has-error':'';?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $newsContentModel->getAttributeLabel('primaryImage'); ?>

            </label>
            <div class="col-sm-9">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                            src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail"
                         style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                    <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo $form->fileField($newsContentModel, 'primaryImage', array()); ?>
														</span>
                        <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                            <i class="fa fa-times"></i> Remove
                        </a>
                    </div>
                    <?php if($newsContentModel->hasErrors('title_ar')):?>
                        <span class="help-block"><?php echo $newsContentModel->getError('primaryImage');?></span>
                    <?php endif;?>
                </div>
            </div>
        </div>

        <div class="form-group<?php echo $blockModel->hasErrors('type')?' has-error':'';echo !$newsContentModel->hasErrors('type')&&isset($_POST['Content'])?' has-success':''?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $blockModel->getAttributeLabel('type'); ?>
                <?php if(!$blockModel->hasErrors('type')&&isset($_POST['Content'])):?>
                    <span class="symbol ok"></span>
                <?php endif;?>
                <?php if($blockModel->hasErrors('type') || !isset($_POST['Content'])):?>
                    <span class="symbol required"></span>
                <?php endif;?>
            </label>
            <div class="col-sm-9">
                <?php
                echo CHtml::activeDropDownList($blockModel,'type',Block::getBlockTypeArray(),array(
                    'prompt'=>Yii::t('content','select.block.type.message'),
                    'class'=>'form-control',
                    'id'=>'block-type-dropdown',
                    'ajax'=>array(
                        'url'=>$this->createUrl('content/getBlockCreateForm'),
                        'type'=>'GET',
                        'data'=>array(
                            'type'=>'js:$(this).val()',
                        ),
                        'beforeSend'=>'js:function(){
                        }',
                        'success'=>'js:function(data){
                            $("#block-form-container").html(data);
                        }',
                        'error'=>'js:function(){
                        }'
                    ),
                ));
                ?>
                <?php if($blockModel->hasErrors('type')):?>
                    <span class="help-block"><?php echo $blockModel->getError('type');?></span>
                <?php endif;?>
            </div>
        </div>

        <div id="block-form-container" style="display: <?php echo isset($_POST['Content'])?'block':'none';?>;">
            <?php if(isset($_POST['Content'])):?>
                <?php $this->renderPartial('partial/_block-form',array(
                    'blockModel'=>$blockModel,
                ));?>
            <?php endif;?>
        </div>

        <div id="add-news-submit-button-container" style="display: <?php echo isset($_POST['Content'])?'block':'none';?>;">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo Yii::t('content','controller.content.action.add.news')?> <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->