<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
	array(
		'name'=>'Content',
		'url'=>Yii::app()->createUrl('content/index'),
	),
	'Create',
);

Yii::app()->clientScript->registerScript('content-create-script','
$(function(){
	$("#content-type-dropdown").on("change",function(){
		window.location.replace($(this).val());
	});
});
',CClientScript::POS_END);
?>
<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>
						<?php echo Content::model()->getAttributeLabel('type'); ?>
					</label>
					<?php if (Content::model()->isAttributeRequired('type')): ?>
						<span class="symbol required"></span>
					<?php endif; ?>
					<?php
					echo CHtml::dropDownList('content_type',Content::TYPE_ARTICLE,Content::getContentTypeUri(),array(
						'prompt'=>Yii::t('content','select.content.type.message'),
						'class'=>'form-control',
						'id'=>'content-type-dropdown',
					));
					?>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12" id="content-form-container">

			</div>
		</div>

	</div>
</div>