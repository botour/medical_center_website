<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/24/15
 * Time: 3:17 PM
 */


$this->breadcrumbs=array(
    array(
        'name'=>'Content',
        'url'=>Yii::app()->createUrl('content/index'),
    ),
    array(
        'name'=>'Create',
        'url'=>Yii::app()->createUrl('content/create'),
    ),
    'Create News',
);


$this->cssAssets[] = 'bootstrap-fileupload.min.css';

$this->jsAssets[] = 'bootstrap-fileupload.min.js';
$this->jsAssets[] = 'ckeditor/ckeditor.js';


Yii::app()->clientScript->registerScript("content-create-script",'
var imagesListJSONFile = '.CJavaScript::encode(Yii::app()->baseUrl."/json/images_list.json").';
',CClientScript::POS_END);

?>
<div class="panel panel-default">
    <div class="panel-body">
        <h2>Add News Item</h2>
        <hr>
        <?php $this->renderPartial('_news-create-form',array(
            'newsContentModel'=>$newsContentModel,
            'blockModel'=>$blockModel,
        ));?>
    </div>
</div>
