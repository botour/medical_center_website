<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/25/15
 * Time: 7:32 AM
 */

Yii::app()->clientScript->registerScript("block-create-script",'
CKEDITOR.replace( "text_field_area",{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );
    CKEDITOR.replace( "text_field_area_ar" ,{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );

',CClientScript::POS_END);

?>

<?php if($blockModel->type >= Block::TYPE_TEXT_ONLY):?>
    <div class="form-group<?php echo $blockModel->hasErrors('title')?' has-error':'';?>">
        <label class="col-sm-2 control-label" for="form-field-2">

            <?php echo $blockModel->getAttributeLabel('title'); ?>
            <?php if ($blockModel->isAttributeRequired('title')): ?>
                <?php if(!$blockModel->hasErrors('title')&&isset($_POST['Block'])):?>
                    <span class="symbol ok"></span>
                <?php endif;?>
                <?php if($blockModel->hasErrors('title') || !isset($_POST['Block'])):?>
                    <span class="symbol required"></span>
                <?php endif;?>
            <?php endif; ?>
        </label>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextField($blockModel,'title',array(
                'class'=>'form-control',
            ))?>
            <?php if($blockModel->hasErrors('text_ar')):?>
                <span class="help-block"><?php echo $blockModel->getError('title');?></span>
            <?php endif;?>
        </div>
    </div>

    <div class="form-group<?php echo $blockModel->hasErrors('title_ar')?' has-error':'';?>">
        <label class="col-sm-2 control-label" for="form-field-2">
            <?php echo $blockModel->getAttributeLabel('title_ar'); ?>
            <?php if ($blockModel->isAttributeRequired('title_ar')): ?>
                <?php if(!$blockModel->hasErrors('title_ar')&&isset($_POST['Block'])):?>
                    <span class="symbol ok"></span>
                <?php endif;?>
                <?php if($blockModel->hasErrors('title_ar') || !isset($_POST['Block'])):?>
                    <span class="symbol required"></span>
                <?php endif;?>
            <?php endif; ?>
        </label>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextField($blockModel,'title_ar',array(
                'class'=>'form-control',
            ))?>
            <?php if($blockModel->hasErrors('text_ar')):?>
                <span class="help-block"><?php echo $blockModel->getError('title_ar');?></span>
            <?php endif;?>
        </div>
    </div>
    <div class="form-group<?php echo $blockModel->hasErrors('text')?' has-error':'';echo !$blockModel->hasErrors('text')&&isset($_POST['Block'])?' has-success':''?>">
        <label class="col-sm-2 control-label" for="form-field-2">
            <?php echo $blockModel->getAttributeLabel('text'); ?>
            <?php if ($blockModel->isAttributeRequired('text')): ?>
                <?php if(!$blockModel->hasErrors('text')&&isset($_POST['Block'])):?>
                    <span class="symbol ok"></span>
                <?php endif;?>
                <?php if($blockModel->hasErrors('text') || !isset($_POST['Block'])):?>
                    <span class="symbol required"></span>
                <?php endif;?>
            <?php endif; ?>
        </label>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextArea($blockModel,'text',array(
                'class'=>'form-control',
                'id'=>'text_field_area',
            ))?>
            <?php if($blockModel->hasErrors('text')):?>
                <span class="help-block"><?php echo $blockModel->getError('text');?></span>
            <?php endif;?>
        </div>
    </div>

    <div class="form-group<?php echo $blockModel->hasErrors('text_ar')?' has-error':'';echo !$blockModel->hasErrors('text_ar')&&isset($_POST['Block'])?' has-success':''?>">
        <label class="col-sm-2 control-label" for="form-field-2">
            <?php echo $blockModel->getAttributeLabel('text_ar'); ?>
            <?php if ($blockModel->isAttributeRequired('text_ar')): ?>
                <?php if(!$blockModel->hasErrors('text_ar')&&isset($_POST['Block'])):?>
                    <span class="symbol ok"></span>
                <?php endif;?>
                <?php if($blockModel->hasErrors('text_ar') || !isset($_POST['Block'])):?>
                    <span class="symbol required"></span>
                <?php endif;?>
            <?php endif; ?>
        </label>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextArea($blockModel,'text_ar',array(
                'class'=>'form-control',
                'id'=>'text_field_area_ar',
            ))?>
            <?php if($blockModel->hasErrors('text_ar')):?>
                <span class="help-block"><?php echo $blockModel->getError('text_ar');?></span>
            <?php endif;?>
        </div>
    </div>
<?php endif;?>

<?php if($blockModel->type != Block::TYPE_TEXT_ONLY):?>
    <?php //Put Images Area Here ?>
    <?php if($blockModel->type == Block::TYPE_IMAGE_ONLY || $blockModel->type == Block::TYPE_TEXT_PLUS_ONE_IMAGE):?>
        <div class="row">
        <div class="col-md-offset-2 col-md-9">
            <div class="row">
                <div class="col-md-4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                                src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                        <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($blockModel, 'image1', array()); ?>
														</span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    <?php endif;?>
    <?php if($blockModel->type == Block::TYPE_TEXT_PLUS_TWO_IMAGES):?>
    <div class="row">
        <div class="col-md-offset-2 col-md-9">
            <div class="row">
                <div class="col-md-4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                                src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                        <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($blockModel, 'image1', array()); ?>
														</span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                                src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                        <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($blockModel, 'image2', array()); ?>
														</span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>
    <?php if($blockModel->type == Block::TYPE_TEXT_PLUS_THREE_IMAGES):?>
        <div class="row">
            <div class="col-md-offset-2 col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 325px; height: 230px;"><img
                                    src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="max-width: 140px; max-height: 250px; line-height: 20px;"></div>
                            <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($blockModel, 'image1', array()); ?>
														</span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 325px; height: 230px;"><img
                                    src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="max-width: 140px; max-height: 250px; line-height: 20px;"></div>
                            <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($blockModel, 'image2', array()); ?>
														</span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 325px; height: 230px;"><img
                                    src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="max-width: 140px; max-height: 250px; line-height: 20px;"></div>
                            <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo CHtml::activeFileField($blockModel, 'image3', array()); ?>
														</span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
<?php endif;?>