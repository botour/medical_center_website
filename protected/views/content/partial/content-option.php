<div class="visible-md visible-lg hidden-sm hidden-xs">
    <a href="<?php echo $this->createUrl('content/update',array('id'=>$data->id));?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
    <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View"><i class="fa fa-share"></i></a>


    <form id="deleteForm_<?php echo $data->id;?>" style="display: inline;" action="<?php echo $this->createUrl('content/delete');?>" method="post">
        <input type="hidden" name="content_id" value="<?php echo $data->id;?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" onclick="if(confirm('Are you Sure you want to delete the contnet?')){$('#deleteForm_<?php echo $data->id;?>').submit();}" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
    </form>
    <form id="activateForm_<?php echo $data->id;?>" style="display: inline;" action="<?php echo $this->createUrl('content/active');?>" method="post">
        <input type="hidden" name="content_id" value="<?php echo $data->id;?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" onclick="if(confirm('Are You Sure?')){$('#activateForm_<?php echo $data->id;?>').submit();}" data-original-title="<?php echo $data->active==Content::STATUS_ACTIVE?"deActivate":"Activate"?>"><i class="fa fa-star fa fa-white"></i></a>
    </form>


</div>