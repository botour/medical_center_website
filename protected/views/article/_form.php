<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form CActiveForm */


$this->jsAssets[] = 'ckeditor/ckeditor.js';

Yii::app()->clientScript->registerScript('cv-section-create-script','
$(function(){
    var jsonFilePath = '.CJavaScript::encode(Yii::app()->baseUrl."/json/images_list.json").';
	CKEDITOR.replace( "body_field", {
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl": jsonFilePath
    });
    CKEDITOR.replace( "body_ar_field", {
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl": jsonFilePath
    });

	CKEDITOR.replace( "summary_field", {
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl": jsonFilePath
    });
    CKEDITOR.replace( "summary_ar_field", {
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl": jsonFilePath
    });
});
',CClientScript::POS_END);

?>



<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'department-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($model, 'title'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>
        <?php if ($model->hasErrors()): ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <?php echo $form->errorSummary($model); ?>

            </div>
        <?php endif; ?>


        <?php if($this->user->role==User::ROLE_EDITOR):?>
            <?php echo $form->hiddenField($model,'doctor_id')?>
        <?php endif;?>
        <?php if($this->user->role==User::ROLE_ADMIN):?>
            <div class="form-group<?php echo $model->hasErrors('doctor_id') ? ' has-error' : '';
            echo !$model->hasErrors('doctor_id') && isset($_POST['Article']) ? ' has-success' : '' ?>">
                <label class="col-sm-2 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('doctor_id'); ?>
                    <?php if ($model->isAttributeRequired('doctor_id')): ?>
                        <?php if (!$model->hasErrors('doctor_id') && isset($_POST['Article'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('doctor_id') || !isset($_POST['Article'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>
                <?php
                $doctorOptionsList = array();
                $doctors = Staff::model()->findAll('job_id' == Job::JOB_DOCTOR_ID);
                foreach ($doctors as $doctor) {
                    $doctorOptionsList[$doctor->id] = $doctor->full_name;
                }

                ?>

                <div class="col-sm-4">
                    <?php echo $form->dropDownList($model, 'doctor_id',$doctorOptionsList,array(
                        'class' => 'form-control',
                        'prompt'=>'Select A doctor',
                    )) ?>
                    <?php if ($model->hasErrors('doctor_id')): ?>
                        <span class="help-block"><?php echo $model->getError('doctor_id'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif;?>

        <div class="form-group<?php echo $model->hasErrors('title') ? ' has-error' : '';
        echo !$model->hasErrors('title') && isset($_POST['Article']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('title'); ?>
                <?php if ($model->isAttributeRequired('title')): ?>
                    <?php if (!$model->hasErrors('title') && isset($_POST['Article'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('title') || !isset($_POST['Article'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'title', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('title')): ?>
                    <span class="help-block"><?php echo $model->getError('title'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('title_ar') ? ' has-error' : '';
        echo !$model->hasErrors('title_ar') && isset($_POST['Article']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('title_ar'); ?>
                <?php if ($model->isAttributeRequired('title_ar')): ?>
                    <?php if (!$model->hasErrors('title_ar') && isset($_POST['Article'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('title_ar') || !isset($_POST['Article'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'title_ar', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('title_ar')): ?>
                    <span class="help-block"><?php echo $model->getError('title_ar'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('summary') ? ' has-error' : '';
        echo !$model->hasErrors('summary') && isset($_POST['Article']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('summary'); ?>
                <?php if ($model->isAttributeRequired('summary')): ?>
                    <?php if (!$model->hasErrors('summary') && isset($_POST['Article'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('summary') || !isset($_POST['Article'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-9">
                <?php echo $form->textArea($model, 'summary', array(
                    'class' => 'form-control',
                    'id'=>'summary_field',
                )) ?>
                <?php if ($model->hasErrors('summary')): ?>
                    <span class="help-block"><?php echo $model->getError('summary'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('summary_ar') ? ' has-error' : '';
        echo !$model->hasErrors('summary_ar') && isset($_POST['Article']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('summary_ar'); ?>
                <?php if ($model->isAttributeRequired('summary_ar')): ?>
                    <?php if (!$model->hasErrors('summary_ar') && isset($_POST['Article'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('summary_ar') || !isset($_POST['Article'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-9">
                <?php echo $form->textArea($model, 'summary_ar', array(
                    'class' => 'form-control',
                    'id'=>'summary_ar_field',
                )) ?>
                <?php if ($model->hasErrors('summary_ar')): ?>
                    <span class="help-block"><?php echo $model->getError('title'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('body') ? ' has-error' : '';
        echo !$model->hasErrors('body') && isset($_POST['Article']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('body'); ?>
                <?php if ($model->isAttributeRequired('body')): ?>
                    <?php if (!$model->hasErrors('body') && isset($_POST['Article'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('body') || !isset($_POST['Article'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-9">
                <?php echo $form->textArea($model, 'body', array(
                    'class' => 'form-control',
                    'id'=>'body_field',
                )) ?>
                <?php if ($model->hasErrors('body')): ?>
                    <span class="help-block"><?php echo $model->getError('body'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('body_ar') ? ' has-error' : '';
        echo !$model->hasErrors('body_ar') && isset($_POST['Article']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('body_ar'); ?>
                <?php if ($model->isAttributeRequired('body_ar')): ?>
                    <?php if (!$model->hasErrors('body_ar') && isset($_POST['Article'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('body_ar') || !isset($_POST['Article'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-9">
                <?php echo $form->textArea($model, 'body_ar', array(
                    'class' => 'form-control',
                    'id'=>'body_ar_field',
                )) ?>
                <?php if ($model->hasErrors('body_ar')): ?>
                    <span class="help-block"><?php echo $model->getError('body_ar'); ?></span>
                <?php endif; ?>
            </div>
        </div>


        <div id="add-Article-submit-button-container">
            <hr/>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <button class="btn btn-yellow btn-block" type="submit">
                        <?php echo $model->isNewRecord ? Yii::t('article', 'Create Article') : Yii::t('article', 'Edit Article') ?>
                        <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->
