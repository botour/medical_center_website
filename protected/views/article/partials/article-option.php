<div class="visible-md visible-lg hidden-sm hidden-xs">
    <a href="<?php echo $this->createUrl('article/update', array('id' => $data->id)); ?>"
       class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit Article"><i
            class="fa fa-edit"></i></a>

    <form id="deleteForm_<?php echo $data->id; ?>" style="display: inline;"
          action="<?php echo $this->createUrl('article/delete'); ?>" method="post">
        <input type="hidden" name="article_id" value="<?php echo $data->id; ?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top"
           onclick="if(confirm('Are you Sure you want to delete the article?')){$('#deleteForm_<?php echo $data->id; ?>').submit();}"
           data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
    </form>
    <form id="activateForm_<?php echo $data->id; ?>" style="display: inline;"
          action="<?php echo $this->createUrl('article/active'); ?>" method="post">
        <input type="hidden" name="article_id" value="<?php echo $data->id; ?>"/>
        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top"
           onclick="if(confirm('Are You Sure?')){$('#activateForm_<?php echo $data->id; ?>').submit();}"
           data-original-title="<?php echo $data->active == Article::STATUS_ACTIVE ? "deActivate" : "Activate" ?>"><i
                class="fa fa-star fa fa-white"></i></a>
    </form>
    <?php if ($this->user->role == User::ROLE_ADMIN): ?>

        <form id="approveForm_<?php echo $data->id; ?>" style="display: inline;"
              action="<?php echo $this->createUrl('article/approve'); ?>" method="post">
            <input type="hidden" name="article_id" value="<?php echo $data->id; ?>"/>
            <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top"
               onclick="if(confirm('Are You Sure?')){$('#approveForm_<?php echo $data->id; ?>').submit();}"
               data-original-title="<?php echo $data->approve == Article::STATUS_APPROVED ? "Don\'t approve" : "Approve" ?>"><i
                    class="fa fa-star fa fa-white"></i></a>
        </form>

    <?php endif; ?>


</div>