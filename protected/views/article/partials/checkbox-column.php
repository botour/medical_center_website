<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 5/3/15
 * Time: 12:43 PM
 */
?>

<div class="checkbox-table">
    <label>
        <?php if($data->active==Article::STATUS_ACTIVE):?>
            <input type="checkbox" name="article_id[]" value="<?php echo $data->id;?>" class="flat-green article-checkbox" checked>
        <?php endif;?>
        <?php if($data->active==Article::STATUS_INACTIVE):?>
            <input type="checkbox" name="article_id[]" value="<?php echo $data->id;?>" class="flat-green article-checkbox">
        <?php endif;?>
    </label>
</div>