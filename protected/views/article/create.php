<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs=array(
	array(
		'name'=>Yii::t('article','Articles'),
		'url'=>Yii::app()->createUrl('article/index'),
	),
	'Create Article',
);


$this->renderPartial('_form',array('model'=>$model));


?>
