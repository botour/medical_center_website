<?php
/* @var $this ArticleController */
/* @var $articleModel Article */
$this->breadcrumbs=array(
	'Articles',
);


Yii::app()->clientScript->registerScript('article-script','
$(function(){
	$(".Tabled table").addClass("table");
	$(".Tabled table").addClass("table-striped");
	$(".Tabled table").addClass("table-bordered");
	$(".pagination").parent("div").removeClass("pager");
    $(".tooltip-link").tooltip();
});
',CClientScript::POS_END);
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'article-grid',
	'rowHtmlOptionsExpression' => 'array("class"=>"center")',
	'showTableOnEmpty'=>true,
	'summaryText' => 'Content Count: {count}',
	'summaryCssClass' => 'complete-summery',

	'afterAjaxUpdate' => 'js:function(){
                    $(".Tabled table").addClass("table");
                    $(".Tabled table").addClass("table-condensed");
                    $(".tooltip-link").tooltip();
                }',
	'htmlOptions' => array(
		'class' => 'Tabled',
	),
	'cssFile' => Yii::app()->baseUrl . '/css/main.css',
	'dataProvider' => $articleModel->search(),
	'showTableOnEmpty'=>false,
	'emptyText'=>'No Articles to manage',
	'pager'=>array(
		'class'=>'CLinkPager',
		'header'=>'',
		'htmlOptions'=>array(
			'class'=>'pagination pagination-sm',
		),
	),
	//'filter' => $userModel,
	'columns' => array(
		'title',
		'title_ar',
		array(
			'name'=>'doctor_id',
			'value'=>'"DR. ".$data->doctor->getFullName()',
		),
		array(
			'name'=>'approve',
			'type'=>'raw',
			'value'=>'CommonFunctions::getLabel($data->approve,CommonFunctions::ARTICLE_APPROVE_STATUS)',
		),
		array(
			'name'=>'active',
			'type'=>'raw',
			'value'=>'CommonFunctions::getLabel($data->active,CommonFunctions::ARTICLE_STATUS)',
		),
		array(
			'header'=>'',
			'value'=>array($this,'renderArticleOptions'),
		),
	),
));
?>


