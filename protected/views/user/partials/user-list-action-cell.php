<div style="display: inline">


    <?php echo CHtml::link('<i class="fa fa-pencil"></i>', $this->createUrl('user/update', array('id' => $data->id)), array(
        'class' => 'action-link tooltip-link',
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'user-id' => $data->id,
        'title' => Yii::t('user', 'user.tooltip.edit.user'),

    )); ?>
    <?php echo CHtml::link('<i class="fa fa-trash"></i>', "#", array(
        'class' => 'action-link tooltip-link delete-user-link',
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'user-id' => $data->id,
        'title' => Yii::t('user', 'user.tooltip.delete.user'),
    )); ?>
</div>