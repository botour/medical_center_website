<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	array(
		'name'=>'Users',
		'url'=>Yii::app()->createUrl('user/index'),
	),
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/user-logic.js",CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl."/assets/js/bootstrap-switch.min.js",CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl."/assets/css/bootstrap-switch.min.css");

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'user-grid',
	'rowHtmlOptionsExpression' => 'array("class"=>"grid-row",
    )',
	'summaryText' => 'User Count: {count}',
	'summaryCssClass' => 'complete-summery',
	'beforeAjaxUpdate'=>'js:function(){
        $("#loading-indicator").show();
    }',
	'afterAjaxUpdate' => 'js:function(){
		$(".Tabled table").addClass("table");
        $(".Tabled table").addClass("table-condensed");
        $(".tooltip-link").tooltip();
	}',
	'htmlOptions' => array(
		'class' => 'Tabled',
	),
	'cssFile' => Yii::app()->baseUrl . '/css/main.css',
	'dataProvider' => $userModel->search(),

	//'filter' => $userModel,
	'columns' => array(
		array(
			'header'=>Yii::t('user','grid.header.action'),
			'value'=>array($this,'renderActionCellValue'),
		),
		array(
			'name'=>'first_name',
			'value'=>'$data->getFirstName()',
		),
		array(
			'name'=>'last_name',
			'value'=>'$data->getLastName()',
		),
		array(
			'name'=>'middle_name',
			'value'=>'$data->getMiddleName()',
		),
		array(
			'name'=>'username',
			'value'=>'$data->getUserName()',
		),
		array(
			'name'=>'role',
			'type'=>'raw',
			'value'=>'CommonFunctions::getLabel($data->role,CommonFunctions::USER_ROLE)',
		),

	),
)); ?>

