<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	array(
		'name'=>'Users',
		'url'=>Yii::app()->createUrl('user/index'),
	),
	'Update -> id:'.$model->id,

);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/user-logic.js",CClientScript::POS_END);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>