
<?php
/**
 *
 * @var $model User
 */
Yii::app()->clientScript->registerScript('user-form-script', '
$(function(){
    var doctorRole = ' . CJavaScript::encode(User::ROLE_EDITOR) . ';
    $("#user-role-dropdown").on("change",function(){
        if($(this).val()==doctorRole){
            $("#user-role-container").show();
        }else{
            $("#user-role-container").hide();
        }
    });
});
', CClientScript::POS_END);
?>

<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'User-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($model, 'first_name'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>

        <div class="form-group<?php echo $model->hasErrors('first_name') ? ' has-error' : '';
        echo !$model->hasErrors('first_name') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('first_name'); ?>
                <?php if ($model->isAttributeRequired('first_name')): ?>
                    <?php if (!$model->hasErrors('first_name') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('first_name') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'first_name', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('first_name')): ?>
                    <span class="help-block"><?php echo $model->getError('first_name'); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group<?php echo $model->hasErrors('last_name') ? ' has-error' : '';
        echo !$model->hasErrors('last_name') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('last_name'); ?>
                <?php if ($model->isAttributeRequired('last_name')): ?>
                    <?php if (!$model->hasErrors('last_name') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('last_name') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'last_name', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('last_name')): ?>
                    <span class="help-block"><?php echo $model->getError('last_name'); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group<?php echo $model->hasErrors('middle_name') ? ' has-error' : '';
        echo !$model->hasErrors('middle_name') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('middle_name'); ?>
                <?php if ($model->isAttributeRequired('middle_name')): ?>
                    <?php if (!$model->hasErrors('middle_name') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('middle_name') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'middle_name', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('middle_name')): ?>
                    <span class="help-block"><?php echo $model->getError('middle_name'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('username') ? ' has-error' : '';
        echo !$model->hasErrors('username') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('username'); ?>
                <?php if ($model->isAttributeRequired('username')): ?>
                    <?php if (!$model->hasErrors('username') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('username') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'username', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('username')): ?>
                    <span class="help-block"><?php echo $model->getError('username'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('login_name') ? ' has-error' : '';
        echo !$model->hasErrors('login_name') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('login_name'); ?>
                <?php if ($model->isAttributeRequired('login_name')): ?>
                    <?php if (!$model->hasErrors('login_name') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('login_name') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'login_name', array(
                    'class' => 'form-control',
                )) ?>
                <?php if ($model->hasErrors('login_name')): ?>
                    <span class="help-block"><?php echo $model->getError('login_name'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <?php if ($model->isNewRecord): ?>
            <div class="form-group<?php echo $model->hasErrors('password') ? ' has-error' : '';
            echo !$model->hasErrors('password') && isset($_POST['User']) ? ' has-success' : '' ?>">
                <label class="col-sm-2 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('password'); ?>
                    <?php if ($model->isAttributeRequired('password')): ?>
                        <?php if (!$model->hasErrors('password') && isset($_POST['User'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('password') || !isset($_POST['User'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-6">
                    <?php echo $form->textField($model, 'password', array(
                        'class' => 'form-control',
                    )) ?>
                    <?php if ($model->hasErrors('password')): ?>
                        <span class="help-block"><?php echo $model->getError('password'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group<?php echo $model->hasErrors('email') ? ' has-error' : '';
        echo !$model->hasErrors('email') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('email'); ?>
                <?php if ($model->isAttributeRequired('email')): ?>
                    <?php if (!$model->hasErrors('email') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('email') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-6">
                <?php echo $form->textField($model, 'email', array(
                    'class' => 'form-control',
                    'type' => 'email',
                )) ?>
                <?php if ($model->hasErrors('email')): ?>
                    <span class="help-block"><?php echo $model->getError('email'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group<?php echo $model->hasErrors('role') ? ' has-error' : '';
        echo !$model->hasErrors('role') && isset($_POST['User']) ? ' has-success' : '' ?>">
            <label class="col-sm-2 control-label" for="form-field-2">
                <?php echo $model->getAttributeLabel('role'); ?>
                <?php if ($model->isAttributeRequired('role')): ?>
                    <?php if (!$model->hasErrors('role') && isset($_POST['User'])): ?>
                        <span class="symbol ok"></span>
                    <?php endif; ?>
                    <?php if ($model->hasErrors('role') || !isset($_POST['User'])): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                <?php endif; ?>
            </label>

            <div class="col-sm-2">
                <?php echo $form->dropdownlist($model, 'role', User::getRolesOptionsList(), array(
                    'class' => 'form-control',
                    'prompt' => Yii::t('user', 'Select Role Type'),
                    'id'=>'user-role-dropdown',
                )); ?>
                <?php if ($model->hasErrors('role')): ?>
                    <span class="help-block"><?php echo $model->getError('role'); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div id="user-role-container" style="display: <?php echo $model->role==User::ROLE_EDITOR && isset($_POST['User'])?"block":"none"?>;">
            <div class="form-group<?php echo $model->role==User::ROLE_EDITOR && $model->hasErrors('doctor_id') ? ' has-error' : '';
            echo !$model->hasErrors('doctor_id') && isset($_POST['User']) && $model->role==User::ROLE_EDITOR ? ' has-success' : '' ?>">
                <label class="col-sm-2 control-label" for="form-field-2">
                    <?php echo $model->getAttributeLabel('doctor_id'); ?>
                    <?php if ($model->isAttributeRequired('doctor_id')): ?>
                        <?php if (!$model->hasErrors('doctor_id') && isset($_POST['User'])): ?>
                            <span class="symbol ok"></span>
                        <?php endif; ?>
                        <?php if ($model->hasErrors('doctor_id') || !isset($_POST['User'])): ?>
                            <span class="symbol required"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </label>

                <div class="col-sm-2">
                    <?php
                    $doctorOptionsList = array();
                    $doctors = Staff::model()->findAll('job_id' == Job::JOB_DOCTOR_ID);
                    foreach ($doctors as $doctor) {
                        $doctorOptionsList[$doctor->id] = $doctor->full_name;
                    }

                    ?>
                    <?php echo $form->dropdownlist($model, 'doctor_id', $doctorOptionsList, array(
                        'class' => 'form-control',
                        'prompt' => Yii::t('user', 'Select Doctor'),

                    )); ?>
                    <?php if ($model->hasErrors('doctor_id') && $model->role==User::ROLE_EDITOR): ?>
                        <span class="help-block"><?php echo $model->getError('doctor_id'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <hr/>
        <div class="row">
            <div class="col-md-offset-8 col-md-4">
                <button class="btn btn-yellow btn-block" type="submit">
                    <?php echo $model->isNewRecord ? Yii::t('user', 'Add User') : Yii::t('user', 'Edit User Info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- End Form Container-->