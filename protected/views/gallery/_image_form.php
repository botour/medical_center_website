<?php CommonFunctions::fixAjax(); ?>
<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'gallery-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'focus' => array($model, 'title'),

            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>
        <?php echo $form->hiddenField($model, 'active'); ?>
        <?php echo $form->hiddenField($model, 'gallery_id'); ?>
        <?php echo $form->hiddenField($model, 'id'); ?>
        <?php echo $form->hiddenField($model, 'url'); ?>

        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'type', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->dropDownList($model, 'type', Image::getImageTypesList(), array('class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'type'); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'name', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'name'); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'name_ar', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->textField($model, 'name_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'name_ar'); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'description', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->textArea($model, 'description', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'description'); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'description_ar', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->textArea($model, 'description_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'description_ar'); ?>
            </div>
        </div>

        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'alternative_text', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->textArea($model, 'alternative_text', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'alternative_text'); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'alternative_text_ar', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->textArea($model, 'alternative_text_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'alternative_text_ar'); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-3 col-md-9">

                <?php echo CHtml::ajaxSubmitButton($model->isNewRecord ? Yii::t('image', 'action.create.image') : Yii::t('image', 'action.update.image'), array('gallery/imageUpdate'), array(
                    'type' => 'POST',
                    'dataType' => 'json',
                    'success' => 'js:function(data){
						$("#updateImageModal .modal-body").html(data.message);
						if(data.status){
							updateImagesList();
						}
						setTimeout(function(){
							$("#updateImageModal").modal("hide");
						},2000);

					}',
                    'error' => 'js:function(){
						$("#updateImageModal").modal("hide");
					}'
                ), array(
                    'id' => 'link-' . uniqid(),
                    'class' => 'btn btn-success',
                )); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>

</div><!-- End Form Container-->