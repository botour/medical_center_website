<?php
/* @var $this GalleryController */
/* @var $galleries []Gallery */

$this->breadcrumbs=array(
	array(
		'name'=>'Galleries',
		'url'=>Yii::app()->createUrl('gallery/index'),
	),
);

$this->widget('application.components.Ajaxmodal', array(
	'name' => 'createGalleryModal',
	'title' => 'Create Gallery',
	'width' => '60',
));
$this->widget('application.components.Ajaxmodal', array(
	'name' => 'updateGalleryModal',
	'title' => 'Update Gallery',
	'width' => '60',
));

Yii::app()->clientScript->registerScript("gallery-script",'
var getGalleryCreateFormUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/getGalleryCreateForm')).';
var updateGalleryListUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/updateGalleryList')).';
var deleteUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/delete')).';
var getGalleryUpdateFormUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/getGalleryUpdateForm')).';
var activateUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/activate')).';
var deactivateUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/deactivate')).';
',CClientScript::POS_END);
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl."/css/assets/bootstrap-switch.min.css",CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/assets/bootstrap-switch.min.js",CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/gallery-logic.js",CClientScript::POS_END);

?>

<button id="showModal" class="btn btn-bricky btn-sm">Create Gallery</button>
<button class="btn btn-success btn-sm">Select All Galleries</button>
<button class="btn btn-primary btn-sm" id="refresh-gallery-list-button">Refresh</button>

<input data-off-text="NO" data-on-text="YES" data-size="small" id="checkbox_id" name="checkbox_name" type="checkbox" />
<hr>
<h4 style="text-decoration: underline;">List of all Galleries</h4>
<br>

<div id="gallery-list">
	<?php $this->renderPartial('partial/galleriesList',array(
		'galleries'=>$galleries,
	));?>
</div>
