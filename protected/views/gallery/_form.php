<?php CommonFunctions::fixAjax();?>
<div class="form row">
	<div class="col-md-12">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'gallery-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'focus' => array($model, 'title'),

			'htmlOptions' => array(
				'role' => 'form',
				'class' => 'form-horizontal',
			),
		)); ?>
		<?php echo $form->hiddenField($model,'active');?>
		<?php if(!$model->isNewRecord):?>
			<?php echo $form->hiddenField($model,'id');?>
		<?php endif;?>
		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model, 'type', array('class' => 'col-md-3 control-label task-control-label')); ?>
			<div class="col-md-9">
				<?php echo $form->dropDownList($model, 'type',Gallery::getGalleriesTypesList() ,array('class' => 'form-control ')); ?>
				<span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
				<span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
				<?php echo $form->error($model, 'type'); ?>
			</div>
		</div>
		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model, 'title', array('class' => 'col-md-3 control-label task-control-label')); ?>
			<div class="col-md-9">
				<?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
				<span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
				<span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
				<?php echo $form->error($model, 'title'); ?>
			</div>
		</div>
		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model, 'title_ar', array('class' => 'col-md-3 control-label task-control-label')); ?>
			<div class="col-md-9">
				<?php echo $form->textField($model, 'title_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
				<span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
				<span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
				<?php echo $form->error($model, 'title_ar'); ?>
			</div>
		</div>
		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model, 'summary', array('class' => 'col-md-3 control-label task-control-label')); ?>
			<div class="col-md-9">
				<?php echo $form->textArea($model, 'summary', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
				<span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
				<span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
				<?php echo $form->error($model, 'summary'); ?>
			</div>
		</div>
		<div class="form-group has-feedback">
			<?php echo $form->labelEx($model, 'summary_ar', array('class' => 'col-md-3 control-label task-control-label')); ?>
			<div class="col-md-9">
				<?php echo $form->textArea($model, 'summary_ar', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
				<span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
				<span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
				<?php echo $form->error($model, 'summary_ar'); ?>
			</div>
		</div>


		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<?php if($model->isNewRecord):?>
				<?php echo CHtml::ajaxSubmitButton($model->isNewRecord ? Yii::t('gallery', 'action.create.gallery') : Yii::t('gallery', 'action.update.gallery'),array('gallery/create'),array(
					'type'=>'POST',
					'dataType'=>'json',
					'success'=>'js:function(data){
						$("#createGalleryModal .modal-body").html(data.message);
						if(data.status){
							updateGalleryList();
						}
						setTimeout(function(){
							$("#createGalleryModal").modal("hide");
						},2000);

					}',
					'error'=>'js:function(){
						$("#createGalleryModal").modal("hide");
					}'
				),array(
					'id'=>'link-'.uniqid(),
					'class'=>'btn btn-success',
				));?>
				<?php endif;?>
				<?php if(!$model->isNewRecord):?>
					<?php echo CHtml::ajaxSubmitButton($model->isNewRecord ? Yii::t('gallery', 'action.create.gallery') : Yii::t('gallery', 'action.update.gallery'),array('gallery/update'),array(
						'type'=>'POST',
						'dataType'=>'json',
						'success'=>'js:function(data){
						$("#updateGalleryModal .modal-body").html(data.message);
						if(data.status){
							updateGalleryList();
						}
						setTimeout(function(){
							$("#updateGalleryModal").modal("hide");
						},2000);

					}',
						'error'=>'js:function(){
						$("#updateGalleryModal").modal("hide");
					}'
					),array(
						'id'=>'link-'.uniqid(),
						'class'=>'btn btn-success',
					));?>
				<?php endif;?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>

</div><!-- End Form Container-->