<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/6/15
 * Time: 3:42 PM
 *
 *
 */

/* @var $galleries []Gallery */
?>

<?php if(empty($galleries)):?>
    <div>No Galleries Available</div>
<?php endif;?>

<?php if(!empty($galleries)):?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="center">Title</th>
                <th class="center">Summary</th>
                <th class="center">Active</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            <?php foreach($galleries as $gallery):?>
                <tr>
                    <td><?php echo $gallery->title?></td>
                    <td><?php echo $gallery->summary?></td>
                    <td class="center">
                            <input data-id="<?php echo $gallery->id;?>" type="checkbox" name="gallery-check" class="active-checkbox" <?php echo $gallery->active==Gallery::STATUS_ACTIVE?"checked":"";?>>
                    </td>
                    <td class="center">
                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                            <a href="#" data-id="<?php echo $gallery->id;?>" class="edit-gallery-btn btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                            <a href="<?php echo Yii::app()->createUrl('gallery/view',array('id'=>$gallery->id))?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Manage"><i class="fa fa-share"></i></a>
                            <a href="#" date-id="<?php echo $gallery->id;?>" class="delete-gallery-btn btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                        </div>
                        <div class="visible-xs visible-sm hidden-md hidden-lg">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li role="presentation">
                                        <a class="edit-gallery-btn" role="menuitem" tabindex="-1" href="#" data-id="<?php echo $gallery->id;?>">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo Yii::app()->createUrl('gallery/view',array('id'=>$gallery->id))?>">
                                            <i class="fa fa-share"></i> Manage
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a class="delete-gallery-btn" role="menuitem" tabindex="-1" href="#" data-id="<?php echo $gallery->id;?>">
                                            <i class="fa fa-times"></i> Remove
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php endif;?>


