<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/13/15
 * Time: 11:57 AM
 */

?>

<?php CommonFunctions::fixAjax(); ?>
<div class="form row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'gallery-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'focus' => array($model, 'title'),

            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>
        <?php echo $form->hiddenField($model,'id');?>
         <div class="form-group has-feedback">
            <?php echo $form->labelEx($model, 'gallery_id', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9">
                <?php echo $form->dropDownList($model, 'gallery_id', CHtml::listData(Gallery::model()->findALl(),'id','title'), array('class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                <?php echo $form->error($model, 'gallery_id'); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-3 col-md-9">

                <?php echo CHtml::ajaxSubmitButton(Yii::t('image', 'action.move.image'), array('gallery/moveImage'), array(
                    'type' => 'POST',
                    'dataType' => 'json',
                    'success' => 'js:function(data){
						$("#moveToGalleryModal .modal-body").html(data.message);
						if(data.status){
							updateImagesList();
						}
						setTimeout(function(){
							$("#moveToGalleryModal").modal("hide");
						},2000);

					}',
                    'error' => 'js:function(){
						$("#moveToGalleryModal").modal("hide");
					}'
                ), array(
                    'id' => 'link-' . uniqid(),
                    'class' => 'btn btn-success',
                )); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>

</div><!-- End Form Container-->