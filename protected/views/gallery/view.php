<?php
/* @var $this GalleryController */
/* @var $model Gallery */

$this->breadcrumbs = array(
    array(
        'name' => 'Galleries',
        'url' => Yii::app()->createUrl('gallery/index'),
    ),
    $model->getTitle(),
);

$this->jsAssets[] = "jquery.colorbox-min.js";
$this->jsCustomFiles[] = "image-logic.js";
$this->cssCustomFiles[] = "colorbox.css";

Yii::app()->clientScript->registerScript("gallery-script",'
var updateImageListUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/updateImageList')).';
var uploadImageUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/processAjaxUpload')).';
var deleteImageUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/deleteImage')).';
var getImageUpdateFormUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/getUpdateImageForm')).';
var getMoveImageFormUrl = '.CJavaScript::encode(Yii::app()->createUrl('gallery/getMoveImageForm')).';
',CClientScript::POS_END);

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'updateImageModal',
    'title' => 'Update Image',
    'width' => '60',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'moveToGalleryModal',
    'title' => 'Select A Gallery To Move Image',
    'width' => '60',
));
?>

<div class="row gallery-options">
    <input type="hidden" id="gallery-id" value="<?php echo $model->id; ?>">

    <div class="col-md-12">
        <div id="gallery-image-upload-form">
            <?php echo CHtml::beginForm(array('gallery/view', 'id' => $model->id), 'POST', array(
                'enctype' => 'multipart/form-data',
                'class' => 'admin-form',
                'id' => 'image-upload-form',
            )); ?>
            <input type="hidden" name="submitted" value="submitted"/>
            <?php echo CHtml::activeHiddenField($image, 'gallery_id'); ?>
            <div class="form-control">
                <?php echo CHtml::activeDropDownList($image, 'type', Image::getImageTypesList(),array()); ?>
            </div>
            <div class="form-control">
            </div>
            <div class="form-control">
                <?php echo CHtml::activeFileField($image, 'image', array()); ?>
            </div>
            <div class="form-control">
                <?php echo CHtml::link('Upload Image','#',array(
                    'onclick'=>'js:uploadImage(this);',
                ))?>
            </div>
            </form>
        </div>
        <hr/>
    </div>
</div>

<h4 style="text-decoration: underline;">List of all Images</h4>
<br>


<div id="images-list">
    <?php $this->renderPartial('partial/images-list', array(
        'galleryImages' => $galleryImages,
    )); ?>
</div>

