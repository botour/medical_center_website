<?php
/* @var $this MediaController */

$this->cssAssets[] = 'bootstrap-fileupload.min.css';
$this->cssAssets[] = 'DT_bootstrap.css';

$this->jsCustomFiles[] = 'media-manager-logic.js';
$this->jsAssets[] = 'bootstrap-fileupload.min.js';
$this->jsAssets[] = 'jquery.datatables.min.js';
$this->jsAssets[] = 'DT_bootstrap.js';



Yii::app()->clientScript->registerScript("media_manager_script", '
$(function(){
    $(".images-summery-container").html($(".images-summery").html());
    $(".images-summery").hide();
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-bordered");
    $(".Tabled table").addClass("table-hover");
    $(".Tabled table").addClass("table-striped");
});
', CClientScript::POS_END);
?>


<?php
if (isset($message)) {
    echo $message;
}
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'image-form',
        'focus' => array($image, 'type'),
        'htmlOptions' => array(
            'role' => 'form',
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

    <div class="row">
        <!-- Left Hand Side -->
        <div class="col-md-4">
            <div class="form-group">
                <div class="col-sm-12">
                    <label>
                        <?php echo $image->getAttributeLabel('image'); ?>
                    </label>
                    <?php if ($image->isAttributeRequired('image')): ?>
                        <span class="symbol required"></span>
                    <?php endif; ?>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 350px; height: 230px;"><img
                                src="http://www.placehold.it/350x230/EFEFEF/AAAAAA?text=no+image" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 250px; line-height: 20px;"></div>
                        <div>
														<span class="btn btn-light-grey btn-file"><span
                                                                class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span
                                                                class="fileupload-exists"><i
                                                                    class="fa fa-picture-o"></i> Change</span>
                                                            <?php echo $form->fileField($image, 'image', array()); ?>
														</span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Right Hand Side -->
        <div class="col-md-4">
            <div class="form-group">
                <label>
                    <?php echo $image->getAttributeLabel('type'); ?>
                </label>
                <?php if ($image->isAttributeRequired('type')): ?>
                    <span class="symbol required"></span>
                <?php endif; ?>
                <?php echo $form->dropDownList($image, 'type', Image::getImageTypesList(), array(
                    'class' => 'form-control',
                )); ?>
            </div>

            <div class="tabbable tabs-left">
                <ul id="myTab3" class="nav nav-tabs tab-green">
                    <li class="active">
                        <a href="#panel_tab_english" data-toggle="tab">
                            <?php echo Yii::t('staff', 'controller.create.panel.tab.english'); ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="#panel_tab_arabic" data-toggle="tab">
                            <?php echo Yii::t('staff', 'controller.create.panel.tab.arabic'); ?>
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="panel_tab_english">
                        <div class="form-group">
                            <label>
                                <?php echo $image->getAttributeLabel('name'); ?>
                            </label>
                            <?php if ($image->isAttributeRequired('name')): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                            <?php echo $form->textField($image, 'name', array(
                                'class' => 'form-control',
                            )); ?>
                        </div>
                        <div class="form-group">
                            <label>
                                <?php echo $image->getAttributeLabel('description'); ?>
                            </label>
                            <?php if ($image->isAttributeRequired('description')): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                            <?php echo $form->textArea($image, 'description', array(
                                'class' => 'form-control',
                            )); ?>
                        </div>
                        <div class="form-group">
                            <label>
                                <?php echo $image->getAttributeLabel('alternative_text'); ?>
                            </label>
                            <?php if ($image->isAttributeRequired('alternative_text')): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                            <?php echo $form->textField($image, 'alternative_text', array(
                                'class' => 'form-control',
                            )); ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="panel_tab_arabic">
                        <div class="form-group">
                            <label>
                                <?php echo $image->getAttributeLabel('name_ar'); ?>
                            </label>
                            <?php if ($image->isAttributeRequired('name_ar')): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                            <?php echo $form->textField($image, 'name_ar', array(
                                'class' => 'form-control',
                            )); ?>
                        </div>
                        <div class="form-group">
                            <label>
                                <?php echo $image->getAttributeLabel('description_ar'); ?>
                            </label>
                            <?php if ($image->isAttributeRequired('description_ar')): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                            <?php echo $form->textArea($image, 'description_ar', array(
                                'class' => 'form-control',
                            )); ?>
                        </div>
                        <div class="form-group">
                            <label>
                                <?php echo $image->getAttributeLabel('alternative_text_ar'); ?>
                            </label>
                            <?php if ($image->isAttributeRequired('alternative_text_ar')): ?>
                                <span class="symbol required"></span>
                            <?php endif; ?>
                            <?php echo $form->textField($image, 'alternative_text_ar', array(
                                'class' => 'form-control',
                            )); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-4">

            <div class="form-group">
                <?php echo CHtml::submitButton(Yii::t('media', 'controller.index.add.image'), array(
                    'class' => 'btn btn-bricky btn-yellow btn-block',
                )); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
<hr/>
<div class="images-summery-container">

</div>
<div class="row">
    <div class="col-md-12">
        <!-- start: Images TABLE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Images Table
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                    <a class="btn btn-xs btn-link panel-expand" href="#">
                        <i class="fa fa-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider'=>$imageSearchModel->search(),
                    'id' => 'image-grid',
                    'summaryText' => 'Images Count: {count}',
                    'summaryCssClass'=>'images-summery',
                    'emptyText' => 'No Images Available',
                    'ajaxType'=>'GET',
                    'htmlOptions' => array(
                        'class' => 'Tabled',
                    ),
                    'pager'=>array(
                        'class'=>'CLinkPager',
                        'header'=>'',
                        'htmlOptions'=>array(
                            'class'=>'pagination pagination-sm',
                        ),
                    ),
                    'columns'=>array(
                        array(
                            'class'=>'CDataColumn',
                            'cssClassExpression'=>'"center checkbox-cell"',
                            'type'=>'raw',
                            'value'=>array($this,'renderCheckbox'),
                        ),
                        array(
                            'type'=>'html',
                            'cssClassExpression'=>'"center"',
                            'value'=>' CHTml::link(CHtml::image(CommonFunctions::IMAGES_PATH . Gallery::getUploadPath($data->type) . CommonFunctions::THUMB_UPLOAD_PATH . $data->url,$data->alternative_text,array(
                                "style"=>"width: 150px;height: 80px;",
                            )),CommonFunctions::UPLOAD_PATH . Gallery::getUploadPath($data->type)  . $data->url,array(
                                "class"=>"image-link",
                                "data-id"=>$data->id,
                            ))',

                        ),

                        array(
                            'header'=>'Gallery',
                            'name'=>'gallery.title',
                            'headerHtmlOptions'=>array(
                                'class'=>'center'
                            ),
                            'cssClassExpression'=>'"center"',
                            'filter'=>false,
                        ),
                        array(
                            'class'=>'CDataColumn',
                            'cssClassExpression'=>'"center checkbox-cell"',
                            'type'=>'raw',
                            'value'=>array($this,'renderOptions'),
                        ),
                    ),
                ));?>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>