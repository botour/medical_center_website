<?php

class RbacCommand extends CConsoleCommand
{
    private $_authManager;

    public function getHelp()
    {
        $description = "DESCRIPTION\n";
        $description .= ' ' . "This command generates an initial RBAC authorization hierarchy For ProjStar Project.\n";
        return parent::getHelp() . $description;
    }

    public function actionIndex()
    {
        $this->ensureAuthManagerDefined();
        $message = "Hey This is Borhan :):This command will create three roles: administrator and editor\n";
        $message .= "Would you like to continue?";
        if ($this->confirm($message)) {
            $this->_authManager->clearAll();

            $this->_authManager->createRole("admin");
            $this->_authManager->createRole("editor");

            echo "Authorization hierarchy successfully generated.\n";

        } else {
            echo "Operation cancelled.\n";
        }
    }

    public function actionDelete()
    {
        $this->ensureAuthManagerDefined();
        $message = "This command will clear all RBAC definitions.\n";
        $message .= "Would you like to continue?";
        if ($this->confirm($message)) {
            $this->_authManager->clearAll();
            echo "Authorization hierarchy removed.\n";
        } else {
            echo "Delete operation cancelled.\n";
        }
    }

    protected function ensureAuthManagerDefined()
    {
        if (($this->_authManager = Yii::app()->authManager) === null) {
            $message = "Error: an authorization manager, named 'authManager' must be con-figured to use this command.";
            $this->usageError($message);
        }
    }
}