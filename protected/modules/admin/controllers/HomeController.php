<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/28/15
 * Time: 3:33 PM
 */

class HomeController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }
}