<?php

/**
 * This is the model class for table "tbl_department_equipment".
 *
 * The followings are the available columns in table 'tbl_department_equipment':
 * @property integer $id
 * @property integer $department_id
 * @property integer $active
 * @property string $name
 * @property string $name_ar
 * @property integer $image_id
 * @property integer $gallary_id
 * @property integer $order
 * @property string $body
 * @property string $body_ar
 *
 * The followings are the available model relations:
 * @property Image $image
 * @property Department $department
 * @property Gallery $gallary
 */
class Equipment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_department_equipment';
	}

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('department_id, name, name_ar, order, body, body_ar', 'required','on'=>'create'),
			array('department_id, name, name_ar, order, body, body_ar', 'required','on'=>'update'),
			array('department_id, active, image_id, gallary_id, order', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, department_id, active, name, name_ar, image_id, gallary_id, order, body, body_ar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'image' => array(self::BELONGS_TO, 'Image', 'image_id'),
			'images' => array(self::MANY_MANY, 'Image', 'tbl_equip_image(equipment_id,image_id)'),
			'department' => array(self::BELONGS_TO, 'Department', 'department_id'),
			'gallary' => array(self::BELONGS_TO, 'Gallery', 'gallary_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('department', 'model.label.equipment.id'),
					'department_id' => Yii::t('department', 'model.label.equipment.department'),
					'active' => Yii::t('department', 'model.label.equipment.active'),
					'name' => Yii::t('department', 'model.label.equipment.name'),
					'name_ar' => Yii::t('department', 'model.label.equipment.name_ar'),
					'image_id' => Yii::t('department', 'model.label.equipment.image'),
					'gallary_id' => Yii::t('department', 'model.label.equipment.gallary'),
					'order' => Yii::t('department', 'model.label.equipment.order'),
					'body' => Yii::t('department', 'model.label.equipment.body'),
					'body_ar' => Yii::t('department', 'model.label.equipment.body_ar'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('active',$this->active);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_ar',$this->name_ar,true);
		$criteria->compare('image_id',$this->image_id);
		$criteria->compare('gallary_id',$this->gallary_id);
		$criteria->compare('order',$this->order);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('body_ar',$this->body_ar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getName(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->name_ar;
		}else{
			return $this->name;
		}
	}
	public function getBody(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->body_ar;
		}else{
			return $this->body;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Equipment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
