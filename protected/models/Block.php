<?php

/**
 * This is the model class for table "tbl_block".
 *
 * The followings are the available columns in table 'tbl_block':
 * @property integer $id
 * @property string $title
 * @property string $title_ar
 * @property string $text
 * @property string $text_ar
 * @property integer $content_id
 * @property integer $type
 *
 * The followings are the available model relations:
 * @property Content $content
 * @property Content[] $contents
 * @property Image[] $images
 */
class Block extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	const TYPE_IMAGE_ONLY = 0;
	const TYPE_TEXT_ONLY = 1;
	const TYPE_TEXT_PLUS_ONE_IMAGE = 2;
	const TYPE_TEXT_PLUS_TWO_IMAGES = 3;
	const TYPE_TEXT_PLUS_THREE_IMAGES = 4;


	public $image1;
	public $image2;
	public $image3;

	public function tableName()
	{
		return 'tbl_block';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type,text,text_ar', 'required','on'=>'create-text-only'),
			array('type,image1','required','on'=>'create-image-only'),
			array('type,text,text_ar,image1','required','on'=>'create-text-image1'),
			array('type,text,text_ar,image1,image2','required','on'=>'create-text-image2'),
			array('type,text,text_ar,image1,image2,image3','required','on'=>'create-text-image3'),
			array('image1','file','maxSize'=>2*1024*1024,'maxFiles'=>1,'types'=>'gif,jpg,png','allowEmpty'=>true),
			array('image2','file','maxSize'=>2*1024*1024,'maxFiles'=>1,'types'=>'gif,jpg,png','allowEmpty'=>true),
			array('image3','file','maxSize'=>2*1024*1024,'maxFiles'=>1,'types'=>'gif,jpg,png','allowEmpty'=>true),
			array('type','in','range'=>array(
				Block::TYPE_TEXT_ONLY,
				Block::TYPE_IMAGE_ONLY,
				Block::TYPE_TEXT_PLUS_ONE_IMAGE,
				Block::TYPE_TEXT_PLUS_TWO_IMAGES,
				Block::TYPE_TEXT_PLUS_THREE_IMAGES,
			)),
			array('content_id', 'numerical', 'integerOnly'=>true),
			array('title, title_ar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, title_ar, text, text_ar, content_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'content' => array(self::BELONGS_TO, 'Content', 'content_id'),
			'contents' => array(self::HAS_MANY, 'Content', 'summary_block_id'),
			'images' => array(self::HAS_MANY, 'Image', 'block_id'),
		);
	}

	public static function getBlockTypeArray(){
		return array(
			self::TYPE_IMAGE_ONLY=>Yii::t('content','model.type.img'),
			self::TYPE_TEXT_ONLY=>Yii::t('content','model.type.txt'),
			self::TYPE_TEXT_PLUS_ONE_IMAGE=>Yii::t('content','model.type.txt.img1'),
			self::TYPE_TEXT_PLUS_TWO_IMAGES=>Yii::t('content','model.type.txt.img2'),
			self::TYPE_TEXT_PLUS_THREE_IMAGES=>Yii::t('content','model.type.txt.img3'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('block', 'model.label.id'),
					'title' => Yii::t('block', 'model.label.title'),
					'title_ar' => Yii::t('block', 'model.label.title_ar'),
					'text' => Yii::t('block', 'model.label.text'),
					'text_ar' => Yii::t('block', 'model.label.text_ar'),
					'content_id' => Yii::t('block', 'model.label.content'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	public function validateImage($imageAttributeString){
		$imageFile = CUploadedFile::getInstance($this, $imageAttributeString);
		$image = new Image('create-for-content');
		if (!is_null($imageFile)) {
			$image->active = Image::STATUS_ACTIVE;
			$image->type = Image::TYPE_NEWS_SPECIFIC;
			$image->image = $this->primaryImage;
			if ($image->validate(array('image', 'type'))) {
				$image->url = sha1($image->image->name . uniqid() . time()) . '.' . $image->image->extensionName;
				$image->name = 'description';
				$image->name_ar = 'description';

			}
		}
		return $image->validate();
	}

	public function addImage($imageAttributeString,$imageType,$imagePath,$imageThumbPath){
		$imageFile = CUploadedFile::getInstance($this, $imageAttributeString);
		$image = new Image('create-for-content');
		if (!is_null($imageFile)) {
			$image->active = Image::STATUS_ACTIVE;
			$image->type = $imageType;
			$image->image = $imageFile;
			if ($image->validate(array('image', 'type'))) {
				$image->url = sha1($image->image->name . uniqid() . time()) . '.' . $image->image->extensionName;
				$image->name = 'description';
				$image->name_ar = 'description';
				if ($image->image->saveAs(CommonFunctions::IMAGES_PATH . $imagePath . $image->url)) {
					$image->block_id = $this->id;
					$image->save(false);
					$imageProcessor = new ImageProcessor();
					$imageProcessor->source_path = CommonFunctions::IMAGES_PATH . $imagePath . $image->url;
					$imageProcessor->target_path = CommonFunctions::IMAGES_PATH . $imagePath . $imageThumbPath . $image->url;
					$imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1);
				}

			}
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('text_ar',$this->text_ar,true);
		$criteria->compare('content_id',$this->content_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getText(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->text_ar;
		}else{
			return $this->text;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Block the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
