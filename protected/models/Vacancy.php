<?php

/**
 * This is the model class for table "tbl_vacancy".
 *
 * The followings are the available columns in table 'tbl_vacancy':
 * @property integer $id
 * @property string $position
 * @property string $position_ar
 * @property string $title
 * @property string $title_ar
 * @property string $description
 * @property string $description_ar
 * @property string $salary
 * @property string $salary_ar
 * @property integer $active
 * @property integer $contact_email
 */
class Vacancy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_vacancy';
	}

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('position, position_ar, title, title_ar, description, description_ar,active,contact_email', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('position, position_ar, title, title_ar, salary, salary_ar', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, position, position_ar, title, title_ar, description, description_ar, salary, salary_ar, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('vacancy', 'model.label.id'),
					'position' => Yii::t('vacancy', 'model.label.position'),
					'position_ar' => Yii::t('vacancy', 'model.label.position_ar'),
					'title' => Yii::t('vacancy', 'model.label.title'),
					'title_ar' => Yii::t('vacancy', 'model.label.title_ar'),
					'description' => Yii::t('vacancy', 'model.label.description'),
					'description_ar' => Yii::t('vacancy', 'model.label.description_ar'),
					'salary' => Yii::t('vacancy', 'model.label.salary'),
					'contact_email' => Yii::t('vacancy', 'model.label.contact_email'),
					'salary_ar' => Yii::t('vacancy', 'model.label.salary_ar'),
					'active' => Yii::t('vacancy', 'model.label.active'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('contact_email',$this->contact_email,true);
		$criteria->compare('position_ar',$this->position_ar,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('description_ar',$this->description_ar,true);
		$criteria->compare('salary',$this->salary,true);
		$criteria->compare('salary_ar',$this->salary_ar,true);
		$criteria->compare('active',$this->active);

		$criteria->order = 'id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function getTitle(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->title_ar;
		}else{
			return $this->title;
		}
	}
	public function getDescription(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->description_ar;
		}else{
			return $this->description;
		}
	}

	public function getPosition(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->position_ar;
		}else{
			return $this->position;
		}
	}
	public function getSalary(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->salary_ar;
		}else{
			return $this->salary;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vacancy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
