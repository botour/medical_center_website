<?php

/**
 * This is the model class for table "tbl_staff".
 *
 * The followings are the available columns in table 'tbl_staff':
 * @property integer $id
 * @property integer $profile_image_id
 * @property integer $job_id
 * @property integer $branch_id_id
 * @property integer $title
 * @property String $full_name
 * @property String $full_name_ar
 * @property String $dop
 * @property String $dop_ar
 * @property String $address
 * @property String $address_ar
 * @property String $contact
 * @property String $contact_ar
 * @property String $m_status
 * @property String $m_status_ar
 * @property String $nationality
 * @property String $nationality_ar
 * The followings are the available model relations:
 * @property CvSection[] $cvSections
 * @property DeptStaffAssignment[] $deptStaffAssignments
 * @property Image $profileImg
 * @property Job $job
 * @property Department[] $tblDepartments
 */
class Staff extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_staff';
	}

	const branch_id_TYPE_1 = 1;
	const branch_id_TYPE_2 = 2;

	const TITLE_DR = 0;
	const TITLE_OTHER = 1;

	public $profileImage;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profileImage,branch_id,job_id,title,full_name,full_name_ar,section_id,dob,dob_ar,address,job_id,address_ar,contact,contact_ar,m_status,m_status_ar,nationality,nationality_ar', 'required','on'=>'create'),
			array('branch_id,job_id,title,full_name,full_name_ar,section_id,dob,dob_ar,address,job_id,address_ar,contact,contact_ar,m_status,m_status_ar,nationality,nationality_ar', 'required','on'=>'update'),
			array('job_id, branch_id, title', 'numerical', 'integerOnly'=>true),
			array('profileImage','file','maxSize'=>2*1024*1024,'maxFiles'=>1,'types'=>'gif,jpg,png','allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,profile_image_id,job_id,section_id,branch_id,title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cvSections' => array(self::HAS_MANY, 'CvSection', 'staff_id'),
			'deptStaffAssignments' => array(self::HAS_MANY, 'DeptStaffAssignment', 'staff_id'),
			'profileImg' => array(self::BELONGS_TO, 'Image', 'profile_image_id'),
			'job' => array(self::BELONGS_TO, 'Job', 'job_id'),
			'section' => array(self::BELONGS_TO, 'Department', 'section_id'),

			'tblDepartments' => array(self::MANY_MANY, 'Department', 'tbl_staff_department(staff_id, department_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('staff', 'model.label.id'),
					'profile_image_id' => Yii::t('staff', 'model.label.profile_image'),
					'job_id' => Yii::t('staff', 'model.label.job'),
					'branch_id' => Yii::t('staff', 'model.label.branch_id'),
					'title' => Yii::t('staff', 'model.label.title'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('profile_image_id',$this->profile_image_id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('title',$this->title);
		$criteria->with = 'profileImg';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getStaffArrayFromModel(){
		$criteria=new CDbCriteria;

		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('section_id',$this->section_id);
		$criteria->with = 'profileImg';
		$staffArray = Staff::model()->findAll($criteria);
		return $staffArray;
	}

	public static function getStaffTitleArray()
	{
		return array(
			self::TITLE_DR => Yii::t('staff', 'staff.title.dr'),
			self::TITLE_OTHER => Yii::t('staff', 'staff.title.other'),
		);
	}

	public static function getStaffbranch_idArray()
	{
		return array(
			self::branch_id_TYPE_1 => Yii::t('staff', 'staff.branch_id.1'),
			self::branch_id_TYPE_2 => Yii::t('staff', 'staff.branch_id.2'),
		);
	}

	public function getTitle(){
		if($this->title == Staff::TITLE_OTHER){
			return "";
		}else if($this->title == Staff::TITLE_DR){
			if(Yii::app()->getLanguage()=="ar") {
				return "د. ";
			}else{
				return "Dr. ";
			}
		}

	}

	public function getFullName(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->full_name_ar;
		}else{
			return $this->full_name;
		}
	}

	public function getDob(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->dob_ar;
		}else{
			return $this->dob;
		}
	}
	public function getMStatus(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->m_status_ar;
		}else{
			return $this->m_status;
		}
	}
	public function getContact(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->contact_ar;
		}else{
			return $this->contact;
		}
	}
	public function getAddress(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->address_ar;
		}else{
			return $this->address;
		}
	}
	public function getNationality(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->nationality_ar;
		}else{
			return $this->nationality;
		}
	}





	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Staff the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
