<?php

/**
 * This is the model class for table "tbl_department".
 *
 * The followings are the available columns in table 'tbl_department':
 * @property integer $id
 * @property string $order
 * @property string $name
 * @property integer $summary
 * @property string $services
 * @property integer $active
 * @property string $create_date
 * @property string $update_date
 * @property integer $gallary_id
 * @property string $contact_info
 * @property string $name_ar
 * @property string $summary_ar
 * @property string $services_ar
 * @property string $contact_info_ar
 * @property integer $type
 *
 *
 *
 * The followings are the available model relations:
 * @property Gallary $gallary
 * @property DepartmentEquipment[] $departmentEquipments
 * @property DeptStaffAssignment[] $deptStaffAssignments
 * @property Faq[] $faqs
 */
class Department extends SuperModel
{


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_department';
    }

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TYPE_SERVICE_CLINIC = 0;
    const TYPE_ORGANIZATIONAL = 1;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name,name_ar,order,active,type', 'required', 'on' => 'create'),
            array('name,name_ar,order,active,type', 'required', 'on' => 'update'),
            array('active,order,gallary_id', 'numerical', 'integerOnly' => true),
            array('name,name_ar', 'length', 'max' => 255),
            array('services,services_ar, contact_info, contact_info_ar', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, summary, services, active, create_date, update_date, gallary_id, contact_info, contact_info_ar', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'gallary' => array(self::BELONGS_TO, 'Gallary', 'gallary_id'),
            'departmentEquipments' => array(self::HAS_MANY, 'DepartmentEquipment', 'department_id'),
            'deptStaffAssignments' => array(self::HAS_MANY, 'DeptStaffAssignment', 'department_id'),
            'equipments'=>array(self::HAS_MANY, 'Equipment', 'department_id'),
            'staff' => array(self::HAS_MANY, 'Staff', 'section_id'),
            'faqs' => array(self::HAS_MANY, 'Faq', 'department_id'),
            'services' => array(self::HAS_MANY, 'ServiceItem', 'department_id'),
        );
    }

    public static function getDepartmentStatusArray()
    {
        return array(
            self::STATUS_INACTIVE => Yii::t('department', 'department.status.inactive'),
            self::STATUS_ACTIVE => Yii::t('department', 'department.status.active'),
        );
    }

    public static function getDepartmentTypeArray()
    {
        return array(
            self::TYPE_SERVICE_CLINIC => Yii::t('department', 'department.type.service.clinic'),
            self::TYPE_ORGANIZATIONAL => Yii::t('department', 'department.type.organizational'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('department', 'model.label.id'),
            'order' => Yii::t('department', 'model.label.order'),
            'name' => Yii::t('department', 'model.label.name'),
            'summary' => Yii::t('department', 'model.label.summary'),
            'services' => Yii::t('department', 'model.label.services'),
            'name_ar' => Yii::t('department', 'model.label.name_ar'),
            'summary_ar' => Yii::t('department', 'model.label.summary_ar'),
            'services_ar' => Yii::t('department', 'model.label.services_ar'),
            'active' => Yii::t('department', 'model.label.active'),
            'create_date' => Yii::t('department', 'model.label.create_date'),
            'update_date' => Yii::t('department', 'model.label.update_date'),
            'gallary_id' => Yii::t('department', 'model.label.gallary_id'),
            'contact_info' => Yii::t('department', 'model.label.contact_info'),
            'contact_info_ar' => Yii::t('department', 'model.label.contact_info_ar'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     *
     */

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('summary', $this->summary);
        $criteria->compare('services', $this->services, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('gallary_id', $this->gallary_id);
        $criteria->compare('contact_info', $this->contact_info, true);
        $criteria->compare('contact_info_ar', $this->contact_info_ar, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getName(){
        if(Yii::app()->getLanguage()=='ar'){
            return $this->name_ar;
        }else{
            return $this->name;
        }
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Department the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
