<?php

/**
 * This is the model class for table "tbl_slider_image".
 *
 * The followings are the available columns in table 'tbl_slider_image':
 * @property integer $id
 * @property string $title
 * @property string $title_ar
 * @property string $description
 * @property string $description_ar
 * @property string $link
 * @property integer $active
 * @property integer $order
 * @property string $create_date
 * @property string $update_date
 */
class SliderImage extends SuperModel
{

    public $sliderImage;
    // A special attribute that stores the images file being uploaded by the user
    public $image;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_slider_image';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title,title_ar,description,description_ar', 'required', 'on' => 'create'),
            array('title,title_ar,description,description_ar', 'required', 'on' => 'update'),
            array('active, order', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('description, link, create_date, update_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, description, link, active, order, create_date, update_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'image' => array(self::BELONGS_TO, 'Image', 'image_id'),
        );
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('home', 'slider.image.model.label.id'),
            'title' => Yii::t('home', 'slider.image.model.label.title'),
            'description' => Yii::t('home', 'slider.image.model.label.description'),
            'title_ar' => Yii::t('home', 'slider.image.model.label.title.ar'),
            'description_ar' => Yii::t('home', 'slider.image.model.label.description.ar'),
            'link' => Yii::t('home', 'slider.image.model.label.link'),
            'image_id' => Yii::t('home', 'slider.image.model.label.image_id'),
            'active' => Yii::t('home', 'slider.image.model.label.active'),
            'order' => Yii::t('home', 'slider.image.model.label.order'),
            'create_date' => Yii::t('home', 'slider.image.model.label.create_date'),
            'update_date' => Yii::t('home', 'slider.image.model.label.update_date'),
        );
    }

    public function delete()
    {

        return parent::delete();
    }

    public function getImage(){
        if(is_null($this->sliderImage)){
            $this->sliderImage = Image::model()->findByPk($this->image_id);
        }
        return $this->sliderImage;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('link', $this->link, true);
        $criteria->compare('image_id', $this->image_id, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('order', $this->order);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('update_date', $this->update_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getTitle(){
        if(Yii::app()->getLanguage()=="ar"){
            return $this->title_ar;
        }else{
            return $this->title;
        }
    }
    public function getDescription(){
        if(Yii::app()->getLanguage()=="ar"){
            return $this->description_ar;
        }else{
            return $this->description;
        }
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SliderImage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
