<?php

/**
 * This is the model class for table "tbl_image".
 *
 * The followings are the available columns in table 'tbl_image':
 * @property integer $id
 * @property integer $active
 * @property string $url
 * @property integer $gallary_id
 * @property string $name
 * @property string $name_ar
 * @property string $description
 * @property string $description_ar
 * @property string $alternative_text
 * @property string $alternative_text_ar
 * @property integer $type
 * @property integer $create_id
 * @property integer $update_id
 * @property integer $host
 *
 * The followings are the available model relations:
 * @property Gallery $gallary
 * @property News[] $news
 * @property Staff[] $staff
 */
class Image extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	const TYPE_GENERAL = 0;
	const TYPE_PROFILE_SPECIFIC = 1;
	const TYPE_EQUIPMENT_SPECIFIC = 2;
	const TYPE_CLINIC_SPECIFIC = 3;
	const TYPE_NEWS_SPECIFIC = 4;
	const TYPE_EVENT_SPECIFIC = 5;


	const HOST_NOT_MEDIA = 0;
	const HOST_MEDIA = 1;

	const TYPE_SLIDER_IMAGE = 6;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	public static function getImageTypesList(){
		return array(
			self::TYPE_GENERAL=>'General',
			self::TYPE_CLINIC_SPECIFIC=>'Clinic Specific',
			self::TYPE_EQUIPMENT_SPECIFIC=>'Equipment Specific',
			self::TYPE_EVENT_SPECIFIC=>'Event Specific',
			self::TYPE_PROFILE_SPECIFIC=>'Profile Specific',
			self::TYPE_NEWS_SPECIFIC=>'News Specific',
		);
	}

	// A special attribute that stores the images file being uploaded by the user
	public $image;

	public function tableName()
	{
		return 'tbl_image';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type,image', 'required','on'=>'create-media'),

			array('image,name,name_ar,description,description_ar,alternative_text,alternative_text_ar','required','on'=>'create-for-equipment'),
			array('active,type,image,name,name_ar,alternative_text,alternative_text_ar','required','on'=>'create-for-slider'),
			array('active,type,url,name,name_ar,alternative_text,alternative_text_ar','required','on'=>'create-for-staff'),
			array('name,name_ar,image,alternative_text,alternative_text_ar', 'required','on'=>'create-for-slider'),
			array('active,url, type,image', 'required','on'=>'create'),
			array('active, url, type', 'required','on'=>'update'),
			array('gallery_id', 'required','on'=>'update-gallery'),
			array('image','file','maxSize'=>2*1024*1024,'maxFiles'=>1,'types'=>'gif,jpg,png'),
			array('type','in','range'=>array(
				Image::TYPE_CLINIC_SPECIFIC,
				Image::TYPE_EQUIPMENT_SPECIFIC,
				Image::TYPE_PROFILE_SPECIFIC,
				Image::TYPE_EVENT_SPECIFIC,
				Image::TYPE_GENERAL,
				Image::TYPE_NEWS_SPECIFIC,
				Image::TYPE_SLIDER_IMAGE,
			)),
			array('active, gallery_id, type', 'numerical', 'integerOnly'=>true),
			array('url, name, name_ar,alternative_text,alternative_text_ar', 'length','min'=>10, 'max'=>255),
			array('description, description_ar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, name_ar, description, description_ar,alternative_text,alternative_text_ar', 'safe', 'on'=>'update'),

			array('id, active, url, gallery_id, name, name_ar, description, description_ar, alternative_text, alternative_text_ar, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gallery' => array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
			'news' => array(self::HAS_MANY, 'News', 'image_id'),
			'staff' => array(self::HAS_MANY, 'Staff', 'image_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'active' => 'Active',
			'url' => 'Image',
			'gallery_id' => 'Gallary',
			'name' => 'Name',
			'name_ar' => 'Name Ar',
			'description' => 'Description',
			'description_ar' => 'Description Ar',
			'alternative_text' => 'Alternative Text',
			'alternative_text_ar' => 'Alternative Text Ar',
			'type' => 'Type',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('host',$this->host);
		$criteria->compare('active',$this->active);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('gallery_id',$this->gallery_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_ar',$this->name_ar,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('description_ar',$this->description_ar,true);
		$criteria->compare('alternative_text',$this->alternative_text,true);
		$criteria->compare('alternative_text_ar',$this->alternative_text_ar,true);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>5,
			),
		));
	}


	public function delete(){
		$imagePath = CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($this->type).$this->url;
		$imageThumbPath = CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($this->type).CommonFunctions::THUMB_UPLOAD_PATH.$this->url;
		unlink($imagePath);
		unlink($imageThumbPath);
		return parent::delete();
	}

	public function deleteImageForContent($path,$thumbPath){
		$imagePath = CommonFunctions::IMAGES_PATH.$path.$this->url;
		$imageThumbPath = CommonFunctions::IMAGES_PATH.$path.$thumbPath.$this->url;
		unlink($imagePath);
		unlink($imageThumbPath);
		return parent::delete();
	}


	public function getPath(){
		return Yii::app()->baseUrl."/".CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($this->type).$this->url;
	}
	public function getThumbPath(){
		return Yii::app()->baseUrl."/".CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($this->type).CommonFunctions::THUMB_UPLOAD_PATH.$this->url;
	}

	public function getName(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->name_ar;
		}else{
			return $this->name;
		}
	}
	public function getDescription(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->description;
		}else{
			return $this->description_ar;
		}
	}
	public function getAlternativeText(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->alternative_text;
		}else{
			return $this->alternative_text_ar;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Image the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
