<?php

/**
 * This is the model class for table "tbl_gallery".
 *
 * The followings are the available columns in table 'tbl_gallery':
 * @property integer $id
 * @property integer $active
 * @property string $title
 * @property string $title_ar
 * @property string $summary
 * @property string $sumary_ar
 * @property integer $type
 * @property string $create_date
 * @property integer $create_id
 * @property string $update_date
 * @property integer $update_id
 * @property integer $image_id
 *
 * The followings are the available model relations:
 * @property Article[] $articles
 * @property Department[] $departments
 * @property DepartmentEquipment[] $departmentEquipments
 * @property DepartmentPageSection[] $departmentPageSections
 * @property DoctorGallaryAssignment[] $doctorGallaryAssignments
 * @property Image[] $images
 * @property News[] $news
 */
class Gallery extends SuperModel
{

	const TYPE_GENERAL = 0;
	const TYPE_PROFILE_SPECIFIC = 1;
	const TYPE_EQUIPMENT_SPECIFIC = 2;
	const TYPE_CLINIC_SPECIFIC = 3;
	const TYPE_NEWS_SPECIFIC = 4;
	const TYPE_EVENT_SPECIFIC = 5;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	const MAIN_GALLERY_ID = 11;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_gallery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, title_ar, summary,summary_ar,active,type', 'required','on'=>'create'),
			array('title, title_ar, summary,summary_ar', 'required','on'=>'update'),

			array('active, type, create_id, update_id', 'numerical', 'integerOnly'=>true),
			array('title, title_ar', 'length', 'max'=>255),
			array('sumary_ar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, active, title, title_ar, summary, sumary_ar, type, create_date, create_id, update_date, update_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articles' => array(self::HAS_MANY, 'Article', 'gallary_id'),
			'departments' => array(self::HAS_MANY, 'Department', 'gallary_id'),
			'departmentEquipments' => array(self::HAS_MANY, 'DepartmentEquipment', 'gallary_id'),
			'departmentPageSections' => array(self::HAS_MANY, 'DepartmentPageSection', 'gallary_id'),
			'doctorGallaryAssignments' => array(self::HAS_MANY, 'DoctorGallaryAssignment', 'gallary_id'),
			'images' => array(self::HAS_MANY, 'Image', 'gallery_id'),
			'news' => array(self::HAS_MANY, 'News', 'gallery_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'active' => 'Active',
			'title' => 'Title',
			'title_ar' => 'Title Ar',
			'summary' => 'Summary',
			'sumary_ar' => 'Sumary Ar',
			'type' => 'Type',
			'create_date' => 'Create Date',
			'create_id' => 'Create',
			'update_date' => 'Update Date',
			'update_id' => 'Update',
		);
	}

	public static function getUploadPath($type = Image::TYPE_GENERAL){
		$paths = array(
			Image::TYPE_GENERAL=>CommonFunctions::GENERAL_UPLOAD_PATH,
			Image::TYPE_NEWS_SPECIFIC=>CommonFunctions::NEWS_UPLOAD_PATH,
			Image::TYPE_EVENT_SPECIFIC=>CommonFunctions::EVENT_UPLOAD_PATH,
			Image::TYPE_PROFILE_SPECIFIC=>CommonFunctions::DOCTOR_UPLOAD_PATH,
			Image::TYPE_EQUIPMENT_SPECIFIC=>CommonFunctions::EQUIPMENT_UPLOAD_PATH,
			Image::TYPE_CLINIC_SPECIFIC=>CommonFunctions::ARTICLE_UPLOAD_PATH,
			Image::TYPE_SLIDER_IMAGE=>CommonFunctions::SLIDER_IMAGES_PATH,
		);
		return $paths[$type];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('active',$this->active);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('sumary_ar',$this->sumary_ar,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_id',$this->create_id);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('update_id',$this->update_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}



	public static function getGalleriesTypesList(){
		return array(
			self::TYPE_GENERAL=>'General',
			self::TYPE_CLINIC_SPECIFIC=>'Clinic Specific',
			self::TYPE_EQUIPMENT_SPECIFIC=>'Equipment Specific',
			self::TYPE_EVENT_SPECIFIC=>'Event Specific',
			self::TYPE_PROFILE_SPECIFIC=>'Profile Specific',
			self::TYPE_NEWS_SPECIFIC=>'News Specific',
		);
	}
	//Getters

	public function getTitle(){
		if(Yii::app()->language=="ar"){
			return $this->title_ar;
		}else{
			return $this->title;
		}
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gallery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
