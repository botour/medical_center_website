<?php

/**
 * This is the model class for table "tbl_article".
 *
 * The followings are the available columns in table 'tbl_article':
 * @property integer $id
 * @property integer $doctor_id
 * @property integer $active
 * @property integer $approve
 * @property string $title
 * @property string $title_ar
 * @property string $summary
 * @property string $summary_ar
 * @property string $body
 * @property string $body_ar
 * @property string $publish_date
 *
 * The followings are the available model relations:
 * @property Staff $doctor
 */
class Article extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_article';
	}

	const STATUS_APPROVED = 1;
	const STATUS_NOT_APPROVED = 0;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctor_id, active, approve, title, title_ar, summary, summary_ar, body, body_ar', 'required'),
			array('doctor_id, active, approve', 'numerical', 'integerOnly'=>true),
			array('publish_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, doctor_id, active, approve, title, title_ar, summary, summary_ar, body, body_ar, publish_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'doctor' => array(self::BELONGS_TO, 'Staff', 'doctor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('article', 'model.label.id'),
					'doctor_id' => Yii::t('article', 'model.label.doctor'),
					'active' => Yii::t('article', 'model.label.active'),
					'approve' => Yii::t('article', 'model.label.approve'),
					'title' => Yii::t('article', 'model.label.title'),
					'title_ar' => Yii::t('article', 'model.label.title_ar'),
					'summary' => Yii::t('article', 'model.label.summary'),
					'summary_ar' => Yii::t('article', 'model.label.summary_ar'),
					'body' => Yii::t('article', 'model.label.body'),
					'body_ar' => Yii::t('article', 'model.label.body_ar'),
					'publish_date' => Yii::t('article', 'model.label.publish_date'),
				);
	}

	public function delete(){
		return parent::delete();
	}

	public static function getApproveStatusArray(){
		return array(
			self::STATUS_NOT_APPROVED=>Yii::t('article','Not Approved'),
			self::STATUS_APPROVED=>Yii::t('article','Approved'),
		);
	}

	public static function getArticleStatusArray(){
		return array(
			self::STATUS_INACTIVE=>Yii::t('article','Inactive'),
			self::STATUS_ACTIVE=>Yii::t('article','Active'),
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('active',$this->active);
		$criteria->compare('approve',$this->approve);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('summary_ar',$this->summary_ar,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('body_ar',$this->body_ar,true);
		$criteria->compare('publish_date',$this->publish_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>5,
			),
		));
	}

	public function getTitle(){
		if(Yii::app()->language=="ar"){
			return $this->title_ar;
		}else{
			return $this->title;
		}
	}

	public function getSummary(){
		if(Yii::app()->language=="ar"){
			return $this->summary_ar;
		}else{
			return $this->summary;
		}
	}

	public function getBody(){
		if(Yii::app()->language=="ar"){
			return $this->body_ar;
		}else{
			return $this->body;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
