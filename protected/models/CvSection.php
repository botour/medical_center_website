<?php

/**
 * This is the model class for table "tbl_cv_section".
 *
 * The followings are the available columns in table 'tbl_cv_section':
 * @property integer $id
 * @property string $title
 * @property string $title_ar
 * @property string $body
 * @property integer $order
 * @property string $body_ar
 * @property integer $staff_id
 *
 * The followings are the available model relations:
 * @property Staff $staff
 */
class CvSection extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cv_section';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, order, title_ar, body, body_ar, staff_id', 'required'),
			array('staff_id', 'numerical', 'integerOnly'=>true),
			array('order', 'numerical', 'integerOnly'=>true),
			array('title, title_ar', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, title_ar, body, body_ar, staff_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'staff' => array(self::BELONGS_TO, 'Staff', 'staff_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('cvsection', 'model.label.id'),
					'title' => Yii::t('cvsection', 'model.label.title'),
					'title_ar' => Yii::t('cvsection', 'model.label.title_ar'),
					'body' => Yii::t('cvsection', 'model.label.body'),
					'body_ar' => Yii::t('cvsection', 'model.label.body_ar'),
					'staff_id' => Yii::t('cvsection', 'model.label.staff'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('body_ar',$this->body_ar,true);
		$criteria->compare('staff_id',$this->staff_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getTitle(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->title_ar;
		}else{
			return $this->title;
		}
	}
	public function getBody(){
		if(Yii::app()->getLanguage()=='ar'){
			return $this->body_ar;
		}else{
			return $this->body;
		}
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CvSection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
