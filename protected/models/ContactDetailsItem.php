<?php

/**
 * This is the model class for table "tbl_contact_details_item".
 *
 * The followings are the available columns in table 'tbl_contact_details_item':
 * @property integer $id
 * @property string $branch
 * @property string $branch_ar
 * @property string $email
 * @property string $tel_1
 * @property string $tel_2
 * @property string $address
 * @property string $address_ar
 * @property string $fax
 * @property string $mobile
 * @property string $mobile_2
 */
class ContactDetailsItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_contact_details_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch, branch_ar, address, address_ar', 'required'),
			array('email,tel_1,tel_2,mobile,mobile_2,fax','safe'),
			array('branch, branch_ar, email, tel_1, tel_2, address, address_ar, fax, mobile, mobile_2', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, branch, branch_ar, email, tel_1, tel_2, address, address_ar, fax, mobile, mobile_2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('contactdetailsitem', 'model.label.id'),
					'branch' => Yii::t('contactdetailsitem', 'model.label.branch'),
					'branch_ar' => Yii::t('contactdetailsitem', 'model.label.branch_ar'),
					'email' => Yii::t('contactdetailsitem', 'model.label.email'),
					'tel_1' => Yii::t('contactdetailsitem', 'model.label.tel_1'),
					'tel_2' => Yii::t('contactdetailsitem', 'model.label.tel_2'),
					'address' => Yii::t('contactdetailsitem', 'model.label.address'),
					'address_ar' => Yii::t('contactdetailsitem', 'model.label.address_ar'),
					'fax' => Yii::t('contactdetailsitem', 'model.label.fax'),
					'mobile' => Yii::t('contactdetailsitem', 'model.label.mobile'),
					'mobile_2' => Yii::t('contactdetailsitem', 'model.label.mobile_2'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('branch',$this->branch,true);
		$criteria->compare('branch_ar',$this->branch_ar,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('tel_1',$this->tel_1,true);
		$criteria->compare('tel_2',$this->tel_2,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('address_ar',$this->address_ar,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('mobile_2',$this->mobile_2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getBranch(){
		if(Yii::app()->language=="ar"){
			return $this->branch_ar;
		}else{
			return $this->branch;
		}
	}
	public function getAddress(){
		if(Yii::app()->language=="ar"){
			return $this->address_ar;
		}else{
			return $this->address;
		}
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContactDetailsItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
