<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $login_name
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $password
 * @property string $create_date
 * @property integer $create_id
 * @property string $update_date
 * @property integer $update_id
 * @property integer $status
 * @property integer $doctor_id
 */
class User extends SuperModel
{
	/**
	 * @return string the associated database table name
	 */
	const STATUS_ONLINE = 1;
	const STATUS_OFFLINE = 0;
	const STATUS_BREAK = 2;

	const ROLE_ADMIN = 0;
	const ROLE_EDITOR = 1;


	public $role;
	const SALT = "jas#&12a1@(/as";

	public function tableName()
	{
		return 'tbl_user';
	}

	public function getName(){
		return $this->first_name." ".$this->last_name;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username,login_name,first_name,last_name,password,role', 'required','on'=>'create'),
			array('username,login_name,first_name,last_name,password,role,doctor_id', 'required','on'=>'create-doctor'),
			array('username,login_name, first_name,last_name,role', 'required','on'=>'update'),
			array('username,login_name, first_name,last_name,role,doctor_id', 'required','on'=>'update-editor'),
			array('email,middle_name','safe','on'=>'create'),
			array('email,middle_name','safe','on'=>'update'),
			array('create_id, update_id, status', 'numerical', 'integerOnly'=>true),
			array('username, login_name, first_name, last_name, middle_name, email, password', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, login_name, first_name, last_name, middle_name, email, password, create_date, create_id, update_date, update_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('user','model.label.id'),
			'username' => Yii::t('user','model.label.username'),
			'login_name' => Yii::t('user','model.label.login_name'),
			'first_name' => Yii::t('user','model.label.first_name'),
			'last_name' => Yii::t('user','model.label.last_name'),
			'middle_name' => Yii::t('user','model.label.middle_name'),
			'email' => Yii::t('user','model.label.email'),
			'password' => Yii::t('user','model.label.password'),
			'create_date' => Yii::t('user','model.label.create_date'),
			'create_id' => Yii::t('user','model.label.create_id'),
			'update_date' => Yii::t('user','model.label.update_date'),
			'update_id' => Yii::t('user','model.label.update_id'),
			'status' => Yii::t('user','model.label.status'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('login_name',$this->login_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_id',$this->create_id);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('update_id',$this->update_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function afterValidate()
	{
		parent::afterValidate();
		if ($this->isNewRecord) {
			$this->password = $this->hashPassword($this->password);
		}
	}

	protected function beforeSave(){
		$this->first_name = strtolower($this->first_name);
		$this->last_name = strtolower($this->last_name);
		$this->middle_name = strtolower($this->middle_name);
		$this->username = strtolower($this->username);
		$this->login_name = strtolower($this->login_name);
		return parent::beforeSave();
	}
	public function hashPassword($password)
	{
		return sha1(self::SALT . $password);

	}
	public function validatePassword($password)
	{
		return $this->password === $this->hashPassword($password);
	}
	/*
	 * ===========================
	 * Getters
	 * ===========================
	 */


	public function getFirstName(){
		return ucwords($this->first_name);
	}
	public function getLastName(){
		return ucwords($this->last_name);
	}
	public function getMiddleName()
	{
		return ucwords($this->middle_name);
	}
	public function getLoginName()
	{
		return $this->login_name;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getEmail(){
		return $this->email;
	}

	/*
	 * ===============================
	 * Authentication/Authorization
	 * ===============================
	 */

	public static function getRolesOptionsList()
	{
		return array(
			self::ROLE_ADMIN => 'Admin',
			self::ROLE_EDITOR => 'Doctor',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
