<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 2/28/15
 * Time: 2:47 PM
 */

class SuperModel extends CActiveRecord{

    public $skipBeforeSaveEvent = false;

    protected function beforeSave(){
        if($this->skipBeforeSaveEvent){
            return parent::beforeSave();
        }
        if($this->isNewRecord){
            $this->create_date = date('Y-m-d H:i:s');
        }
        $this->update_date = date('Y-m-d H:i:s');
        return parent::beforeSave();
    }

}