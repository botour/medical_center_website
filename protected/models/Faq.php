<?php

/**
 * This is the model class for table "tbl_faq".
 *
 * The followings are the available columns in table 'tbl_faq':
 * @property integer $id
 * @property integer $department_id
 * @property integer $order
 * @property string $question
 * @property string $question_ar
 * @property string $answer
 * @property string $answer_ar
 *
 * The followings are the available model relations:
 * @property Department $department
 */
class Faq extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_faq';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('department_id, order, question, question_ar, answer, answer_ar', 'required','on'=>'create'),
			array('department_id, question, question_ar, answer, answer_ar', 'required','on'=>'update'),

			array('department_id, order', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, department_id, order, question, question_ar, answer, answer_ar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'department' => array(self::BELONGS_TO, 'Department', 'department_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('faq', 'model.label.id'),
					'department_id' => Yii::t('faq', 'model.label.department'),
					'order' => Yii::t('faq', 'model.label.order'),
					'question' => Yii::t('faq', 'model.label.question'),
					'question_ar' => Yii::t('faq', 'model.label.question_ar'),
					'answer' => Yii::t('faq', 'model.label.answer'),
					'answer_ar' => Yii::t('faq', 'model.label.answer_ar'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function getQuestion(){
		if(Yii::app()->language == 'ar'){
			return $this->question_ar;
		}else{
			return $this->question;
		}
	}
	public function getAnswer(){
		if(Yii::app()->language == 'ar'){
			return $this->answer_ar;
		}else{
			return $this->answer;
		}
	}
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('order',$this->order);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('question_ar',$this->question_ar,true);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('answer_ar',$this->answer_ar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Faq the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
