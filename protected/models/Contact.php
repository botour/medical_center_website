<?php

/**
 * This is the model class for table "tbl_contact_item".
 *
 * The followings are the available columns in table 'tbl_contact_item':
 * @property integer $id
 * @property string $full_name
 * @property string $full_name_ar
 * @property string $email
 * @property integer $contact_type_id
 * @property string $body
 */
class Contact extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_contact_item';
	}

	const CONTACT_TYPE_COMPLAINT = 0;
	const CONTACT_TYPE_INQUIRY = 1;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('full_name,full_name_ar,email,contact_type_id,body', 'required'),
			array('contact_type_id', 'numerical', 'integerOnly'=>true),
			array('full_name, full_name_ar, email', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, full_name, full_name_ar, email, contact_type_id, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactType'=>array(self::BELONGS_TO,'ContactType','contact_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('contact', 'model.label.id'),
					'full_name' => Yii::t('contact', 'model.label.full_name'),
					'full_name_ar' => Yii::t('contact', 'model.label.full_name_ar'),
					'email' => Yii::t('contact', 'model.label.email'),
					'contact_type_id' => Yii::t('contact', 'model.label.contact_type'),
					'body' => Yii::t('contact', 'model.label.body'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/*public static function getStaffBranchArray()
	{
		return array(
			self::CONTACT_TYPE_COMPLAINT => Yii::t('contact', 'contact.type.inquiry'),
			self::CONTACT_TYPE_INQUIRY => Yii::t('contact', 'contact.type.complaint'),
		);
	}*/
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('full_name_ar',$this->full_name_ar,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('contact_type_id',$this->contact_type_id);
		$criteria->compare('body',$this->body,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
