<?php

/**
 * This is the model class for table "tbl_department_page_section".
 *
 * The followings are the available columns in table 'tbl_department_page_section':
 * @property integer $id
 * @property integer $type
 * @property integer $order
 * @property string $title
 * @property string $title_ar
 * @property string $summary
 * @property string $summary_ar
 * @property string $body
 * @property string $body_ar
 * @property integer $gallary_id
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Gallery $gallary
 */
class CustomSection extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_department_page_section';
	}

	const TYPE_OTHER = 0;
	const TYPE_DOCTOR = 1;
	const TYPE_EQUIPMENT =2;

	const STATUS_ACTIVE =1;
	const STATUS_INACTIVE = 0;


	const CUSTOM_SECTION_CREATE = 1;
	const CUSTOM_SECTION_UPDATE = 2;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('active,department_id, order, title, title_ar', 'required','on'=>'create'),
			array('title, title_ar', 'required','on'=>'update'),
			array('body,body_ar,summary_ar,summary','safe','on'=>'update'),
			array('type, order, gallary_id, active', 'numerical', 'integerOnly'=>true),
			array('summary, summary_ar, body, body_ar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, order, title, title_ar, summary, summary_ar, body, body_ar, gallary_id, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gallary' => array(self::BELONGS_TO, 'Gallery', 'gallary_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('customsection', 'model.label.id'),
					'type' => Yii::t('customsection', 'model.label.type'),
					'order' => Yii::t('customsection', 'model.label.order'),
					'title' => Yii::t('customsection', 'model.label.title'),
					'title_ar' => Yii::t('customsection', 'model.label.title_ar'),
					'summary' => Yii::t('customsection', 'model.label.summary'),
					'summary_ar' => Yii::t('customsection', 'model.label.summary_ar'),
					'body' => Yii::t('customsection', 'model.label.body'),
					'body_ar' => Yii::t('customsection', 'model.label.body_ar'),
					'gallary_id' => Yii::t('customsection', 'model.label.gallary'),
					'active' => Yii::t('customsection', 'model.label.active'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('order',$this->order);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('summary_ar',$this->summary_ar,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('body_ar',$this->body_ar,true);
		$criteria->compare('gallary_id',$this->gallary_id);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function getTitle(){
		if(Yii::app()->language=="ar"){
			return $this->title_ar;
		}else{
			return $this->title;
		}
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomSection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
