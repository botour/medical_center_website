<?php

/**
 * This is the model class for table "tbl_content".
 *
 * The followings are the available columns in table 'tbl_content':
 * @property integer $id
 * @property string $title
 * @property string $title_ar
 * @property string $text
 * @property string $text_ar
 * @property string $image
 * @property integer $active
 * @property integer $type
 * @property integer $order
 * @property string $create_date
 * @property string $update_date
 * @property string $publish_date
 * @property integer $summary_block_id
 *
 * The followings are the available model relations:
 * @property Block[] $blocks
 * @property Block $summaryBlock
 */
class Content extends SuperModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_content';
	}

	const TYPE_ARTICLE = 0;
	const TYPE_NEWS = 1;
	const TYPE_EVENT = 2;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	public $primaryImage;


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, title_ar','required','on'=>'create-news'),
			array('active, type, order, summary_block_id', 'numerical', 'integerOnly'=>true),
			array('publish_date', 'length', 'max'=>45),
			array('primaryImage','file','maxSize'=>2*1024*1024,'maxFiles'=>1,'types'=>'gif,jpg,png','allowEmpty'=>true),

			array('text, text_ar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, title_ar, text, text_ar, active, type, order, create_date, update_date, publish_date, summary_block_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'blocks' => array(self::HAS_MANY, 'Block', 'content_id'),
			'summaryBlock' => array(self::BELONGS_TO, 'Block', 'summary_block_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
					'id' => Yii::t('content', 'model.label.id'),
					'title' => Yii::t('content', 'model.label.title'),
					'title_ar' => Yii::t('content', 'model.label.title_ar'),
					'text' => Yii::t('content', 'model.label.text'),
					'text_ar' => Yii::t('content', 'model.label.text_ar'),
					'image_id' => Yii::t('content', 'model.label.image'),
					'primaryImage' => Yii::t('content', 'model.label.primary.image.file'),


					'active' => Yii::t('content', 'model.label.active'),
					'type' => Yii::t('content', 'model.label.type'),

					'order' => Yii::t('content', 'model.label.order'),
					'create_date' => Yii::t('content', 'model.label.create_date'),
					'update_date' => Yii::t('content', 'model.label.update_date'),
					'publish_date' => Yii::t('content', 'model.label.publish_date'),
					'summary_block_id' => Yii::t('content', 'model.label.summary_block'),
				);
	}

	public function delete(){

		return parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_ar',$this->title_ar,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('text_ar',$this->text_ar,true);
		$criteria->compare('image_id',$this->image_id,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('type',$this->type);

		$criteria->compare('publish_date',$this->publish_date,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>5,
			),
		));
	}

	public static function getContentTypeArray(){
		return array(
			self::TYPE_ARTICLE=>Yii::t('content','model.type.article'),
			self::TYPE_NEWS=>Yii::t('content','model.type.news'),
		);
	}

	public static function getContentTypeUri(){
		return array(
			Yii::app()->createUrl('article/create')=>Yii::t('content','model.type.article'),
			Yii::app()->createUrl('content/newsCreate')=>Yii::t('content','model.type.news'),
		);
	}


	public function getTitle(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->title_ar;
		}else{
			return $this->title;
		}
	}
	public function getText(){
		if(Yii::app()->getLanguage()=="ar"){
			return $this->text_ar;
		}else{
			return $this->text;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Content the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
