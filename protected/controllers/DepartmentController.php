<?php

class DepartmentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'checkSideBarSubId +index,create',
			'checkAccess',
			'checkAdminAccess',

			'postOnly + delete', // we only allow deletion via POST request
			'AjaxAction +'.implode(',',$this->ajaxActions()),
		);
	}

	public function ajaxActions(){
		return array(
			'createFAQItem',
			'deleteFAQ',
			'updateFAQ',
			'createCustomSection',
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel(intval($id));
		$faqs = $model->faqs;
		$this->pageTitle = Yii::t('department', 'page.title.view.department');
		$this->contentTitle = Yii::t('department', 'controller.page.view.content.title')." : ".$model->name;


		$services = ServiceItem::model()->findAll('department_id=:department_id',array(
			':department_id'=>$model->id,
		));

		$customSections = CustomSection::model()->findAll('department_id=:department_id',array(
			':department_id'=>$model->id,
		));

		// Search Model for Equipments
		$equipmentModel = new Equipment('search');
		$equipmentModel->department_id = $model->id;

		$this->render('view',array(
			'model'=>$model,
			'faqs'=>$faqs,
			'services'=>$services,
			'customSections'=>$customSections,
			'equipmentModel'=>$equipmentModel,
		));
	}



	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->pageTitle = Yii::t('department', 'page.title.create.department');
		$this->contentTitle = Yii::t('department', 'controller.page.create.content.title');

		// $this->updateImagesListJSONFile();
		$model=new Department('create');

		if(isset($_POST['Department']))
		{
			$model->attributes=$_POST['Department'];
			$model->active = Department::STATUS_INACTIVE;
			if($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->pageTitle = Yii::t('department', 'page.title.update.department');
		$this->contentTitle = Yii::t('department', 'controller.page.update.content.title');

		$model=$this->loadModel($id);
		$model->setScenario("update");
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Department']))
		{
			$model->attributes=$_POST['Department'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->pageTitle = Yii::t('department', 'page.title.index.department');
		$this->contentTitle = Yii::t('department', 'controller.page.index.content.title');

		$departmentModel = new Department('search');
		$departmentModel->unsetAttributes();
		
		$this->render('index',array(
			'departmentModel'=>$departmentModel,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Department('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Department']))
			$model->attributes=$_GET['Department'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/*
	 *
	 * FAQ management
	 *
	 */

	public function actionCreateFAQItem(){
		if(isset($_POST['Faq']['department_id'])){
			$department = $this->loadModel(intval($_POST['Faq']['department_id']));
			$faqs = Faq::model()->findAll('department_id=:department_id',array(
				':department_id'=>$department->id,
			));
			$faqModel = new Faq('create');
			$faqModel->attributes = $_POST['Faq'];
			if(count($faqs)==0){
				$faqModel->order = 1;
			}else{
				$count = count($faqs);
				$orderArray = range(1,$count+1,1);
				foreach($faqs as $faq) {
					$orderKey = array_search($faq->order,$orderArray);
					unset($orderArray[$orderKey]);
				}
				foreach($orderArray as $orderValue){
					$faqModel->order = $orderValue;
				}
			}
			if($faqModel->save()){
				echo CJSON::encode(array(
					'status'=>true,
					'order'=>$faqModel->order,
					'message'=>Yii::t('department','alert.faq.create.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'order'=>$faqModel->order,
					'message'=>Yii::t('department','alert.faq.create.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionDeleteFAQ(){
		if(isset($_POST['id'])){
			$faqModel = $this->loadFAQModel(intval($_POST['id']));
			if($faqModel->delete()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('department','alert.faq.delete.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('department','alert.faq.delete.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionUpdateFAQ(){
		if(isset($_POST['Faq']['id'])){
			$department = $this->loadModel(intval($_POST['Faq']['department_id']));
			$faqModel = $this->loadFAQModel($_POST['Faq']['id']);
			if($faqModel->department_id==$department->id){
				$faqModel->attributes = $_POST['Faq'];
				if($faqModel->save()){
					echo CJSON::encode(array(
						'status'=>true,
						'message'=>Yii::t('department','alert.faq.update.success'),
					));
				}else{
					echo CJSON::encode(array(
						'status'=>false,
						'message'=>Yii::t('department','alert.faq.update.error'),
					));
				}
			}else{
				throw new CHttpException(400);
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionUpdateFQAList($department_Id){
		$department = $this->loadModel(intval($department_Id));
		echo $this->renderPartial('_faq_list',array(
			'faqs'=>$department->faqs(array(
				'order'=>'faqs.order ASC',
			))
		));
	}

	public function actionGetCreateFAQForm($department_id){
		$model = $this->loadModel(intval($department_id));
		if(!is_null($model)){
			$faqModel = new Faq;
			$faqModel->department_id = $model->id;
			echo $this->renderPartial('_faq_form',array(
				'faqModel'=>$faqModel,
			),false,true);
		}else{
			throw new CHttpException(400);
		}

	}
	/*
	 *
	 * End of FAQ management
	 *
	 */

	/*
	 *
	 *
	 * Equipment Management
	 *
	 */

	public function actionEquipmentCreate($dID){
		$this->pageTitle = Yii::t('department', 'page.title.create.equipment');
		$this->contentTitle = Yii::t('department', 'controller.page.create.equipment.content.title');

		$departmentModel = $this->loadModel(intval($dID));
		$equipmentCreateModel = new Equipment('create');
		$equipmentCreateModel->department_id = $departmentModel->id;
		$equipmentCreateModel->order = 0;

		if(isset($_POST['Equipment'])){
			$equipmentCreateModel->attributes = $_POST['Equipment'];
			$equipmentCreateModel->active = Equipment::STATUS_ACTIVE;
			if($equipmentCreateModel->save()){
				$this->redirect(array('equipmentView','id'=>$equipmentCreateModel->id));
			}
		}
		$this->render('equipment_create',array(
			'departmentModel'=>$departmentModel,
			'equipmentCreateModel'=>$equipmentCreateModel,
		));
	}

	public function actionEquipmentView($id){
		$equipmentModel = $this->loadEquipmentModel(intval($id));

		$this->pageTitle = Yii::t('department', 'page.title.view.equipment')." : ".$equipmentModel->getName();
		$this->contentTitle = Yii::t('department', 'controller.page.view.equipment.content.title')." : ".$equipmentModel->getName();

		$this->render('equipment_view',array(
			'equipmentModel'=>$equipmentModel,
		));

	}

	public function actionEquipmentImageCreate($eID){
		$equipmentModel = $this->loadEquipmentModel(intval($eID));

		$this->pageTitle = Yii::t('department', 'page.title.create.equipment.image')." : For ".$equipmentModel->getName();
		$this->contentTitle = Yii::t('department', 'controller.page.create.equipment.image.content.title')." : For ".$equipmentModel->getName();

		$imageModel = new Image('create-for-equipment');

		if(isset($_POST['Image'])){
			$imageModel->image = CUploadedFile::getInstance($imageModel, 'image');
			$imageModel->attributes = $_POST['Image'];
			if($imageModel->validate()){
				$imageModel->url = sha1($imageModel->image->name . uniqid() . time()) . '.' . $imageModel->image->extensionName;
				$imageModel->type = Image::TYPE_EQUIPMENT_SPECIFIC;
				$imageModel->active = Image::STATUS_ACTIVE;
				if ($imageModel->image->saveAs(CommonFunctions::IMAGES_PATH . CommonFunctions::EQUIPMENT_IMAGES_PATH . $imageModel->url)) {
					$imageModel->save(false);
					Yii::app()->db->createCommand()->insert('tbl_equip_image',array(
						'equipment_id'=>$equipmentModel->id,
						'image_id'=>$imageModel->id,
					));
					$imageProcessor = new ImageProcessor();
					$imageProcessor->source_path = CommonFunctions::IMAGES_PATH . CommonFunctions::EQUIPMENT_IMAGES_PATH . $imageModel->url;
					$imageProcessor->target_path = CommonFunctions::IMAGES_PATH . CommonFunctions::EQUIPMENT_IMAGES_PATH . CommonFunctions::EQUIPMENT_IMAGES_THUMBS_PATH . $imageModel->url;
					$imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1);
					$this->redirect(array('equipmentView','id'=>$equipmentModel->id));
				}
			}
		}

		$this->render('equipment_image_create',array(
			'equipmentModel'=>$equipmentModel,
			'imageModel'=>$imageModel,
		));
	}



	public function actionEquipmentUpdate($id){
		$equipmentModel = $this->loadEquipmentModel(intval($id));

		$equipmentModel->setScenario('update');
		$this->pageTitle = Yii::t('department', 'page.title.update.equipment')." : ".$equipmentModel->getName();
		$this->contentTitle = Yii::t('department', 'controller.page.update.equipment.content.title')." : ".$equipmentModel->getName();

		if(isset($_POST['Equipment'])){
			$equipmentModel->attributes = $_POST['Equipment'];
			if($equipmentModel->save()){
				$this->redirect(array('view','id'=>$equipmentModel->department->id,'ref'=>'equip_update'));
			}
		}
		$this->render('equipment_update',array(
			'equipmentUpdateModel'=>$equipmentModel,
		));
	}


	public function actionEquipmentDelete(){
		if (isset($_POST['e_id'])) {
			$equipmentModelId = intval($_POST['e_id']);
			$equipmentModel = $this->loadEquipmentModel($equipmentModelId);
			$equipmentModel->delete();
			$this->redirect(array('department/view','id'=>$equipmentModel->department->id,'ref'=>'equip_update'));
		}
	}

	public function actionEquipmentActive()
	{
		if (isset($_POST['e_id'])) {
			$equipmentModelId = intval($_POST['e_id']);
			$equipmentModel = $this->loadEquipmentModel($equipmentModelId);
			if($equipmentModel->active==Equipment::STATUS_ACTIVE){
				$equipmentModel->active = Equipment::STATUS_INACTIVE;
			}else if($equipmentModel->active==Equipment::STATUS_INACTIVE){
				$equipmentModel->active = Equipment::STATUS_ACTIVE;

			}
			if($equipmentModel->save(false)){
				$this->redirect(array('view','id'=>$equipmentModel->department->id,'ref'=>'equip_update'));
			}
		}
	}



	/*
	 *
	 * End of Equipment Management
	 *
	 */

	/*
	 *
	 *
	 * Service Item Management
	 *
	 *
	 */

	public function actionGetCreateServiceItemForm($department_id){
		$model = $this->loadModel(intval($department_id));
		if(!is_null($model)){
			$serviceItemModel = new ServiceItem;
			$serviceItemModel->department_id = $model->id;
			echo $this->renderPartial('_services_form',array(
				'serviceItemModel'=>$serviceItemModel,
			),false,true);
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionGetUpdateServiceItemForm($id){
		$serviceItemModel = $this->loadServiceItem(intval($id));
		echo $this->renderPartial('_services_form',array(
			'serviceItemModel'=>$serviceItemModel,
		),false, true);
	}

	public function actionUpdateServiceItemList($department_Id){
		$model = $this->loadModel(intval($department_Id));
		$services = ServiceItem::model()->findAll('department_id=:department_id',array(
			':department_id'=>$model->id,
		));
		echo $this->renderPartial('_services_list',array(
			'services'=>$services,
		),false,true);
	}

	public function actionDeleteServiceItem(){
		if(isset($_POST['id'])){
			$serviceItem = $this->loadServiceItem(intval($_POST['id']));
			if($serviceItem->delete()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('department','alert.service.item.delete.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('department','alert.service.item.delete.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionCreateServiceItem(){
		if(isset($_POST['ServiceItem']['department_id'])){
			$department = $this->loadModel(intval($_POST['ServiceItem']['department_id']));

			$serviceItemModel = new ServiceItem;
			$serviceItemModel->attributes = $_POST['ServiceItem'];

			if($serviceItemModel->save()){
				echo CJSON::encode(array(
					'status'=>true,

					'message'=>Yii::t('department','alert.service.item.create.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,

					'message'=>Yii::t('department','alert.service.item.create.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionUpdateServiceItem(){
		if(isset($_POST['ServiceItem']['id'])){
			$department = $this->loadModel(intval($_POST['ServiceItem']['department_id']));
			$serviceItemModel = $this->loadServiceItem($_POST['ServiceItem']['id']);
			if($serviceItemModel->department_id==$department->id){
				$serviceItemModel->attributes = $_POST['ServiceItem'];
				if($serviceItemModel->save()){
					echo CJSON::encode(array(
						'status'=>true,
						'message'=>Yii::t('department','alert.faq.update.success'),
					));
				}else{
					echo CJSON::encode(array(
						'status'=>false,
						'message'=>Yii::t('department','alert.faq.update.error'),
					));
				}
			}else{
				throw new CHttpException(400);
			}
		}else{
			throw new CHttpException(400);
		}
	}
	public function actionGetUpdateFAQForm($id){
		$faqModel = $this->loadFAQModel(intval($id));
		echo $this->renderPartial('_faq_form',array(
			'faqModel'=>$faqModel,
		),false, true);
	}

	public function actionUpdateFAQList($department_id){
		$departmentModel = Department::model()->with('faqs')->loadModel(intval($department_id));
		echo $this->renderPartial('_faq_list',array(
			'faqs'=>$departmentModel->faqs,
		));
	}




	/*
	 *
	 *
	 * End of Service Item Management
	 *
	 */


	/*
	 *
	 * Custom Section management
	 *
	 */

	public function actionCreateCustomSection(){
		if(isset($_POST['CustomSection'])){
			$model = new CustomSection('create');
			$department = $this->loadModel(intval($_POST['CustomSection']['department_id']));

			$model->attributes = $_POST['CustomSection'];
			$model->active = CustomSection::STATUS_INACTIVE;
			// Set the order
			$customs = CustomSection::model()->findAll('department_id=:department_id',array(
				':department_id'=>$department->id,
			));
			$model->order = 1;

			if($model->save()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('department','alert.custom.section.create.success'),
					'type'=>CustomSection::CUSTOM_SECTION_CREATE,
					'id'=>$model->id,
					'title'=>$model->getTitle(),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('department','alert.custom.section.create.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}
	public function actionGetCustomSectionCreateForm($department_id){
		$department = $this->loadModel(intval($department_id));
		$model = new CustomSection('create');
		$model->department_id = $department->id;
		echo $this->renderPartial('partial/_custom_section_form',array(
			'model'=>$model,
		),false,true);
	}
	public function actionEditCustomSection(){

	}
	public function actionGetCustomSectionEditForm($id){

	}
	public function actionDeleteCustomSection(){
		if(isset($_POST['id'])){
			$model = $this->loadCustomSectionModel(intval($_POST['id']));
			if($model->delete()){
				echo CJSON::encode(array(
					'status'=>true,
					'id'=>$model->id,
					'message'=>Yii::t('department','alert.custom.section.delete.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('department','alert.custom.section.delete.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}


	public function actionGetCustomSectionTabContent($custom_section_id){
		$customSection = $this->loadCustomSectionModel(intval($custom_section_id));
		echo $this->renderPartial('partial/_custom_section_tab',array(
			'customSection'=>$customSection,
		),false,true);
	}

	public function renderEquipmentOptions($data, $row)
	{
		$this->renderPartial('partial/equipment-options', array(
			'data' => $data,
		), false, false);
	}

	/*
	 *
	 * End of Custom Section management
	 *
	 */



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Department the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Department::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadServiceItem($id){
		$model=ServiceItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadFAQModel($id)
	{
		$model=Faq::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadCustomSectionModel($id)
	{
		$model=CustomSection::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadEquipmentModel($id)
	{
		$model=Equipment::model()->with('department')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Department $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='department-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


}
