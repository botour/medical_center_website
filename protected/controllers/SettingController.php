<?php

class SettingController extends Controller
{
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('setting', 'page.title.index');
        $this->contentTitle = Yii::t('setting', 'controller.page.index.content.title');
        $this->render('index');
    }

    // Uncomment the following methods and override them if needed

    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'checkSideBarSubId +index',
            'checkAccess',
            'checkAdminAccess',
        );
    }


    public function actionBranchMgr()
    {
        $this->pageTitle = Yii::t('setting', 'page.title.branch.index');
        $this->contentTitle = Yii::t('setting', 'controller.page.branch.index.content.title');
    }


    /*
     *
     * Branch Setting Management
     *
     */

    public function actionBranchIndex()
    {
        $this->pageTitle = Yii::t('setting', 'page.title.index.branch');
        $this->contentTitle = Yii::t('setting', 'controller.page.index.branch.content.title');

        $branchSearchModel = new branch('search');
        $branchSearchModel->unsetAttributes();

        $this->render('branch/branch_index', array(
            'branchSearchModel' => $branchSearchModel,
        ));
    }

    public function actionBranchCreate()
    {
        $this->pageTitle = Yii::t('setting', 'page.title.create.branch');
        $this->contentTitle = Yii::t('setting', 'controller.page.create.branch.content.title');

        $branchCreateModel = new Branch;

        if (isset($_POST['Branch'])) {
            $branchCreateModel->attributes = $_POST['Branch'];
            if ($branchCreateModel->save()) {
                $this->redirect(array('setting/branchIndex'));
            }
        }
        $this->render('branch/branch_create', array(
            'branchCreateModel' => $branchCreateModel,
        ));
    }

    public function actionBranchUpdate($branch_id)
    {
        $this->pageTitle = Yii::t('setting', 'page.title.update.branch');
        $this->contentTitle = Yii::t('setting', 'controller.page.update.branch.content.title');

        $branchUpdateModel = $this->loadbranchType(intval($branch_id));

        if (isset($_POST['Branch'])) {
            $branchUpdateModel->attributes = $_POST['Branch'];
            if ($branchUpdateModel->save()) {
                $this->redirect(array('setting/branchIndex'));
            }
        }

        $this->render('branch/branch_update', array(
            'branchUpdateModel' => $branchUpdateModel,
        ));

    }

    public function actionBranchDelete()
    {

        if (isset($_POST['b_id'])) {
            $branchModelId = intval($_POST['b_id']);
            $branchModel = $this->loadbranchType($branchModelId);
            $branchModel->delete();
            $this->redirect(array('setting/branchIndex'));
        }

    }

    public function renderBranchOptions($data, $row)
    {
        $this->renderPartial('branch/partial/branch-options', array(
            'data' => $data,
        ), false, false);
    }

    public function loadBranchType($id)
    {
        $model = Branch::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /*
     *
     * End of Branch Setting Management
     *
     */
    /*
     *
     * Contact Types Setting Management
     *
     */

    public function actionContactTypeIndex()
    {
        $this->pageTitle = Yii::t('setting', 'page.title.index.contact.type');
        $this->contentTitle = Yii::t('setting', 'controller.page.index.contact.type.content.title');

        $contactSearchModel = new ContactType('search');
        $contactSearchModel->unsetAttributes();

        $this->render('contact-type/contact_index', array(
            'contactSearchModel' => $contactSearchModel,
        ));
    }

    public function actionContactTypeCreate()
    {
        $this->pageTitle = Yii::t('setting', 'page.title.create.contact.type');
        $this->contentTitle = Yii::t('setting', 'controller.page.create.contact.type.content.title');

        $contactTypeCreateModel = new ContactType;

        if (isset($_POST['ContactType'])) {
            $contactTypeCreateModel->attributes = $_POST['ContactType'];
            if ($contactTypeCreateModel->save()) {
                $this->redirect(array('setting/contactTypeIndex'));
            }
        }
        $this->render('contact-type/contact_create', array(
            'contactTypeCreateModel' => $contactTypeCreateModel,
        ));
    }

    public function actionContactTypeUpdate($contact_id)
    {
        $this->pageTitle = Yii::t('setting', 'page.title.update.contact.type');
        $this->contentTitle = Yii::t('setting', 'controller.page.update.contact.type.content.title');

        $contactTypeUpdateModel = $this->loadContactType(intval($contact_id));

        if (isset($_POST['ContactType'])) {
            $contactTypeUpdateModel->attributes = $_POST['ContactType'];
            if ($contactTypeUpdateModel->save()) {
                $this->redirect(array('setting/contactTypeIndex'));
            }
        }

        $this->render('contact-type/contact_update', array(
            'contactTypeUpdateModel' => $contactTypeUpdateModel,
        ));

    }

    public function actionContactTypeDelete()
    {
        if (isset($_POST['c_id'])) {
            $contactTypeModelId = intval($_POST['c_id']);
            $contactTypeModel = $this->loadContactType($contactTypeModelId);
            $contactTypeModel->delete();
            $this->redirect(array('setting/contactTypeIndex'));
        }
    }

    public function renderContactTypeOptions($data, $row)
    {
        $this->renderPartial('contact-type/partial/contact-options', array(
            'data' => $data,
        ), false, false);
    }

    public function loadContactType($id)
    {
        $model = ContactType::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /*
     *
     * End of Branch Setting Management
     *
     */


}