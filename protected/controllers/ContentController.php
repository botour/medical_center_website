<?php

class ContentController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'postOnly + delete', // we only allow deletion via POST request

            'checkSideBarSubId +index,create',
            'checkAccess',
            'checkAdminAccess',

        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = Yii::t('content', 'page.title.create.content');
        $this->contentTitle = Yii::t('content', 'controller.page.create.content.title');
        //$this->updateImagesListJSONFile();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Content'])) {
            $model = new Content('create');
            $model->attributes = $_POST['Content'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create');
    }


    public function actionNewsCreate()
    {
        $this->pageTitle = Yii::t('content', 'page.title.create.news.content');
        $this->contentTitle = Yii::t('content', 'controller.page.create.news.content.title');

        $newsContentModel = new Content('create-news');
        $blockModel = new Block;

        //Validation Variables
        $newsContentInfoValid = false;
        $blockModelInfoValid = false;
        $primaryImageInfoValid = true;
        $primaryImageExists = false;


        if (isset($_POST['Content']) && isset($_POST['Block'])) {
            // Fill the Content Model
            $newsContentModel->active = Content::STATUS_INACTIVE;
            $newsContentModel->order = 0;
            $newsContentModel->type = Content::TYPE_NEWS;

            $newsContentModel->title = $_POST['Content']['title'];
            $newsContentModel->title_ar = $_POST['Content']['title_ar'];
            $newsContentModel->primaryImage = CUploadedFile::getInstance($newsContentModel, 'primaryImage');

            $blockModel->type = $_POST['Block']['type'];
            // Setup the appropriate Block Model
            switch ($blockModel->type) {
                case Block::TYPE_TEXT_ONLY:
                    $blockModel->setScenario('create-text-only');
                    break;
                case Block::TYPE_IMAGE_ONLY:
                    $blockModel->setScenario('create-image-only');
                    $blockModel->image1 = CUploadedFile::getInstance($blockModel, 'image1');
                    break;
                case Block::TYPE_TEXT_PLUS_ONE_IMAGE:
                    $blockModel->setScenario('create-text-image1');
                    $blockModel->image1 = CUploadedFile::getInstance($blockModel, 'image1');

                    break;
                case Block::TYPE_TEXT_PLUS_TWO_IMAGES:
                    $blockModel->setScenario('create-text-image2');
                    $blockModel->image1 = CUploadedFile::getInstance($blockModel, 'image1');
                    $blockModel->image2 = CUploadedFile::getInstance($blockModel, 'image2');
                    break;
                case Block::TYPE_TEXT_PLUS_THREE_IMAGES:
                    $blockModel->setScenario('create-text-image3');
                    $blockModel->image1 = CUploadedFile::getInstance($blockModel, 'image1');
                    $blockModel->image2 = CUploadedFile::getInstance($blockModel, 'image2');
                    $blockModel->image3 = CUploadedFile::getInstance($blockModel, 'image3');
                    break;
                default:
                    throw new CHttpException(400);
            }
            //Add Block Images and Text
            $blockModel->attributes = $_POST['Block'];
            if (!$blockModel->type == Block::TYPE_IMAGE_ONLY) {
                $blockModel->text = $_POST['Block']['text'];
                $blockModel->text_ar = $_POST['Block']['text_ar'];
            }

            if ($newsContentModel->validate() & $blockModel->validate()) {
                $newsContentInfoValid = true;
                $blockModelInfoValid = true;
                // Add the primary image of the content (if exists)
                $image = new Image('create-for-content');
                if (!is_null($newsContentModel->primaryImage)) {
                    $primaryImageExists = true;
                    $image->active = Image::STATUS_ACTIVE;
                    $image->type = Image::TYPE_NEWS_SPECIFIC;
                    $image->image = $newsContentModel->primaryImage;
                    if ($image->validate(array('image', 'type'))) {
                        $image->url = sha1($image->image->name . uniqid() . time()) . '.' . $image->image->extensionName;
                        $image->name = $newsContentModel->title;
                        $image->name_ar = $newsContentModel->title_ar;
                        $primaryImageInfoValid = $image->validate();

                    }
                }

                // Check if the form is valid

                $valid = $blockModelInfoValid && $primaryImageInfoValid && $newsContentInfoValid;

                if ($valid) {
                    if ($primaryImageExists && $primaryImageInfoValid) {
                        if ($image->image->saveAs(CommonFunctions::IMAGES_PATH . CommonFunctions::NEWS_IMAGES_PATH . $image->url)) {
                            $image->save(false);
                            $newsContentModel->image_id = $image->id;
                            $imageProcessor = new ImageProcessor();
                            $imageProcessor->source_path = CommonFunctions::IMAGES_PATH . CommonFunctions::NEWS_IMAGES_PATH . $image->url;
                            $imageProcessor->target_path = CommonFunctions::IMAGES_PATH . CommonFunctions::NEWS_IMAGES_PATH . CommonFunctions::NEWS_IMAGES_THUMBS_PATH . $image->url;
                            $imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1);
                        }
                    }

                    $newsContentModel->save(false);
                    $blockModel->content_id = $newsContentModel->id;
                    $blockModel->save(false);
                    switch ($blockModel->type) {
                        case Block::TYPE_IMAGE_ONLY:
                            $blockModel->addImage('image1', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
                            break;
                        case Block::TYPE_TEXT_PLUS_ONE_IMAGE:
                            $blockModel->addImage('image1', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
                            break;
                        case Block::TYPE_TEXT_PLUS_TWO_IMAGES:
                            $blockModel->addImage('image1', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
                            $blockModel->addImage('image2', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);

                            break;
                        case Block::TYPE_TEXT_PLUS_THREE_IMAGES:
                            $blockModel->addImage('image1', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
                            $blockModel->addImage('image2', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
                            $blockModel->addImage('image3', Image::TYPE_NEWS_SPECIFIC, CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
                            break;
                    }
                    $this->redirect(array('content/index'));
                }
            }


        }else{
            $this->updateImagesListJSONFile();
        }

        $this->render('news-create', array(
            'newsContentModel' => $newsContentModel,
            'blockModel' => $blockModel,
        ));
    }

    public function renderContentOptions($data, $row)
    {
        $this->renderPartial('partial/content-option', array(
            'data' => $data,
        ), false, false);
    }

    public function actionDelete()
    {
        if (isset($_POST['content_id'])) {
            $contentModelId = intval($_POST['content_id']);
            $contentModel = $this->loadModel($contentModelId);
            switch ($contentModel->type) {
                case Content::TYPE_NEWS:
                    $this->deleteNewsContent($contentModel);
                    break;
                case Content::TYPE_ARTICLE:
                    $this->deleteArticleContent($contentModel);
                    break;
            }
            $this->redirect(array('content/index'));
        }
    }

    public function actionActive()
    {
        if (isset($_POST['content_id'])) {
            $contentModelId = intval($_POST['content_id']);
            $contentModel = $this->loadModel($contentModelId);
            if($contentModel->active==Content::STATUS_ACTIVE){
                $contentModel->active = Content::STATUS_INACTIVE;


            }else if($contentModel->active==Content::STATUS_INACTIVE){
                $contentModel->active = Content::STATUS_ACTIVE;
                $contentModel->publish_date = date('Y-m-d h:i');
            }
            if($contentModel->save(false)){
                $this->redirect(array('content/index'));
            }
        }
    }

    public function deleteNewsContent($contentModel)
    {
        $blocks = $contentModel->blocks;
        foreach ($blocks as $block) {
            $images = $block->images;
            foreach ($images as $image) {
                $image->deleteImageForContent(CommonFunctions::NEWS_IMAGES_PATH, CommonFunctions::NEWS_IMAGES_THUMBS_PATH);
            }
            $block->delete();
        }
        $contentModel->delete();
    }

    public function deleteArticleContent($contentModel)
    {

    }


    public function actionArticleCreate()
    {
        $this->render('article-create');
    }

    public function generateCreateForm($type, $contentModel, $blockModel)
    {
        $contentModel->type = $type;

    }

    public function actionGetBlockCreateForm($type)
    {
        $blockModel = null;
        switch ($type) {
            case Block::TYPE_TEXT_ONLY:
                $blockModel = new Block('create-text-only');
                break;
            case Block::TYPE_IMAGE_ONLY:
                $blockModel = new Block('create-image-only');
                break;
            case Block::TYPE_TEXT_PLUS_ONE_IMAGE:
                $blockModel = new Block('create-text-image1');
                break;
            case Block::TYPE_TEXT_PLUS_TWO_IMAGES:
                $blockModel = new Block('create-text-image2');
                break;
            case Block::TYPE_TEXT_PLUS_THREE_IMAGES:
                $blockModel = new Block('create-text-image3');
                break;
            default:
                throw new CHttpException(400);
        }
        $blockModel->type = intval($type);
        echo $this->renderPartial('partial/_block-form', array(
            'blockModel' => $blockModel,
        ), false, true);

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('content', 'page.title.index.content');
        $this->contentTitle = Yii::t('content', 'controller.page.index.content.title');

        // Fetch All Content Of All Types
        $contentSearchModel = new Content('search');
        $contentSearchModel->unsetAttributes();

        if (isset($_GET['Content'])) {

        }

        $contentDataProvider = $contentSearchModel->search();


        $this->render('index', array(
            'contentDataProvider' => $contentDataProvider,
        ));
    }

    /**
     * Manages all models.
     */

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Content the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Content::model()->with('blocks', 'blocks.images')->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Content $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'content-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
