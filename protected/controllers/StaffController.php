<?php

class StaffController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'postOnly + delete,jobDelete', // we only allow deletion via POST request
			'checkAccess',
			'checkAdminAccess',
			'checkSideBarSubId +index,create',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if(!isset($_GET['ref'])){
			throw new CHttpException(400);
		}


		$ref = 'view';
		$viewModel = $this->loadModel(intval($id));
		$staffModel = $this->loadModel(intval($id));

		$cvSectionSearchModel = new CvSection('search');
		$cvSectionSearchModel->staff_id = $staffModel->id;
		$this->pageTitle = Yii::t('staff', 'page.title.view').': '.$staffModel->full_name;
		$this->contentTitle = Yii::t('staff', 'controller.page.view.content.title').': '.$staffModel->full_name;
		$ref = "view";
		if(isset($_POST['Staff']))
		{
			$staffModel->attributes=$_POST['Staff'];
			if($staffModel->save()){
				$this->redirect(array('view','id'=>$staffModel->id,'ref'=>'view'));
			}else{
				$ref="submit_edit";
			}
		}
		$this->render('view',array(
			'viewModel'=>$viewModel,
			'staffModel'=>$staffModel,
			'cvSectionSearchModel'=>$cvSectionSearchModel,
			'ref'=>$ref,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$staffCreateModel=new Staff('create');
		$this->pageTitle = Yii::t('staff', 'page.title.create');
		$this->contentTitle = Yii::t('staff', 'controller.page.create.content.title');

		if(isset($_POST['Staff']))
		{
			$staffCreateModel->attributes=$_POST['Staff'];
			$staffCreateModel->profileImage = CUploadedFile::getInstance($staffCreateModel, 'profileImage');
			if($staffCreateModel->validate()){
				$imageModel = new Image('create-for-staff');
				$imageModel->url = sha1($staffCreateModel->profileImage->name . uniqid() . time()) . '.' . $staffCreateModel->profileImage->extensionName;
				$imageModel->type = Image::TYPE_PROFILE_SPECIFIC;
				$imageModel->active = Image::STATUS_ACTIVE;
				$imageModel->name = $staffCreateModel->title==Staff::TITLE_DR?"Dr. ".$staffCreateModel->full_name:$staffCreateModel->full_name;
				$imageModel->name_ar = $staffCreateModel->title==Staff::TITLE_DR?"Dr. ".$staffCreateModel->full_name_ar:$staffCreateModel->full_name_ar;
				$imageModel->alternative_text = $staffCreateModel->title==Staff::TITLE_DR?"Dr. ".$staffCreateModel->full_name:$staffCreateModel->full_name;
				$imageModel->alternative_text_ar = $staffCreateModel->title==Staff::TITLE_DR?"Dr. ".$staffCreateModel->full_name_ar:$staffCreateModel->full_name_ar;
				if ($staffCreateModel->profileImage->saveAs(CommonFunctions::IMAGES_PATH . CommonFunctions::PROFILE_IMAGES_PATH . $imageModel->url)) {
					$imageModel->save(false);
					$imageProcessor = new ImageProcessor();
					$imageProcessor->source_path = CommonFunctions::IMAGES_PATH . CommonFunctions::PROFILE_IMAGES_PATH . $imageModel->url;
					$imageProcessor->target_path = CommonFunctions::IMAGES_PATH . CommonFunctions::PROFILE_IMAGES_PATH . CommonFunctions::PROFILE_IMAGES_THUMBS_PATH . $imageModel->url;
					$imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1);
					$staffCreateModel->profile_image_id = $imageModel->id;
					if($staffCreateModel->save(false)){
						$this->redirect(array('staff/index'));
					}
				}
			}
		}

		$this->render('create',array(
			'staffCreateModel'=>$staffCreateModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$staffUpdateModel=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Staff']))
		{
			$staffUpdateModel->attributes=$_POST['Staff'];
			if($staffUpdateModel->save()){
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'staffUpdateModel'=>$staffUpdateModel,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(isset($_POST['s_id'])){
			$staffModel = Staff::model()->with('cvSections','profileImg')->findByPk(intval($_POST['s_id']));
			Yii::app()->db->createCommand()->delete('tbl_cv_section','staff_id=:staff_id',array(':staff_id'=>intval($_POST['s_id'])));
			$profileImageModel = $staffModel->profileImg;
			$staffModel->delete();
			$profileImageModel->delete();

		}else{
			throw new CHttpException(400);
		}

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->pageTitle = Yii::t('staff', 'page.title.index');
		$this->contentTitle = Yii::t('staff', 'controller.page.index.content.title');

		$staffSearchModel = new Staff('search');

		$this->render('index',array(
			'staffSearchModel'=>$staffSearchModel,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Staff the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Staff::model()->with('profileImg')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadCVSectionModel($id)
	{
		$model=CvSection::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadJobType($id){
		$model=Job::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/*
	 *
	 * Jobs Management
	 *
	 */

	public function actionJobIndex(){
		$this->pageTitle = Yii::t('setting', 'page.title.index.job');
		$this->contentTitle = Yii::t('setting', 'controller.page.index.job.content.title');

		$jobSearchModel = new Job('search');
		$jobSearchModel->unsetAttributes();

		$this->render('job_index',array(
			'jobSearchModel'=>$jobSearchModel,
		));
	}
	public function actionJobCreate(){
		$this->pageTitle = Yii::t('setting', 'page.title.create.job');
		$this->contentTitle = Yii::t('setting', 'controller.page.create.job.content.title');

		$jobCreateModel = new Job;

		if(isset($_POST['Job'])){
			$jobCreateModel->attributes = $_POST['Job'];
			if($jobCreateModel->save()){
				$this->redirect(array('staff/jobIndex'));
			}

		}

		$this->render('job_create',array(
			'jobCreateModel'=>$jobCreateModel,
		));

	}
	public function actionJobUpdate($job_id){
		$this->pageTitle = Yii::t('setting', 'page.title.update.job');
		$this->contentTitle = Yii::t('setting', 'controller.page.update.job.content.title');

		$jobUpdateModel = $this->loadJobType(intval($job_id));

		if(isset($_POST['Job'])){
			$jobUpdateModel->attributes = $_POST['Job'];
			if($jobUpdateModel->save()){
				$this->redirect(array('staff/jobIndex'));
			}
		}

		$this->render('job_update',array(
			'jobUpdateModel'=>$jobUpdateModel,
		));

	}
	public function actionJobDelete(){

		if (isset($_POST['j_id'])) {
			if($_POST['j_id']==Job::JOB_DOCTOR_ID){
				$this->redirect(array('staff/jobIndex'));
			}
			$jobModelId = intval($_POST['j_id']);
			$jobModel = $this->loadJobType($jobModelId);
			$jobModel->delete();
			$this->redirect(array('staff/jobIndex'));
		}

	}
	public function renderJobOptions($data, $row){
		$this->renderPartial('partial/job-options', array(
			'data' => $data,
		), false, false);
	}
	public function renderStaffOptions($data, $row){
		$this->renderPartial('partial/staff-options', array(
			'data' => $data,
		), false, false);
	}
	public function renderStaffProfileImage($data, $row){
		$this->renderPartial('partial/staff-profile-image', array(
			'data' => $data,
		), false, false);
	}

	public function renderCVSectionOptions($data, $row){
		$this->renderPartial('partial/cv-section-options', array(
			'data' => $data,
		), false, false);
	}

	/*
	 *
	 * End of Jobs Management
	 *
	 */
	/*
	 *
	 * CV Section Management
	 */

	public function actionCVSectionCreate($staff_id){
		$staffModel = $this->loadModel(intval($staff_id));
		$this->pageTitle = Yii::t('staff', 'page.title.cv.create')." for : ".$staffModel->full_name;
		$this->contentTitle = Yii::t('staff', 'controller.page.create.cv.content.title')." for : ".$staffModel->full_name;

		$cvSectionCreateModel = new CvSection;
		$cvSectionCreateModel->staff_id = $staffModel->id;
		if(isset($_POST['CvSection'])){
			$cvSectionCreateModel->attributes = $_POST['CvSection'];
			if($cvSectionCreateModel->save()){
				$this->redirect(array('staff/view','id'=>$staffModel->id,'ref'=>'view'));
			}
		}

		$this->render('cv_section_create',array(
			'staffModel'=>$staffModel,
			'cvSectionCreateModel'=>$cvSectionCreateModel,
		));

	}



	public function actionCvSectionUpdate($id){
		$CvSectionUpdateModel = $this->loadCVSectionModel(intval($id));
		$staffModel = $this->loadModel(intval($CvSectionUpdateModel->staff_id));
		$this->pageTitle = Yii::t('staff', 'Edit CV Section')." for : ".$staffModel->full_name;
		$this->contentTitle = Yii::t('staff', 'Edit CV Section')." for : ".$staffModel->full_name;

		if(isset($_POST['CvSection'])){
			$CvSectionUpdateModel->attributes = $_POST['CvSection'];
			if($CvSectionUpdateModel->save()){
				$this->redirect(array('staff/view','id'=>$staffModel->id,'ref'=>'view'));
			}
		}

		$this->render('cv_section_update',array(
			'staffModel'=>$staffModel,
			'cvSectionUpdateModel'=>$CvSectionUpdateModel,
		));

	}


	/*
	 *
	 * End CV Section Management
	 *
	 */

	/**
	 * Performs the AJAX validation.
	 * @param Staff $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='staff-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
