<?php

class VacancyController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkSideBarSubId +index,create',
            'checkAccess',
            'checkAdminAccess',
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = Yii::t('vacancy', 'page.title.create.vacancy');
        $this->contentTitle = Yii::t('vacancy', 'controller.page.create.vacancy.content.title');


        $vacancyCreateModel = new Vacancy;

        $vacancyCreateModel->active = Vacancy::STATUS_ACTIVE;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Vacancy'])) {
            $vacancyCreateModel->attributes = $_POST['Vacancy'];
            $vacancyCreateModel->publish_date = date('Y-m-d');
            if ($vacancyCreateModel->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'vacancyCreateModel' => $vacancyCreateModel,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = Yii::t('vacancy', 'page.title.update.vacancy');
        $this->contentTitle = Yii::t('vacancy', 'controller.page.update.vacancy.content.title');

        $vacancyUpdateModel = $this->loadModel(intval($id));

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Vacancy'])) {
            $vacancyUpdateModel->attributes = $_POST['Vacancy'];
            $vacancyUpdateModel->publish_date = date('Y-m-d');
            if ($vacancyUpdateModel->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'vacancyUpdateModel' => $vacancyUpdateModel,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete()
    {
        if (isset($_POST['v_id'])) {
            $vacancyModelId = intval($_POST['v_id']);
            $vacancyModel = $this->loadModel($vacancyModelId);
            $vacancyModel->delete();
            $this->redirect(array('vacancy/index'));
        }
    }

    public  function actionActive(){
        if (isset($_POST['vacancy_id'])) {
            $vacancyModelId = intval($_POST['vacancy_id']);
            $vacancyModel = $this->loadModel($vacancyModelId);
            if($vacancyModel->active==Vacancy::STATUS_ACTIVE){
                $vacancyModel->active = Vacancy::STATUS_INACTIVE;
            }else if($vacancyModel->active==Vacancy::STATUS_INACTIVE){
                $vacancyModel->active = Vacancy::STATUS_ACTIVE;
            }
            $vacancyModel->publis_date = date('Y-m-d');
            if($vacancyModel->save(false)){
                $this->redirect(array('vacancy/index'));
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('vacancy', 'page.title.index.vacancy');
        $this->contentTitle = Yii::t('vacancy', 'controller.page.index.vacancy.content.title');


        $searchModel = new Vacancy('search');
        $searchModel->unsetAttributes();
        if (isset($_GET['Vacancy'])) {
            $searchModel->attributes = $_GET['Vacancy'];
        }

        $this->render('index', array(
            'searchModel' => $searchModel,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Vacancy('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Vacancy']))
            $model->attributes = $_GET['Vacancy'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Vacancy the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Vacancy::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function renderVacancyOptions($data, $row)
    {
        $this->renderPartial('vacancy-options', array(
            'data' => $data,
        ), false, false);
    }

    /**
     * Performs the AJAX validation.
     * @param Vacancy $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'vacancy-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
