<?php

class ContactDetailsItemController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(

            'checkAccess',
            'checkSideBarSubId +index,create,messages',
            'checkAdminAccess',
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = Yii::t('setting', 'Add Contact Details');
        $this->contentTitle = Yii::t('setting', 'Add Contact Details');
        $model = new ContactDetailsItem;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ContactDetailsItem'])) {
            $model->attributes = $_POST['ContactDetailsItem'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionMessages()
    {
        $this->pageTitle = Yii::t('setting', 'Contact Messages');
        $this->contentTitle = Yii::t('setting', 'Contact Messages');

        $messageModel = new Contact('search');
        $messageModel->unsetAttributes();


        $this->render('messages', array(
            'messageModel' => $messageModel,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = Yii::t('setting', 'Edit Contact Details');
        $this->contentTitle = Yii::t('setting', 'Edit Contact Details');
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ContactDetailsItem'])) {
            $model->attributes = $_POST['ContactDetailsItem'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete()
    {
        if (isset($_POST['c_id'])) {
            $model = $this->loadModel(intval($_POST['c_id']));
            if ($model->delete()) {
                $this->redirect(array('index'));
            }
        } else {
            throw new CHttpException(400);
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('setting', 'Contact Details Management');
        $this->contentTitle = Yii::t('setting', 'Contact Details Management');

        $searchModel = new ContactDetailsItem('search');
        $searchModel->unsetAttributes();

        $this->render('index', array(
            'searchModel' => $searchModel,
        ));
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ContactDetailsItem the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ContactDetailsItem::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadContactModel($id)
    {
        $model = Contact::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function renderContactDetailsItemOptions($data, $row)
    {
        $this->renderPartial('contact-details-options', array(
            'data' => $data,
        ), false, false);
    }

    public function actionViewMessage($id)
    {
        $this->pageTitle = Yii::t('setting', 'Message Preview');
        $this->contentTitle = Yii::t('setting', 'Message Preview');
        $model = $this->loadContactModel(intval($id));
        $this->render('message-view', array(
            'model' => $model,
        ));

    }

    public function actionDeleteMessage()
    {
        if (isset($_POST['m_id'])) {
            $model = $this->loadContactModel(intval($_POST['m_id']));
            $model->delete();
            $this->redirect(array('messages'));
        } else {
            throw new CHttpException(400);
        }
    }

    public function renderMessageOptions($data, $row)
    {
        $this->renderPartial('message-options', array(
            'data' => $data,
        ), false, false);
    }

    /**
     * Performs the AJAX validation.
     * @param ContactDetailsItem $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contact-details-item-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
