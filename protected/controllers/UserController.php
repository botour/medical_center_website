<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkSideBarSubId +index,create',
            'postOnly + delete', // we only allow deletion via POST request
            'checkAccess',
            'checkAdminAccess',
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = Yii::t('user', 'page.title.create.user');
        $this->contentTitle = Yii::t('user', 'controller.page.create.content.title');

        $model = new User('create');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];
            $model->status = User::STATUS_OFFLINE;
            if ($model->role == User::ROLE_EDITOR) {
                $model->setScenario('create-doctor');
                if (strlen($_POST['User']['doctor_id']) > 0) {
                    $model->doctor_id = intval($_POST['User']['doctor_id']);
                }else{
                    $model->unsetAttributes(array('doctor_id'));
                }
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            } else {
                $model->password = "";
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = Yii::t('user', 'page.title.update.user');
        $this->contentTitle = Yii::t('user', 'controller.page.update.content.title');

        $model = $this->loadModel($id);
        $model->setScenario('update');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->role == User::ROLE_EDITOR) {
                $model->setScenario('update-editor');
                if (strlen($_POST['User']['doctor_id']) > 0) {
                    $model->doctor_id = intval($_POST['User']['doctor_id']);
                }else{
                    $model->unsetAttributes(array('doctor_id'));
                }
            }
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('user', 'Users');
        $this->contentTitleAvailable = true;
        $this->contentTitle = Yii::t('user', 'Users');

        $userModel = new User('search');
        $userModel->unsetAttributes();
        $this->render('index', array(
            'userModel' => $userModel,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function renderActionCellValue($data, $row)
    {
        echo $this->renderPartial('partials/user-list-action-cell', array(
            'data' => $data,
        ), false, false);
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}