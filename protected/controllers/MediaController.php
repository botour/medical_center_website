<?php

class MediaController extends Controller
{
    public function filters()
    {
        return array(
            'checkAccess',
            'checkAdminAccess',
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('media', 'page.title.index.media');
        $this->contentTitle = Yii::t('media', 'controller.page.index.content.title');
        $image = new Image('create-media');
        $message = "";
        $this->updateImagesListJSONFile();

        if (isset($_POST['Image'])) {
            $image->attributes = $_POST['Image'];
            $image->active = Image::STATUS_INACTIVE;
            $image->image = CUploadedFile::getInstance($image, 'image');
            if ($image->validate(array('image', 'type'))) {
                $uploadPath = Gallery::getUploadPath($image->type);
                $image->url = sha1($image->image->name . uniqid() . time()) . '.' . $image->image->extensionName;
                $message = "Validate Success";
                $image->host = Image::HOST_MEDIA;
                if($image->validate()){
                    if ($image->image->saveAs(CommonFunctions::IMAGES_PATH . $uploadPath . $image->url)) {
                        $image->save(false);
                        $imageProcessor = new ImageProcessor();
                        $imageProcessor->source_path = CommonFunctions::IMAGES_PATH . $uploadPath . $image->url;
                        $imageProcessor->target_path = CommonFunctions::IMAGES_PATH . $uploadPath . CommonFunctions::THUMB_UPLOAD_PATH . $image->url;
                        if ($imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1)) {
                            $message = "Upload Successful";
                        } else {
                            $message = "Upload Not Successful";
                        }
                    }
                }
            } else {
                $message = "Error Uploading the File";
            }
        }
        // Bring the images Data

        $imageSearchModel = new Image('search');
        $imageSearchModel->host = Image::HOST_MEDIA;
        $imageSearchModel->active = Image::STATUS_ACTIVE;
        $this->render('index', array(
            'image' => $image,
            'message'=>$message,
            'imageSearchModel'=>$imageSearchModel,
        ),false,true);
    }

    public function renderCheckbox($data,$row){
        echo $this->renderPartial('partial/checkbox-column',array(
            'data'=>$data,
        ),false,false);

    }
    public function renderOptions($data,$row){
        echo $this->renderPartial('partial/options-column',array(
            'data'=>$data,
        ),false,false);
    }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}