<?php

class HomeController extends Controller
{
    /**
     * Declares class-based actions.
     */

    const HOME_PAGE = 0;
    const NEWS_PAGE = 1;
    const ARTICLE_PAGE = 2;

    public $_pageCode;
    public $currentUser = null;

    public $customContents = null;

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/home/pages'
            // They can be accessed via: index.php?r=home/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }


    public function filters()
    {
        return array(
            'checkAccess -' . implode(',', HomeController::getActionsForFrontendLoad()) . ',login,logout',
            'checkAdminAccess -' . implode(',', HomeController::getActionsForFrontendLoad()) . ',login,logout,error',
            'CheckSiteLanguage +' . implode(",", self::getActionsForLanguageCheck()),
            'loadFrontEnd +' . implode(",", self::getActionsForFrontendLoad()),
            'LoadCustomContent +home,contact',
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    private static function getActionsForLanguageCheck()
    {
        return array(
            'home',
            'news',
            'services',
            'service',
            'staff',
            'galleries',
            'gallery',
            'staffm',
            'contact',
            'vacancies',
            'whoWeAre',
            'ourMessage',
            'welcomeMessage',
            'articles',
            'article',
            'search',
        );
    }

    private static function getActionsForFrontendLoad()
    {
        return array(
            'home',
            'news',
            'services',
            'service',
            'staff',
            'gallery',
            'galleries',
            'staffm',
            'contact',
            'search',
            'vacancies',
            'whoWeAre',
            'ourMessage',
            'welcomeMessage',
            'articles',
            'article',
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->contentTitle = Yii::t('home', 'controller.page.title.main');
        $this->pageTitle = Yii::t('home', 'page.title.index.main');

        Yii::app()->language = "en";
        $slidesSearchModel = new SliderImage('search');


        // renders the view file 'protected/views/home/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', array(
            'slidesSearchModel' => $slidesSearchModel,
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }

    }

    public function actionHome()
    {
        Yii::app()->theme = "website";
        $this->loadBootstrap = true;
        $this->layout = '//layouts/main';
        $this->pageTitle = Yii::t('website', 'home.page.title');

        $imageSlides = SliderImage::model()->with('image')->findAll('t.active=:active', array(
            ':active' => SliderImage::STATUS_ACTIVE,
        ));

        $news = Content::model()->with('blocks', 'blocks.images')->findAll('t.active=:active AND t.type=:type', array(
            ':active' => Content::STATUS_ACTIVE,
            ':type' => Content::TYPE_NEWS,
        ));

        $this->render('home', array(
            'imageSlides' => $imageSlides,
            'news' => $news,
        ));
    }
    public function actionSearch($q)
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/main';
        $this->pageTitle = Yii::t('website', 'search.result');

        $this->render('search', array(

        ));
    }

    public function actionWhoWeAre()
    {

    }

    public function actionOurMessage()
    {

    }

    public function actionWelcomeMessage()
    {

    }


    public function actionNews()
    {
        $this->pageTitle = "News";
        Yii::app()->theme = "website";

        $this->layout = '//layouts/right_side_bar';

        if (isset($_COOKIE['lang'])) {
            Yii::app()->language = $_COOKIE['lang'];
        } else {
            Yii::app()->language = 'en';
        }
        $news = Content::model()->with('blocks', 'blocks.images')->findAll('t.active=:active AND t.type=:type', array(
            ':active' => Content::STATUS_ACTIVE,
            ':type' => Content::TYPE_NEWS,
        ));
        $this->render('news', array(
            'news' => $news,
        ));
    }


    public function actionServices()
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/right_side_bar';
        $this->render('services');
    }

    public function actionService($id)
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/right_side_bar';

        $serviceModel = Department::model()->with('faqs', 'equipments', 'equipments.images', 'services')->findByPk(intval($id));
        if (is_null($serviceModel)) {
            throw new CHttpException(404);
        }
        if (Yii::app()->language == 'ar') {
            $this->pageTitle = $serviceModel->name_ar;
        } else {
            $this->pageTitle = $serviceModel->name;
        }

        $this->render('service', array(
            'serviceModel' => $serviceModel,
        ));
    }

    public function actionStaff()
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar';

        $staffModel = new Staff('search');
        $staffModel->unsetAttributes();

        if (Yii::app()->language == 'ar') {
            $this->pageTitle = "صفحة الطاقم";
        } else {
            $this->pageTitle = "Doctors Home";
        }

        if (isset($_GET['Staff'])) {
            $staffModel->attributes = $_GET['Staff'];
        }
        $this->render('staff', array(
            'staffModel' => $staffModel,
            'staffArray' => $staffModel->getStaffArrayFromModel(),
        ));
    }


    public function actionStaffM($sId)
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar';
        $staffModel = Staff::model()->with('profileImg', 'cvSections')->findByPk(intval($sId));
        $this->pageTitle = $staffModel->getFullName();

        $this->render('staffm', array(
            'staffModel' => $staffModel,
        ));

    }


    public function actionVacancies()
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar_custom';

        $this->pageTitle = Yii::t('website', 'job.vacancies');


        $criteria = new CDbCriteria();
        $criteria->compare('active', Vacancy::STATUS_ACTIVE);
        $criteria->order = 'id DESC';

        $vacancies = Vacancy::model()->findAll($criteria);

        $this->render('vacancies', array(
            'vacancies' => $vacancies,
        ));
    }

    public function actionGalleries()
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar_custom';
        $this->pageTitle = Yii::t('website', 'galleries');

        $galleries = Gallery::model()->findAll('active=:active', array(
            ':active' => Gallery::STATUS_ACTIVE,
        ));
        $this->render('galleries', array(
            'galleries' => $galleries,
        ));
    }

    public function actionArticles()
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar_custom';
        $this->pageTitle = Yii::t('website', 'articles');

        $articles = Article::model()->findAll('active=:active AND approve=:approve', array(
            ':active' => Article::STATUS_ACTIVE,
            ':approve' => Article::STATUS_APPROVED,

        ));
        $this->render('articles', array(
            'articles' => $articles,
        ));
    }

    public function actionArticle($id)
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar_custom';
        $article = Article::model()->findByPk(intval($id),'approve=:approve AND active=:active',array(
            ':approve'=>Article::STATUS_APPROVED,
            ':active'=>Article::STATUS_ACTIVE,
        ));
        if($article==null){
            $this->redirect(array('home/articles'));
        }
        $this->pageTitle = Yii::t('website', 'article')." | ".$article->getTitle();

        $this->render('article', array(
            'article' => $article,
        ));
    }


    public function actionGallery($id)
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar';
        $this->loadGalleryPluginLibrary = true;

        $galleryModel = Gallery::model()->with('images')->findByPk(intval($id));
        if (is_null($galleryModel)) {
            throw new CHttpException(404);
        }
        $this->pageTitle = Yii::t('website', 'gallery') . " : " . $galleryModel->getTitle();

        $this->render('gallery', array('galleryModel' => $galleryModel));
    }

    public function actionContact()
    {
        Yii::app()->theme = "website";
        $this->layout = '//layouts/no_side_bar';

        $contactModel = new Contact;
        if (Yii::app()->language == 'ar') {
            $this->pageTitle = "نموذج التواصل";
        } else {
            $this->pageTitle = "Contact Form";
        }
        $contactDetails = ContactDetailsItem::model()->findAll();
        $saved = false;
        if (isset($_POST['Contact'])) {
            $contactModel->attributes = $_POST['Contact'];
            $contactModel->body = strip_tags($contactModel->body);

            $saved = $contactModel->save();
        }
        if($saved){
            $contactModel->unsetAttributes();
        }
        $this->render('contact', array(
            'contactModel' => $contactModel,
            'contactDetails'=>$contactDetails,
            'saved' => $saved,
        ));
    }

    /*
     *
     * Custom Content Management
     */

    public function actionMOurVision()
    {
        $this->contentTitle = Yii::t('home', 'Our Vision Text');
        $this->pageTitle = Yii::t('home', 'Our Vision Text');

        $model = CustomContent::model()->find('name=:name', array(':name' => CustomContent::OUR_VISION_KEY));
        if (isset($_POST['CustomContent'])) {
            $model->attributes = $_POST['CustomContent'];
            if ($model->save()) {
                $this->redirect(array('home/index'));
            }
        }
        $this->render('custom-content-view/our_vision', array(
            'model' => $model,
        ));
    }

    public function actionMContactDetails()
    {
        $this->contentTitle = Yii::t('home', 'Contact Details Text');
        $this->pageTitle = Yii::t('home', 'Contact Details Text');
        $model = CustomContent::model()->find('name=:name', array(':name' => CustomContent::CONTACT_INFO_KEY));
        if (isset($_POST['CustomContent'])) {
            $model->attributes = $_POST['CustomContent'];
            if ($model->save()) {
                $this->redirect(array('home/index'));
            }
        }
        $this->render('custom-content-view/contact_details', array(
            'model' => $model,
        ));
    }

    public function actionMWelcomeMessage()
    {
        $this->contentTitle = Yii::t('home', 'Welcome Message Text');
        $this->pageTitle = Yii::t('home', 'Welcome Message Text');
        $model = CustomContent::model()->find('name=:name', array(':name' => CustomContent::WELCOME_MESSAGE_KEY));
        if (isset($_POST['CustomContent'])) {
            $model->attributes = $_POST['CustomContent'];
            if ($model->save()) {
                $this->redirect(array('home/index'), array(
                    'model' => $model,
                ));
            }
        }
        $this->render('custom-content-view/welcome_message');
    }

    public function actionMWhoWeAre()
    {
        $this->contentTitle = Yii::t('home', 'Who We Are Text');
        $this->pageTitle = Yii::t('home', 'Who We Are Text');
        $model = CustomContent::model()->find('name=:name', array(':name' => CustomContent::WHO_WE_ARE_KEY));
        if (isset($_POST['CustomContent'])) {
            $model->attributes = $_POST['CustomContent'];
            if ($model->save()) {
                $this->redirect(array('home/index'));
            }
        }
        $this->render('custom-content-view/who_we_are', array(
            'model' => $model,
        ));
    }
    /*
     *
     * Custom Content Management End
     */
    /*
     *
     *  Images Slider Actions
     *
     */

    public function actionAddSliderImage()
    {
        $this->contentTitle = Yii::t('home', 'controller.page.title.add.slider');
        $this->pageTitle = Yii::t('home', 'page.title.index.add.slider');

        $model = new SliderImage('create');
        $imageModel = new Image('create-for-slider');


        if (isset($_POST['SliderImage'])) {
            $model->attributes = $_POST['SliderImage'];
            $imageModel->attributes = $_POST['Image'];

            $imageModel->type = Image::TYPE_SLIDER_IMAGE;
            $imageModel->active = Image::STATUS_ACTIVE;

            $model->order = 1;
            $model->active = SliderImage::STATUS_ACTIVE;
            $imageModel->image = CUploadedFile::getInstance($imageModel, 'image');

            $imageModelValid = $imageModel->validate();
            $sliderImageModelValid = $model->validate();


            if ($imageModelValid && $sliderImageModelValid) {
                $imageModel->url = sha1($imageModel->image->name . uniqid() . time()) . '.' . $imageModel->image->extensionName;
                if ($imageModel->validate()) {
                    if ($imageModel->image->saveAs(CommonFunctions::IMAGES_PATH . CommonFunctions::SLIDER_IMAGES_PATH . $imageModel->url)) {
                        $imageModel->save(false);
                        $imageProcessor = new ImageProcessor();
                        $imageProcessor->source_path = CommonFunctions::IMAGES_PATH . CommonFunctions::SLIDER_IMAGES_PATH . $imageModel->url;
                        $imageProcessor->target_path = CommonFunctions::IMAGES_PATH . CommonFunctions::SLIDER_IMAGES_PATH . CommonFunctions::SLIDER_IMAGES_THUMBS_PATH . $imageModel->url;
                        $imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1);
                        $model->image_id = $imageModel->id;
                        if ($model->save()) {
                            $this->redirect(array('home/index'));
                        }
                    }

                }
            }
        }

        $this->render('add_slider_image', array(
            'model' => $model,
            'imageModel' => $imageModel,
        ));
    }

    public function renderSliderImagesOptions($data, $row)
    {
        $this->renderPartial('partial/_slider_images_options', array(
            'data' => $data,
        ));
    }


    public function actionSliderImageDelete()
    {
        if (isset($_POST['slide_id'])) {
            $sliderImage = $this->loadSliderImageModel(intval($_POST['slide_id']));
            $image = Image::model()->findByPk($sliderImage->image_id);
            $sliderImage->delete();
            $image->delete();
            $this->redirect(array('home/index'));
        }


    }

    public function actionSliderImageUpdate($id)
    {
        $this->pageTitle = Yii::t('home','Slider Image Update');
        $this->contentTitle = Yii::t('home','Slider Image Update');

        $model = $this->loadSliderImageModel(intval($id));
        $model->setScenario('update');
        if(isset($_POST['SliderImage'])){
            $model->attributes = $_POST['SliderImage'];
            if($model->save()){
                $this->redirect(array('home/index'));
            }
        }
        $this->render('edit_slider_image',array(
            'model'=>$model,
            'imageModel'=>null,
        ));
    }

    public function actionActivateSliderImage()
    {
        if (isset($_POST['slide_id'])) {
            $sliderImageModelId = intval($_POST['slide_id']);
            $sliderModel = $this->loadSliderImageModel($sliderImageModelId);


            if ($sliderModel->active == SliderImage::STATUS_ACTIVE) {
                $sliderModel->active = SliderImage::STATUS_INACTIVE;

            } else if ($sliderModel->active == SliderImage::STATUS_INACTIVE) {
                $sliderModel->active = SliderImage::STATUS_ACTIVE;
                //$articleModel->publish_date = date('Y-m-d');
            }
            if ($sliderModel->save(false)) {
                $this->redirect(array('home/index'));
            }
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $this->layout = '//layouts/login';
        $model = new LoginForm;
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (Yii::app()->user->role == User::ROLE_ADMIN) {
                    $this->redirect(array('home/index'));
                } else if (Yii::app()->user->role == User::ROLE_EDITOR) {
                    $this->redirect(array('article/index'));
                }
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        $currentUser = User::model()->findByPk(Yii::app()->user->id);
        $currentUser->status = User::STATUS_OFFLINE;
        $currentUser->save(false);
        Yii::app()->user->logout();
        $this->redirect(array('home/login'));
    }

    public function renderImagesSlidesOptions()
    {

    }

    private function loadSliderImageModel($id)
    {
        $model = SliderImage::model()->findByPk($id);
        if (is_null($model)) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function filterLoadCustomContent($chain)
    {
        $contentValueLanguage = 'value';
        if (Yii::app()->getLanguage() == 'ar') {
            $contentValueLanguage = 'value_ar';
        } else {
            $contentValueLanguage = 'value';
        }
        $this->customContents = CHtml::listData(CustomContent::model()->findAll(), 'name', $contentValueLanguage);
        $chain->run();
    }
}