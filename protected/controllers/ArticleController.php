<?php

class ArticleController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkAccess', // perform access control for CRUD operations
            'checkAdminAccess',
            'checkSideBarSubId +index,create',
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = Yii::t('article', 'Create Article');
        $this->contentTitle = Yii::t('article', 'Create Article');

        if (!isset($_POST['Article'])) {
            $this->updateImagesListJSONFile();
        }

        $model = new Article;

        if ($this->user->role == User::ROLE_EDITOR) {
            $model->doctor_id = $this->user->doctor_id;
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Article'])) {
            $model->attributes = $_POST['Article'];
            $model->active = Article::STATUS_INACTIVE;
            $model->approve = Article::STATUS_NOT_APPROVED;
            if ($this->user->role == User::ROLE_EDITOR) {

                if (intval($model->doctor_id) != intval($this->user->doctor_id)) {
                    throw new CHttpException(400);
                }
            }

            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = Yii::t('article', 'Edit Article');
        $this->contentTitle = Yii::t('article', 'Edit Article');
        $model = $this->loadModel($id);

        if ($this->user->role == User::ROLE_EDITOR) {

            if (intval($model->doctor_id) != intval($this->user->doctor_id)) {
                throw new CHttpException(400);
            }
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);


        if (isset($_POST['Article'])) {
            $model->attributes = $_POST['Article'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionActive()
    {
        if (isset($_POST['article_id'])) {
            $articleModelId = intval($_POST['article_id']);
            $articleModel = $this->loadModel($articleModelId);

            if($this->user->role==User::ROLE_EDITOR){
                if($articleModel->doctor_id!=$this->user->doctor_id){
                    throw new CHttpException(400);
                }
            }
            if ($articleModel->active == Article::STATUS_ACTIVE) {
                $articleModel->active = Article::STATUS_INACTIVE;

            } else if ($articleModel->active == Article::STATUS_INACTIVE) {
                $articleModel->active = Article::STATUS_ACTIVE;
                //$articleModel->publish_date = date('Y-m-d');
            }
            if ($articleModel->save(false)) {
                $this->redirect(array('article/index'));
            }
        }
    }

    public function actionApprove()
    {
        if (isset($_POST['article_id'])) {
            if ($this->user->role != User::ROLE_ADMIN) {
                throw new CHttpException(400);
            }
            $articleModelId = intval($_POST['article_id']);
            $articleModel = $this->loadModel($articleModelId);
            if ($articleModel->approve == Article::STATUS_NOT_APPROVED) {
                $articleModel->approve = Article::STATUS_APPROVED;

            } else if ($articleModel->approve == Article::STATUS_APPROVED) {
                $articleModel->approve = Article::STATUS_NOT_APPROVED;
                $articleModel->publish_date = date('Y-m-d');
            }
            if ($articleModel->save(false)) {
                $this->redirect(array('article/index'));
            }
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = Yii::t('article', 'Articles');
        $this->contentTitle = Yii::t('article', 'Articles');

        $articleModel = new Article('search');
        $articleModel->unsetAttributes();
        if ($this->user->role == User::ROLE_EDITOR) {
            $articleModel->doctor_id = $this->user->doctor_id;
        }
        $this->render('index', array(
            'articleModel' => $articleModel,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Article('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Article']))
            $model->attributes = $_GET['Article'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Article the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Article::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Article $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'article-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function renderArticleOptions($data, $row)
    {
        $this->renderPartial('partials/article-option', array(
            'data' => $data,
        ), false, false);
    }

    public function filterCheckAdminAccess($chain)
    {
        if (!is_null($this->user)) {
            if ($this->user->role == User::ROLE_ADMIN || $this->user->role == User::ROLE_EDITOR) {
                $chain->run();
            } else {
                throw new CHttpException(400);
            }
        } else {
            throw new CHttpException(400);
        }
    }
}
