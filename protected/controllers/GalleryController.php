<?php

class GalleryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'checkSideBarSubId +index',
			'checkAccess',
			'checkAdminAccess',

			'AjaxAction +'.implode(',',$this->ajaxActions()),
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function ajaxActions(){
		return array(
			'create',
			'delete',
			'update',
			'imageUpdate',
			'moveImage',
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel(intval($id));
		$this->pageTitle = Yii::t('gallery', 'page.title.view.gallery')." | ".$model->getTitle();
		$this->contentTitleAvailable = true;

		$image = new Image('create');
		$image->gallery_id = $model->id;
		$image->type = $model->type;
		$this->contentTitle = Yii::t('gallery', 'controller.page.view.content.title')." : ".$model->getTitle();
		$galleryImages = $model->images;

		if(isset($_POST['Image'])){
			$image->attributes = $_POST['Image'];
			$image->active = Image::STATUS_INACTIVE;
			$image->image = CUploadedFile::getInstance($image,'image');
			if($image->validate(array('image','type'))){
				$uploadPath = Gallery::getUploadPath($image->type);
				$image->url = sha1($image->image->name.uniqid().time()).'.'.$image->image->extensionName;
				if($image->validate()){
					if($image->image->saveAs(CommonFunctions::IMAGES_PATH.$uploadPath.$image->url)){
						$image->save(false);
						$imageProcessor = new ImageProcessor();
						$imageProcessor->source_path = CommonFunctions::IMAGES_PATH.$uploadPath.$image->url;
						$imageProcessor->target_path = CommonFunctions::IMAGES_PATH.$uploadPath.CommonFunctions::THUMB_UPLOAD_PATH.$image->url;
						if ($imageProcessor->resize(334, 251, ZEBRA_IMAGE_BOXED, -1)){

						} else{
							$this->show_error($imageProcessor->error, $imageProcessor->source_path, $imageProcessor->target_path);
						}


					}
				}
			}
		}
		$this->render('view',array(
			'model'=>$model,
			'image'=>$image,
			'galleryImages'=>$galleryImages,
		));
	}

	public function actionProcessAjaxUpload(){
		if(isset($_POST['Image'])){
			$image = new Image('create');
			$image->attributes = $_POST['Image'];
			$image->active = Image::STATUS_INACTIVE;
			$image->image = CUploadedFile::getInstance($image,'image');
			if($image->validate(array('image','type'))){
				$uploadPath = Gallery::getUploadPath($image->type);
				$image->url = sha1($image->image->name.uniqid().time()).'.'.$image->image->extensionName;
				if($image->validate()){
					if($image->image->saveAs(CommonFunctions::IMAGES_PATH.$uploadPath.$image->url)){
						$image->save(false);
						$imageProcessor = new ImageProcessor();
						$imageProcessor->source_path = CommonFunctions::IMAGES_PATH.$uploadPath.$image->url;
						$imageProcessor->target_path = CommonFunctions::IMAGES_PATH.$uploadPath.CommonFunctions::THUMB_UPLOAD_PATH.$image->url;
						if ($imageProcessor->resize(75, 75, ZEBRA_IMAGE_BOXED, -1)){

						} else{
							$this->show_error($imageProcessor->error, $imageProcessor->source_path, $imageProcessor->target_path);
						}
						echo CJSON::encode(array(
							'status'=>true,
							'message'=>'Successfully Saved!',
						));
					}else{
						echo CJSON::encode(array(
							'status'=>false,
							'message'=>'Error When Saved',
						));
					}
				}else{
					echo CJSON::encode(array(
						'status'=>false,
						'message'=>'Some Image Data Are missing',
					));
				}
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>'Choose an Image File',
				));
			}
		}
	}

	function show_error($error_code, $source_path, $target_path)
	{

		// if there was an error, let's see what the error is about
		switch ($error_code) {

			case 1:
				echo 'Source file "' . $source_path . '" could not be found!';
				break;
			case 2:
				echo 'Source file "' . $source_path . '" is not readable!';
				break;
			case 3:
				echo 'Could not write target file "' . $source_path . '"!';
				break;
			case 4:
				echo $source_path . '" is an unsupported source file format!';
				break;
			case 5:
				echo $target_path . '" is an unsupported target file format!';
				break;
			case 6:
				echo 'GD library version does not support target file format!';
				break;
			case 7:
				echo 'GD library is not installed!';
				break;
			case 8:
				echo '"chmod" command is disabled via configuration!';
				break;

		}

	}

	public function actionUpdateImageList($id){
		$gallery = $this->loadModel(intval($id));
		$galleryImages = $gallery->images;
		echo $this->renderPartial('partial/images-list',array(
			'galleryImages'=>$galleryImages,
		),false,true);
		Yii::app()->end();
	}


	public function actionGetUpdateImageForm($id){
		$model = $this->loadImageModel($id);
		$model->setScenario('update');
		echo $this->renderPartial('_image_form',array(
			'model'=>$model,
		),false,true);
		Yii::app()->end();
	}

	public function actionMoveImage(){
		if(isset($_POST['Image']['id'])){
			$model = $this->loadImageModel(intval($_POST['Image']['id']));
			$model->gallery_id = $_POST['Image']['gallery_id'];
			$model->setScenario('update-gallery');
			if($model->save()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('gallery','alert.image.move.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('gallery','alert.image.move.fail'),
				));
			}
		}else{
			throw new CHttpException(400);
		}

	}

	public function actionGetMoveImageForm($id){
		$model = $this->loadImageModel($id);
		$model->setScenario('update-gallery');
		echo $this->renderPartial('_move_image_form',array(
			'model'=>$model,
		),false,true);
		Yii::app()->end();
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$model=new Gallery('create');

		if(isset($_POST['Gallery']))
		{
			$model->attributes=$_POST['Gallery'];
			$model->type = Gallery::TYPE_GENERAL;
			$model->active = Gallery::STATUS_ACTIVE;

			if($model->save()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('gallery','alert.gallery.create.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('gallery','alert.gallery.create.error'),
				));
			}

		}else{
			throw new CHttpException(400,'Bad Request');
		}
		Yii::app()->end();
	}


	public function actionUpdateGalleryList(){
		$galleryModels = Gallery::model()->findAll();
		echo $this->renderPartial('partial/galleriesList',array(
			'galleries'=>$galleryModels,
		),false,true);
	}

	public function actionActivate(){
		if(isset($_POST['gallery_id'])){
			$gallery = $this->loadModel(intval($_POST['gallery_id']));
			$gallery->active = Gallery::STATUS_ACTIVE;
			if($gallery->save(false)){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('gallery','alert.gallery.activate.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('gallery','alert.gallery.activate.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionDeactivate(){
		if(isset($_POST['gallery_id'])){
			$gallery = $this->loadModel(intval($_POST['gallery_id']));
			$gallery->active = Gallery::STATUS_INACTIVE;
			if($gallery->save(false)){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('gallery','alert.gallery.deactivate.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('gallery','alert.gallery.deactivate.error'),
				));
			}
		}else{
			throw new CHttpException(400);
		}
	}

	public function actionGetGalleryCreateForm(){
		$model = new Gallery('create');

		$model->type = Gallery::TYPE_GENERAL;
		$model->active = Gallery::STATUS_INACTIVE;

		echo $this->renderPartial('_form',array(
			'model'=>$model,
		),false,true);
		Yii::app()->end();
	}

	public function actionGetGalleryUpdateForm($id){
		$model = $this->loadModel($id);
		$model->setScenario('update');
		echo $this->renderPartial('_form',array(
			'model'=>$model,
		),false,true);
		Yii::app()->end();
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel(intval($_POST['Gallery']['id']));
		$model->setScenario('update');
		if(isset($_POST['Gallery']))
		{
			$model->attributes=$_POST['Gallery'];
			if($model->save()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('gallery','alert.gallery.update.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('gallery','alert.gallery.update.error'),
				));
			}
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		$gallery = $this->loadModelWithImages($_POST['id']);
		$images = $gallery->images;
		foreach($images as $image){
			$image->delete();
		}
		if($gallery->delete()){
			echo CJSON::encode(array(
				'status'=>true,
				'message'=>Yii::t('gallery','alert.gallery.delete.success'),
			));
		}else{
			echo CJSON::encode(array(
				'status'=>true,
				'message'=>Yii::t('gallery','alert.gallery.delete.error'),
			));
		}
		Yii::app()->end();

	}

	public function actionDeleteImage(){
		if(isset($_POST['data-id'])){
			$image = $this->loadImageModel(intval($_POST['data-id']));


			if($image->delete()){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>'Image was deleted Successfully!',
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>'ERROR',
				));
			}
		}else{
			throw new CHttpException(400,"Bad Request");
		}

	}

	public function actionImageUpdate(){
		$model=$this->loadImageModel(intval($_POST['Image']['id']));
		$model->setScenario('update');
		if(isset($_POST['Image']))
		{
			$moveToNewLocationStatus = true;
			$imageType= $model->type;
			$model->attributes=$_POST['Image'];

			if($imageType != $_POST['Image']['type']){
				$moveToNewLocationStatus = $this->updateImageType($model,$imageType,intval($_POST['Image']['type']));
			}
			if($model->save() && $moveToNewLocationStatus){
				echo CJSON::encode(array(
					'status'=>true,
					'message'=>Yii::t('gallery','alert.image.update.success'),
				));
			}else{
				echo CJSON::encode(array(
					'status'=>false,
					'message'=>Yii::t('gallery','alert.image.update.error'),

				));
			}
		}else{
			throw new CHttpException(400);
		}
	}
	/**
	 * Lists all models.
	 */
	private function updateImageType($imageModel,$oldType,$newType){
		$imagePath = CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($oldType).$imageModel->url;
		$imageThumbPath = CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($oldType).CommonFunctions::THUMB_UPLOAD_PATH.$imageModel->url;
		$newImagePath = CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($newType).$imageModel->url;
		$newImageThumbPath = CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($newType).CommonFunctions::THUMB_UPLOAD_PATH.$imageModel->url;
		$success1 = rename($imagePath,$newImagePath);
		$success2 = rename($imageThumbPath,$newImageThumbPath);
		return $success1 && $success2 ;
	}
	public function actionIndex()
	{
		$this->pageTitle = Yii::t('gallery', 'page.title.index.gallery');
		$this->contentTitleAvailable = true;
		$this->contentTitle = Yii::t('gallery', 'controller.page.index.content.title');


		$galleryModels = Gallery::model()->findAll();

		$this->render('index',array(
			'galleries'=>$galleryModels,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Gallery('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gallery']))
			$model->attributes=$_GET['Gallery'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Gallery the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Gallery::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelWithImages($id)
	{
		$model=Gallery::model()->with('images')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadImageModel($id)
	{
		$model=Image::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Gallery $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gallery-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
