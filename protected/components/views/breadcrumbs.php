<?php foreach($this->breadcrumbs as $crumb):?>

        <?php if(is_array($crumb)):?>
        <li>
            <?php echo CHtml::link($crumb['name'],$crumb['url']);?>
        </li>
        <?php endif;?>

        <?php if(!is_array($crumb)):?>
        <li class="active">
            <?php echo $crumb;?>
        </li>
        <?php endif;?>

<?php endforeach;?>
