<?php
/**
 * @var $this SidebarNavigation
 */

?>
<div class="navbar-content">
    <!-- start: SIDEBAR -->
    <div class="main-navigation navbar-collapse collapse">
        <!-- start: MAIN MENU TOGGLER BUTTON -->
        <div class="navigation-toggler">
            <i class="clip-chevron-left"></i>
            <i class="clip-chevron-right"></i>
        </div>
        <!-- end: MAIN MENU TOGGLER BUTTON -->
        <!-- start: MAIN NAVIGATION MENU -->
        <ul class="main-navigation-menu">

            <?php if ($this->controller->user->role == User::ROLE_ADMIN): ?>
                <li id="frontend-view">
                    <a href="<?php echo Yii::app()->createUrl('home/home'); ?>"><i class="clip-pictures"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.front-end'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                </li>

                <li id="home-management">
                    <a href="<?php echo Yii::app()->createUrl('home/index') ?>"><i class="clip-screen"></i>
                        <span class="title"><?php echo Yii::t('template', 'sidebar.option.main.homepage'); ?>
                            <span class="selected"></span>
                    </a>
                </li>

                <li id="content-management">
                    <a href="javascript:void(0)"><i class="clip-file-3"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.content-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="content-create">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.content-management.sub.create-content') . '</span>', Yii::app()->createUrl('content/create'));
                            ?>
                        </li>
                        <li id="manage-contents">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.content-management.sub.manage-contents') . '</span>', Yii::app()->createUrl('content/index'));
                            ?>
                        </li>
                    </ul>
                </li>

                <li id="service-management">
                    <a href="javascript:void(0)"><i class="clip-copy-3"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.department-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="service-create">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.department-management.sub.create-department') . '</span>', Yii::app()->createUrl('department/create'));
                            ?>
                        </li>
                        <li id="manage-services">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.department-management.sub.manage-departments') . '</span>', Yii::app()->createUrl('department/index'));
                            ?>
                        </li>
                    </ul>
                </li>

                <li id="media-management">
                    <a href="<?php echo Yii::app()->createUrl('media/index'); ?>"><i class="clip-pictures"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.media-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>

                </li>


                <li id="staff-management">
                    <a href="javascript:void(0)"><i class="clip-user-2"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.staff-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="manage-staffs">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.staff-management.sub.manage-staffs') . '</span>', Yii::app()->createUrl('staff/index'));
                            ?>
                        </li>
                        <li id="staff-create">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.staff-management.sub.create-staff') . '</span>', Yii::app()->createUrl('staff/create'));
                            ?>
                        </li>

                    </ul>
                </li>
            <?php endif; ?>


            <li id="article-management">
                <a href="javascript:void(0)"><i class="clip-pencil"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.article-management'); ?></span><i
                        class="icon-arrow"></i>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    <li id="article-create">
                        <?php
                        echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.article-management.sub.create-article') . '</span>', Yii::app()->createUrl('article/create'));
                        ?>
                    </li>
                    <li id="manage-articles">
                        <?php
                        echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.article-management.sub.manage-articles') . '</span>', Yii::app()->createUrl('article/index'));
                        ?>
                    </li>
                </ul>
            </li>
            <?php if ($this->controller->user->role == User::ROLE_ADMIN): ?>

                <li id="gallery-management">
                    <a href="javascript:void(0)"><i class="clip-image"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.gallery-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="manage-galleries">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.department-management.sub.manage-galleries') . '</span>', Yii::app()->createUrl('gallery/index'));
                            ?>
                        </li>
                    </ul>
                </li>

                <li id="contact-management">
                    <a href="javascript:void(0)"><i class="clip-phone"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.contact-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="contact-details-create">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.contact-management.sub.create-contact-details') . '</span>', Yii::app()->createUrl('contactDetailsItem/create'));
                            ?>
                        </li>
                        <li id="manage-contact-details">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.contact-management.sub.manage-contact-details') . '</span>', Yii::app()->createUrl('contactDetailsItem/index'));
                            ?>
                        </li>
                        <li id="manage-messages">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.contact-management.sub.manage-messages') . '</span>', Yii::app()->createUrl('contactDetailsItem/messages'));
                            ?>
                        </li>
                    </ul>
                </li>
                <li id="job-management">
                    <a href="javascript:void(0)"><i class="clip-archive"></i>
                        <span
                            class="title"><?php echo Yii::t('template', 'sidebar.option.main.job-management'); ?></span><i
                            class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="job-create">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.department-management.sub.create-job') . '</span>', Yii::app()->createUrl('vacancy/create'));
                            ?>
                        </li>
                        <li id="manage-jobs">
                            <?php
                            echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.department-management.sub.manage-jobs') . '</span>', Yii::app()->createUrl('vacancy/index'));
                            ?>
                        </li>
                    </ul>
                </li>

                <li id="management">
                    <a href="javascript:;" class="active">
                        <i class="clip-cog-2"></i>
                        <span class="title"><?php echo Yii::t('template', 'sidebar.option.main.management'); ?></span>
                        <i class="icon-arrow"></i>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="javascript:;"><?php echo Yii::t('template', 'sidebar.option.main.user-management'); ?>
                                <i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li id="user-create">
                                    <?php
                                    echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.user-management.sub.create-user') . '</span>', Yii::app()->createUrl('user/create'));
                                    ?>
                                </li>
                                <li id="manage-users">
                                    <?php
                                    echo CHtml::link('<span class="title">' . Yii::t('template', 'sidebar.option.user-management.sub.manage-users') . '</span>', Yii::app()->createUrl('user/index'));
                                    ?>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;"><?php echo Yii::t('template', 'sidebar.option.main.ads-management'); ?>
                                <i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;"><?php echo Yii::t('template', 'sidebar.option.main.link-management'); ?>
                                <i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;"><?php echo Yii::t('template', 'sidebar.option.main.visitor-messages'); ?></i>
                            </a>
                        </li>
                        <li id="setting-management">
                            <a href="<?php echo Yii::app()->createUrl('setting/index'); ?>"><?php echo Yii::t('template', 'sidebar.option.main.website-settings'); ?></i>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
        <!-- end: MAIN NAVIGATION MENU -->
    </div>
    <!-- end: SIDEBAR -->
</div>