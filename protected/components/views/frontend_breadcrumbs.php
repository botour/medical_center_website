<?php

$breadcrumbMessage = '<span class="bold">' . Yii::t('website', 'you.are.in') . '</span>';

foreach ($breadcrumbs as $breadcrumb) {
    if(is_array($breadcrumb)){
        $breadcrumbMessage .= '<a href="'.$breadcrumb["url"].'">'.$breadcrumb["name"].'</a>';
    }else{
        if($breadcrumb=="/"){
            $breadcrumbMessage .= '<span class="line-separate">/</span>';
        }else{
            $breadcrumbMessage .= '<span>'.$breadcrumb.'</span>';
        }
    }
}


echo $breadcrumbMessage;
?>