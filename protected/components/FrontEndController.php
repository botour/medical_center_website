<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/10/15
 * Time: 12:18 PM
 */

class FrontEndController extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';


    public $pageTitle = "";
    // Used to include additional libraries and their styles
    public $cssAssets = array();
    public $jsAssets = array();

    // Used to add custom css and js scripts
    public $cssCustomFiles = array();
    public $jsCustomFiles = array();


    public $breadcrumbs = array();


    public function filterAjaxAction($chain)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $chain->run();
        } else {
            throw new CHttpException(400);
        }
    }

    public function beforeAction($var){
        Yii::app()->theme = "website";
        Yii::app()->language = 'ar';
    }

}