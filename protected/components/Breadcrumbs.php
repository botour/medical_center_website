<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 2/28/15
 * Time: 1:35 PM
 */

class Breadcrumbs extends CWidget{
    public $breadcrumbs=array();

    public function run()
    {
        $this->render('breadcrumbs', array(
            'breadcrumbs'=>$this->breadcrumbs,
        ));
    }

}