<?php

class FrontEndBreadcrumbs extends CWidget{

    public $breadcrumbs=array();

    public function run()
    {
        $this->render('frontend_breadcrumbs', array(
            'breadcrumbs'=>$this->breadcrumbs,
        ));
    }
}