<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

    /**
     *
     * @var $user User
     */
    public $user;
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $contentTitleAvailable = true;
    public $contentTitle = "";
    public $contentSubTitle = "";


    public $loadGalleryPluginLibrary = false;
    public $loadBootstrap = false;

    //Services
    public $services;
    //Doctors
    public $doctors;

    public $languageLink;

    // Used to include additional libraries and their styles
    public $cssAssets = array();
    public $jsAssets = array();

    // Used to add custom css and js scripts
    public $cssCustomFiles = array();
    public $jsCustomFiles = array();

    //current SideBar navigation active
    public $menuItemId = "";
    public $subMenuItemId = "";
    public $checkSideBarSubId = false;

    public $sideBarItemIds = array(
        'user' => 'user-management',
        'home' => 'home-management',
        'department' => 'service-management',
        'media' => 'media-management',
        'gallery' => 'gallery-management',
        'staff'=>'staff-management',
        'article'=>'article-management',
        'content'=>'content-management',
        'setting'=>'management',
        'vacancy'=>'job-management',
        'article'=>'article-management',
        'contactDetailsItem'=>'contact-management',
    );

    public $sideBarSubItemIds = array(
        'user.create' => 'user-create',
        'user.index' => 'manage-users',
        'department.create' => 'service-create',
        'department.index' => 'manage-services',
        'gallery.index' => 'manage-galleries',
        'staff.create'=>'staff-create',
        'article.create'=>'article-create',
        'article.index'=>'manage-articles',
        'staff.createDoctor'=>'doctor-create',
        'staff.index'=>'manage-staffs',
        'content.create'=>'content-create',
        'content.index'=>'manage-contents',
        'setting.index'=>'setting-management',
        'vacancy.create'=>'job-create',
        'vacancy.index'=>'manage-jobs',
        'article.create'=>'article-create',
        'article.index'=>'manage-articles',
        'contactDetailsItem.create'=>'contact-details-create',
        'contactDetailsItem.index'=>'manage-contact-details',
        'contactDetailsItem.messages'=>'manage-messages',
    );

    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */

    public $breadcrumbs = array();


    protected function beforeAction($action)
    {
        $this->menuItemId = $this->sideBarItemIds[Yii::app()->controller->id];
        if ($this->checkSideBarSubId) {
            $this->subMenuItemId = $this->sideBarSubItemIds[Yii::app()->controller->id . "." . Yii::app()->controller->action->id];
        }
        return parent::beforeAction($action);
    }

    public function filterCheckSideBarSubId($chain)
    {
        $this->checkSideBarSubId = true;
        $chain->run();
    }

    public function filterAjaxAction($chain)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $chain->run();
        } else {
            throw new CHttpException(400);
        }
    }

    public function updateImagesListJSONFile()
    {
        $images = Image::model()->findAll('active=:active', array(
            ':active' => Image::STATUS_ACTIVE,
        ));
        $images_list_file_content_array = array();
        foreach ($images as $image) {
            $images_list_file_content_array[] = array(
                'image'=>Yii::app()->baseUrl."/".CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($image->type).$image->url,
                'thumb'=>Yii::app()->baseUrl."/".CommonFunctions::IMAGES_PATH.Gallery::getUploadPath($image->type).CommonFunctions::THUMB_UPLOAD_PATH.$image->url,
                'folder'=>Gallery::getUploadPath($image->type),
            );
        }
        file_put_contents("json/images_list.json", CJSON::encode($images_list_file_content_array));
    }

    public function filterCheckSiteLanguage($chain)
    {
        if (isset($_POST['lang'])) {
            if ($_POST['lang'] == 'ar') {
                Yii::app()->language = 'ar';
                setcookie('lang','ar',time()+3600);
            } else {
                Yii::app()->language = 'en';
                setcookie('lang','en',time()+3600);
            }
        }else{
            if(isset($_COOKIE['lang'])){
                Yii::app()->language = $_COOKIE['lang'];
            }else{
                Yii::app()->language = 'en';
            }
        }
        $chain->run();
    }

    public function filterCheckAccess($chain){
        if(Yii::app()->user->isGuest){
            $this->redirect(array('home/login'));
        }else{
            $this->user = User::model()->findByPk(intval(Yii::app()->user->id));
            $chain->run();
        }
    }

    public function filterCheckAdminAccess($chain){
        if(!is_null($this->user)){
            if($this->user->role == User::ROLE_ADMIN){
                $chain->run();
            }else{
                throw new CHttpException(400);
            }
        }else{
            throw new CHttpException(400);
        }
    }


    public function filterLoadFrontEnd($chain){
        //Setup the Language Link
        $languageLink = Yii::app()->request->url;
        $queryString = Yii::app()->request->queryString;


        // Setup the DATABASE LINK
        $criteria = new CDbCriteria();


        $criteria->order = 't.order DESC';
        $criteria->compare('type',Department::TYPE_SERVICE_CLINIC);
        $this->services = Department::model()->findAll($criteria);


        $criteriaDoctor = new CDbCriteria();
        $criteriaDoctor->order = 'id';
        $criteriaDoctor->compare('job_id',Job::model()->find('title=:title',array('title'=>'Doctor'))->id);
        $this->doctors = Staff::model()->findAll($criteriaDoctor);
        $chain->run();
    }
}