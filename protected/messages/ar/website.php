<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/8/15
 * Time: 4:09 PM
 */

return array(
    'homepage.nav.item.home'=>'الرئيسية',
    'homepage.nav.item.clinics'=>'خدماتنا',
    'homepage.nav.item.vacancies'=>'وظائف',
    'homepage.nav.item.doctors'=>'فريق العمل',
    'homepage.nav.item.contact'=>'اتصل بنا',
    'homepage.nav.item.news'=>'أخبار',
    'homepage.nav.item.articles'=>'معلومات للمرضى',
    'homepage.nav.item.gallery'=>'صور',
    'read.more'=>'اقرأ المزيد',
    'welcome.title'=>'اهلا بكم في مركز الحياة الطبي ',

    'links'=>'روابط',
    'faq'=>'اسئلة شائعة',

    'our.services'=>'خدماتنا',
    'our.doctors'=>'اطباؤنا',
    'search'=>'بحث',

    'enter.search.keywords'=>'ادخل كلمات مفتاحية للبحث ...',

    'home.page.title'=>'مركز الحياة الطبي | الرئيسية',

    'internal'=>'عيادة الامراض الداخلية',
    'ob.gyn'=>'عيادة الامراض النسائية',
    'dermatology'=>'عيادة الاطفال',
    'dental'=>'عيادة طب الاسنان',
    'laboratory'=>'المختبر',
    'pharmacy'=>'الصيدلية',
    'pediartic'=>'عيادة اخرى',

    'dr.fawaz.title'=>' مدير مركز الحياة الطبي',
    'dr.fawaz'=>'د. فواز سعد',
    'dr.1'=>'د.فواز سعد',
    'dr.2'=>'د.برهان عطور',
    'dr.3'=>'د.زاهر سعد',
    'dr.4'=>'د.هدى عطور',

    'welcome.news'=>'الأخبار',

    'staff.filter.label.branch'=>'الفرع',
    'staff.filter.label.job'=>'المهنة',
    'staff.filter.label.section'=>'القسم',
    'service.tab.content.staff.header'=>'الأطباء',

    'please.select.a.job'=>'رجاء اختر العمل',
    'please.select.a.branch'=>'رجاء اختر فرع',
    'please.select.a.section'=>'رجاء اختر قسما',

    'our.message'=>'رسالتنا',
    'service.tab.content.staff.read.more'=>'اقرأ السيرة الذاتية',
    'service.tab.description'=>'الوصف العام',
    'service.tab.staff'=>'فريق العمل',
    'service.tab.equipment'=>'التجهيزات الطبية',
    'service.tab.content.faq.q'=>'ماذا تريد مني ان افعل ؟ ',
    'service.tab.content.faq.a'=>'لااريد منك شيءا ههههه هههه هه ههههه هه',
    'branch1'=>'فرع ١',
    'branch2'=>'فرع ٢',
    'filter.staff'=>'بجث في النتائج',

    'breadcrumb.gallery'=>'معارض الصور',

    'you.are.in'=>'انت في',
    'service.tab.faq'=>'الأسئلة الشائعة',
    'service.tab.content.description'=>'ت توصيفتوصيفتوصيفتوصيفتوصيفتوصيفتوصيفتوصيفتوصيفتوصيفتوصيفوصيف عن الخدمات ',

    'staff.dob'=>'مكان و تاريخ الولادة',
    'staff.nationality'=>'الجنسية',
    'staff.contact'=>'معلومات الاتصال',
    'staff.m.status'=>'الحالة الاجتماعية',
    'staff.job'=>'المهنة',
    'staff.cv.sections'=>'اقسام السيرة الذاتية',
    'staff.address'=>'العنوان',
    'get.in.touch'=>'كن على تواصل',


    'homepage.nav.item.contact.info'=>'معلومات الاتصال',

    'please.select.type'=>'رجاء اختر نوع التواصل',
    'inquiry'=>'تساؤل',
    'complaint'=>'شكوى',
    'homepage.nav.item.contact.info'=>'معلومات التواصل',
    'homepage.nav.item.contact.address'=>'تواصل معنا',


    'homepage.nav.item.contact.address'=>'تواصل معنا',
    'contact.submit'=>'إرسال',

    'adds.goes.here'=>'الاعلانات هنا',

    'job.vacancies'=>'وظائف شاغرة',
    'job.opportunity'=>'فرص عمل متاحة',
    'homepage.nav.item.home.welcome'=>'الرسالة الترحيبية',
    'enter.search.keyword.here'=>'ادخل كلمة مفتاحية للبحث هنا ...',

    'homepage.nav.item.home.vision'=>'رؤيتنا',

    'homepage.nav.item.home.who.we.are'=>'من نحن؟',
    'no.articles.available.for.now'=>'لايوجد مقالات علمية متاحة حاليا',
    'your.contact.has.been.successfully.sent'=>'تم ارسال طلبك بنجاح',
    'By'=>'مقال ل: ',
    'articles'=>'مقالات طبية',
    'breadcrumb.contact'=>'تواصل معنا',
    'breadcrumb.staff'=>'الطاقم',
    'breadcrumb.articles'=>'المقالات الطبية',
    'breadcrumb.home'=>'الرئيسية',
    'breadcrumb.vacancies'=>'شواغر عمل',
    'welcome.insurance'=>'نرحب بحاملي بطاقات التأمين التالية',
    'vacancy.label.position'=>'المركز',
    'vacancy.label.description'=>'التوصيف',
    'vacancy.label.salary'=>'الراتب',
    'vacancy.label.contact'=>'بريد التواصل',
    'no.contents.available'=>'لا يوجد محتوى',
    'no.galleries.available.for.now'=>'لايوجد معارض صور متوفرة حاليا',
    'galleries'=>'معارض الصور',
    'no.job.opportunities.available.for.now'=>'لا يوجد وظائف شاغرة حاليا',
    'vacancy.label.contact'=>'التواصل',

    'gallery'=>'معرض الصور',
    'search.result'=>'نتائج البحث',
    'contact.details'=>'معلومات التواصل',
);
