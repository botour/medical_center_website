<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/6/15
 * Time: 8:00 AM
 */


return array(
    'model.label.id'=>'ID',
    'model.label.full_name'=>'الاسم الكامل (الانكليزي)',
    'model.label.full_name_ar'=>'الاسم الكامل (العربي)',
    'model.label.email'=>'البريد الالكتروني',
    'model.label.contact_type'=>'النوع',
    'model.label.body'=>'الرسالة',
);