<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/27/15
 * Time: 9:51 PM
 */

return array(
    'model.label.id'=>'ID',
    'model.label.branch' => 'الفرع',
    'model.label.branch_ar'=>'(Arabic)',
    'model.label.email'=>'البريد الالكتروني',
    'model.label.tel_1'=>'هاتف ١',
    'model.label.tel_2'=>'هاتف ٢',
    'model.label.address'=>'العنوان',
    'model.label.address_ar'=>'Address (Arabic)',
    'model.label.fax'=>'الفاكس',
    'model.label.mobile'=>'جوال ١',
    'model.label.mobile_2'=>'جوال ٢',
);