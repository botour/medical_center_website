<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/16/15
 * Time: 8:33 AM
 */


return array(
    'model.label.id'=>'ID',
    'model.label.position'=>'Position',
    'model.label.position_ar'=>'Position(Arabic)',
    'model.label.title'=>'Title',
    'model.label.title_ar'=>'Title(Arabic)',
    'model.label.description'=>'Job Description',
    'model.label.description_ar'=>'Job Description(Arabic)',
    'model.label.salary'=>'Salary',
    'model.label.salary_ar'=>'Salary(Arabic)',
    'model.label.contact_email'=>'Contact email',
    'model.label.active'=>'Active',

    'controller.page.index.vacancy.content.title'=>'Manage Vacancy',
    'page.title.index.vacancy'=>'Manage Vacancy',
    'action.create.vacancy'=>'Add Job Vacancy',
    'vacancy.status.inactive'=>'Inactive',
    'vacancy.status.active'=>'Active',

    'page.title.create.vacancy'=>'Add Vacancy',
    'controller.page.create.vacancy.content.title'=>'Add Job Vacancy',

    'controller.vacancy.action.add.vacancy'=>'Add Vacancy',
    'controller.vacancy.action.update.vacancy'=>'Edit Job Vacancy',
    'controller.page.update.vacancy.content.title'=>'Edit Job Vacancy',
    'page.title.update.vacancy'=>'Edit Job Vacancy',

    'no.galleries.available.for.now'=>'No Galleries Available for now',
);