<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/6/15
 * Time: 12:57 PM
 */


return array(
    'page.title.index.gallery'=>'Manage Galleries',
    'controller.page.index.content.title'=>'Manage Galleries',
    'action.create.gallery'=>'Create Gallery',

    'page.title.view.gallery'=>'Gallery',
    'controller.page.view.content.title'=>'Gallery',

    // Alerts
    'alert.gallery.create.success'=>'A Gallery Was Created successfully',
    'alert.gallery.create.error'=>'Error Creating the gallery',
);