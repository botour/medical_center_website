<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 2/28/15
 * Time: 1:26 PM
 */

return array(
    'controller.page.title.main'=>'Homepage Management',

    'home.login.label.remember.me'=>'Keep me signed in',
    'home.login.label.password'=>'Password',
    'home.login.label.login.name'=>'Login Name',
    'home.login.action.login'=>'Login',
    'page.title.index.main'=>'Homepage Management',
    'controller.page.title.add.slider'=>'Add Slider Image',

    'breadcrumb.home'=>'Home',
    'breadcrumb.add.slider.image'=>'Add Slider Image',
    'breadcrumb.edit.slider.image'=>'Edit Slider Image',
    'breadcrumb.view.slider.image'=>'View Slider Image',

    'home.login.sub.header'=>'Please enter your name and password to log in. ',
    'home.login.header'=>'Sign in to your account',

    'controller.add.slider.action'=>'Add Slider Image',

    'home.index.tab.sliders'=>'Image Slider Management',
    'home.index.tab.content'=>'HomePage Content Management',

    'slider.image.model.label.id'=>'',
    'slider.image.model.label.title'=>'Title',
    'slider.image.model.label.description'=>'Description',
    'slider.image.model.label.title.ar'=>'Title(Arabic)',
    'slider.image.model.label.description.ar'=>'Description(Arabic)',
    'slider.image.model.label.link'=>'Read More Link',
    'slider.image.model.label.image_id'=>'Image',
    'slider.image.model.label.active'=>'Active',
    'slider.image.model.label.order'=>'Order',
    'slider.image.model.label.create_date'=>'Create Date',
    'slider.image.model.label.update_date'=>'Update Date',

    'home.index.tab.custom.content'=>'Custom Content Management',
);