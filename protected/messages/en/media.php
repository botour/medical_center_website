<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/5/15
 * Time: 3:18 PM
 */

return array(
    'page.title.index.media'=>'Media Manager',
    'controller.page.index.content.title'=>'Media Manager',

    //Actions
    'controller.index.add.image'=>'Add Image'
);