<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/6/15
 * Time: 8:00 AM
 */


return array(
    'model.label.id'=>'ID',
    'model.label.full_name'=>'Full Name',
    'model.label.full_name_ar'=>'Full Name(Arabic)',
    'model.label.email'=>'Email',
    'model.label.contact_type'=>'Contact Type',
    'model.label.body'=>'Message',
);