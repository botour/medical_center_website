<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/23/15
 * Time: 1:01 PM
 */

return array(
    'page.title.create.content'=>'Add Content',
    'controller.page.create.content.title'=>'Add Content',

    'page.title.create.news.content'=>'Add News',
    'controller.page.create.news.content.title'=>'Add News',

    'page.title.create.article.content'=>'Add Article',
    'controller.page.create.article.content.title'=>'Add Article',

    'controller.create.panel.tab.english'=>'English',
    'controller.create.panel.tab.arabic'=>'Arabic',

    'page.title.index.content'=>'Content Management',
    'controller.page.index.content.title'=>'Content Management',

    'model.type.event'=>'Event',
    'model.type.news'=>'News',
    'model.type.article'=>'Article',
    'select.content.type.message'=>'Select Content Type',
    'select.block.type.message'=>'Select Block Type',
    'controller.content.action.add.news'=>'Add News Item',
    'model.type.img'=>'Image Only',
    'model.type.txt'=>'Text Only',
    'model.type.txt.img1'=>'Text plus one Image',
    'model.type.txt.img2'=>'Text plus 2 images',
    'model.type.txt.img3'=>'Text plus 3 images',

    'content.type.news'=>'News',
    'content.type.article'=>'Article',

    'content.type.active'=>'Active',
    'content.type.inactive'=>'Inactive',

    'model.label.title'=>'Content Title',
    'model.label.type'=>'Content Type',
    'model.label.active'=>'Content Status',
    'model.label.title_ar'=>'Content Title (Arabic)',
    'model.label.text'=>'Content Text',
    'model.label.text_ar'=>'Content Text (Arabic)',
    'model.label.primary.image.file'=>'Primary Image',

);