<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/5/15
 * Time: 2:26 PM
 */

return array(
    'page.title.index'=>'Settings',
    'controller.page.index.content.title'=>'Settings',

    //Branches
    'model.label.branch.id'=>'Id',
    'model.label.branch.name'=>'Name',
    'model.label.branch.name_ar'=>'Name(Arabic)',

    'page.title.index.branch'=>'Branches Management',
    'controller.page.index.branch.content.title'=>'Branches Management',

    'controller.page.create.branch.content.title'=>'Add Branch',
    'page.title.create.branch'=>'Add Branch',

    'action.create.branch.type'=>'Add Branch',

    'controller.branch.action.add.branch'=>'Add',
    'controller.branch.action.update.branch'=>'Update',

    'page.title.update.branch'=>'Edit Branch Info',
    'controller.page.update.branch.content.title'=>'Edit Branch Info',

    //ContactTypes
    'model.label.contact.type.id'=>'Id',
    'model.label.contact.type.name'=>'Name',
    'model.label.contact.type.name_ar'=>'Name(Arabic)',

    'page.title.index.contact.type'=>'Contact Types Management',
    'controller.page.index.contact.type.content.title'=>'Contact Types Management',

    'controller.page.create.contact.type.content.title'=>'Add Contact Type',
    'page.title.create.contact.type'=>'Add Contact Type',

    'action.create.contact.type.type'=>'Add Contact Type',

    'controller.contact.type.action.add.contact.type'=>'Add',
    'controller.contact.type.action.update.contact.type'=>'Update',

    'page.title.update.contact.type'=>'Edit Branch Info',
    'controller.page.update.contact.type.content.title'=>'Edit Contact Type Info',


    //Staff Jobs
    'model.label.job.id'=>'ID',
    'model.label.job.title'=>'Name',
    'model.label.job.title_ar'=>'Name(Arabic)',
    'page.title.index.job'=>'Jobs Types',
    'controller.page.index.job.content.title'=>'Jobs Type',
    'page.title.create.job'=>'Job Create',
    'controller.page.create.job.content.title'=>'Job Create',
    'page.title.update.job'=>'Job Update',
    'controller.page.update.job.content.title'=>'Job Update',
    'action.create.job.type'=>'Add Job Type',
    'controller.job.action.add.Job'=>'Add',
    'controller.job.action.update.Job'=>'Update',



);