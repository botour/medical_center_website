<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/27/15
 * Time: 3:35 PM
 */

return array(
    'page.title.create.doctor'=>'Add a Doctor',
    'controller.page.create.content.title'=>'Add a Doctor',

    'controller.doctor.create.tab.content.personal.info'=>'Personal Info',
    'controller.doctor.create.tab.content.cv.info'=>'CV Info',
    'controller.doctor.create.tab.content.save'=>'Save Data',
    'controller.create.panel.tab.english'=>'English',
    'controller.create.panel.tab.arabic'=>'Arabic',
    'controller.doctor.create.tab.content.contact'=>'Contact Info',

    'controller.action.create.label.contact.info'=>'Contact Info',
    //Actions
    'controller.doctor.create.tab.content.add.doctor'=>'Add Doctor',

    'page.title.index.job'=>'Jobs Types',
    'controller.page.index.job.content.title'=>'Jobs Type',
    'page.title.create.job'=>'Job Create',
    'controller.page.create.job.content.title'=>'Job Create',
    'page.title.update.job'=>'Job Update',
    'controller.page.update.job.content.title'=>'Job Update',

    'staff.title.dr'=>'Dr',
    'staff.title.other'=>'Other',

    'staff.branch.1'=>'Branch 1',
    'page.title.view'=>'Staff Member',
    'controller.page.view.content.title'=>'Staff Member',
    'staff.branch.2'=>'Branch 2',

    'page.title.create'=>'Add Staff Memeber',
    'controller.page.create.content.title'=>'Add Staff Member',
    'model.label.title'=>'Title',
    'model.label.branch_id'=>'Branch',
    'model.label.job'=>'Job',
    'controller.Staff.action.add.Staff'=>'Add Staff Member',
    'controller.Staff.action.update.Staff'=>'Update Staff Member',


    'page.title.index'=>'Staff Member Management',
    'controller.page.index.content.title'=>'Staff Member Management',
    'model.label.profile_image'=>'Profile Image',

    'page.title.cv.create'=>'Add CV Section',
    'controller.page.create.cv.content.title'=>'Add CV Section',


);