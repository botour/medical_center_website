<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 7/8/15
 * Time: 4:09 PM
 */



return array(
    'homepage.nav.item.home'=>'Home',
    'homepage.nav.item.clinics'=>'Clinics & Services',
    'homepage.nav.item.vacancies'=>'Jobs',
    'homepage.nav.item.doctors'=>'Staff',
    'homepage.nav.item.contact'=>'Contact Us',
    'homepage.nav.item.news'=>'News',
    'homepage.nav.item.articles'=>'Articles',
    'homepage.nav.item.gallery'=>'Gallery',


    'call.us'=>'Call Us',

    'homepage.nav.item.home.welcome'=>'',
    'homepage.nav.item.home.vision'=>'',
    'homepage.nav.item.home.who.we.are'=>'',

    'homepage.nav.item.clinics.obg'=>'OB',
    'homepage.nav.item.clinics.internal'=>'Internal illnesses',
    'homepage.nav.item.clinics.skin'=>'Skin',
    'homepage.nav.item.clinics.natural'=>'Natural',
    'homepage.nav.item.clinics.kids'=>'Child',
    'homepage.nav.item.clinics.pharmacy'=>'pharmacy',

    'read.more'=>'Read More',
    'welcome.title'=>'Welcome to Al-Hayat Medical Center',
    'links'=>'Links',
    'faq'=>'FAQ',
    'home.page.title'=>'Al-Hayat Medical Center | Homepage',

    'internal'=>'internal',
    'dr.fawaz'=>'Dr.Fawaz Saad',
    'dr.fawaz.title'=>' Al-Hayat Medical Center Director',
    'ob.gyn'=>'ob.gyn',
    'dermatology'=>'dermatology',
    'dental'=>'dental',
    'laboratory'=>'laboratory',
    'pharmacy'=>'pharmacy',
    'dr.1'=>'Dr.Fawaz Saad',
    'dr.2'=>'Dr.Burhan Otour',
    'dr.3'=>'Dr.Zaher Saad',
    'dr.4'=>'Dr.Huda Otour',
    'welcome.news'=>'News',
    'service.tab.content.staff.header'=>'Doctors',
    'our.message'=>'Our Message',
    'service.tab.description'=>'Description',
    'service.tab.staff'=>'Staff',
    'service.tab.equipment'=>'Equipments',
    'service.tab.faq'=>'FAQ',
    'service.tab.content.staff.read.more'=>'Check the CV',
    'service.tab.content.description'=>'Description for the service  Description for the service Description for the service Description for the service Description for the service Description for the service Description for the service Description for the service Description for the service Description for the service ',
    'service.tab.staff'=>'Staff',
    'service.tab.equipments'=>'Equipments',
    'service.tab.content.faq.q'=>'What do you want from me?',
    'service.tab.content.faq.a'=>'I want nothing from you!!! I want nothing from you!!! I want nothing from you!!! I want nothing from you!!! I want nothing from you!!! I want nothing from you!!!',
    'service.tab.faq'=>'FAQ',
    'staff.filter.label.branch'=>'Branch',
    'staff.filter.label.job'=>'Job',
    'staff.filter.label.section'=>'Section',

    'please.select.a.job'=>'Please select a Job',
    'please.select.a.branch'=>'Please select a branch',
    'please.select.a.section'=>'Please select a Section',

    'branch1'=>'Branch1',
    'branch2'=>'Branch2',

    'filter.staff'=>'Filter Staff',
    'you.are.in'=>'You are in',
    'staff.cv.sections'=>'CV Sections',

    'staff.dob'=>'Date and place of birth',
    'staff.nationality'=>'Nationality',
    'staff.contact'=>'Contact Info',
    'staff.m.status'=>'Marital Status',
    'staff.job'=>'Job',
    'get.in.touch'=>'Get In Touch',
    'staff.address'=>'Address',

    'please.select.type'=>'Please Select Type',
    'inquiry'=>'Inquiry',
    'complaint'=>'Complaint',
    'homepage.nav.item.contact.info'=>'Contact Info',
    'homepage.nav.item.contact.address'=>'Contact Us',

    'contact.submit'=>'Submit',
    'adds.goes.here'=>'Adds Goes Here',

    'job.vacancies'=>'Job Vacancies',

    'job.opportunity'=>'Job Opportunity',

    'vacancy.label.position'=>'Position',
    'vacancy.label.description'=>'Description',
    'vacancy.label.salary'=>'Salary',
    'vacancy.label.contact'=>'Contact Email',
'our.services'=>'Our Services',
    'our.doctors'=>'Our Doctors',

    'enter.search.keyword.here'=>'Enter Search Keyword Here ...',
    'homepage.nav.item.home.welcome'=>'Welcome Message',

    'homepage.nav.item.home.vision'=>'Our Vision',

    'no.contents.available'=>'No Content Available',
    'homepage.nav.item.home.who.we.are'=>'Who We Are',

    'your.contact.has.been.successfully.sent'=>'Your Contact Has been sent successfully',
    'breadcrumb.home'=>'Home',

    'no.articles.available.for.now'=>'No Articles Available for now',
    'breadcrumb.articles'=>'Medical Articles',
    'articles'=>'Medical Articles',

    'enter.search.keywords'=>'Enter Search Keyword',
    'contact.details'=>'Contact Details',
    'breadcrumb.gallery'=>'Galleries',
    'galleries'=>'Galleries',
    'welcome.insurance'=>'We accept insurance from the following Insurance companies:',
    'no.galleries.available.for.now'=>'No Galleries Available for now',
    'breadcrumb.staff'=>'Staff',
    'breadcrumb.vacancies'=>'Vacancies',
    'breadcrumb.contact'=>'Contact Us',

    'search.result'=>'Search Result',
    'gallery'=>'Gallery',
    'no.job.opportunities.available.for.now'=>'No Job Opportunities Available for now',
);
