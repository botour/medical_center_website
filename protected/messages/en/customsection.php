<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 4/26/15
 * Time: 4:41 PM
 */

return array(
    'model.label.id'=>'Id',
    'model.label.type'=>'Type',
    'model.label.order'=>'Order',
    'model.label.title'=>'Title',
    'model.label.title_ar'=>'Title (in Arabic)',
    'model.label.summary'=>'Summary',
    'model.label.summary_ar'=>'Summary (in Arabic)',
    'model.label.body'=>'Body',
    'model.label.body_ar'=>'Body (in Arabic)',
    'model.label.gallary'=>'Gallery',
    'model.label.active'=>'Active',
);