<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 9/27/15
 * Time: 9:50 PM
 */

return array(
    'model.label.id'=>'ID',
    'model.label.branch' => 'Branch',
    'model.label.branch_ar'=>'Branch(Arabic)',
    'model.label.email'=>'Email',
    'model.label.tel_1'=>'Tel 1',
    'model.label.tel_2'=>'Tel 2',
    'model.label.address'=>'Address',
    'model.label.address_ar'=>'Address (Arabic)',
    'model.label.fax'=>'FAX',
    'model.label.mobile'=>'Mobile 1',
    'model.label.mobile_2'=>'Mobile 2',
);