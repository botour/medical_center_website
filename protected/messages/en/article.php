<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 5/3/15
 * Time: 1:18 PM
 */

return array(
    'page.title.create.article'=>'Create Article',
    'controller.page.create.content.title'=>'Create Article',

    'controller.create.panel.tab.english'=>'English',
    'controller.create.panel.tab.arabic'=>'Arabic',

    'select.gallery'=>'Select A Gallery',
    'controller.index.add.article'=>'Create Article',

    'page.title.index.article'=>'Articles',
    'controller.page.index.content.title'=>'Articles',
    'controller.index.update.article'=>'Save Article',

    'model.status.inactive'=>'In Active',
    'model.status.active'=>'Active',

    'controller.index.create.success.message'=>'A new Article was created successfully!',
    'controller.index.update.success.message'=>'An Article was updated successfully!',

    'model.label.id'=>'ID',
    'model.label.doctor'=>'The Doctor',
    'model.label.active'=>'Active',
    'model.label.approve'=>'Approve',
    'model.label.title'=>'Title',
    'model.label.title_ar'=>'Title (Arabic)',
    'model.label.summary'=>'Summary',
    'model.label.summary_ar'=>'Summary (Arabic)',
    'model.label.body'=>'Body',
    'model.label.body_ar'=>'Body (Arabic)',
    'model.label.publish_date'=>'Publish Date',

);