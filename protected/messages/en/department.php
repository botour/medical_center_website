<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 3/2/15
 * Time: 11:00 AM
 */

return array(


    //Model Text (Attributes)
    'model.label.id'=>'ID',
    'department','model.label.name'=>'Service Name',
    'department','model.label.summary'=>'Service Summary',
    'department','model.label.services'=>'Service Body',
    'model.label.order'=>'Order (0,1,2,...)',

    'model.label.name_ar'=>'Service Name (Arabic)',
    'model.label.summary_ar'=>'Service Summary (Arabic)',
    'model.label.services_ar'=>'Service Body (Arabic)',
    'model.label.status'=>'Status',
    'model.label.create_date'=>'create date',
    'model.label.create_id'=>'create id',
    'model.label.update_date'=>'update date',
    'model.label.update_id'=>'update id',
    'model.label.gallary_id'=>'Attached Gallery',
    'model.label.contact_info'=>'Contact Info',
    'model.label.contact_info_ar'=>'Contact Info (Arabic)',
    'department.model.no.content.available'=>'No Content Available',
    'controller.view.panel.tab.service-info'=>'Info',
    'controller.view.panel.tab.service-faq'=>'FAQ',
    'controller.view.panel.tab.service-equipment'=>'Equipment',
    'controller.view.panel.tab.service-item'=>'Service Item',
    'controller.view.panel.tab.service-staff'=>'Staff',
    //Page Titles and subtitle (page title and content title)
    'page.title.create.department'=>'Create Service',
    'page.title.index.department'=>'Manage Services',
    'page.title.update.department'=>'Update Service',
    'controller.page.index.content.title'=>'Manage Services',
    'controller.page.create.content.title'=>'Create Service',
    'controller.page.update.content.title'=>'Update Service',
    'controller.page.view.info.update'=>'Edit Service/Clinic info',
    'page.title.view.department'=>'View Service',
    'controller.page.update.view'=>'View Service/Clinic Info',
    'controller.page.view.content.title'=>'View Service',
    'controller.view.panel.heading.title'=>'Service',
    'controller.create.panel.tab.english'=>'English',
    'controller.create.panel.tab.arabic'=>'Arabic',
    'controller.view.panel.tab.add-new-section'=>'Add New Section',

    'controller.content.action.add.service'=>'Add Service Info',
    'controller.content.action.update.service'=>'Edit Service Info',

    //Actions
    'action.create.department'=>'Create Service',
    'action.update.department'=>'Save Service',
    'action.view.faq.update'=>'Update',
    'action.view.faq.delete'=>'Delete',
    'action.create.faq'=>'Add an FAQ',
    'action.update.faq'=>'Add an FAQ',
    'action.create.service.item'=>'Add Service Item',
    'controller.content.action.update.service.item'=>'Update Service Item',
    'controller.content.action.add.service.item'=>'Add Service Item',
    'action.update.service.item'=>'Edit Service Item',
    'grid.header.action'=>'Actions',
    'controller.content.action.add.equipment'=>'Add Equipment',
    'page.title.create.equipment'=>'Create Equipment',
    'controller.page.create.equipment.content.title'=>'Create Equipment',
    'controller.equipment.action.add.equipment'=>'Add Equipment',
    'controller.equipment.action.update.equipment'=>'Update Equipment',

    'model.label.equipment.body_ar'=>'Body(Arabic)',
    'model.label.equipment.body'=>'Body',
    'model.label.equipment.name'=>'Name',
    'model.label.equipment.name_ar'=>'Name(Arabic)',
    'model.label.item'=>'Service Item',
    'model.label.item_ar'=>'Service Item (in Arabic)',
    'model.label.active'=>'Active',
    'controller.equipment.action.add.news'=>'Add Equipment',

    'department.status.inactive'=>'inActive',
    'department.status.active'=>'Active',
    'department.type.service.clinic'=>'Service/Clinic',
    'department.type.organizational'=>'Organizational',
    'alert.service.item.create.success'=>'A new Service Item was created successfully',
    'alert.service.item.create.error'=>'Error Creating the Service Item',

    'alert.faq.create.success'=>'A new FAQ Item was created successfully',
    'alert.faq.create.error'=>'Error Creating the FAQ Item',

    'controller.page.update.equipment.content.title'=>'Update Equipment',

    'action.create.equipment.image'=>'Add Image',
    'page.title.update.equipment'=>'Update Equipment',
    'page.title.create.equipment'=>'Create Equipment',
    'page.title.view.equipment'=>'View Equipment',
    'controller.page.view.equipment.content.title'=>'View Equipment',
    'controller.page.create.equipment.content.title'=>'Create Equipment',
    'action.create.equipment'=>'Create Equipment',

    'controller.page.create.equipment.image.content.title'=>'Add Image',
    'page.title.create.equipment.image'=>'Add Image',
    'model.label.equipment.order'=>'Order',
    'action.view.service.item.update'=>'Edit Service Item',
    'action.view.service.item.delete'=>'Delete Service Item',

    'model.label.equipment.active'=>'Status',
    'controller.equipment.Image.action.add.Image'=>'Add Image',
    'controller.equipment.Image.action.update.Image'=>'Edit Image',
);