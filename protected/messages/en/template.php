<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 2/28/15
 * Time: 12:54 PM
 */

return array(

    // Side Bar
    //Dashboard
    'sidebar.option.main.dashboard'=>'Dashboard',
    //User Management
    'sidebar.option.main.homepage'=>'Homepage',
    'sidebar.option.main.management'=>'Management',
    'sidebar.option.main.media-management'=>'Media Center',
    'sidebar.option.main.user-management'=>'Users',
    'sidebar.option.user-management.sub.create-user'=>'Create User',
    'sidebar.option.user-management.sub.manage-users'=>'Manage Users',

    'sidebar.option.main.department-management'=>'Clinics & Services',
    'sidebar.option.department-management.sub.create-department'=>'Create Service',
    'sidebar.option.department-management.sub.manage-departments'=>'Manage Services',

    'sidebar.option.main.content-management'=>'Content',
    'sidebar.option.content-management.sub.create-content'=>'Create Content',
    'sidebar.option.content-management.sub.manage-contents'=>'Manage Contents',


    'sidebar.option.main.staff-management'=>'Staff',

    'sidebar.option.staff-management.sub.create-doctor'=>'Create Doctor',
    'sidebar.option.staff-management.sub.create-staff'=>'Create Staff',
    'sidebar.option.staff-management.sub.manage-staffs'=>'Manage Staff',

    'sidebar.option.main.job-management'=>'Jobs & Vacancies',
    'sidebar.option.department-management.sub.create-job'=>'Create Vacancy',
    'sidebar.option.department-management.sub.manage-jobs'=>'Manage Vacancies',

    'sidebar.option.main.front-end'=>'Front End',
    'sidebar.option.main.article-management'=>'Articles',
    'sidebar.option.article-management.sub.create-article'=>'Create Article',
    'sidebar.option.article-management.sub.manage-articles'=>'Manage Articles',

    'sidebar.option.main.gallery-management'=>'Galleries',
    'sidebar.option.department-management.sub.create-gallery'=>'Create Gallery',
    'sidebar.option.department-management.sub.manage-galleries'=>'Manage Galleries',

    'sidebar.option.main.ads-management'=>'Ads',
    'sidebar.option.user-management.sub.create-ad'=>'Create User',
    'sidebar.option.user-management.sub.manage-ads'=>'Manage Users',

    'sidebar.option.main.link-management'=>'Links',
    'sidebar.option.user-management.sub.create-link'=>'Create Link',
    'sidebar.option.user-management.sub.manage-links'=>'Manage Links',

    'sidebar.option.main.contact-management'=>'Contact Management',
    'sidebar.option.contact-management.sub.create-contact-details'=>'Create Contact Details',
    'sidebar.option.contact-management.sub.manage-contact-details'=>'Manage Contact Details',
    'sidebar.option.contact-management.sub.manage-messages'=>'Manage Messages',


    'sidebar.option.main.visitor-messages'=>'Visitor Messages',
    'sidebar.option.main.website-settings'=>'Settings',

    //Breadcrumbs
    'main.function.start.searching'=>'Search ...',
    //Actions
    'main.function.logout'=>'Logout',
    // Language options
    'content.language.option.arabic'=>'Arabic',
    'content.language.option.english'=>'English',
);