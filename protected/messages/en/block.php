<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 8/4/15
 * Time: 4:56 PM
 */


return array(
    'model.label.title'=>'Block Title',
    'model.label.type'=>'Block Type',
    'model.label.active'=>'Block Status',
    'model.label.title_ar'=>'Block Title (Arabic)',
    'model.label.text'=>'Block Text',
    'model.label.text_ar'=>'Block Text (Arabic)',
    'model.label.primary.image.file '=>'Block Primary Image',

);