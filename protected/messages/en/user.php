<?php
/**
 * Created by PhpStorm.
 * User: 3mkBurhan
 * Date: 2/28/15
 * Time: 1:19 PM
 */

return array(
    'controller.page.title.main'=>'User Management',
    //Model Text (Attributes)
    'model.label.id'=>'ID',
    'model.label.first_name'=>'First Name',
    'model.label.last_name'=>'Last Name',
    'model.label.middle_name'=>'Middle Name',
    'model.label.username'=>'User Name',
    'model.label.login_name'=>'Login Name',
    'model.label.email'=>'Email',
    'model.label.password'=>'Password',
    'model.label.status'=>'Status',
    'model.label.create_date'=>'Create Date',
    'model.label.create_id'=>'Create ID',
    'model.label.update_date'=>'Update Date',
    'model.label.update_id'=>'Update ID',
    // Form placeholders
    'user.form.header.roles.and.permissions'=>'Roles And Permissions',
    //Page Titles and subtitle (page title and content title)
    'page.title.create.user'=>'Create User',
    'page.title.index.user'=>'Manage Users',
    'page.title.update.user'=>'Update User',
    'controller.page.update.content.title'=>'Update User',
    'controller.page.index.content.title'=>'Manage Users',
    'controller.page.create.content.title'=>'Create User',
    // Constants
    'user.status.offline'=>'Offline',
    'user.status.break'=>'Break',
    'user.status.online'=>'Online',
    //Tooltips
    'user.tooltip.edit.user'=>'Edit User',
    'user.tooltip.delete.user'=>'Delete User',
    //Actions
    'action.create.user'=>'Create User',
    'action.update.user'=>'Save',
    'grid.header.action'=>'Actions',
);