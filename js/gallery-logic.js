/**
 * Created by 3mkBurhan on 4/6/15.
 */


$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
    updateGridLinks();
    $("#refresh-gallery-list-button").click(function(){
        updateGalleryList();
    });
    $("#showModal").click(function(){
       $.ajax({
            'url':getGalleryCreateFormUrl,
            'data':{},
            'success':function(data){
                $("#createGalleryModal .modal-body").html(data);
                $(".ok-sign").hide();
                $(".error-sign").hide();
            },
            'beforeSend':function(){
                $("#createGalleryModal .modal-body").html("Please Wait...");
                $("#createGalleryModal").modal("show");
            },
            'error':function(){
                $("#createGalleryModal").modal("hide");
            }
        });

    });


});

var updateGridLinks = function(){
    $(".delete-gallery-btn").bind('click',function(){
        $.ajax({
            'url':deleteUrl,
            'type':'POST',
            'dataType':'json',
            'data':{
                'id':parseInt($(this).attr('date-id'))
            },
            'success':function(data){
                if(data.status){
                    updateGalleryList();
                }
            },
            'error':function(){

            }
        });
    });
    $(".edit-gallery-btn").bind('click',function(){
        $.ajax({
            'url':getGalleryUpdateFormUrl,
            'type':'GET',
            'data':{
                'id':parseInt($(this).attr('data-id'))
            },
            'success':function(data){
                $("#updateGalleryModal .modal-body").html(data);
                $(".ok-sign").hide();
                $(".error-sign").hide();
            },
            'beforeSend':function(){
                $("#updateGalleryModal .modal-body").html("Please Wait...");
                $("#updateGalleryModal").modal("show");
            },
            'error':function(){
                $("#updateGalleryModal").modal("hide");
            }
        });
    });
    $(".active-checkbox").bind('change',function(){
        if($(this).is(":checked")){
            $.ajax({
                'url':activateUrl,
                'type':'POST',
                'dataType':'json',
                'data':{
                    'gallery_id':$(this).attr('data-id')
                },
                'success':function(data){
                    $(this).val(data.status)
                }
            });
        }else{
            $.ajax({
                'url':deactivateUrl,
                'type':'POST',
                'dataType':'json',
                'data':{
                    'gallery_id':$(this).attr('data-id')
                },
                'success':function(data){
                    $(this).val(!data.status)
                }
            });
        }
    });
}

var updateGalleryList = function(){
    $.ajax({
        'url':updateGalleryListUrl,
        'data':{},
        'type':'GET',
        'success':function(data){
            $("#gallery-list").html(data);
            updateGridLinks();
            $(".gallery-list-checkbox").bootstrapSwitch();
        },
        'error':function(){

        },
        'beforeSend':function(){

        }
    });
}
