/**
 * Created by 3mkBurhan on 4/11/15.
 */


var updateImagesList = function(){
    $.ajax({
        'url':updateImageListUrl,
        'data':{
            'id':parseInt($("#gallery-id").val())
        },
        'type':'GET',
        'success':function(data){
            $("#images-list").html(data);
            updateImagesGridLinks();
        },
        'error':function(){

        },
        'beforeSend':function(){

        }
    });
}

var updateImagesGridLinks = function(){
    $(".delete-image").bind('click',function( event ){
        event.preventDefault();
        if(confirm("Are you sure you want to delete the image?")){

            $.ajax({
                url: deleteImageUrl,
                data: {
                    "data-id": $(this).attr("data-id")
                },
                'type': 'POST',
                'dataType': 'json',
                'success': function(data){
                    if(data.status){
                        updateImagesList();
                    }else{
                        alert(data.message);
                    }
                },
                'error': function(){
                    alert("Error");
                }
            });
        }
    });
    $(".edit-image").bind('click',function( event ){
        event.preventDefault();
        $.ajax({
            'url':getImageUpdateFormUrl,
            'type':'GET',
            'data':{
                'id':parseInt($(this).attr('data-id'))
            },
            'success':function(data){
                $("#updateImageModal .modal-body").html(data);
                $(".ok-sign").hide();
                $(".error-sign").hide();
            },
            'beforeSend':function(){
                $("#updateImageModal .modal-body").html("Please Wait...");
                $("#updateImageModal").modal("show");
            },
            'error':function(){
                $("#updateImageModal").modal("hide");
            }
        });
    });
    $(".move-image").bind('click',function( event ){
        event.preventDefault();
        $.ajax({
            'url':getMoveImageFormUrl,
            'type':'GET',
            'data':{
                'id':parseInt($(this).attr('data-id'))
            },
            'success':function(data){
                $("#moveToGalleryModal .modal-body").html(data);
                $(".ok-sign").hide();
                $(".error-sign").hide();
            },
            'beforeSend':function(){
                $("#moveToGalleryModal .modal-body").html("Please Wait...");
                $("#moveToGalleryModal").modal("show");
            },
            'error':function(){
                $("#moveToGalleryModal").modal("hide");
            }
        });
    });
    $(".gallery").colorbox({
        rel: 'gallery',
        transition: "none",
        retinaImage: true
    });
    $('.wrap-image .chkbox').bind('click', function () {
        $(".wrap-image").removeClass('selected').children('a').children('img').removeClass('selected');
        $.ajax({

        });
        if ($(this).parent().hasClass('selected')) {
            $(this).parent().removeClass('selected').children('a').children('img').removeClass('selected');
        } else {
            $(this).parent().addClass('selected').children('a').children('img').addClass('selected');
        }
    });
}

var uploadImage = function(){
    var formData = new FormData();
    var imageToUpload = document.getElementById('Image_image');
    formData.append("Image[image]",$(imageToUpload)[0].files[0]);
    formData.append("Image[gallery_id]",$("#Image_gallery_id").val());
    formData.append("Image[type]",$("#Image_type").val());
    $.ajax({
        url: uploadImageUrl,
        type: 'POST',
        cache: false,
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status){
                updateImagesList();
            }
        },
        error: function () {
            alert("ERROR in upload");
        }
    });
}

$(function(){
    updateImagesGridLinks();
});