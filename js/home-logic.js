var _URL = window.URL || window.webkitURL;


$(function(){
    $("#notifyOnFormSubmitModal").on("hidden.bs.modal",function(){
        $(".fileupload").fileupload("reset");
    });
    $("#slider-image-controll").change(function (e) {
        var image, file;
        if ((file = this.files[0])) {
            image = new Image();
            image.onload = function () {
                if(this.width!=1920 || this.height!=520){
                    $("#notifyOnFormSubmitModal .modal-body").html("<p>The image width or height is not appropriate</p><p>Select an image in the size of: 1920X520 for maximum fit</p>");
                    $("#notifyOnFormSubmitModal").modal("show");
                }
            };
            image.src = _URL.createObjectURL(file);
        }
    });
});

