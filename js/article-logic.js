/**
 * Created by 3mkBurhan on 5/3/15.
 */


$(function(){
    CKEDITOR.replace( 'body-field',{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );
    CKEDITOR.replace( 'body-ar-field',{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );

    $(".article-checkbox").on("ifChecked", function () {
        checkBox = this;
        $.ajax({
            "type": "post",
            "dataType": "json",
            "url": changeArticleStatusToActiveURL,
            "data": {
                "id": $(checkBox).val()
            },
            "success": function () {
                $(checkBox).iCheck("check");
                $(".article-checkbox").iCheck("enable");
            },
            "error": function () {
                $(checkBox).iCheck("uncheck");
                $(".article-checkbox").iCheck("enable");
            },
            "beforeSend": function () {
                $(".article-checkbox").iCheck("disable");
            }
        });
    });
    $(".article-checkbox").on("ifUnchecked", function () {
        checkBox = this;
        $.ajax({
            "type": "post",
            "dataType": "json",
            "url": changeArticleStatusToInActiveURL,
            "data": {
                "id": $(checkBox).val()
            },
            "success": function () {
                $(checkBox).iCheck("uncheck");
                $(".article-checkbox").iCheck("enable");
            },
            "error": function () {
                $(checkBox).iCheck("check");
                $(".article-checkbox").iCheck("enable");
            },
            "beforeSend": function () {
                $(".article-checkbox").iCheck("disable");
            }
        });
    });
});