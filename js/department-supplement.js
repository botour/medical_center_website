/**
 * Created by 3mkBurhan on 4/19/15.
 */

var updateFAQFormSubmit = function(){
    $("#"+faqFormId).on("submit",function(event){
        event.preventDefault();
        $.ajax({
            url: faqFormSubmitUrl,
            type: "POST",
            dataType: "json",
            data: $(this).serialize(),
            success: function(data){
                if(data.status){
                    updateFAQList();

                }
                $("#"+faqModalId+" .modal-body").html(data.message);
                setTimeout(function(){
                    $("#"+faqModalId).modal("hide");
                },2000);
                $("#"+faqModalId+" .modal-body").html(data.message);
                setTimeout(function(){
                    $("#"+faqModalId).modal("hide");
                },2000);
            },
            error: function(){
                $("#"+faqModalId).modal("hide");
            },
            beforeSend: function(){
                $("#"+faqModalId+" .modal-body").html("Please Wait..");
            }
        });

    });
}

var updateServiceItemFormSubmit = function(){
    $("#"+serviceItemFormId).on("submit",function(event){
        event.preventDefault();
        $.ajax({
            url: serviceItemFormSubmitUrl,
            type: "POST",
            dataType: "json",
            data: $(this).serialize(),
            success: function(data){

                $("#"+serviceItemModalId+" .modal-body").html(data.message);
                setTimeout(function(){
                    $("#"+serviceItemModalId).modal("hide");
                },2000);
                if(data.status){
                    updateServiceItemList();
                }
            },
            error: function(){
                $("#"+serviceItemModalId).modal("hide");
            },
            beforeSend: function(){
                $("#"+serviceItemModalId+" .modal-body").html("Please Wait..");
            }
        });

    });
}

var updateServiceItemList = function(){
    $.ajax({
        url: updateServiceItemListUrl,
        data: {
            'department_Id': $("#department_id").val()
        },

        type: "GET",
        success: function(data){
            $("#services-list").html(data);
            updateServiceItemListLinks();
        }
    });
}


var updateServiceItemListLinks = function(){
    $(".service-item-update-link").on('click',function(){
        $.ajax({
            url: getUpdateServiceItemFormUrl,
            type: "GET",
            data: {
                'id': parseInt($(this).attr("data-id"))
            },
            success: function(data){
                $("#updateServiceItemModal .modal-body").html(data);
                $("#updateServiceItemModal").modal("show");
            }
        });
    });
    $(".service-item-delete-link").on('click',function(){
        if(confirm("Delete?")){
            $.ajax({
                url: deleteServiceItemUrl,
                type: "POST",
                dataType: "json",
                data: {
                    'id': parseInt($(this).attr("data-id"))
                },
                success: function(data){
                    if(data.status){
                        updateServiceItemList();
                    }
                }
            });
        }

    });

}



var updateCustomSectionFormSubmit = function(){
    $("#"+customSectionFormId).on("submit",function(event){
        event.preventDefault();
        $.ajax({
            url: customSectionFormSubmitUrl,
            type: "POST",
            dataType: "json",
            data: $(this).serialize(),
            success: function(data){
                if(data.status){
                    if(data.type==CUSTOM_SECTION_CREATE){
                        addSectionTab(data.id,data.title);
                    }else if (data.type==CUSTOM_SECTION_UPDATE){
                        updateCustomSectionTabContent(data.id);
                    }
                }
                $("#"+customSectionModalId+" .modal-body").html(data.message);
                setTimeout(function(){
                    $("#"+customSectionModalId).modal("hide");
                },2000);
                $("#"+customSectionModalId+" .modal-body").html(data.message);
                setTimeout(function(){
                    $("#"+customSectionModalId).modal("hide");
                },2000);
            },
            error: function(){
                $("#"+customSectionModalId).modal("hide");
            },
            beforeSend: function(){
                $("#"+customSectionModalId+" .modal-body").html("Please Wait..");
            }
        });

    });
}
var updateFAQListLinks = function(){
    $(".faq-update-link").on('click',function(){
        $.ajax({
            url: getUpdateFAQFormUrl,
            type: "GET",
            data: {
                'id': parseInt($(this).attr("data-id"))
            },
            success: function(data){
                $("#updateFAQModal .modal-body").html(data);
                $("#updateFAQModal").modal("show");
            }
        });
    });
    $(".faq-delete-link").on('click',function(){
        if(confirm("Delete?")){
            $.ajax({
                url: deleteFAQUrl,
                type: "POST",
                dataType: "json",
                data: {
                    'id': parseInt($(this).attr("data-id"))
                },
                success: function(data){
                    updateFAQList();
                }
            });
        }

    });

}

var updateCustomTabLinks = function () {
    $('#custom-section-create-tab').on('click',function(){
        $.ajax({
            url: $(this).attr("url"),
            type: "GET",
            data: {
                department_id: $("#department_id").val()
            },
            success: function(data){
                $("#createCustomSectionModal .modal-body").html(data);
            },
            beforeSend: function(){
                $("#createCustomSectionModal .modal-body").html("Please Wait...");
                $("#createCustomSectionModal").modal("show");
            },
            error: function(){
                $("#createCustomSectionModal").modal("hide");
            }
        });
    });
}
$(function(){
    updateFAQListLinks();
    updateServiceItemListLinks();
    updateCustomTabLinks();
    updateCustomSectionLinks();
});
var updateFAQList = function(){
    $.ajax({
        url: updateFQAListUrl,
        data: {
            'department_Id': $("#department_id").val()
        },

        type: "GET",
        success: function(data){
            $("#faq-list").html(data);
            updateFAQListLinks();
        }
    });
}

var updateCustomSectionTabContent = function(sectionId){
    $.ajax({
        url: actionGetCustomSectionTabContentUrl,
        data: {
            custom_section_id: sectionId
        },
        type: 'GET',
        success: function(data){
            $("#content-"+sectionId).html(data);
            updateCustomSectionLinks();
        }
    });
}

var updateCustomSectionLinks = function(){
    $(".custom-section-delete-link").unbind('click');
    $(".custom-section-delete-link").on('click',function(event){
        event.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            dataType: 'json',
            type: 'POST',
            data: {
                id: $(this).attr('data-id')
            },
            success: function(data){
                alert(data.status);
                if(data.status){
                    $(".tab-pane#content-"+data.id).remove();
                    $("#tab-"+data.id).parent("li").remove();
                }
            }
        });
    });
}
var addSectionTab = function(sectionId,title){
    $("#custom-section-create-tab").parent("li").before('<li><a data-toggle="tab" href="#content-'+sectionId+'" id="tab-'+sectionId+'">'+title+'</a></li>');
    $(".tab-pane").last().after('<div class="tab-pane in" id="content-'+sectionId+'">Loading ... </div>');
    updateCustomSectionTabContent(sectionId);
}