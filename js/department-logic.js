/**
 *
 * Created by 3mkBurhan on 3/2/15.
 *
 */

$(function(){
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-condensed");
    $(".tooltip-link").tooltip();
    $(".ok-sign").hide();
    $(".error-sign").hide();
    updateDepartmentFormControls();
    updateCustomTabLinks();
});

var updateDepartmentFormControls = function(){
    CKEDITOR.replace( 'summary-field',{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );
    CKEDITOR.replace( 'summary-ar-field' ,{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );
    CKEDITOR.replace( 'services-field',{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    }  );
    CKEDITOR.replace( 'services-ar-field',{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    }  );
    CKEDITOR.replace( 'contact-info-field',{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    }  );
    CKEDITOR.replace( 'contact-info-ar-field' ,{
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl" : imagesListJSONFile
    } );
}


var updateFAQFormSubmit = function(){
    $("").on('submit',function(event){
        event.preventDefault();
    });
}