-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 05:10 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medical_center`
--
CREATE DATABASE IF NOT EXISTS `medical_center` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `medical_center`;

-- --------------------------------------------------------

--
-- Table structure for table `authassignment`
--

DROP TABLE IF EXISTS `authassignment`;
CREATE TABLE `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

DROP TABLE IF EXISTS `authitem`;
CREATE TABLE `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

DROP TABLE IF EXISTS `authitemchild`;
CREATE TABLE `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article`
--

DROP TABLE IF EXISTS `tbl_article`;
CREATE TABLE `tbl_article` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `approve` tinyint(4) NOT NULL,
  `title` tinytext NOT NULL,
  `title_ar` tinytext NOT NULL,
  `summary` text NOT NULL,
  `summary_ar` text NOT NULL,
  `body` text NOT NULL,
  `body_ar` text NOT NULL,
  `publish_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_article`
--

INSERT INTO `tbl_article` (`id`, `doctor_id`, `active`, `approve`, `title`, `title_ar`, `summary`, `summary_ar`, `body`, `body_ar`, `publish_date`) VALUES
(3, 1, 1, 1, 'adasdasdasdasadasdasdasdas a', 'adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas    a', '<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n', '<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n', '<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n', '<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>adasdasdasdasadasdasdasdas adasdasdasdasadasdasdasdas</p>\r\n', '2015-09-27'),
(4, 1, 1, 0, 'approveapproveapprove approveapproveapprove ', 'approveapproveapprove approveapproveapprove ', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '2015-09-28'),
(5, 1, 0, 0, 'approveapproveapprove approveapproveapprove ', 'approveapproveapprove approveapproveapprove ', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '<p>approveapproveapprove approveapproveapprove</p>\r\n', '2015-09-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_block`
--

DROP TABLE IF EXISTS `tbl_block`;
CREATE TABLE `tbl_block` (
  `id` int(11) NOT NULL,
  `title` tinytext,
  `title_ar` tinytext,
  `text` text,
  `text_ar` text,
  `content_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_block`
--

INSERT INTO `tbl_block` (`id`, `title`, `title_ar`, `text`, `text_ar`, `content_id`, `type`) VALUES
(1, '', '', '<p>zxczczczxcxzczxczxczxczxczxczxc</p>\r\n', '<p>zczxczxczxczxcxzczxczxc</p>\r\n', 1, 1),
(2, '', '', '<p>asdasdsadadasdasdasd</p>\r\n', '<p>asdasdasdasdsad</p>\r\n', 2, 1),
(3, '', '', '<p>sadasdasdsadasdasd</p>\r\n', '<p>asdasdasdasdasd</p>\r\n', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch`
--

DROP TABLE IF EXISTS `tbl_branch`;
CREATE TABLE `tbl_branch` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_ar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_branch`
--

INSERT INTO `tbl_branch` (`id`, `name`, `name_ar`) VALUES
(2, 'Branch1', 'فرع١'),
(3, 'Branch2', 'فرع٢');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_details_item`
--

DROP TABLE IF EXISTS `tbl_contact_details_item`;
CREATE TABLE `tbl_contact_details_item` (
  `id` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `branch_ar` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tel_1` varchar(255) DEFAULT NULL,
  `tel_2` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `address_ar` varchar(255) NOT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `mobile_2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_item`
--

DROP TABLE IF EXISTS `tbl_contact_item`;
CREATE TABLE `tbl_contact_item` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `full_name_ar` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_type_id` int(11) NOT NULL COMMENT '(complaint - inquiry)',
  `body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_contact_item`
--

INSERT INTO `tbl_contact_item` (`id`, `full_name`, `full_name_ar`, `email`, `contact_type_id`, `body`) VALUES
(2, 'Burhan', 'برهان عطور', 'asd@asd.com', 2, 'asdasdasdasdadasd'),
(3, 'Burhan', 'برهان عطور', 'burhan@gmail.com', 2, 'هذه هي الرسالة المقدمة من قبلي اليوم .');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_type`
--

DROP TABLE IF EXISTS `tbl_contact_type`;
CREATE TABLE `tbl_contact_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_ar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_contact_type`
--

INSERT INTO `tbl_contact_type` (`id`, `name`, `name_ar`) VALUES
(2, 'Inquery', 'استفسار'),
(3, 'Suggestion', 'اقتراح'),
(4, 'Complain', 'شكوى');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_content`
--

DROP TABLE IF EXISTS `tbl_content`;
CREATE TABLE `tbl_content` (
  `id` int(11) NOT NULL,
  `title` tinytext NOT NULL,
  `title_ar` tinytext NOT NULL,
  `text` text,
  `text_ar` text,
  `image_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `order` tinyint(4) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `publish_date` varchar(45) DEFAULT NULL,
  `summary_block_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_content`
--

INSERT INTO `tbl_content` (`id`, `title`, `title_ar`, `text`, `text_ar`, `image_id`, `active`, `type`, `order`, `create_date`, `update_date`, `publish_date`, `summary_block_id`) VALUES
(1, 'zxczxczxzxczxczxczx', 'czxczxcxzczxczxczx', NULL, NULL, NULL, 0, 1, 0, '2015-09-27 14:32:21', '2015-09-27 14:32:21', NULL, NULL),
(2, 'asdasdadasdad', 'asdasdasdasdadasddsa', NULL, NULL, NULL, 0, 1, 0, '2015-09-27 14:32:40', '2015-09-27 14:32:40', NULL, NULL),
(3, 'asdasdasd', 'asdasdasdasdadasd', NULL, NULL, NULL, 0, 1, 0, '2015-09-27 14:32:55', '2015-09-27 14:32:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_custom_content`
--

DROP TABLE IF EXISTS `tbl_custom_content`;
CREATE TABLE `tbl_custom_content` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `value_ar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_custom_content`
--

INSERT INTO `tbl_custom_content` (`id`, `name`, `value`, `value_ar`) VALUES
(1, 'welcome_message', 'welcome_message welcome_message welcome_message welcome_message welcome_message welcome_message welcome_message welcome_message', 'الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية الرسالة الترحيبية '),
(2, 'our_vision', '<p>our_vision our_vision our_vision our_vision our_vision our_vision our_vision our_vision our_vision our_vision our_vision</p>\r\n', '<p>تعديل ل رؤيتنا</p>\r\n\r\n<p>تعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتناتعديل ل رؤيتنا</p>\r\n'),
(3, 'who_we_are', 'who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are who_we_are', 'من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن من نحن '),
(4, 'contact_info', '<p>No Content</p>\r\n', '<p>لايوجد محتوى</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cv_section`
--

DROP TABLE IF EXISTS `tbl_cv_section`;
CREATE TABLE `tbl_cv_section` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `body_ar` text NOT NULL,
  `staff_id` int(11) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_cv_section`
--

INSERT INTO `tbl_cv_section` (`id`, `title`, `title_ar`, `body`, `body_ar`, `staff_id`, `order`) VALUES
(1, 'asdasd', 'asdad', '<p>asdadasdasdasd</p>\r\n', '<p>asdasdasdasd</p>\r\n', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

DROP TABLE IF EXISTS `tbl_department`;
CREATE TABLE `tbl_department` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '(organizational - clinic)',
  `order` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `name_ar` varchar(255) NOT NULL,
  `summary` text,
  `summary_ar` text,
  `services` text,
  `services_ar` text,
  `active` tinyint(4) NOT NULL COMMENT '(active-inactive)',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `gallary_id` int(11) DEFAULT NULL,
  `contact_info` text,
  `contact_info_ar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_department`
--

INSERT INTO `tbl_department` (`id`, `type`, `order`, `name`, `name_ar`, `summary`, `summary_ar`, `services`, `services_ar`, `active`, `create_date`, `update_date`, `gallary_id`, `contact_info`, `contact_info_ar`) VALUES
(2, 0, 0, 'Pharmacy', 'الصيدلية', NULL, NULL, NULL, NULL, 1, '2015-08-05 10:46:26', '2015-09-18 11:37:37', NULL, NULL, NULL),
(3, 0, 2, 'Internals Clinic', 'عيادة الأمراض الداخلية', NULL, NULL, NULL, NULL, 1, '2015-08-22 18:37:57', '2015-09-18 11:38:04', NULL, NULL, NULL),
(4, 1, 0, 'Children Clinic', 'عيادة الأطفال', NULL, NULL, NULL, NULL, 1, '2015-09-02 19:17:55', '2015-09-18 11:36:46', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_equipment`
--

DROP TABLE IF EXISTS `tbl_department_equipment`;
CREATE TABLE `tbl_department_equipment` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` tinytext NOT NULL,
  `name_ar` tinytext NOT NULL,
  `image_id` int(11) DEFAULT NULL COMMENT 'Default image for the equipment',
  `gallary_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `body` text NOT NULL,
  `body_ar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_department_equipment`
--

INSERT INTO `tbl_department_equipment` (`id`, `department_id`, `active`, `name`, `name_ar`, `image_id`, `gallary_id`, `order`, `body`, `body_ar`) VALUES
(3, 4, 0, 'asdasdasd1313', 'يلاشسةي لاشسيو شسيناشس يتشسي شسني لاشي', NULL, NULL, 0, '<p>asdasdasdas das das d asd a sdas das dasd asd a sd asd as dasd</p>\r\n', '<p>شسيت شسيولاشسيشسلايشس</p>\r\n\r\n<p>&nbsp;يشسياشسني لاشستني لاشسنتيى شسي</p>\r\n\r\n<p>شس يشسنتي شسيشس</p>\r\n'),
(4, 2, 1, 'asd a d adi;lfhdbv mfdsv bbhdf,mv ', 'شسي ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسي', NULL, NULL, 0, '<p>asd a d adi;lfhdbv mfdsv bbhdf,mv asd a d adi;lfhdbv mfdsv bbhdf,mv&nbsp; asd a d adi;lfhdbv mfdsv bbhdf,mv asd a d adi;lfhdbv mfdsv bbhdf,mv asd a d adi;lfhdbv mfdsv bbhdf,mv asd a d adi;lfhdbv mfdsv bbhdf,mv</p>\r\n', '<p>شسي ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسيشسي ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسيشسي ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسيشسي ا ن</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u>لاةشر يتر ةتشر سيةر</u>ش يةاشسلي نشسيشسيشسي ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسيشسي ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسيشسي ا ن لاةشر يتر ة<strong>تشر سيةرش يةاشسلي نشسيشسيشسي</strong> ا ن لاةشر يتر ةتشر سيةرش يةاشسلي نشسيشسي</p>\r\n'),
(5, 3, 1, 'A very new Device ', 'جهاز جديد جدا!', NULL, NULL, 0, '<p>A very new Device A very new Device A very new Device A very new Device A very new Device A very new Device</p>\r\n', '<p>جهاز جديد جدا!جهاز جديد جدا!جهاز جديد جدا! جهاز جديد جدا!جهاز جديد جدا!جهاز جديد جدا!جهاز جديد <strong>جدا!جهاز جديد جدا!ج</strong>هاز جديد ج</p>\r\n\r\n<p>دا!جهاز جديد جدا!جهاز جديد جدا!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>يد جدا!جهاز جديد جدا!</strong></p>\r\n\r\n<p><strong>جهاز جديد جدا!</strong></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_page_section`
--

DROP TABLE IF EXISTS `tbl_department_page_section`;
CREATE TABLE `tbl_department_page_section` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `order` tinyint(4) NOT NULL,
  `title` tinytext NOT NULL,
  `title_ar` tinytext NOT NULL,
  `summary` tinytext,
  `summary_ar` tinytext,
  `body` text,
  `body_ar` text,
  `gallary_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dept_staff_assignment`
--

DROP TABLE IF EXISTS `tbl_dept_staff_assignment`;
CREATE TABLE `tbl_dept_staff_assignment` (
  `department_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equip_image`
--

DROP TABLE IF EXISTS `tbl_equip_image`;
CREATE TABLE `tbl_equip_image` (
  `equipment_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

DROP TABLE IF EXISTS `tbl_faq`;
CREATE TABLE `tbl_faq` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `question` tinytext NOT NULL,
  `question_ar` tinytext NOT NULL,
  `answer` text NOT NULL,
  `answer_ar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`id`, `department_id`, `order`, `question`, `question_ar`, `answer`, `answer_ar`) VALUES
(1, 2, 1, ' Now updated FAQ', 'asdasdasdasdasd', 'asdsadasdas', 'asdasdasdasdasdasd'),
(2, 4, 1, 'What is that? ', ' شسيشسي شي  شسي ش ي شسيشسيشسيشس', 'asdasda das d asd asd a d as f as f as', 'يشسيشسيشسيش ي شسي شس يشس ي شس ي شس ي شسيشسيشسي'),
(3, 4, 2, 'asd', 'asdad', 'asd', 'asdasdasdasd'),
(4, 2, 2, 'jm fmcv m vgcncmhc fhc', 'ىةر ىر لىة ل ةىل بلةى بي بوةبلض', 'h h ,vh, vh, gj, ,hmv b,m v,hj v,mv ,vm b., . hj , .j g,g u gkgh hkg ,h ', 'ن ترلة لوب  تةبة لبوا باوب ةلب و رز لان او  انل واب  بلتب نعبمغبتم نبتنللت '),
(5, 3, 1, 'What is important ?', 'ماهو الشيء المهم؟', 'It\'s to finish the \r\napplication! ', 'الشيء المهم ان ننهي التطبيق!');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

DROP TABLE IF EXISTS `tbl_gallery`;
CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `image_id` int(11) DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `summary_ar` text,
  `type` tinyint(4) NOT NULL COMMENT '(general - private )',
  `create_date` datetime NOT NULL,
  `create_id` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image`
--

DROP TABLE IF EXISTS `tbl_image`;
CREATE TABLE `tbl_image` (
  `id` int(11) NOT NULL,
  `directory_type` tinyint(4) DEFAULT NULL COMMENT '(image directory - upload directory)',
  `active` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `description` text,
  `description_ar` text,
  `alternative_text` tinytext,
  `alternative_text_ar` tinytext,
  `type` tinyint(4) NOT NULL COMMENT '(profile - department - equipment)',
  `block_id` int(11) DEFAULT NULL,
  `host` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'media - not media'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_image`
--

INSERT INTO `tbl_image` (`id`, `directory_type`, `active`, `url`, `gallery_id`, `name`, `name_ar`, `description`, `description_ar`, `alternative_text`, `alternative_text_ar`, `type`, `block_id`, `host`) VALUES
(1, NULL, 1, '05263ed16de8d259029221c965043da7001b540a.jpg', NULL, 'Dr. Fawas Saad', 'Dr. فواز سعد', NULL, NULL, 'Dr. Fawas Saad', 'Dr. فواز سعد', 1, NULL, 0),
(3, NULL, 1, '09bec0a2879ded7af224b4d2e4b50a4385c54285.jpg', NULL, 'asdasdasdasdasd', 'asdadasdasdasdasdasd', NULL, NULL, 'sadadsasdasdasdasdasdadas', 'sadasdasdasdasd', 6, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

DROP TABLE IF EXISTS `tbl_job`;
CREATE TABLE `tbl_job` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`id`, `title`, `title_ar`) VALUES
(1, 'Doctor', 'طبيب'),
(3, 'Tech', 'تقني'),
(4, 'Nursing', 'التمريض');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

DROP TABLE IF EXISTS `tbl_news`;
CREATE TABLE `tbl_news` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '(featured - normal)',
  `active` tinyint(4) DEFAULT NULL,
  `title` tinytext NOT NULL,
  `title_ar` tinytext NOT NULL,
  `body` text NOT NULL,
  `body_ar` text NOT NULL,
  `summary` text,
  `summary_ar` text,
  `image_id` int(11) DEFAULT NULL,
  `gallary_id` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `create_id` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_item`
--

DROP TABLE IF EXISTS `tbl_service_item`;
CREATE TABLE `tbl_service_item` (
  `id` int(11) NOT NULL,
  `item` tinytext NOT NULL,
  `item_ar` tinytext NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service_item`
--

INSERT INTO `tbl_service_item` (`id`, `item`, `item_ar`, `department_id`) VALUES
(1, 'A new Service Item', 'A new ', 2),
(3, 'asdasdasdasdasdadas', 'asdasdasdasdasdasd', 2),
(4, 'A new Service Item again1', 'A new Service Item again', 2),
(5, 'sdfsdfsdfsdfsdfsdf', 'sdfsdfsdfsdfsdfsdfsdfsdfsdfsfsdfsdf', 2),
(6, 'asdasdasdasdasd', 'asdasdasdasdasdasdasd', 2),
(7, 'asdasdasdasfdvdvxcvjjjjjjjjjjj', 'vcxvxcvxcvnjjjjjjjjjjjj', 2),
(8, 'A good clinic', 'عيادة جيدة', 4),
(9, 'Service Internal Clinic', 'عيادة الأمراض الداخلية', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(266) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider_image`
--

DROP TABLE IF EXISTS `tbl_slider_image`;
CREATE TABLE `tbl_slider_image` (
  `id` int(11) NOT NULL,
  `title` tinytext NOT NULL,
  `title_ar` tinytext NOT NULL,
  `description` text,
  `description_ar` text NOT NULL,
  `link` tinytext,
  `image_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `order` tinyint(4) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slider_image`
--

INSERT INTO `tbl_slider_image` (`id`, `title`, `title_ar`, `description`, `description_ar`, `link`, `image_id`, `active`, `order`, `create_date`, `update_date`) VALUES
(2, 'Hello Slider', 'افتتاح مركز الحياة الطبي', 'The opening of Al hayat Medical center', 'افتتاح مركز اليحاة الطبيافتتاح مركز اليحاة الطبيافتتاح مركز اليحاة الطبي', 'http://www.google.com', 3, 1, 1, '2015-10-01 16:10:24', '2015-10-01 18:51:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

DROP TABLE IF EXISTS `tbl_staff`;
CREATE TABLE `tbl_staff` (
  `id` int(11) NOT NULL,
  `profile_image_id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `title` tinyint(4) NOT NULL,
  `section_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `full_name_ar` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `dob_ar` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `address_ar` text NOT NULL,
  `contact` text NOT NULL,
  `contact_ar` text NOT NULL,
  `m_status` varchar(255) NOT NULL,
  `m_status_ar` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `nationality_ar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`id`, `profile_image_id`, `job_id`, `branch_id`, `title`, `section_id`, `full_name`, `full_name_ar`, `dob`, `dob_ar`, `address`, `address_ar`, `contact`, `contact_ar`, `m_status`, `m_status_ar`, `nationality`, `nationality_ar`) VALUES
(1, 1, 1, 2, 0, 3, 'Fawas Saad', 'فواز سعد', '1990', '1990', '19901990', 'شسيشسي', '19901990', 'شسيشسي', 'Maried', 'متزوج', 'SYRIAN', 'سوري');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_department`
--

DROP TABLE IF EXISTS `tbl_staff_department`;
CREATE TABLE `tbl_staff_department` (
  `staff_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

DROP TABLE IF EXISTS `tbl_type`;
CREATE TABLE `tbl_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `login_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_id` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '(active - inactive)',
  `role` tinyint(4) NOT NULL DEFAULT '0',
  `doctor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `login_name`, `first_name`, `last_name`, `middle_name`, `email`, `password`, `create_date`, `create_id`, `update_date`, `update_id`, `status`, `role`, `doctor_id`) VALUES
(4, 'burhan', 'burhan', 'burhan', 'otour', 'ahmad maher', 'borhan@alhayat.com', '625356652f70fce3a42bfebab57943441a0c5da7', '2015-07-01 00:00:00', 1, '2015-10-01 01:04:12', 1, 0, 0, NULL),
(5, 'fawaz', 'fawaz', 'fawaz', 'saad', '', '', '625356652f70fce3a42bfebab57943441a0c5da7', '2015-09-27 13:41:17', 0, '2015-09-28 23:18:45', 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vacancy`
--

DROP TABLE IF EXISTS `tbl_vacancy`;
CREATE TABLE `tbl_vacancy` (
  `id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `position_ar` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_ar` text NOT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `salary_ar` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `contact_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_vacancy`
--

INSERT INTO `tbl_vacancy` (`id`, `position`, `position_ar`, `title`, `title_ar`, `description`, `description_ar`, `salary`, `salary_ar`, `active`, `contact_email`) VALUES
(3, 'dasdasdasdasda', 'asdasdada', 'asda', 'd', '<p>dsdasdasd</p>\r\n', '<p>asdasdasdasdasd</p>\r\n', 'dasd', 'd`', 1, 'asd@sadas.com'),
(4, 'asdasdasdasdasd', 'asdasdasdasdasdasd', 'asdasdasdas', 'dasdasdasdasd', '<p>asdasdasdasdasdasdasdasdas</p>\r\n', '<p>dasdasdasdasd</p>\r\n', '', '', 1, 'asdas@dasdas.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD PRIMARY KEY (`itemname`,`userid`);

--
-- Indexes for table `authitem`
--
ALTER TABLE `authitem`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `tbl_article`
--
ALTER TABLE `tbl_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_article_tbl_doctor1_idx` (`doctor_id`);

--
-- Indexes for table `tbl_block`
--
ALTER TABLE `tbl_block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_content_idx` (`content_id`);

--
-- Indexes for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_details_item`
--
ALTER TABLE `tbl_contact_details_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_item`
--
ALTER TABLE `tbl_contact_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contact_type_idx` (`contact_type_id`);

--
-- Indexes for table `tbl_contact_type`
--
ALTER TABLE `tbl_contact_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_content`
--
ALTER TABLE `tbl_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_block_summary_idx` (`summary_block_id`),
  ADD KEY `fk_image_content_idx` (`image_id`);

--
-- Indexes for table `tbl_custom_content`
--
ALTER TABLE `tbl_custom_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cv_section`
--
ALTER TABLE `tbl_cv_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_staff_idx` (`staff_id`);

--
-- Indexes for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_tbl_department_tbl_gallary1_idx` (`gallary_id`);

--
-- Indexes for table `tbl_department_equipment`
--
ALTER TABLE `tbl_department_equipment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_department_equipment_tbl_gallary1_idx` (`gallary_id`),
  ADD KEY `fk_tbl_department_equipment_tbl_department1_idx` (`department_id`),
  ADD KEY `fk_equipment_image_idx` (`image_id`);

--
-- Indexes for table `tbl_department_page_section`
--
ALTER TABLE `tbl_department_page_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_department_section_tbl_gallary1_idx` (`gallary_id`),
  ADD KEY `fk_deprtment_idx` (`department_id`);

--
-- Indexes for table `tbl_dept_staff_assignment`
--
ALTER TABLE `tbl_dept_staff_assignment`
  ADD KEY `fk_tbl_dept_staff_assignment_tbl_department1_idx` (`department_id`),
  ADD KEY `fk_tbl_dept_staff_assignment_tbl_staff1_idx` (`staff_id`);

--
-- Indexes for table `tbl_equip_image`
--
ALTER TABLE `tbl_equip_image`
  ADD PRIMARY KEY (`equipment_id`,`image_id`),
  ADD KEY `fk_tCurrent_image_idx` (`image_id`);

--
-- Indexes for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_faq_tbl_department1_idx` (`department_id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_image_tbl_gallary1_idx` (`gallery_id`),
  ADD KEY `fk_image_tbl_block1_idx` (`block_id`);

--
-- Indexes for table `tbl_job`
--
ALTER TABLE `tbl_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_news_image1_idx` (`image_id`),
  ADD KEY `fk_tbl_news_tbl_gallary1_idx` (`gallary_id`);

--
-- Indexes for table `tbl_service_item`
--
ALTER TABLE `tbl_service_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fl_department_item_idx` (`department_id`);

--
-- Indexes for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider_image`
--
ALTER TABLE `tbl_slider_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_slides_images_idx` (`image_id`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_staff_image1_idx` (`profile_image_id`),
  ADD KEY `fk_job_idx` (`job_id`),
  ADD KEY `fk_section_idx` (`section_id`),
  ADD KEY `fk_branch_idx` (`branch_id`);

--
-- Indexes for table `tbl_staff_department`
--
ALTER TABLE `tbl_staff_department`
  ADD PRIMARY KEY (`staff_id`,`department_id`),
  ADD KEY `fk_department_idx` (`department_id`);

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `login_name_UNIQUE` (`login_name`),
  ADD KEY `fk_tbl_staff_idx` (`doctor_id`);

--
-- Indexes for table `tbl_vacancy`
--
ALTER TABLE `tbl_vacancy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_article`
--
ALTER TABLE `tbl_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_block`
--
ALTER TABLE `tbl_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_contact_details_item`
--
ALTER TABLE `tbl_contact_details_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contact_item`
--
ALTER TABLE `tbl_contact_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_contact_type`
--
ALTER TABLE `tbl_contact_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_content`
--
ALTER TABLE `tbl_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_custom_content`
--
ALTER TABLE `tbl_custom_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_cv_section`
--
ALTER TABLE `tbl_cv_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_department_equipment`
--
ALTER TABLE `tbl_department_equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_department_page_section`
--
ALTER TABLE `tbl_department_page_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_image`
--
ALTER TABLE `tbl_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_job`
--
ALTER TABLE `tbl_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_service_item`
--
ALTER TABLE `tbl_service_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_slider_image`
--
ALTER TABLE `tbl_slider_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_type`
--
ALTER TABLE `tbl_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_vacancy`
--
ALTER TABLE `tbl_vacancy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_article`
--
ALTER TABLE `tbl_article`
  ADD CONSTRAINT `fk_tbl_doctor` FOREIGN KEY (`doctor_id`) REFERENCES `tbl_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_block`
--
ALTER TABLE `tbl_block`
  ADD CONSTRAINT `fk_content` FOREIGN KEY (`content_id`) REFERENCES `tbl_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_contact_item`
--
ALTER TABLE `tbl_contact_item`
  ADD CONSTRAINT `fk_contact_type` FOREIGN KEY (`contact_type_id`) REFERENCES `tbl_contact_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_content`
--
ALTER TABLE `tbl_content`
  ADD CONSTRAINT `fk_block_summary` FOREIGN KEY (`summary_block_id`) REFERENCES `tbl_block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_image_content` FOREIGN KEY (`image_id`) REFERENCES `tbl_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_cv_section`
--
ALTER TABLE `tbl_cv_section`
  ADD CONSTRAINT `fl_staff` FOREIGN KEY (`staff_id`) REFERENCES `tbl_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD CONSTRAINT `fk_tbl_department_tbl_gallary1` FOREIGN KEY (`gallary_id`) REFERENCES `tbl_gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_department_equipment`
--
ALTER TABLE `tbl_department_equipment`
  ADD CONSTRAINT `fk_equipment_image` FOREIGN KEY (`image_id`) REFERENCES `tbl_image` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_department_equipment_tbl_department1` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_department_equipment_tbl_gallary1` FOREIGN KEY (`gallary_id`) REFERENCES `tbl_gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_department_page_section`
--
ALTER TABLE `tbl_department_page_section`
  ADD CONSTRAINT `fk_deprtment` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_department_section_tbl_gallary1` FOREIGN KEY (`gallary_id`) REFERENCES `tbl_gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_dept_staff_assignment`
--
ALTER TABLE `tbl_dept_staff_assignment`
  ADD CONSTRAINT `fk_tbl_dept_staff_assignment_tbl_department1` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_dept_staff_assignment_tbl_staff1` FOREIGN KEY (`staff_id`) REFERENCES `tbl_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_equip_image`
--
ALTER TABLE `tbl_equip_image`
  ADD CONSTRAINT `fk_tCurrent_equipment` FOREIGN KEY (`equipment_id`) REFERENCES `tbl_department_equipment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tCurrent_image` FOREIGN KEY (`image_id`) REFERENCES `tbl_image` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD CONSTRAINT `fk_tbl_faq_tbl_department1` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD CONSTRAINT `fk_image_tbl_block1` FOREIGN KEY (`block_id`) REFERENCES `tbl_block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_image_tbl_gallary1` FOREIGN KEY (`gallery_id`) REFERENCES `tbl_gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD CONSTRAINT `fk_tbl_news_image1` FOREIGN KEY (`image_id`) REFERENCES `tbl_image` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_news_tbl_gallary1` FOREIGN KEY (`gallary_id`) REFERENCES `tbl_gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_service_item`
--
ALTER TABLE `tbl_service_item`
  ADD CONSTRAINT `fl_department_item` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_slider_image`
--
ALTER TABLE `tbl_slider_image`
  ADD CONSTRAINT `fk_slider_image` FOREIGN KEY (`image_id`) REFERENCES `tbl_image` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD CONSTRAINT `fk_branch` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_image` FOREIGN KEY (`profile_image_id`) REFERENCES `tbl_image` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_job` FOREIGN KEY (`job_id`) REFERENCES `tbl_job` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_section` FOREIGN KEY (`section_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_staff_department`
--
ALTER TABLE `tbl_staff_department`
  ADD CONSTRAINT `fk_department` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_staff` FOREIGN KEY (`staff_id`) REFERENCES `tbl_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `fk_tbl_staff` FOREIGN KEY (`doctor_id`) REFERENCES `tbl_staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
